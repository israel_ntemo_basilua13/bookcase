<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentArticle extends Model
{
     protected $fillable=['document_id','article_id'];
}
