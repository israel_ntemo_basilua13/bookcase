<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    use Notifiable;

    protected $fillable = ['name_attach','email', 'official_number', 'code_bookcase', 'utilize_status', 'teacher_id'];

    public function universities()
    {
        return $this->belongsToMany('App\University', 'university_references');
    }
}
