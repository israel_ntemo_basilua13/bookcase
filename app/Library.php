<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    protected $guarded=[];
    protected $with=['articles'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function library_abonnement_prices(){
        return $this->hasMany('App\LibraryAbonnementPrice');
    }

    public function articles(){
        return $this->hasMany('App\Article');
    }
}
