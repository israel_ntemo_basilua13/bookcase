<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LibraryAbonnement extends Model
{
    protected $guarded = [];

    public function library_abonnement_price()
    {
        return $this->belongsTo('App\LibraryAbonnementPrice');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function payment()
    {
        return $this->belongsTo('App\payment');
    }

    /* recherche des abonnements en cours de validité  */
    public function scopeValidateByAbonnementLibrary($query)
    {
        return $query->where('end_date', '>=', Carbon::now()->addDays(1));
    }
}
