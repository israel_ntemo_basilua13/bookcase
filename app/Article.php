<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model
{
    protected $fillable = ['article_name', 'slug', 'publishing_at', 'author', 'domain_exploitation', 'file','video', 'edition', 'price', 'teacher_id','membership','library_id','status_id'];

    protected $dates = ['publishing_at'];

    /* Relations */
    public function teacher()
    {
        return $this->belongsTo('App\Teacher');
    }

    public function library()
    {
        return $this->belongsTo('App\Library');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'article_categories');
    }

    public function studentarticles()
    {
        return $this->hasMany('App\StudentArticle');
    }


    public function documents()
    {
        return $this->belongsToMany('App\Document', 'document_articles');
    }

    public function pay_article()
    {
        return $this->belongsTo('App\PayArticle');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'student_articles');
    }

    public function exchang()
    {
        return $this->belongsTo('App\Excahng');
    }

    /* scope*/


    /* setters et getters */

    public function setSlugAttribute($value)
    {
        // dd(trim($value));

        $this->attributes['slug'] =  str_replace(
            " ",
            " - ",
            trim($value) . ' ' .
                strtoupper(explode('.', $this->attributes['article_name'])[0]) . ' ' . strtolower(explode('.', $this->attributes['article_name'])[0]) . ' ' .
                strtoupper($this->attributes['author']) . ' ' . strtolower($this->attributes['author']) . ' ' .
                $this->attributes['edition'] . ' ' . $this->attributes['price']
        );
    }

    // public function setDomainExploitationAttribute($value)
    // {
    //     $this->attributes['domain_exploitation'] = str_replace(",", " - ",  strtoupper($value) . ',' . strtolower($value) . ',' . ucfirst($value));
    // }

    /* fin setters et getters */

    /* recherche des articles par un contains ($q) */
    public function scopeSearchByArticle($query, $q)
    {
        return $query->where('article_name', 'LIKE', '%' . $q . '%')
            ->orwhere('slug', 'LIKE', '%' . $q . '%')
            ->orwhere('domain_exploitation', 'LIKE', '%' . $q . '%')
            ->orderBy('article_name');
    }

    /* recherche des articles payant */
    public function scopePayArticle($query)
    {
        return $query->where('price', '>', '0');
    }

    /* recherche des articles gratuit a 0 */
    public function scopeFreeArticle($query)
    {
        return $query->where('price', '0');
    }

    /* Recherche article public */
    public function scopePublicArticle($query)
    {
        return $query->where('status_id', '1');
    }

    /* Recherche article private */
    public function scopePrivatecArticle($query)
    {
        return $query->where('status_id', '2');
    }


    /* Ordre desc de la création des articles */
    public function scopePublishDesc($query)
    {
        return $query->orderBy('publishing_at', 'desc');
    }

    /* article recent d'un teacher */
    public function scopeNewsArticleDesc($query)
    {
        // Periode de 6 mois de nouveauté
        return $query->where('created_at', '>=', Carbon::now()->subDays(186))
            ->orderBy('created_at', 'desc');
    }
}
