<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class StudentArticle extends Model
{
    protected $fillable = ['user_id', 'article_id', 'created_at'];

    protected $dates = ['created_at'];

    protected $with = ['article'];

    public function article()
    {
        return $this->belongsTo('App\Article');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //scope
    /* Ordre desc de la création des articles */
    public function scopeDownloadRecentDesc($query)
    {
        // return $query->orderBy('created_at', 'desc');
        // Periode de 9 mois derrière
        return $query->where('created_at', '>=', Carbon::now()->subDays(279));
    }
}
