<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['document_name','university_id','department_id'];
    protected $with=["university"];

    /* Relations */

    public function articles()
    {
        return $this->belongsToMany('App\Article', 'document_articles');
    }
   
    public function university()
    {
        return  $this->belongsTo('App\University');
    }

    public function department()
    {
        return  $this->belongsTo('App\Department');
    }
}
