<?php

namespace App\Http\Controllers;

use App\Like;
use App\News;
use App\Rule;
use App\User;
use App\Comment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\General_p_student;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class NewsController extends Controller
{
    // Bloc utilisation des traits
    use General_p_student;
    // Fin bloc traits

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { }

    public function commetNews(Request $request){
        $this->validate($request, [
            'news_id' => 'required|exists:news,id',
            "note" => 'required|string',
        ]);

        Comment::Create([
            'user_id'=> Auth::user()->id,
            'news_id'=> $request->news_id,
            'note'=>$request->note
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = auth::user();
        $news = News::with('university')->where('id', $id)->first();

        $like = $news->likes()->where('user_id', $user->id)->get();

        if ($like->count() == 0) {

            $like =  Like::create([
                'like_at' => Carbon::now(),
                'like_to' => false,
                'join_to' => false,
                'user_id' => $user->id,
                'news_id' => $news->id
            ]);
        } else {

            $like = $like[0];
        }

        return view('news.show', compact('news', 'like'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Quand on aime une publication
    public function like_to($id)
    {
        // Utilisation de la function  attach_like_join_to(id,like) du trait General_p_student
        $this->attach_like_join_to($id, 'like');

        // Redirection vers la page precedente
        return back();
    }


    public function like_to_show($id)
    {

        // Utilisation de la function  attach_like_join_show(id,like) du trait General_p_student

        $this->attach_like_join_show($id, 'like');

        // Redirection vers la page precedente
        return back();
    }

    // Quand on desir prendre part a une publication
    public function join_to($id)
    {
        // Utilisation de la function  attach_like_join_to(id, join) du trait General_p_student

        $this->attach_like_join_to($id, 'join');

        // Redirection vers la page precedente
        return back();
    }

    public function join_to_show($id)
    {

        // Utilisation de la function  attach_like_join_show(id,like) du trait General_p_student

        $this->attach_like_join_show($id, 'join');

        // Redirection vers la page precedente
        return back();
    }
}
