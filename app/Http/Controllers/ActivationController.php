<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Activation;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use App\Traits\General_p_student;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ActivationRequest;

class ActivationController extends Controller
{
    use General_p_student;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $activation = Activation::where('user_id', auth::user()->id)
        //     ->where('university_id', $request->university_id)
        //     ->first();

        $request->merge([
            'user_id' => auth::user()->id,
            'start_at' => Carbon::now()
        ]);

        // if(is_null($activation)){
        // }
        // else{
        //     Activation::destroy($activation->id);
        // }

        Activation::create($request->all());
        // Flashy::success("Votre demande d'admission a été acceptée !!!");
        return back();
        // SA marche aussi

        // $datas = $this->view_profile_data();
        // $student = $datas['student'];
        // $activation = $datas['activation'];
        // $universities = $datas['universities'];


        // return view('auth.profile', compact('student','activation','universities'));
    }

    public function stor($id)
    {
        // $activation = Activation::where('user_id', auth::user()->id)
        //     ->where('university_id', $id)
        //     ->first();

        // Activation::create($request->all());
        Activation::Create(
            [
                'university_id' => $id,
                'user_id' => auth::user()->id,
                'start_at' => Carbon::now()
            ]
        );
        Flashy::primary("Votre attachement a été éffectué avec success !!!");

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function act_destroy($id)
    {
        $user = auth::user();

        $activation = Activation::with('user')->findOrFail($id);
        if ($user->rule_id != 1) {

            if (Activation::where('user_id', $user->id)->count() == 1) {
                Activation::Create(
                    [
                        'university_id' => 1,
                        'user_id' => $user->id,
                        'start_at' => Carbon::now()
                    ]
                );
            }
        }
        $activation->delete($id);

        return back();
    }
}
