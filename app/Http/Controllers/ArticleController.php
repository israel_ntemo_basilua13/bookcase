<?php

namespace App\Http\Controllers;

use App\User;
use App\Article;
use App\Payment;
use App\Teacher;
use App\Category;
use App\Document;
use Carbon\Carbon;
use App\StudentArticle;
use App\ArticleCategory;
use App\DocumentArticle;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use App\Traits\General_p_article;
use App\Traits\General_p_teacher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;

class ArticleController extends Controller
{


    use General_p_article;
    use General_p_teacher;
    // A reprogrammé a cause de la recherche au niveau du welcome

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    // Fin reprogrammage

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if($user !=null){
            // Recupere les données de la methode view_news_data() du trait  General_p_article
            $data_articles = $this->article_data($user, 'all', 'notwelcome');
    
            $type_contenu_article = "Tout les articles";
            $articles =  $data_articles['articles'];
    
            return view('article.index', compact('type_contenu_article', 'articles'));
        }else{
            return redirect('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    // pour brouillon (draft)
    public function draft()
    {

        $teacher = Teacher::with('user')->where('user_id', auth::user()->id)->first();

        $dashboard = $this->dashboard_data($teacher, 2);

        $articles =  $dashboard['articles'];

        $categories = Category::orderBy('category')->get();
        $document = Document::where('document_name', 'Draft')->first();

        $documents = Document::where('document_name', '!=', 'Draft')
            ->where('document_name', '!=', 'Pubic')->orderby('document_name')->get();

        return view('teacher.draft', compact('articles', 'categories', 'document', 'documents'));
    }


    // pulish brouillon
    public function draft_publish(Request $request, $id)
    {
        // $teacher = Teacher::with('user')->where('user_id', auth::user()->id)->first();

        $request->merge(
            [
                'status_id' => 1,
            ]
        );

        $article = Article::findOrFail($id);
        $article->update($request->all());

        $colect_request = preg_split("/-/", $request->collect_pub);
        $document_colect = collect();

        foreach ($colect_request as $fold) {
            if ($fold != "") {

                if ($document_colect->isEmpty()) {
                    $document_colect->push($fold);
                } else {

                    $coucnt = $document_colect->count() - 1;

                    foreach ($document_colect as $key => $colect_folder) {
                        if ($colect_folder == $fold) {
                            unset($document_colect[$key]);
                            break;
                        } else {
                            if ($key == $coucnt) {
                                $document_colect->push($fold);
                            }
                            // if(
                            // $document_collect->push($folder);
                        }
                    }

                    // if ($document_collect->contains($folder)) {
                    //     // unset($document_collect[$key]);
                    //     // $document_collect->forget($folder);
                    //     // retirer element dans la collection
                    // } else {
                    //     $document_collect->push($folder);
                    // }
                }
            }
        }

        // Effacer l'attachement a la collection  draft
        $docs = DocumentArticle::where('article_id', $id)->firstOrFail();
        $docs->delete($id);

        // Attachement a des collections (dossiers ou promotion)
        foreach ($document_colect as $dos) {
            DocumentArticle::firstOrCreate([
                'document_id' => $dos,
                'article_id' => $article->id
            ]);
        }
        return back();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('teacher_id')){
            $prof = User::with('teacher')->findOrFail(auth::user()->id);
        }elseif($request->has('library_id')){
            $user_library = User::with('library')->findOrFail(auth::user()->id);
            $request->merge([
                'membership'=> "LBRY"
            ]);
        }else{
            $prof = User::with('teacher')->findOrFail(auth::user()->id);
        }


        if (is_null($request->collect)) {
            $request->collect  = 1;
        }

        $collect_request = preg_split("/-/", $request->collect);

        $document_collect = collect();

        // Pour supprimer les les dossiers passées a false, nombre impair = true, nombre paire = false dans le tableau du $collect_request
        foreach ($collect_request as $folder) {
            if ($folder != "") {

                if ($document_collect->isEmpty()) {
                    $document_collect->push($folder);
                } else {

                    $coubnt = $document_collect->count() - 1;

                    foreach ($document_collect as $key => $collect_folder) {
                        // dd($key);
                        if ($collect_folder == $folder) {
                            unset($document_collect[$key]);
                            break;
                        } else {
                            if ($key == $coubnt) {
                                $document_collect->push($folder);
                            }
                            // if(
                            // $document_collect->push($folder);
                        }
                    }

                    // if ($document_collect->contains($folder)) {
                    //     // unset($document_collect[$key]);
                    //     // $document_collect->forget($folder);
                    //     // retirer element dans la collection
                    // } else {
                    //     $document_collect->push($folder);
                    // }
                }
            }
        }


        if (!empty($request->file_article)) {
            for ($i = 0; $i < count($request->file_article); $i++) {
                // dd(explode('.', $request->file_article[$i]->getClientOriginalName())[0]);

                $video=null;

                if($request->file_article[$i]->getClientOriginalExtension() =="mp4" || $request->file_article[$i]->getClientOriginalExtension() =="mp3"  || $request->file_article[$i]->getClientOriginalExtension() =="avi"){
                    $video =$request->file_article[$i]->getClientOriginalName();
                }else{
                    $video =null;
                }



                $article =   Article::firstOrCreate(
                    [
                        'article_name' => $request->file_article[$i]->getClientOriginalName(),
                        'edition' => $request->edition
                    ],
                    [
                        'publishing_at' => Carbon::now(),
                        'author' => $request->has('teacher_id') ? $prof->teacher->name_teacher : $user_library->library->name_library,
                        'domain_exploitation' => $request->has('teacher_id') ? $prof->teacher->domain_exploitation : "all",
                        'file' => $request->file_article[$i]->getClientOriginalExtension(),
                        'price' => $request->price,
                        'video' => $video,
                        'teacher_id' => $request->has('teacher_id') ? $prof->teacher->id : null,
                        'library_id' => $request->has('library_id') ? $user_library->library->id : null,
                        'status_id' => $request->statut_id,
                        'membership' => $request->has('membership') ? $request->membership : "TCH",
                        'slug' => $request->has('teacher_id') ? $prof->teacher->domain_exploitation :"all"
                    ]
                );

                Storage::putFileAs('/public/uploads/files/', $request->file_article[$i], $request->file_article[$i]->getClientOriginalName());

                // Attachement a une catégorie
                ArticleCategory::firstOrCreate([
                    'category_id' => $request->category_id,
                    'article_id' => $article->id
                ]);

                // Quand la selction passera a plusieurs categories
                // foreach ($request->category_id as $cat_id) {
                //     ArticleCategory::firstOrCreate([
                //         'category_id' => $cat_id,
                //         'article_id' => $article->id
                //     ]);
                // }


                // Attachement a des collections (dossiers ou promotion)
                foreach ($document_collect as $dos) {
                    DocumentArticle::firstOrCreate([
                        'document_id' => $dos,
                        'article_id' => $article->id
                    ]);
                }
            }
        }


        if (count($request->file_article) == 1) {
            if ($request->statut_id == 1) {
                Flashy::success("Ajout d'un fichier, éffectué avec succès !!!");
            } else {
                Flashy::success("Un brouillon stocké avec succès !!!");
            }
        } else {
            if ($request->statut_id == 1) {
                Flashy::success('Ajout de ' . count($request->file_article) . ' fichiers, éffectué avec succès !!!');
            } else {
                Flashy::success('Stockage ' . count($request->file_article) . ' brouillons, éffectué avec succès !!!');
            }
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();

        $article = Article::find($id);

        // dd($article);
        // ->with('bien', 'Téléchargement éffectué avc succès !!!')
        $univ = collect();

        foreach ($article->teacher->user->activations as $uni) {

            $vars = false;

            if ($univ->isEmpty()) {
                $univ->push($uni);
            } else {
                $counts = $univ->count();

                foreach ($univ as $val) {

                    if ($val->university_id != $uni->university_id) {

                        if ($counts == $univ->count()) {
                            $vars = false;
                        } else { }
                    } else {
                        $vars = true;
                        break;
                    }
                }

                if ($vars == false) {
                    $univ->push($uni);
                }
            }
        }

        return view('article.show', compact('article', 'univ'));
    }

    //Pour l'affichage des articles all, free, recent et payant
    public function article_show($type)
    {
        $user = Auth::user();

        $type_contenu_article = "Tous les articles";

        if ($type == 'free') {

            $type_contenu_article = "Tout les articles gratuits";

            // Recupere les données de la methode view_news_data() du trait  General_p_article
            $data_articles = $this->article_data($user, 'free', 'notwelcome');
        } elseif ($type == 'payant') {

            $type_contenu_article = "Tout les articles payants";

            // Recupere les données de la methode view_news_data() du trait  General_p_article
            $data_articles = $this->article_data($user, 'payant', 'notwelcome');
        } else {

            // Recupere les données de la methode view_news_data() du trait  General_p_article
            $data_articles = $this->article_data($user, 'all', 'notwelcome');
        }

        $articles =  $data_articles['articles'];

        return view('article.index', compact('type_contenu_article', 'articles'));
    }


    public function article_collections(){

        $user = Auth::user();
        $type_contenu_article = "Trier par Collection";

        // Recupere les données de la methode view_news_data() du trait  General_p_article
        $data_articles = $this->article_data($user, 'all', 'notwelcome');

        $articles =  $data_articles['articles'];

        $collections = collect();

        // dd($articles);
        
        // Avoir la liste des documents (collections) sans doublons

        foreach ($articles as $art) {

            foreach ($art->documents as $docmt) {

                if ($collections->isEmpty()) 
                {
                    $collections->push($docmt);
                } 
                else 
                {
                    $countcollecs =  $collections->count() - 1;

                    foreach ($collections as $key => $atc) 
                    {
                            if ($atc->id == $docmt->id) {
                                break;
                            } else {
                                if ($key == $countcollecs) {
                                    $collections->push($docmt);
                                }
                            }
                    }
                }

            }

        }

        return view('article.collect', compact('type_contenu_article', 'articles','collections'));

    }

        // Pour les articles recents

    public  function article_favorites()
    {
        $user = auth::id();
        // Utilisation de la function view_news_data du trait General_p_article
        $articles_favorite_alls = StudentArticle::whereUserId($user)->get();

        // dd($recent_articles[0]->article->documents->count());
        $articles_favorites = collect();

        foreach ($articles_favorite_alls as  $art) {
            $articles_favorites->push($art->article);
        }

        // dd($articles_favorites[2]->library->name_library);

        return view('article.favorite', compact('articles_favorites'));
    }
    // Fin articles recents



    // Pour le welcome_article
    public function article_welcome(Request $request)
    {
        $user = new  User;

        $domains_student = str_slug($request->search_article);

        $user->domain_exploitation = $domains_student;


        // Recupere les données de la methode view_news_data() du trait  General_p_article
        $data_articles = $this->article_data($user, 'free', 'welcome');


        // $articles = Article::searchbyarticle($request->search_article)
        //     ->freearticle()->get();

        $articles =  $data_articles['articles'];

        return view('article.search_welcome_article', compact('articles'));
    }
    // FIN welcome_article


    // Pour les articles recents

    public  function article_recent()
    {
        $user = auth::user();

        // Utilisation de la function view_news_data du trait General_p_article
        $articles_recent = $this->recent_article($user);
        $recent_articles = $articles_recent['recent_article'];

        // dd($recent_articles[0]->article->documents->count());

        return view('article.recent', compact('recent_articles'));
    }
    // Fin articles recents

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);

        $article->delete($id);

        Storage::delete('uploads/files/' . $article->article_name);

        return back();
    }


    public function downloadfile()
    {
        // $file = storage_path() . "\uploads\files\Offre 2.pdf";

        // dd($file);
        // $headers = array(
        //     'Content-Type:application/pdf'
        // );

        return  Storage::download("/uploads/files/Offre 2.pdf", 'Offre 2.pdf');
        // return Response::download($file, "Offre 2.pdf", $headers);


        // return back()->with('success', 'Téléchargement éffectué avc succès !!!');
    }

    public function download_file($id)
    {
        $user = auth::user();
        $article = Article::findOrFail($id);

        StudentArticle::firstOrCreate(
            [
                'user_id' => $user->id,
                'article_id' => $article->id
            ]
        );

        $file = "/public/uploads/files/" . $article->article_name;

        return Storage::download($file, $article->article_name);

        return back()->with('success', 'Téléchargement éffectué avc succès !!!');
    }

    // enregistré ou supprimé de la liste des articles library favorites
    public function article_favorite_store_toggle($id)
    {
        $user = auth::user();
        $article = Article::findOrFail($id);

        $art_exist = StudentArticle::whereUserId($user->id)->whereArticleId($article->id)->first();

        if(is_null($art_exist)){
            StudentArticle::firstOrCreate(
                [
                    'user_id' => $user->id,
                    'article_id' => $article->id
                ]
            );
        }else{
            $art_exist->delete();
        }

        return back()->with('success', 'Article mis en favoris !!!');
    }




    public function paiement_recent($type)
    {
        $user = auth::user();

        if ($type == 'all') {
            $pays = Payment::with('user', 'article')
                ->where('user_id', $user->id)
                ->orderBy('payment_at', 'desc')->paginate(9);
        } else {
            $pays = Payment::with('user', 'article')
                ->where('user_id', $user->id)
                ->payrecent()
                ->orderBy('payment_at', 'desc')->paginate(9);
        }

        return view('article.pay', compact('pays'));
    }


    public function payments($id)
    {
        return view('article.pay');
    }



    // 
}
