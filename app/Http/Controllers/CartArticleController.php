<?php

namespace App\Http\Controllers;

use App\CartArticle;
use Illuminate\Http\Request;

class CartArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CartArticle  $cartArticle
     * @return \Illuminate\Http\Response
     */
    public function show(CartArticle $cartArticle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CartArticle  $cartArticle
     * @return \Illuminate\Http\Response
     */
    public function edit(CartArticle $cartArticle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CartArticle  $cartArticle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CartArticle $cartArticle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CartArticle  $cartArticle
     * @return \Illuminate\Http\Response
     */
    public function destroy(CartArticle $cartArticle)
    {
        //
    }
}
