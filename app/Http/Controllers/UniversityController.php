<?php

namespace App\Http\Controllers;

use Image;
use Flashy;
use App\News;
use App\User;
use App\Option;
use App\Faculty;
use App\Teacher;
use App\Category;
use App\Document;
use App\Agreement;
use App\Reference;
use Carbon\Carbon;
use App\Activation;
use App\Department;
use App\University;
use App\UniversityFaculty;
use App\Traits\KeyGenerate;
use App\UniversityReference;
use Illuminate\Http\Request;
use App\Traits\Trait_university;
use PhpParser\Node\Stmt\ElseIf_;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Notifications\SendKeyTeacher;
use Illuminate\Support\Facades\Storage;

class UniversityController extends Controller
{

    use KeyGenerate;
    use Trait_university;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('univ.create');
    }
   
    
    public function storeNews()
    {
        $user = Auth::user();

        $university = University::with('user')->where('user_id', $user->id)->firstOrFail();

        $News = News::where('university_id',$university->id)
                            ->orderBy('created_at', 'desc')
                            ->get();

        return view('univ.storeNews', compact('university','News'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $agreem = Agreement::findOrFail($request->agreem_id);     

            //Picture university
            $picture = $request->file('file_picture');
            $logo = $request->file('file_logo');

            if(!empty($picture)){

                $filename = 'picture_' . $agreem->name_institution . '_' . time() . '.' . $picture->getClientOriginalExtension();
                Image::make($picture)->resize(900, 600)->save(storage_path('app\\public\\uploads\\images\\' . $filename));
            }

            // Logo
            if(!empty($logo)){

                $filename_logo = 'logo_' . $agreem->name_institution . '_' . time() . '.' . $logo->getClientOriginalExtension();
                Image::make($logo)->resize(900, 600)->save(storage_path('app\\public\\uploads\\avatars\\' . $filename_logo));
            }


            $request->merge(
            [
                'email' => $agreem->email_agreement,
                'name_university' => $agreem->name_institution,
                'sigle'=> $request->sigle,
                'start_at' => Carbon::now(),
                'picture' => $filename,
                'logo_picture'=> $filename_logo
            ]
        );
        // creation d'une university
        $university =  University::create($request->all());


        //Update agreement
        $agreem->status_id = 1;
        $agreem->university_id = $university->id;
        $agreem->save();


        // Creation du prof_université
        $request->merge(
            [
                'name_teacher' => $university->name_university,
                'sex' => "Homme",
                'phone' => $university->phone,
                'domain_exploitation' => "tfc,tfe,these,article,livre,graduat,licence,public,universite",
                'visibilities' => "11",
                'title_id' => 1,
                'user_id' => $request->user_id
            ]
        );

        $teacher =  Teacher::create($request->all());

        $request->merge(
            [
                'university_id' => $university->id,
                'start_at' => Carbon::now()
            ]
        );

        $activation =  Activation::create($request->all());
        $request->merge(
            [
                'end_at' => Carbon::now()->addDays(14),
                'activation_id' => $activation->id,
                'status_id' => 1,
            ]
        );


        // Envoie d'email
        $user = User::find($request->user_id);
        $data = array(
            'name' => $user->name,
            'role' => $user->rule->rule,
            'time' => "2 semaines",
        );

        // dd($user->email);
        // Mail::to($user->email)->send(new SendMail($data));

        // Fin d'envoie d'email

        Flashy::primary($university->name_university . ', votre compte a été créee avec succès !!!');

        Auth()->login($user);
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function university_all()
    {
        $universities = University::where('name_university', '!=', 'BookCase')
            ->orderBy('name_university')
            ->paginate(3);

        return view('univ.university_all', compact('universities'));
    }


    public function dashboard()
    {
        $user = auth::user();
        $university = University::with('user')->where('user_id', $user->id)->first();

        // Trait Trait_university
        $datas = $this->view_dashBord($university);

        $News = $datas['News'];
        $news_all = $datas['news_all'];
        $all_news = $datas['all_news'];
        $teacher_univ = $datas['teacher_univ'];
        $teacher_reference = $datas['teacher_reference'];
        $student_univ = $datas['student_univ'];
        $student_belongsTo = $datas['student_belongsTo'];
        $facultys = $datas['facultys'];
        $faculties = $datas['faculties'];
        $collections = $datas['collections'];
        $collections_all = $datas['collections_all'];
        $departments = $datas['departments'];
        $departmenties = $datas['departmenties'];

        return view('univ.dashboard',
                    compact('News','university','news_all','all_news',
                            'teacher_univ','teacher_reference',
                            'student_univ','student_belongsTo',
                            'facultys','faculties',
                            'collections','collections_all',
                            'departmenties','departments'
                            )
                    );

        // Tous ses articles (Public, car il en a seulement de public)
    }

    public function store_news(Request $request)
    {
        // Par university
        if (is_null($request->news_public) && is_null($request->news_private)) {
            $news_visibility = 1;
        } elseif (is_null($request->news_private)) {
            $news_visibility = 1;
        } else {
            $news_visibility = 2;
        }

        // $university = User::find(auth::user()->id);
        $university = University::with('user')->where('user_id', auth::user()->id)->firstOrFail();

        if ($request->hasFile('picture_news')) {

            $picture = $request->file('picture_news');

            $picture_name = 'news_' . Auth::user()->name . '_' . date("dmY-Hs",strtotime(Carbon::now())) . '.' . $picture->getClientOriginalExtension();

            Image::make($picture)->resize(2500, 1667)->save(storage_path('app\\public\\uploads\\images\\' . $picture_name));

            $request->merge(
                [
                    'picture' => $picture_name
                ]
            );
        }

        News::firstOrCreate(
            [
                'picture' => $request->picture,
                'title_news' => $request->title,
                'describe' => $request->describe,
                'university_id' => $university->id
            ],
            [
                'date' => Carbon::now(),
                'localization' => $request->localization,
                'exploitation_domain' => $request->domains,
                'status_id' => $news_visibility
            ]
        );

        if($news_visibility==1){
           
            Flashy::primary('Manager, Votre news a été publié dans la communauté avec succès !!!');
        }
        else{
            Flashy::primary('Manager, Votre news a été publié a vos étudiants avec succès !!!');
        }

        return back();
    }


    //TeacherRessource ::Get
    public function teacher_ressource(){

        $user = Auth::user();

        $university = University::with('user')->where('user_id', $user->id)->firstOrFail();

        $teachers = User::with('activations')->where('rule_id',4)->get();

        $teacher_univ = collect();

        foreach ($teachers as $teacher) {
           foreach($teacher->activations as $teach_act){
               if($teach_act->university_id == $university->id){
                   if($teacher_univ->isEmpty()){
                        $teacher_univ->push($teacher);
                    }
                    else{

                        $countteachs = $teacher_univ->count()-1;
                        foreach ($teacher_univ as $key => $atc) 
                        {
                            if ($atc->id == $teacher->id) {
                                break;
                            } else {
                                if ($key == $countteachs) {
                                    $teacher_univ->push($teacher);
                                }
                            }
                        }


                    }
               }
           }
        }
        
        return view('univ.teacher_ressource',compact('university','teacher_univ'));
    }

     //TeacherRessource ::store createTeacher (accès creation de la du teacher)
    public function teacherstore_reference(Request $request){
        $reference_verify = Reference::where('email',$request->email_attach)->first();

        $user = Auth::user();
        $university = University::with('user')->where('user_id', $user->id)->firstOrFail();


        if(is_null($reference_verify)){

           $request->merge(
               [
                   'code_bookcase' => $this->keycode()
               ]
           );
    
           $reference = Reference::firstOrCreate(
               [
                   'email'=> $request->email_attach
               ],
               [
                   'name_attach' => $request->name_attach,
                   'official_number' => $request->official_number,
                   'code_bookcase' => $request->code_bookcase,
                   'utilize_status' => 0,
               ]
           );
    
           if (!is_null($reference)) {

                UniversityReference::firstOrCreate(
                    [
                       'university_id'=>$university->id,
                       'reference_id'=>$reference->id
                    ]);
    
                // Mail notification  pour envoyer le code de creation (code bookcase)
    
                $data = array(
                   'name_attach' => $reference->name_attach,
                   'official_number' => $reference->official_number,
                   'code_bookcase' => $reference->code_bookcase,
                   'university_name' =>$university->name_university,
                   'type_request'=>'created'
                );
    
               $reference->notify(new SendKeyTeacher($data));
    
               Flashy::primary('Manager, l\'instructeur '.$reference->name_attach.' a été ajouter avec succès !!!');
           }
           else{
               Flashy::error('Manager, l\'instructeur '.$reference->name_attach.' n\'a pas été ajouté, suite a une error, veuillez verifier ses coordonnées !!!');
           }
        }
        else{

            if($reference_verify->utilize_status !="0")
            {
                $reference = UniversityReference::firstOrCreate(
                        [
                            'university_id'=>$university->id,
                            'reference_id'=>$reference_verify->id
                        ]);
                
                $teach = User::where('email',$request->email_attach)->first();

                Activation::create([
                    'university_id' => $university->id,
                    'user_id' => $teach->id,
                    'start_at' => Carbon::now()
                ]);

                $ref = Reference::find($reference_verify->id);

                if(!is_null($ref)){

                    //     $data = array(
                    //        'name_attach' => $ref->name_attach,
                    //        'official_number' => $ref->official_number,
                    //        'university_name' =>$university->name_university,
                    //         'type_request'=>'update'
                    //     );
            
                    //    $ref->notify(new SendKeyTeacher($data));
            
                    Flashy::success('Manager, l\'instructeur '.$reference->name_attach.' a été ajouter avec succès !!!');
                }
                else{
                     Flashy::primary('Manager, l\'instructeur '.$reference->name_attach.' existe déjà !!!');
                }
             
            }
    
        }
        
        return back();
        
    }

    //ArticleRessource ::get
    public function article_ressource(){

        $university = University::with('user')->where('user_id', Auth::user()->id)->firstOrFail();
        $teacher = Teacher::where('user_id',$university->user_id)->first();
        $categories = Category::all();

        if(!is_null($teacher)){
            return view('univ.article_ressource',compact('university','teacher','categories'));
        }
       
    }

    // InstitutionRessource::get
    public function instutition_ressource(){
        $user = Auth::user();
        $university = University::with('user')->where('user_id', $user->id)->firstOrFail();
        
        // Trait Trait_university
        $datas = $this->view_dashBord($university);
        $departmenties = $datas['departmenties'];

        $facults = $datas['facultys'];
        $facultys = collect();

        foreach ($facults as $facult) {
            $facultys->push($facult->faculty);
        }

        return view('univ.institution_ressource',compact('university','facultys','departmenties'));
    }

    // InstitutionStore Documents store::post

    public function storeinstitution(Request $request){

        if($request->indice=="faculty"){

             // Faculty store::post
            $university = University::with('user')->where('user_id', Auth::user()->id)->firstOrFail();

            $faculty =  Faculty::firstOrCreate(
                [
                    'title'=> $request->title
                ],
                [
                    'describe'=> $request->describe
                ]);

            if(!is_null($faculty)){

                UniversityFaculty::firstOrCreate(
                [
                    'faculty_id'=> $faculty->id,
                    'university_id'=> $university->id
                ]);

                Flashy::primary('La faculté '.$faculty->title.' a été ajouter avec succès !!!');
            }else{
                Flashy::error('L\'ajout de la faculté'.$faculty->title.' a échoué !!!');
            }

        }
        elseif ($request->indice=="department") {
           
                // Department store::post
            $department =  Department::firstOrCreate(
            [
                'title_department'=> $request->title_department
            ],
            [
                'faculty_id'=> $request->faculty_id
            ]);

            if(!is_null($department)){

                Flashy::primary('Le Departement/Section '.$department->title_department.' a été ajouter avec succès !!!');
            }else{
                Flashy::error('L\'ajout du Departement'.$department->title_department.' a échoué !!!');
            }

        }
        elseif ($request->indice=="collection"){

            // Documents store::post
            $university = University::with('user')->where('user_id',auth::user()->id)->firstOrFail();

            $document =  Document::firstOrCreate(
                [
                    'document_name'=> $request->document_name .'/'.$university->sigle,
                    'university_id'=> $university->id,
                    'department_id'=> $request->department_id
                ]);

            if(!is_null($document)){
                Flashy::primary('La collection '.$document->document_name.' a été ajouter avec succès !!!');
            }else{
                Flashy::error('L\'ajout de la collection'.$request->document_name.' a échoué !!!');
            }

        }
        else{}

        return back();
    }

    // Promotion ou Documents :get
    public function promotion_ressource(){
        $user = auth::user();
        $university = University::with('user')->where('user_id', $user->id)->first();

        // Trait Trait_university
        $datas = $this->view_dashBord($university);

        $collections = $datas['collections'];

        return view('univ.promotion_ressource',compact('collections'));
       
    }

     // department :get
    public function department_ressource(){
        $user = auth::user();
        $university = University::with('user')->where('user_id', $user->id)->first();

        // Trait Trait_university
        $datas = $this->view_dashBord($university);

        $departments = $datas['departmenties'];
        $collections = $datas['collections'];
       
        return view('univ.department_ressource',compact('departments','collections'));
    }

    // faculty:get
    public function faculty_ressource(){
        $user = auth::user();
        $university = University::with('user')->where('user_id', $user->id)->first();

        // Trait Trait_university
        $datas = $this->view_dashBord($university);

        $facultys = $datas['facultys'];
        $departments = $datas['departmenties'];

        // dd($facultys);
       
        return view('univ.faculty_ressource',compact('facultys','departments'));
    }


    // Student:get
    public function student_ressource(){
        $user = auth::user();
        $university = University::with('user')->where('user_id', $user->id)->first();

        // Trait Trait_university
        $datas = $this->view_dashBord($university);

        $student_belongsTo = $datas['student_belongsTo'];

        return view('univ.student_ressource',compact('university','student_belongsTo'));
       
    }
}
