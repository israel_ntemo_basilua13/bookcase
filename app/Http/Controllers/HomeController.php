<?php

namespace App\Http\Controllers;

use App\Comment;
use APP\Like;
use App\Rule;
use App\User;
use App\Student;
use App\University;
use App\StudentArticle;
use Illuminate\Http\Request;
use App\Traits\General_p_student;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    // Utilisation du trait
    use General_p_student;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::with('rule')->where('id', Auth::id())->first();

        // a REVOIR 
        // if(is_null($user)){

        //     $user = new User();
        //     $rule = new Rule();
        //     $rule->id = 0;
        //     $user->rule()->associate($rule);

        // } 

        switch ($user->rule->id) {
            case '1':
                // Utilisation de la function  view_news_data() du trait General_p_student
                $data_news = $this->view_news_data($user);

                $user_student = $data_news['user_student'];
                $universities_news = $data_news['universities_news'];
                $likes = $data_news['likes'];

                $favourites = $data_news['favourites'];
                $support_pays = $data_news['support_pays'];
                $library_abonnements = $data_news['library_abonnements'];
                $article_suggestions = $data_news['article_suggestions'];

                // Utilisation de la function  univ_teachers() du trait General_p_student
                $datas_teachers = $this->univ_teachers($user);
                $data_teachers = $datas_teachers['data_teachers'];

                // if ($data_teachers->count() >= 2) {
                //     $data_teachers = $data_teachers;
                // }

                $universities = University::where('name_university', '!=', 'BookCase')->get();

                if ($universities->count() >= 2) {
                    $universities = $universities->random(2);
                }
                return view(
                    'student.index',
                    compact(
                        'user_student',
                        'universities_news',
                        'likes',
                        'data_teachers',
                        'universities',
                        'favourites',
                        'support_pays',
                        'library_abonnements',
                        'article_suggestions',
                    )
                );
                break;

            case '2':
                return redirect()->route('wall');
                // route vers les interfaces univ
                // return view('univ.index', compact('user', 'rule'));
                break;

            case '3':
                // route vers les interfaces student
                return view('admin.index', compact('user', 'rule'));
                break;

            case '4':
                // route vers l'administration teacher
                return redirect()->route('dashboard');
                // return view('teacher.dashboard', compact('user'));
                break;
            case '5':
                // route vers l'administration library
                return redirect()->route('dashboardLibrary');
                break;

            default:
                return view('auth.login');
                break;
        }
    }

    // public function welcome()
    // {
    //     return view('welcome');
    // }
}
