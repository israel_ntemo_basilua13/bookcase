<?php

namespace App\Http\Controllers;

use App\Activation;
use App\Article;
use App\LibraryAbonnement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Rule;
use App\StudentArticle;
use App\Payment;
use App\Student;
use App\University;
use App\Period;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Image;
use App\Traits\General_p_student;
use PhpParser\Node\Stmt\Foreach_;
use PhpParser\Node\Stmt\If_;


class UserController extends Controller
{

    use General_p_student;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.choice');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function choice_compte()
    {
        return view('auth.choice');
    }

    public function direction_user($user)
    {

        $rule = Rule::where($user->rule_id);

        switch ($rule->id) {
            case '1':
                // route vers les interfaces student
                return view('student.index');
                break;

            case '2':
                // route vers les interfaces univ
                return view('univ.index');
                break;

            case '3':
                // route vers les interfaces student
                return view('admin.index');
                break;

            case '4':
                return view('teacher.index');
                break;

            default:
                break;
        }
    }

    public function profile()
    {
        $user = auth::user();

        $datas = $this->view_profile_data($user);
        $student = $datas['student'];
        $activation = $datas['activation'];
        $universities = $datas['universities'];

        if($user->rule_id == 1 || $user->rule_id == 4){
            // Utilisation de la function  univ_teachers() du trait General_p_student
            $datas_teachers = $this->univ_teachers($user);
            $data_teachers = $datas_teachers['data_teachers'];
        }

        
        $semaine = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","samedi");

        $mois = array(1=>"Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre");

        $jour_day = $semaine[getdate()["wday"]];
        $mois_day = $mois[getdate()["mon"]];

        if ($user->rule_id == 1) {

            $news_data =  $this->view_news_data($user);
            $news_exit = $news_data['universities_news'];

            // $not_like = $news_data['notview_news'];
            $likes = $news_data['likes'];

            $likes_like = collect();
            $likes_join = collect();

            foreach ($likes as $lik) {
                if ($lik->like_to == 1) {
                    $likes_like->push($lik);
                }

                if ($lik->join_to == 1) {
                    $likes_join->push($lik);
                }
            }

            //article telecharger pour une periode entre today et 9 mois avant avec une marge de 60 telechargement par année
            $articles_downs = StudentArticle::where('user_id', $user->id)
                ->downloadrecentdesc()->get();

            //article telecharger pour une periode entre today et 9 mois avant avec une marge de 40 paiement par année
            $pays = Payment::where('user_id', $user->id)
                ->payrecent()->get();

            // $pourc_like = $likes_like->count() != 0 ? ($likes_like->count() * 100) / $news_exit->count() : 0;
            // $pourc_join = $likes_join->count() != 0 ? ($likes_join->count() * 100) / $news_exit->count() : 0;

            if ($news_exit->count() != 0) {
                $pourc_like = ($likes_like->count() * 100) / $news_exit->count();
                $pourc_join = ($likes_join->count() * 100) / $news_exit->count();
            } else {
                $pourc_like = 0;
                $pourc_join  = 0;
            }

            $pourc_down = ($articles_downs->count() * 100) / 60;
            $pourc_pay = ($pays->count() * 100) / 40;

            $abonnements = LibraryAbonnement::where('user_id',$user )->get();
            // dd($student);
            return view('auth.profile', compact('student', 'activation','data_teachers' ,'universities', 'news_exit','articles_downs','pays', 'pourc_like', 'pourc_join', 'pourc_down', 'pourc_pay','jour_day','mois_day','abonnements'));

        }elseif ($user->rule_id == 4) {
            $cours = Article::where('teacher_id',$user-> teacher->id)->get();
            return view('auth.profile', compact('student', 'activation', 'universities','jour_day','mois_day','cours'));

        }elseif ($user->rule_id == 2) {
            $cours = Article::where('teacher_id',$user-> teacher->id)->get();
            return view('auth.profile', compact('student', 'activation', 'universities','jour_day','mois_day','cours'));
        }elseif ($user->rule_id == 5) {
            $cours = Article::where('library_id',$user-> library->id)->get();

            // $consultations = Article::get();
            $consultations = StudentArticle::with('article')->get()->filter(function($value){
                    return $value->article->library_id !=null;
            });

            return view('auth.profile', compact('student', 'activation', 'universities','jour_day','mois_day','cours','consultations'));
        }else{}
    }

    public function update_profile(Request $request)
    {

        if ($request->hasFile('avatar')) {

            $user = auth::user();

            // Supprimer l'ancien
            if ($user->avatar != 'avatar_defaut.png') {
                Storage::delete('uploads/avatars/' . $user->avatar);
            }

            $avatar = $request->file('avatar');

            // dd(realpath($avatar->getClientOriginalName()));


            $filename = 'avatar_' . Auth::user()->name . '_' . time() . '.' . $avatar->getClientOriginalExtension();


            // sa marche sans Image::make

            // Storage::putFileAs(
            //     'uploads/avatars/', $request->file('avatar'), $filename
            // );

            Image::make($avatar)->resize(900, 600)->save(storage_path('app\\public\\uploads\\avatars\\' . $filename));

            // Sa marche sans Image::make
            // Storage::put('uploads/avatars/',$avatar = $request->file('avatar'),$filename);

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }

        return back();
    }
}
