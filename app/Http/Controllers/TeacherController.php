<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;
use App\Article;
use App\Document;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Category;
use App\ArticleCategory;
use app\DocumentArticle;
use App\StudentArticle;
use Carbon\Carbon;
use App\Activation;
use App\Period;
use App\Reference;
use App\UniversityReference;
use Flashy;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use App\Traits\General_p_teacher;
use App\Title;
use App\Notifications\ConfirmNotify;


class TeacherController extends Controller
{
    use General_p_teacher;

    private $mail_visibility;
    private $phone_visibility;
    private $file_exist;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacher = Teacher::with('user')->where('user_id', auth::user()->id)->first();

        $dashboard = $this->dashboard_data($teacher, 1);

        $articles =  $dashboard['articles'];
        $revenus = $dashboard['revenus'];

        $categories = Category::orderBy('category')->get();


        $documnts = Document::where('document_name', '!=', 'Draft')
                            ->where('document_name', '!=', 'Public')
                            ->orderby('document_name')->get();


        $activate_teach = Activation::where('user_id', auth::user()->id)->get();

        $activation_collect = collect();

        $documents=collect();

        // selectionné tous les univ du teacher  en cours sans doublons

        // Unicité des valeurs des universités par rapport au teacher
        foreach ($activate_teach as $activ) {

            if ($activation_collect->isEmpty()) 
            {
                $activation_collect->push($activ);
            } 
            else 
            {
                $countactivs =  $activation_collect->count() - 1;

                foreach ($activation_collect as $key => $atc) 
                {
                        if ($atc->university_id == $activ->university_id) {
                            break;
                        } else {
                            if ($key == $countactivs) {
                                $activation_collect->push($activ);
                            }
                        }
                }
            }
        }

        foreach($documnts as $doc){
            foreach ($activation_collect as $activec) {
                if($activec->university_id == $doc->university_id){
                    $documents->push($doc);
                break;
                }
            }
        }

        return view('teacher.index', compact('teacher', 'articles', 'revenus', 'categories', 'documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function inquiry($code)
    {
        $reference = Reference::where('official_number',$code)
                    ->where('utilize_status', '!=', 1)
                    ->first();
        
        if (is_null($reference)) {
               
            session()->flash('notreference', 1);

            return view('univ.notvalidate');
              
            } else {
                
                session()->flash(
                        'reference',$reference
                    );

                return redirect()->route('register', ['rule_id' => 4]);
            }
    }


    public function verification(Request $request)
    {

        $reference = Reference::with('universities')
        ->where('official_number', $request->official_number)
        ->where('code_bookcase', $request->code_bookcase)
        ->where('utilize_status', '!=', 1)
        ->first();

        if (is_null($reference)) {
            $reference = new Reference;
            session(
                [
                 'msg'=> 'notcreated',
                 'reference'=> $reference
                ]);
            return back();

        } else {

            session(
                 [
                 'msg'=> 'created',
                 'reference'=> $reference
                ]);
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (is_null($request->mail_visibility)) {
            $mail_visibility = 0;
        } elseif ($request->mail_visibility == true) {
            $mail_visibility = 1;
        } else {
            $mail_visibility = 0;
        }


        if (is_null($request->phone_visibility)) {
            $phone_visibility = 0;
        } elseif ($request->phone_visibility == true) {
            $phone_visibility = 1;
        } else {
            $phone_visibility = 0;
        }

        $request->merge(
            [
                'visibilities' => $mail_visibility . $phone_visibility,
            ]
        );

        // creation d'un etudiant
        $teacher =  Teacher::create($request->all());

        $reference = Reference::with('universities')->where('id', $request->reference)->first();

        $request->merge(
            [
                'university_id' => $reference->universities[0]->id,
                'start_at' => Carbon::now()
            ]
        );

        // creation de  son activation avec une université par defaut (Aucun)

        $activation =  Activation::create($request->all());
        $request->merge(
            [
                'end_at' => Carbon::now()->addMonths(2),
                'activation_id' => $activation->id,
                'status_id' => 1,
            ]
        );

        $reference->utilize_status = 1;
        $reference->teacher_id =  $teacher->id;
        $reference->save();

        // creation de sa periode de validité de 6 mois
        Period::create($request->all());

        // Envoie d'email
        // Mail notification 
        $user = User::find($request->user_id);
            // Désactivité en mode prod
        // $user->notify(new ConfirmNotify($user)); 
        // Fin d'envoie d'email

        Flashy::primary($teacher->name_teacher . ', votre compte a été créee avec succès !!!');

        Auth()->login($user);
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::with('user')->findOrFail($id);

        return view('teacher.show', compact('teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::with('user')->findOrFail($id);
        return view('teacher.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teacher = Teacher::with('user')->findOrFail($id);
        $teacher->update($request->all());
        return redirect(route('teacher.show', compact('teacher')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = Teacher::with('user')->findOrFail($id);
        $teacher->delete($id);
        // return back();
        return back()->with('success', 'Suppression éffectué avec succès  !!!');
    }


    public function download_files(Request $request)
    {
        $prof = User::with('Teacher')->findOrFail(auth::user()->id);
        $document = Document::firstOrFail($request->document_id);
        // $category = Category::firstOrFail($request->category_id);

        $files = $request->file('files');
        $files_stockage = Article::all();
        $documents = $request->documents;
        $categories = $request->categories;


        if (!empty($files)) {
            foreach ($files as $file) {

                $file_create = Article::firstOrCreate(
                    [
                        'article_name' => $file->getClientOriginalName(),
                        'edition' => $request->edition
                    ],
                    [
                        'slug' => strtoupper($file->getClientOriginalName()) . ' ' . strtolower($file->getClientOriginalName()),
                        'publishing_at' => Carbon::now(),
                        'author' => $prof->user->teacher->name_teacher,
                        'domain_exploitation' => $request->domain_exploitation,
                        'file' => $request->ucfirst($file->getClientOriginalName()),
                        'price' => $request->price,
                        'teacher_id' => $prof->user->teacher->id,
                        'status_id' => $request->status_id
                    ]
                );

                foreach ($categories as $category) {
                    ArticleCategory::firstOrCreate(['category_id' => $category->id, 'article_id' => $file_create->id]);
                }

                foreach ($documents as $document) {
                    DocumentArticle::firstOrCreate(['document_id' => $document->id, 'article_id' => $file_create->id]);
                }
            }
        }
    }

    public function student_articles()
    {
        return view('teacher.student_articles');
    }

    public function dashboard()
    {
        $user = auth::user();

        $teacher = Teacher::with('user')->where('user_id', $user->id)->first();
        //Tout les articles (Public et privé)

        $db_articles = Article::with('teacher', 'documents')->where('teacher_id', $teacher->id)->get();

        //les collections dans lequelles sont contenus les articles
        $db_collections = collect();
        foreach ($db_articles as $articl) {

            foreach ($articl->documents as $fld) {

                if ($db_collections->isEmpty()) {
                    $db_collections->push($fld);
                } else {

                    $countdocs =  $db_collections->count() - 1;

                    foreach ($db_collections as $key => $cdc) {

                        if ($cdc->id == $fld->id) {
                            break;
                        } else {
                            if ($key == $countdocs) {
                                $db_collections->push($fld);
                            }
                        }
                    }
                }
            }
        }

        // la liste des etudiants ayant acheter ou telecharger ses supports
        $artc_teach = Article::with('users')
            ->where('teacher_id', $teacher->id)->get();


        $art_students = collect();

        foreach ($artc_teach as $artc) {

            if ($artc->users->isEmpty()) {
                // break;
            } else {
                foreach ($artc->users as  $users_student) {
                    if ($art_students->isEmpty()) {
                        $art_students->push($users_student);
                    } else {

                        $artcount =  $art_students->count() - 1;

                        foreach ($art_students as $key => $exit_stud) {
                            if ($exit_stud->id == $users_student->id) {
                                break;
                            } else {
                                if ($key == $artcount) {
                                    $art_students->push($users_student);
                                }
                            }
                        }
                    }
                }
            }
        }

        // Nouveaux articles du professeur

        $db_new_articles = $db_articles->where('created_at', '>=', Carbon::now()->subDays(186))
            ->sortByDesc('created_at')->take(10);

        // Etudiant ayant telecharger ou acheter un article
        $students_down = $art_students->where('rule_id', 1)->take(10);


        $dashboard = $this->dashboard_data($teacher, 1);
        $db_revenus = $dashboard['revenus'];

        $semaine = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","samedi");

        $mois = array(1=>"Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre");

        $jour_day = $semaine[getdate()["wday"]];
        $mois_day = $mois[getdate()["mon"]];

        // dd($teacher->title->lampoon);

        return view('teacher.dashboard', compact('db_articles','teacher', 'db_revenus', 'db_collections', 'art_students', 'db_new_articles', 'students_down','jour_day','mois_day'));
    }
}
