<?php

namespace App\Http\Controllers;

use App\User;
use App\Article;
use App\Exchang;
use App\Library;
use App\Payment;
use App\Category;
use App\Document;
use Carbon\Carbon;
use App\Activation;
use App\AgreeLibrary;
use App\LibraryAbonnement;
use Illuminate\Http\Request;
use App\LibraryAbonnementPrice;
use App\Period;
use MercurySeries\Flashy\Flashy;
use App\Traits\General_p_teacher;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class LibraryController extends Controller
{
    use General_p_teacher;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth::user();

        if(!is_null($user)){
            $library = Library::with('user')->where('user_id', $user->id)->first();
    
            $dashboard = $this->dashboard_data_library($library, 1);
    
            $articles = $dashboard['articles'];
            $revenus = $dashboard['revenus'];
    
            $categories = Category::orderBy('category')->get();
    
            $documents = Document::where('document_name', '==', 'Public')->get();
    
            $activate_library = Activation::where('user_id', $user->id)->get();

            return view('libraries.index', compact('library','articles', 'revenus', 'categories', 'documents'));
        }else{
            return redirect('login');
        }
    }

    // la liste des abonnements d'une bibliio
    public function abonnements($id)
    {
        $abonnements = LibraryAbonnementPrice::with('library')->whereLibraryId($id)->get();
        return view('libraries.pay_abonnement', compact('abonnements'));
    }


    // la liste des abonnements d'une bibliio
    public function dashboard()
    {
        $user = auth::id();

        if(!is_null($user)){
                $library = Library::with('user')->where('user_id', $user)->first();

            //Tout les articles (Public et privé)
            $db_articles = Article::with('library', 'documents')->where('library_id', $library->id)->get();

            //les collections dans lequelles sont contenus les articles
            $db_collections = collect();
            foreach ($db_articles as $articl) {

                foreach ($articl->documents as $fld) {

                    if ($db_collections->isEmpty()) {
                        $db_collections->push($fld);
                    } else {

                        $countdocs = $db_collections->count() - 1;

                        foreach ($db_collections as $key => $cdc) {

                            if ($cdc->id == $fld->id) {
                                break;
                            } else {
                                if ($key == $countdocs) {
                                    $db_collections->push($fld);
                                }
                            }
                        }
                    }
                }
            }

            // la liste des etudiants ayant acheter ou telecharger ses supports
            $artc_library = Article::with('users')
                ->where('library_id', $library->id)->get();

            $art_students = collect();

            foreach ($artc_library as $artc) {

                if ($artc->users->isEmpty()) {
                    // break;
                } else {
                    foreach ($artc->users as $users_student) {
                        if ($art_students->isEmpty()) {
                            $art_students->push($users_student);
                        } else {

                            $artcount = $art_students->count() - 1;

                            foreach ($art_students as $key => $exit_stud) {
                                if ($exit_stud->id == $users_student->id) {
                                    break;
                                } else {
                                    if ($key == $artcount) {
                                        $art_students->push($users_student);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Nouveaux articles du professeur

            $db_new_articles = $db_articles->where('created_at', '>=', Carbon::now()->subDays(186))
                ->sortByDesc('created_at')->take(10);

            // Etudiant ayant telecharger ou acheter un article
            $students_views = $art_students->where('rule_id', 1)->take(10);

            $dashboard = $this->dashboard_data_library($library, 1);
            $db_revenus = $dashboard['revenus'];

            $semaine = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","samedi");

            $mois = array(1=>"Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre");

            $jour_day = $semaine[getdate()["wday"]];
            $mois_day = $mois[getdate()["mon"]];


            return view('libraries.dashboard', compact('library','db_articles', 'db_revenus', 'db_collections', 'art_students', 'db_new_articles', 'students_views','jour_day','mois_day'));

        }else{
            return redirect('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agreem = AgreeLibrary::findOrFail($request->agreem_id);
        //Picture university
        $picture = $request->file('file_picture');
        $logo = $request->file('file_logo');

        if (!empty($picture)) {

            $filename = 'picture_' . $agreem->name_institution . '_' . time() . '.' . $picture->getClientOriginalExtension();
            Image::make($picture)->resize(900, 600)->save(storage_path('app\\public\\uploads\\images\\' . $filename));
        }

        // Logo
        if (!empty($logo)) {

            $filename_logo = 'logo_' . $agreem->name_institution . '_' . time() . '.' . $logo->getClientOriginalExtension();
            Image::make($logo)->resize(900, 600)->save(storage_path('app\\public\\uploads\\avatars\\' . $filename_logo));
        }

        // creation d'une university
        $library =  Library::firstOrCreate([
            'email' => $agreem->email_agreement,
            'name_library' => $agreem->Library_name,
            'phone' => $request->phone,
            'user_id' => $request->user_id
        ], [
            'website' => $request->website,
            'describe' => $request->describe,
            'adress' => $request->adress,
            'picture' => $filename,
            'logo' => $filename_logo
        ]);

        //Update agreement
        $agreem->status_id = 1;
        $agreem->library_id = $library->id;
        $agreem->save();

        $activation = Activation::create([
            'user_id' => $request->user_id,
            'university_id' => 1
        ]);

        Period::create([
            'start_at' => Carbon::now(),
            'end_at' => Carbon::now()->addMonths(2),
            'activation_id' => $activation->id,
            'status_id' => 1,
        ]);

        // Envoie d'email
        $user = User::find($request->user_id);
        $data = array(
            'name' => $user->name,
            'role' => $user->rule->rule,
            'time' => "2 semaines",
        );

        // Mail::to($user->email)->send(new SendMail($data));

        // Fin d'envoie d'email

        Flashy::primary($library->name_library . ', votre compte a été créee avec succès !!!');

        Auth()->login($user);
        return redirect('/');
    }

    // Payer l'abonnement
    public function pay_abonnement(Request $request)
    {
        $user = auth::user();

        if(!is_null($user)){
            $montant_net_pay = 0;
            $exchange =  Exchang::get()->last();

            $library_price = LibraryAbonnementPrice::findOrFail($request->abonnement_price_id);
            if ($request->devise == $library_price->devise) {
                $montant_net_pay = $request->price * $request->timing_abonnement;
            } else {
                if ($request->devise == "USD" && $library_price->devise == "CDF") {

                    $montant_net_pay = ($request->price * $request->timing_abonnement) / $exchange->exchange;
                } else {

                    $montant_net_pay = ($request->price * $request->timing_abonnement) * $exchange->exchange;
                }
            }

            $gain = (($montant_net_pay * $request->timing_abonnement) * 2) / 100;
            $gain_library = (($montant_net_pay * $request->timing_abonnement) * 96) / 100;


            // if($request->devise == "USD"){
            //     $gain = (($request->price * $request->timing_abonnement ) * 2) / 100;
            //     $gain_library = (($request->price * $request->timing_abonnement ) * 96) / 100;
            // }else{
            //     $gain = ((($request->price * $request->timing_abonnement)* $exchange->exchange ) * 2) / 100;
            //     $gain_library = ((($request->price * $request->timing_abonnement)* $exchange->exchange ) * 96) / 100;
            // }

            $payement = Payment::firstOrCreate([
                // a changer avec la connection
                'number_transaction' => $request->num . Carbon::now(),
                'phone' => $request->num
            ], [
                'to_concern' => "ABONNEMENT LIBRARY",
                'price' => $montant_net_pay,
                'exchange' => $exchange->exchange,
                'provider' => $request->provider,
                'fee' => $gain,
                'fee_devise' => $request->devise,
                'user_id' => Auth::id()
            ]);

            LibraryAbonnement::firstOrCreate([
                'payment_id' => $payement->id,
                'user_id' => $payement->user_id,
                'library_abonnement_price_id' => $request->abonnement_price_id
            ], [
                'timing_abonnement' => $request->timing_abonnement,
                'start_date' => Carbon::now(),
                'end_date' => Carbon::now()->addDays(31 * $request->timing_abonnement),
                'fee' => $gain,
                'fee_devise' => $request->devise,
                'price_lbry' => $gain_library,
                'price_devise_lbry' => $request->devise
            ]);

            $client = new \GuzzleHttp\Client();
            $response = $client->post(
                'https://apis-merchant.rosepay.org/v1/debit',
                [
                    'json' => [
                        'auth_key' => "odjPomd930HkddPor209",
                        'nom' => "Israel",
                        'phone' => $request->num,
                        'montant' => $montant_net_pay,
                        'devise' => $request->devise,
                        'type' => $request->provider,
                    ],
                ]
            );

            Flashy::success("Achat abonnement éffectué avec succès !!!");
        return redirect('/article');
        }else{
            return redirect('login');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lib = Library::findOrFail($id);
        return view('libraries.show', compact('lib'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
