<?php

namespace App\Http\Controllers;

use Flashy;
use App\Like;
use App\User;
use DateTime;
use App\Period;
use App\Article;
use App\Student;
use App\Teacher;
use Carbon\Carbon;
use App\Activation;
use App\University;
use App\Type_student;
use App\Mail\SendMail;
use App\LibraryAbonnement;
use Illuminate\Http\Request;
use App\Traits\General_p_student;
use App\Notifications\ConfirmNotify;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\StudentRequest;
use App\StudentArticle;

class StudentController extends Controller
{

    use General_p_student;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('student.index');
    }


    public function privatenews()
    {
        $user =  auth::user();

        if(!is_null($user)){
            $data_news = $this->view_news_data($user);
            $user_student = $data_news['user_student'];
            $news_private = $data_news['news_private'];
            // $likes = $data_news['likes'];
    
            foreach ($news_private as $news) {
                Like::firstOrCreate([
                    'user_id' => $user->id,
                    'news_id' => $news->id
                ], [
                    'like_at' => Carbon::now(),
                    'like_to' => false,
                    'join_to' => false
                ]);
            }
    
            $data_after_like_news = $this->view_news_data($user);
            $likes = $data_after_like_news['likes'];
    
            // Utilisation de la function  univ_teachers() du trait General_p_student
            $datas_teachers = $this->univ_teachers($user);
            $data_teachers = $datas_teachers['data_teachers'];
    
            if ($data_teachers->count() >= 2) {
    
                $data_teachers = $data_teachers->random(2);
            }
    
            $universities = University::where('name_university', '!=', 'BookCase')->get();
    
            if ($universities->count() >= 2) {
                $universities = $universities->random(2);
            }
    
            return view(
                'student.privatenews',
                compact(
                    'user_student',
                    'news_private',
                    'likes',
                    'data_teachers',
                    'universities'
                )
            );

        }else{
            return redirect('login');
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('student.create');
    }

    public function student_create($user)
    {
        $user = User::findOrFail($user);
        $type_etudiant = Type_student::get();
        return view('student.create', compact('user', 'type_etudiant'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(
            [
                'university_id' => 1,
                'start_at' => Carbon::now(),
                'type_student_id' => 1
            ]
        );

        // creation d'un etudiant
        $student =  Student::create($request->all());

        // creation de  son activation avec une université par defaut (Aucun)
        $activation =  Activation::create($request->all());
        $request->merge(
            [
                'end_at' => Carbon::now()->addMonths(6),
                'activation_id' => $activation->id,
                'status_id' => 1
            ]
        );
        // creation de sa periode de validité de 6 mois
        Period::create($request->all());

        $user = User::find($request->user_id);

        $data = array(
            'name' => $user->name,
            'role' => $user->rule->rule,
            'time' => "3 ans",
        );

        // $data = array('name' => $user->name, "body" => "Test mail");
        // Mail::send(['text' => 'auth.SendEmail'], ['name', 'basil'], function ($message) {
        //     $message->to('basiluantemo@gmail.com', 'Tobip')->subject('New Bookcase');
        //     $message->from($user->email, 'Asix');
        // });

        // Mail::to($user->email)->send(new SendMail($data)); tooo

        // $to_name = $user->name;
        // $to_email = 'mosesbulantemo@gmail.com';

        // Mail::send('auth.sendEmail', $data, function ($message) use ($to_name,  $to_email) {
        //     $message->to($to_email, $to_name)
        //         ->subject('Artisans Web Testing Mail');
        //     $message->from('basiluantemo@gmail.com', 'Artisans Web');
        // });


        // Mail notification desctivé en mode dev
        $user->notify(new ConfirmNotify($user));

        // message flash

        Flashy::primary($student->student_name . ', votre compte a été créee avec succès !!!');

        Auth()->login($user);
        return redirect('/home');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // la liste de teachers d'une university x
    public function univ_teacher()
    {
        $user = auth::user();

        if(!is_null($user)){
            $datas_teachers = $this->univ_teachers($user);
    
            $data_teachers = $datas_teachers['data_teachers'];
            $user_university = $datas_teachers['user_university'];
            return view('student.teacher_show', compact('data_teachers', 'user_university'));
        }else{
            return redirect('login');
        }

    }

    // les données uniquement d'un seul enseignant
    public function show_teacher($id)
    {
        // $user = auth::user();

        $teacher_article = Article::with('teacher', 'documents', 'categories')->where('teacher_id', $id)->get();

        // Categorie articles : 
        // -Thèse
        // -Syllabus
        // -Memoire
        // -TFC
        // -TP
        // -Livre

        $dataThèse = collect();
        $dataSyllabus = collect();
        $dataMemoire = collect();
        $dataTFC = collect();
        $dataTP = collect();
        $dataLivre = collect();

        $teacher = collect();

        // $datas->push($data[$i][$z]);

        foreach ($teacher_article as $article) {

            foreach ($article->categories as $category) {

                switch ($category->category) {
                    case 'Thèse':
                        $dataThèse->push($category);
                        break;

                    case 'Syllabus':
                        $dataSyllabus->push($category);
                        break;

                    case 'Memoire':
                        $dataMemoire->push($category);
                        break;

                    case 'TFC':
                        $dataTFC->push($category);
                        break;

                    case 'TP':
                        $dataTP->push($category);
                        break;

                    default:
                        $dataLivre->push($category);
                        break;
                }
            }
        }

        if ($teacher_article->count() == 0) {

            $teacher = Teacher::with('user', 'articles')->where('id', $id)->firstOrFail();
        }

        return view('student.show_teacher', compact('teacher_article', 'dataThèse', 'dataSyllabus', 'dataMemoire', 'dataTFC', 'dataTP', 'dataLivre', 'teacher'));

    }



    public function s_libraries($datav = "ACB")
    {
        $user = Auth::id();

        if (!is_null($user)) {

            if ($datav == "ACB") {
                $libraries  = LibraryAbonnement::with('library_abonnement_price')->whereUserId($user)->get();

                $lib_abonn = collect();

                if ($libraries->count() != 0) {
                    foreach ($libraries as $lib) {
                        if ($lib_abonn->count() == 0) {
                            $lib_abonn->push($lib->library_abonnement_price->library);
                        } else {
                            if ($lib_abonn->where('id', $lib->library_abonnement_price->library->id)) {
                            } else {
                                $lib_abonn->push($lib->library_abonnement_price->library);
                            }
                        }
                    }

                    $data_articles = Article::with('library')->where('library_id', '!=', 'null')->get();
                    $articles = collect();
                    //  a revoir
                    foreach ($data_articles as $art) {
                        foreach ($lib_abonn as $lib_abo) {
                            if ($lib_abo->id == $art->library_id) {
                                $articles->push($art);
                            }
                        }
                    }
                    $articles->unique('id');
                } else {
                    $articles = Article::with('library')->where('library_id', '!=', 'null')->get();
                }

                if ($articles->count() > 6) {
                    $articles->random(6);
                }
                $titles = "ACB";
            } else {
                // // rajout des articles favorites
                $titles = "MFB";
                $articles = collect();
                $favorites = StudentArticle::where('user_id', $user)->get();
                $articles_data = Article::where('library_id', '!=', null)->get();

                foreach ($articles_data as $dt) {
                    foreach ($favorites as $fav) {
                        if ($dt->id == $fav->article_id) {
                            $articles->push($dt);
                        }
                    }
                }
            }
            return view('student.library_show', compact('articles', 'titles'));
        } else {
            return redirect('/login');
        }
    }
}
