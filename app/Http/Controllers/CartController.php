<?php

namespace App\Http\Controllers;

use App\Article;
use App\Exchang;
use App\Payment;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $panier_count = Cart::Count();
        $panier_content = Cart::Content();
        $panier_subtotal = Cart::subtotal();
        $user = Auth::user()->load('student');
        return view('pay.panier', compact('panier_count', 'panier_content', 'panier_subtotal','user'));
    }

    public function CartPay(Request $request)
    {
        $panier_count = Cart::Count();
        $total_net = str_replace(' ','',Cart::subtotal()) + ((str_replace(' ','',Cart::subtotal()) * 20)/100);

        // dd($request->provider);
        if($request->devise=="USD"){
            $total_net = $total_net / 2000;
        }

        if($panier_count > 0){
            $client = new \GuzzleHttp\Client();
            $data_response =  $client->post(
                'https://apis-merchant.rosepay.org/v1/debit',
                [
                    'json' => [
                        'auth_key'=>"odjPomd930HkddPor209",
                        'nom'=>"Israel",
                        'phone'=>$request->num,
                        'montant'=>$total_net,
                        'devise'=> $request->devise,
                        'type'=>$request->provider
                    ],
                ]
                );
                $response  = json_decode($data_response->getBody());

                // $client = new \GuzzleHttp\Client();
                // $confirm_response =  $client->post(
                //     'https://apis-merchant.rosepay.org/v1/confirm',
                //     [
                //         'json' => [
                //             'auth_key'=>"odjPomd930HkddPor209",
                //             'reference'=>$response->transaction->reference
                //         ],
                //     ]
                //     );

                //  $response_confirm  = json_decode($confirm_response->getBody());
                //  dd($response_confirm);
                
                $reference  = $response->transaction->reference;

                if( $reference){
                    $exchange =  Exchang::get()->last();
                    $gain = ($total_net * 2) / 100;
                    $payement = Payment::firstOrCreate([
                        'number_transaction' => $reference,
                        'phone' => $request->num
                    ], [
                        'to_concern' => "ACHAT SUPPORT",
                        'price' => $total_net,
                        'exchange' => $exchange->exchange,
                        'provider' => $request->provider,
                        'fee' => $gain,
                        'fee_devise' => $request->devise,
                        'status' => "EN COURS",
                        'user_id' => Auth::id()
                    ]);
                }
            return view('pay.PayConfirm', compact('reference'));

        }
    }
    public function PayConfirm(Request $request)
    {
        if($request->reference){

            $client = new \GuzzleHttp\Client();

                $confirm_response =  $client->post(
                    'https://apis-merchant.rosepay.org/v1/confirm',
                    [
                        'json' => [
                            'auth_key'=>"odjPomd930HkddPor209",
                            'reference'=>$request->reference
                        ],
                    ]
                    );

                 $response_confirm  = json_decode($confirm_response->getBody());

                if($response_confirm->transaction->status == "success"){

                    $pay = Payment::where('reference',$request->reference)->first();
                    $pay->update([
                        'status'=>"SUCCESS"
                    ]);

                    Flashy::success("Votre paiement a été fait avec success !!!");
                }else{
                    Flashy::error("Votre paiement a échoué!!!");
                }
                
                return  view('article.index');
        }
    }

    

    /**
     * Show the form for creating a new resource.
     *comm
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    public function storeart($id){

        $existp = null;
        // verification de k'existence du produit dans le panier
        $duplic = Cart::search(function ($cartItem, $rowId) use ($id) {
            return $cartItem->id == $id;
        });

        if ($duplic->isNotEmpty()) {
            $existp = null;
        } else {
            $existp = 0;
            $article = Article::find($id);
            $exchange = Exchang::orderBy('updated_at','desc')->first();
            if(!is_null($article)){
                // ajouter au panier
                if($article->devise=="CDF"){
                    Cart::add($id, $article->article_name,1, $article->price)
                        ->associate('App\Article');
                }else{
                    Cart::add($id, $article->article_name,1, ($article->price * $exchange->exchange)* 1 )
                        ->associate('App\Article');
                }
            }
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($rowId)
    {
        Cart::remove($rowId);
        $panier_count = Cart::Count();

        if ($panier_count == 0) {
            return redirect('article');
        }
        return back();
    }

    public function CartEmpty()
    {
        Cart::destroy();
        return redirect('article');
    }
}
