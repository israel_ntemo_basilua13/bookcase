<?php

namespace App\Http\Controllers;

use App\Agreement;
use App\AgreeLibrary;
use App\Traits\KeyGenerate;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class AgreementController extends Controller
{

    use KeyGenerate;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(session()->has('type_agreem')){
            Session::forget('type_agreem');
        }

        return view('univ.index');
    }

    public function library()
    {
        session([
            'type_agreem' => "BIBLIO",
        ]);

        return view('univ.index');
    }

    public function agreement($code)
    {
        // $codegenerat = $this->keycode();
        $agreem = Agreement::where('code_activation',$code)
                    ->where('status_id',2)->first();
        
        if (is_null($agreem)) {
               
               return view('univ.notvalidate');
              
            } else {
                
                session([
                    'agreement'=> $agreem
                    ]);

                return redirect()->route('register', ['rule_id' => 2]);
            }

        // session()->flash('agreement', $agreem->id);
        // Flashy::success("Votre demande d'admission a été acceptée !!!");

        // return redirect()->route('register', ['rule_id' => 2]);

        // return view('univ.notvalidate',compact('agreem'));
    }

    public function validate_library($code)
    {
        $agreem = AgreeLibrary::where('code_activation',$code)
                    ->where('status_id',2)->first();
        
        if (is_null($agreem)) {
               return view('univ.notvalidate');
            } else {
                session([
                    'agreement'=> $agreem
                    ]);

                return redirect()->route('register', ['rule_id' => 5]);
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd('iciii koo agre');

        //  \print_r($request->all());

        $request->merge(
            [
                'status_id' => 2
            ]
        );

        $file = $request->file('file_request');

        if (!empty($file)) {
            if (session()->has('type_agreem')) {
                AgreeLibrary::firstOrCreate(
                    [
                        'Library_name' => $request->name_inst,
                        'email_agreement' => $request->email,
                    ],
                    [
                        'file_request' => $file->getClientOriginalName(),
                        'status_id' => 2
                    ]
                );
            }else{
                Agreement::firstOrCreate(
                    [
                        'name_institution' => $request->name_inst,
                        'email_agreement' => $request->email,
                    ],
                    [
                        'file_request' => $file->getClientOriginalName(),
                        'university_id' => 1,
                        'status_id' => 2
                    ]
                );
            }


            Storage::putFileAs('/uploads/files/', $file, $file->getClientOriginalName());

            Flashy::success("Votre demande de création a été envoyé avec success !!!");
        } else {
            Flashy::error("Votre demande de création a echoué !!!");
            // Flashy::Primary("Votre demande sera traité dans un delai de deux semaine. \n Merci pour l'interet que vous portez a BookCase");
        }
        return back();
    }

    public function validateagreement(Request $request)
    {

        $agreement = Agreement::where('email_agreement', $request->email)
            ->where('password_agreement', $request->password)->first();


        if (is_null($agreement)) {
            Flashy::error("Vos identifiants pour un  agrément n'est pas reconnu !!!");
            return back();
        } else {
            if ($agreement->code_activation == $request->c_validate) {
                $request->merge(
                    [
                        'status_id' => 1,
                    ]
                );
                $agreement->update($request->all());
                session()->flash('id_agreement', $agreement->id);

                return redirect()->route('register', ['rule_id' => 2]);
            } else {
                Flashy::error("Le code d'activation pour la validation n'est pas valide !!!");
                return back();
            }
        }

        // dd('validate Request');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
