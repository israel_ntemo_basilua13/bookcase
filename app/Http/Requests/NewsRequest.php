<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'picture' => 'nullable|string|image|between:5,300',
            'title_news' => 'required|string|between:5,500',
            'describe' => 'nullable|string|between:5,900',
            'localization' => 'nullable|string|between:5,150',
            'exploitation_domain' => 'nullable|string|between:5,900',
            'university_id' => 'required|integer',
        ];
    }
}
