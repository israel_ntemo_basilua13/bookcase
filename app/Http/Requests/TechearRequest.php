<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TechearRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_teacher'=>'required|string|between:5,500',
            'sex'=>'required|string|between:5,20',
            'email'=> 'required|email|string|between:5,100',
            'phone'=>'required|integer',
            'domain_exploitation'=> 'required|string|between:5,900',
            'title_id'=>'required|integer',
            'user_id'=>'required|integer',
        ];
    }
}
