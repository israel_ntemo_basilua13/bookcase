<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UniversityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_university' => 'required|string|between: 5,300|unique:universities',
            'picture' => 'required|string|between: 5,500',
            'logo_picture' => 'required|string|between: 5,500',
            'longitude' => 'required|integer|unique:universities',
            'latitude' => 'required|integer|unique:universities',
            'adress' => 'required|string|between:5,500',
            'describe' => 'nullable|string|between:5,800',
            'email' => 'required|email|string|between:5,100',
            'phone' => 'required|integer',
        ];
    }
}
