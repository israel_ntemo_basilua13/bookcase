<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'article_name'=>'required|string|between:5,300',
            'slug'=>'required|string|between:5,800',
            'publishing_at'=>'required|date',
            'author'=>'required|string|between:5,200',
            'domain_exploitation'=>'required|string|between:5,300',
            'file'=>'required|file|string|between:5,300',
            'price'=>'reauired|integer',
            'teacher_id'=>'required|integer',
            'status_id'=>'required|integer',
            
        ];
    }
}
