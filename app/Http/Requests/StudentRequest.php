<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_name'=>'required|string|between:5,500|unique:students',
            'sex'=>'required|string|between:5,20',
            'phone'=>'required|string|between:5,20',
            'domain_exploitation'=> 'required|string|between:5,500',
            'type_student_id'=>'required|integer',
            'user_id'=> 'required|integer',
        ];
    }
}
