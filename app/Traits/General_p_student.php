<?php

namespace App\Traits;

use App\Like;
use App\News;
use App\User;
use App\Article;
use App\Payment;
use App\Student;
use App\Teacher;
use Carbon\Carbon;
use App\Activation;
use App\Library;
use App\University;
use App\StudentArticle;
use App\LibraryAbonnement;
use Illuminate\Support\Facades\Auth;

trait General_p_student
{
    public function view_profile_data(User $user)
    {
        $data_user = null;
        if ($user->rule_id == 1) {
            $data_user = Student::with('user')->where('user_id', $user->id)->firstOrFail();
        }elseif($user->rule_id == 2) {
            $data_user = University::with('user')->where('user_id', $user->id)->firstOrFail();
        } elseif($user->rule_id == 4) {
            $data_user = Teacher::with('user')->where('user_id', $user->id)->firstOrFail();
        }elseif($user->rule_id == 5){
            $data_user = Library::with('user')->where('user_id', $user->id)->firstOrFail();
        }else{
            $data_user = Student::with('user')->where('user_id', $user->id)->firstOrFail();
        }

        $activation = Activation::with('user', 'university')
            ->where('user_id', $user->id)
            // ->where('university_id', '!=', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        if (is_null($activation)) {
            $activation = new Activation();
        }

        $universities = University::where('id', "!=", 1)
            ->orderBy('name_university')
            ->paginate(5);

        $data = [
            'student' => $data_user,
            'activation'  => $activation,
            'universities' => $universities
        ];

        return $data;
    }

    // Action sur le news
    public function view_news_data(User $user)
    {
        /// A REVOIR
        // // $user = User::with('rule')->where('id',$this->user->id)->first();
        $user_student = Student::where('user_id', $user->id)->firstOrFail();

        $domains = preg_split("/-/", $user_student->domain_exploitation);

        $data = collect();

        foreach ($domains as $domain) {
            // $news_university = News::with('university','comments')
            $news_university = News::searchbynews(trim($domain))
                ->validatebynews()
                ->publicnews()
                ->paginate(10);

            $data->push($news_university)->toArray();
        }

        $datas = collect();
        $universities_news = collect();
        $notview_news = collect();
        $notviewprivate_news = collect();
        $value = 0;

        // Mise ensemble des données dans un seul tableau venant de plusieurs collections
        for ($i = 0; $i < $data->count(); $i++) {

            for ($z = 0; $z < $data[$i]->count(); $z++) {

                $datas->push($data[$i][$z]);
            }
        }

        // Verification des données pour supprimer les doublons
        if ($datas->count() != 0) {

            foreach ($datas as $datasi) {

                if ($universities_news->count() == 0) {

                    $universities_news->push($datasi);
                } else {
                    for ($b = 0; $b < $universities_news->count(); $b++) {

                        if ($datasi->id == $universities_news[$b]->id) {

                            $value = 1;
                            break;
                        } else {
                            $value = 0;
                        }
                    }

                    if ($value == 0) {
                        $universities_news->push($datasi);
                    }
                }
            }
        }

        // en rapport avec les news privé

        // $user_active = User::with('activations')->where('id', $user->id)->first();
        // $user_active = User::where('id', $user->id)->first();
        // $user_activation = $user_active->activations()->lastactivation()->first();
        $user_activation = $user->activations()->lastactivation()->first();

        $news_private = News::privatenews()->where('university_id', $user_activation->university_id)->get();


        // Ceux qui n'ont pas encore été vu par l'utilisateur (news public)

        // $likes = Like::with('news')->where('user_id', $user->id)->get();
        $likes = Like::where('user_id', $user->id)->get();

        if ($likes->count() > 0) {
            foreach ($universities_news as $recent) {
                foreach ($likes as $likeid) {
                    if ($recent->id == $likeid->news_id) {
                        break;
                    } else {

                        if ($likeid->id == $likes->last()->id) {

                            $notview_news->push($recent);
                            break;
                        }
                    }
                }
            }
        } else {

            foreach ($universities_news as $news_count) {
                $notview_news->push($news_count);
            }
        }

        // Ceux qui n'ont pas encore été vu par l'utilisateur (news private)
        if ($likes->count() > 0) {
            foreach ($news_private as $young) {
                foreach ($likes as $likeid) {
                    if ($young->id == $likeid->news_id) {
                        break;
                    } else {

                        if ($likeid->id == $likes->last()->id) {

                            $notviewprivate_news->push($young);

                            break;
                        }
                    }
                }
            }
        } else {

            foreach ($news_private as $news_priv) {
                $notviewprivate_news->push($news_priv);
            }
        }

        // artice déjà telecharger
        $favourites =  StudentArticle::whereUserId(
            $user->id
        )->get();

        // articke achetéé
        $support_pays =  Payment::whereUserId(
            $user->id
        )->whereStatus('SUCCESS')->whereToConcern("ACHAT SUPPORT")->get();

        // abonnement pour les library en cours de validité
        $library_abonnements =  LibraryAbonnement::validatebyabonnementlibrary()
            ->whereUserId($user->id)->get();

        // les suggestions des cours a télecharger
        $last_down =  StudentArticle::whereUserId($user->id)->orderByDesc('updated_at')->first();

        $article_suggestion = collect();

        if(!is_null($last_down)){
           $domains = preg_split("/-/", $last_down->article->slug);
        }else{
            $domains = preg_split("/-/", $user->student->domain_exploitation);
        }

        foreach ($domains as $domain) {
            $suggestions = Article::with('teacher', 'documents', 'categories')
                ->searchbyarticle(trim($domain))
                ->publicarticle()
                ->freearticle()
                ->publishdesc()
                ->get();

            $article_suggestion->push($suggestions)->toArray();
        }

        $article_suggestions = collect();
        $article_suggestions_corrects = collect();

        // Mise ensemble des données dans un seul tableau venant de plusieurs collections
        for (
            $i = 0;
            $i < $article_suggestion->count();
            $i++
        ) {
            for ($z = 0; $z < $article_suggestion[$i]->count(); $z++) {
                $article_suggestions->push($article_suggestion[$i][$z]);
            }
        }

        // Verification des données pour supprimer les doublons
        if ($article_suggestions->count() != 0) {
            foreach ($article_suggestions as $datasi) {

                if ($article_suggestions_corrects->count() == 0) {
                        $article_suggestions_corrects->push($datasi);
                } else {
                    for ($b = 0; $b < $article_suggestions_corrects->count(); $b++) {

                        if (
                            $datasi->id == $article_suggestions_corrects[$b]->id
                        ) {
                            $value = 1;
                            break;
                        } else {
                            $value = 0;
                        }
                    }

                    if ($value == 0) {
                        if(!is_null($last_down) && $datasi->id != $last_down->article->id){
                            $article_suggestions_corrects->push($datasi);
                        }
                    }
                }
            }
        }

        $data_news = [
            'user_student' => $user_student,
            'universities_news'  => $universities_news->sortByDesc('created_at'),
            'likes' => $likes,
            'notview_news' => $notview_news,
            'news_private' => $news_private->sortByDesc('created_at'),
            'notviewprivate_news'  => $notviewprivate_news,
            'favourites'  => $favourites,
            'support_pays'  => $support_pays,
            'library_abonnements'  => $library_abonnements,
            'article_suggestions'  => $article_suggestions_corrects,
        ];

        return $data_news;
    }

    //teacher d'une l'université d'attache

    public function univ_teachers(User $user)
    {
        $data_teachers = collect();
        // $user_student = User::with('student', 'activations')->where('id', $user->id)->first();
        $user_student = User::where('id', $user->id)->first();

        $user_activation = $user_student->activations()->lastactivation()->first();

        $user_university = $user_activation->university()->where('id', $user_activation->university_id)->first();

        if (is_null($user_university)) {
            $user_university = collect();
            $user_university->id = 1;
        }


        if ($user_activation->university_id == 1) {
            $data_teachers = collect();
        } else {
            //ICI a travailler 
            // avoir les professeurs
            // $teacher_activation = Activation::with('user', 'university')
            $teacher_activation = Activation::with('user', 'university')
                ->where('university_id', $user_activation->university_id)
                ->get();

            // Suppression des datas pour avoir l'unicité des professeurs
            foreach ($teacher_activation as $data) {

                if ($data->user->rule_id == 4) {
                    if ($data_teachers->count() == 0) {

                        $data_teachers->push($data);
                    } else {

                        foreach ($data_teachers as $dt) {

                            if ($data->user->id != $dt->user->id) {
                                $data_teachers->push($data);
                                break;
                            } else {

                                break;
                            }
                        }
                    }
                }
            }
        }

        $datax_teachers = collect();

       foreach ($data_teachers as $data) {
         $datax_teachers ->push($data);
       }


        $data_teachers = $data_teachers->sortByDesc('start_at');
        $datas_teachers = [
            'data_teachers' => $datax_teachers,
            'user_university'  =>$user_university
        ];

        return $datas_teachers;
    }

    // Action sur les likes ou join au niveau all
    public function attach_like_join_to($id, $value)
    {
        $user = Auth::user();

        $news = News::find($id);
        $like = Like::where('user_id', $user->id)
            ->where('news_id', $news->id)
            ->first();

        if (is_null($like)) {
            if ($value == 'like') {
                $like =  Like::create([
                    'like_at' => Carbon::now(),
                    'like_to' => true,
                    'user_id' => $user->id,
                    'news_id' => $news->id
                ]);
            } else {
                $like =  Like::create([
                    'like_at' => Carbon::now(),
                    'join_to' => true,
                    'user_id' => $user->id,
                    'news_id' => $news->id
                ]);
            }
        } else {
            if ($value == 'like') {

                if ($like->like_to == false) {
                    $like->like_to = true;
                } else {
                    $like->like_to = false;
                }
            } else {

                if ($like->join_to == false) {
                    $like->join_to = true;
                } else {
                    $like->join_to = false;
                }
            }

            $like->save();
        }

        return true;
    }

    // Action sur les likes ou join au niveau single
    public function attach_like_join_show($id, $value)
    {
        $user = Auth::user();

        $news = News::with('university')->where('id', $id)->first();
        $like = $news->likes()->where('user_id', $user->id)->first();

        if (is_null($like) || $like->count() == 0) {
            if ($value == 'like') {
                // pour j'aime
                $like =  Like::create([
                    'like_at' => Carbon::now(),
                    'like_to' => true,
                    'user_id' => $user->id,
                    'news_id' => $news->id
                ]);
            } else {
                // pour prendre part
                $like =  Like::create([
                    'like_at' => Carbon::now(),
                    'join_to' => true,
                    'user_id' => $user->id,
                    'news_id' => $news->id
                ]);
            }
        } else {
            if ($value == 'like') {
                // pour j'aime
                if ($like->like_to == false) {
                    $like->like_to = true;
                } else {
                    $like->like_to = false;
                }
            } else {
                // pour prendre part
                if ($like->join_to == false) {
                    $like->join_to = true;
                } else {
                    $like->join_to = false;
                }
            }
            $like->save();
        }

        // Sa marche quand meme
        //     $data_like_join =[
        //         'news' => $news,
        //         'like'  => $like,
        //     ];
        //    return $data_like_join;
    }
}
