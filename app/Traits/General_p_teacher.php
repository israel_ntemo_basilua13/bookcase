<?php

namespace App\Traits;

use App\User;
use App\Article;
use App\Library;
use App\Payment;
use App\Student;
use App\Teacher;
use Carbon\Carbon;
use App\Activation;
use App\University;
use Illuminate\Support\Facades\Auth;

trait General_p_teacher
{
    // Action sur le profile
    // public function view_profile_data_teacher()
    // {
    //     $student = Student::with('user')->where('user_id', Auth::user()->id)->firstOrFail();
    //     $activation = Activation::with('user', 'university')
    //         ->where('user_id', Auth::user()->id)
    //         // ->where('university_id', '!=', 1)
    //         ->orderBy('created_at', 'desc')
    //         ->get();

    //     if (is_null($activation)) {
    //         $activation = new Activation();
    //     }

    //     $universities = University::where('id', "!=", 1)
    //         ->orderBy('name_university')
    //         ->paginate(5);


    //     $data = [
    //         'student' => $student,
    //         'activation'  => $activation,
    //         'universities' => $universities
    //     ];
    //     return $data;
    // }


    public function dashboard_data(Teacher $teacher, $status)
    {
        $articles = Article::with('categories', 'documents')
            ->where('teacher_id', $teacher->id)
            ->where('status_id', $status)
            ->orderBy('publishing_at', 'desc')
            ->paginate(12);


        $prices = 0;

        $paiements = Payment::with('article')->where('user_id', $teacher->user->id)->get();

        foreach ($paiements as $pay) {
            $prices = $pay->article->price;
        }

        $revenus = $prices;
        // calcul a faire sur les revenus pour soustraire les commissions
        //

        $datas = [
            'articles' => $articles,
            'revenus' => $revenus
        ];

        return $datas;
    }


    public function dashboard_data_library(Library $library, $status)
    {
        $articles = Article::with('categories', 'documents')
            ->where('library_id', $library->id)
            ->where('status_id', $status)
            ->orderBy('publishing_at', 'desc')
            ->paginate(12);


        $prices = 0;

        $paiements = Payment::with('article')->where('user_id', $library->user->id)->get();

        foreach ($paiements as $pay) {
            $prices = $pay->article->price;
        }

        $revenus = $prices;
        // calcul a faire sur les revenus pour soustraire les commissions
        //

        $datas = [
            'articles' => $articles,
            'revenus' => $revenus
        ];

        return $datas;
    }
}
