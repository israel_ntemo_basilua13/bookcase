<?php
  namespace App\Traits;

  /**
   *
   */
  trait KeyGenerate
  {
    // gen key
    public function keycode(){
      $genKey="";
      $string ="0123456789@azertyuiopqsdfghjkmwxcvbnAZERTYUOPMLKJHGFDSQNBVCXW";
      // srand((double)microtime()*1000000000);
      for ($i=0; $i < 10; $i++) {
          $genKey .= $string[rand()%strlen($string)];
        }
      return $genKey.date("Hs");
    }
  }
