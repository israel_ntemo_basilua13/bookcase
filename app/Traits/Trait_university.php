<?php

namespace App\Traits;

use App\Student;
use App\Teacher;
use App\Activation;
use App\University;
use App\User;
use App\News;
use App\Like;
use Carbon\Carbon;
use App\UniversityReference;
use App\Faculty;
use App\UniversityFaculty;
use App\Document;
use App\Department;
use Illuminate\Support\Facades\Auth;

trait Trait_university
{

    // Action sur le news
    public function view_dashBord(University $university)
    {
        
        $all_news = News::all();
        $news_all = News::where('university_id',$university->id)->get();

        $News = News::where('university_id',$university->id)
                    ->validatebynews()
                    ->orderBy('created_at','desc')
                    ->paginate(3);

        $teachers = User::with('activations')->where('rule_id',4)->get();
        $teacher_univ = collect();

        foreach ($teachers as $teacher) {
           foreach($teacher->activations as $teach_act){
               if($teach_act->university_id == $university->id){
                   if($teacher_univ->isEmpty()){
                        $teacher_univ->push($teacher);
                    }
                    else{

                        $countteachs = $teacher_univ->count()-1;
                        foreach ($teacher_univ as $key => $atc) 
                        {
                            if ($atc->id == $teacher->id) {
                                break;
                            } else {
                                if ($key == $countteachs) {
                                    $teacher_univ->push($teacher);
                                }
                            }
                        }

                    }
               }
           }
        }

        $teacher_reference = UniversityReference::where('university_id',$university->id)->get();

        // Student université

        $students = User::with('activations')->where('rule_id',1)->get();
        $student_univ = collect();

        foreach ($students as $student) {
           foreach($student->activations as $student_act){
               if($student_act->university_id == $university->id){
                   if($student_univ->isEmpty()){
                        $student_univ->push($student);
                    }
                    else{

                        $countstuds = $student_univ->count()-1;
                        foreach ($student_univ as $key => $atst) 
                        {
                            if ($atst->id == $student->id) {
                                break;
                            } else {
                                if ($key == $countstuds) {
                                    $student_univ->push($student);
                                }
                            }
                        }

                    }
               }
           }
        }

        $student_belongsTo = collect();

        foreach ($student_univ as $belongsTo) {

            if($belongsTo->activations->sortByDesc('created_at')->first()->university_id == $university->id)
            {
                $student_belongsTo->push($belongsTo);
            }
        }

        // $teacher_created = collect();

        // foreach ($teacher_univ as $created) {
        //   if ($created->activations[0]->university_id = $university->id) {
        //         $teacher_created->push($created);
        //   }
        // }

        // Faculty

        $faculties = Faculty::where('id','!=',1)->get();
        $facultys = UniversityFaculty::where('university_id',$university->id)
                                        ->where('id','!=',1)
                                        ->get();

        // Departements
        $departments = Department::where('id','!=',1)->get();
        $departmenties = collect();

        foreach ($departments as $departement) {
            foreach ($departement->faculty->universities as $depart) {
                if($depart->id==$university->id){
                    $departmenties->push($departement);
                }
            }
        }

        // Collections ou Documents

        $collections = Document::where('university_id',$university->id)->get();
        $collections_all = Document::all();

        $data = [
            
            'News' => $News,
            'news_all' => $news_all,
            'all_news' => $all_news,
            'teacher_univ' => $teacher_univ,
            'teacher_reference' => $teacher_reference,
            'student_univ' => $student_univ,
            'student_belongsTo' => $student_belongsTo,
            'facultys' => $facultys,
            'faculties' => $faculties,
            'collections' => $collections,
            'collections_all'=> $collections_all,
            'departments'=> $departments,
            'departmenties'=> $departmenties,
        ];

        return $data;
    }

}
