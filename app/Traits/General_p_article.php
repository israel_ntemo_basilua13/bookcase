<?php

namespace App\Traits;

use App\Student;
use App\University;
use App\User;
use App\Article;
use app\Document;
use app\Category;
use app\Teacher;
use app\Status;
use App\StudentArticle;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait General_p_article
{
    // Action sur les articles
    public function article_data(User $user, $type, $page)
    {
        if ($page == 'welcome') {
            $user_student = new Student;
            $user_student->domain_exploitation = $user->domain_exploitation;
        } else {
            $user_student = Student::where('user_id', $user->id)->first();
        }

        $domains = preg_split("/-/", $user_student->domain_exploitation);

        
        $data = collect();

        //Pour le data des articles all, free, recent et payant
        if ($type == 'free') {

            foreach ($domains as $domain) {
                $articles_search = Article::with('teacher', 'documents', 'categories')
                    ->searchbyarticle(trim($domain))
                    ->publicarticle()
                    ->freearticle()
                    ->publishdesc()
                    ->get();

                $data->push($articles_search)->toArray();
            }
        } elseif ($type == 'payant') {

            foreach ($domains as $domain) {
                $articles_search = Article::with('teacher', 'documents', 'categories')
                    ->searchbyarticle(trim($domain))
                    ->publicarticle()
                    ->payarticle()
                    ->publishdesc()
                    ->get();

                $data->push($articles_search)->toArray();
            }
        } else {

            foreach ($domains as $domain) {
                $articles_search = Article::with('teacher', 'documents', 'categories')
                    ->searchbyarticle(trim($domain))
                    ->publicarticle()
                    ->publishdesc()
                    ->get();

                $data->push($articles_search)->toArray();
            }
        }


        $datas = collect();
        $articles = collect();
        $value = 0;

        // Mise ensemble des données dans un seul tableau venant de plusieurs collections
        for ($i = 0; $i < $data->count(); $i++) {
            for ($z = 0; $z < $data[$i]->count(); $z++) {
                $datas->push($data[$i][$z]);
            }
        }

        // Verification des données pour supprimer les doublons
        if ($datas->count() != 0) {
            foreach ($datas as $datasi) {

                if ($articles->count() == 0) {

                    $articles->push($datasi);
                } else {
                    for ($b = 0; $b < $articles->count(); $b++) {

                        if ($datasi->id == $articles[$b]->id) {

                            $value = 1;
                            break;
                        } else {
                            $value = 0;
                        }
                    }

                    if ($value == 0) {
                        $articles->push($datasi);
                    }
                }
            }
        }

        // ajout requetes nombre de vues et puis de telechargement

        $data_news = [
            'articles' => $articles,
        ];

        return $data_news;
    }


    //article recent account

    public function recent_article(User $user)
    {
        $recent_articles = StudentArticle::with('article', 'user')->where('user_id', $user->id)
            ->get();

        return  $recent_articles = [
            'recent_article' => $recent_articles
        ];
    }
}
