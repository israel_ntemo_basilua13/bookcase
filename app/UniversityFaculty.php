<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversityFaculty extends Model
{
    protected $fillable=['university_id','faculty_id'];

    public function university()
    {
        return $this->belongsTo('App\University');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Faculty');
    }

}
