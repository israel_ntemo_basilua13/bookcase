<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $fillable = ['start_at','end_at','status_id','activation_id'];

    public function activation(){
        return $this->belongsTo('App\Activation');
    }

    public function status(){
        return $this->belongsTo('App\Status');
    }

    /* scope*/
    /* rechercher seulement les etudiants avec une periode avec un statut égal a 1  */
    public function scopeStatusValidate($query)
    {
        return $query->where('status_id','1');
    }
}
