<?php

namespace App\Utilities;

use App\Like;
// use App\News;
use App\StudentArticle;
use App\Traits\General_p_student;
// use App\Student;
// use App\User;

use Illuminate\Support\Facades\Auth;

class NotViewNews
{

    use General_p_student;
    public $user;

    function __construct() {
        $this->user = auth::user();
    }

    // Nombre des news public qu'un compte n'a pas encore aimé
    public function notviewsnews()
    {
        $data_news = $this->view_news_data($this->user);

        return  $notview_news = $data_news['notview_news'];
    }

    // Nombre des news private qu'un compte n'a pas encore aimé
    public function notviewsnewsprivate()
    {
        if ($this->user->rule_id == 4) {
            $data_news = [
                'notviewprivate_news' => $this->user,
            ];
        } else {
            $data_news = $this->view_news_data($this->user);
        }

        return  $data_news['notviewprivate_news'];
    }

    
    // Nombre des news private
    public function newsprivate()
    {
        if ($this->user->rule_id == 4) {
            $data_news = [
                'notviewprivate_news' => $this->user,
            ];
        } else {
            $data_news = $this->view_news_data($this->user);
        }

        return  $data_news['news_private'];
    }

    // Nombre de j'aime a une news
    public function likecount($lk)
    {
        $likes_count = Like::where('news_id', $lk)
            ->where('like_to', '1')->get();
        return $likes_count;
    }

    // Nombre de j'y serais a une news
    public function joincount($lk)
    {
        $joins_count = Like::where('news_id', $lk)
            ->where('join_to', '1')->get();
        return $joins_count;
    }

    // Nombre des telechargement par rapport a un compte
    public function filedownloadcount($id)
    {
        $file_download_count = StudentArticle::where('article_id', $id)->get();
        return  $file_download_count;
    }
}
