<?php

namespace App\Utilities;
use App\Library;
use Carbon\Carbon;
use App\LibraryAbonnement;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;

class CoreData
{

    // Datas Paniers
    public function CartList()
    {
        return $cartlist = [
            'cart_count'=> Cart::Count(),
            'panier_content'=>Cart::Content(),
        ];
        
    }

    // Datas Paniers
    public function LibraryList()
    {
        return Library::all();
    }


    // Nombre d'abonnées dans une biblio
    public function CountAbon($biblio)
    {
        $library_abon_count = 0;
        $library = Library::with('library_abonnement_prices')->whereId($biblio->id)->first();

        if($library !=null){
            foreach ($library->library_abonnement_prices as $abonn_price) {
                $library_abon_count += $abonn_price->abonnements->unique('user_id')->count();
            }
        }
        return $library_abon_count;
    }

    // Voir si un user est abonnée dans une biblio
    public function AbonUser($biblio)
    {
        $abonn_account = 0;
        $user = Auth::id();
        
        $lib_abos = LibraryAbonnement::whereUserId($user)->get();

        if($lib_abos->count() != 0){
                
            foreach ($lib_abos as $abon) {
                if ($abon->library_abonnement_price->library->id == $biblio->id) {
                    $abonn_account =  $abonn_account + 1;
                }
            }
        }
        return $abonn_account;
    }

    // Voir si un user est abonnée dans une biblio et que son abonnement est valide
    public function ValidateViewArticle($biblio)
    {
        $access = false;
        $user = Auth::id();

        if (!is_null($user)) {
            $lib_abos = LibraryAbonnement::with('library_abonnement_price')->whereUserId($user)->get();

            if ($lib_abos->count() != 0) {
                foreach ($lib_abos as $abon) {
                    if ($abon->library_abonnement_price->library->id == $biblio->id) {
                        if($abon->end_date >= Carbon::now()->subDays(1)){
                            $access = true;
                            break;
                        }
                    }
                }
            }
        }else{
            return redirect('/login');
        }
        return $access;
    }

}
