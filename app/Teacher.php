<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = ['name_teacher', 'sex', 'phone', 'domain_exploitation', 'visibilities', 'title_id', 'user_id'];


    /* Relations */
    public function articles()
    {
        return  $this->hasMany('App\Article');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function title()
    {
        return $this->belongsTo('App\Title');
    }

    //Attributes
    public function setNameTeacherAttribute($value)
    {
        $this->attributes['name_teacher'] = ucfirst($value);
    }

    public function getNameTeacherAttribute($value)
    {
        return ucfirst($value);
    }

    public function setDomainExploitationAttribute($value)
    {
        $this->attributes['domain_exploitation'] = str_replace(",", "-",  strtoupper($value) . ',' . strtolower($value) . ',' . ucfirst($value));
    }

    // Fin Attributes


    /* Scopes */
    public function scopeSearchByTeacher($query, $q)
    {
        return $query->where('name_teacher', 'LIKE', '%' . $q . '%')
            ->orwhere('domain_exploitation', 'LIKE', '%' . $q . '%')
            ->orderBy('name_teacher');
    }
}
