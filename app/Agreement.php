<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    protected $fillable = ['name_institution', 'email_agreement', 'file_request', 'code_activation', 'university_id', 'status_id'];


    /*  relation */
    public function university()
    {
        return $this->belongsTo('App\University');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }


    //getters et setters
    public function setNameInstitutionAttribute($value)
    {
        $this->attributes['name_institution'] = ucfirst($value);
    }

}
