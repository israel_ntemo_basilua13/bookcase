<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    protected $fillable=['lampoon'];

    public function teachers(){
        return $this->hasMany('App\Teacher');
    }

    //Attributes
    public function setLampoonAttribute($value){
        return ucfirst($value);
    }

  
    public function getLampoonAttribute($value){
        return ucfirst($value);
    }

    // Fin Attributes
}
