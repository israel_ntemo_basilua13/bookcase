<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $fillable = ['name_university','sigle', 'picture', 'logo_picture', 'longitude', 'latitude', 'adress', 'describe', 'email', 'phone','user_id'];


    /*  relation */

    public function news()
    {
        return $this->hasMany('App\News');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function activations()
    {
        return $this->hasMany('App\Activation');
    }

    public function agreement()
    {
        return $this->hasOne('App\Agreement');
    }

    public function faculties()
    {
        return $this->belongsToMany('App\Faculty', 'university_faculties');
    }

    public function references()
    {
        return $this->belongsToMany('App\Reference', 'university_references');
    }

    public function documents()
    {
        return $this->hasMany('App\Document');
    }

    //Attributes
    public function setNameAttribute($value)
    {
        return ucfirst($value);
    }
}
