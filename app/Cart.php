<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded=[];
    
    public function articles(){
        return $this->hasMany('App\CartArticle');
    }

    public function user(){
        return $this->hasOne('App\User','user_id');
    }
}
