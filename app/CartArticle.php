<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartArticle extends Model
{
    protected $guarded=[];
    
    public function cart(){
        return $this->belongsTo('App\Cart');
    }

    public function article(){
        return $this->hasOne('App\Article','article_id');
    }
}
