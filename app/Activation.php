<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{
    protected $fillable=['start_at','university_id','user_id','document_id'];

    protected $dates=['start_at'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function university(){
       return  $this->belongsTo('App\University');
    }

    public function periods(){
        return $this->hasMany('App\Period');
    }

    public function scopeUniversity($query){
        return $query->where('university_id','!=','1');
    }

    /* Scope */

    /* revoit  l'activation dernier de l'etudiant  */
    public function scopeLastActivation($query)
    {
        return $query->orderBy('start_at','desc')
                    ->orderBy('id','desc')
                        ->take(1);
    }
}
