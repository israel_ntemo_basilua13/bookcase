<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Payment extends Model
{
    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo('App\user');
    }


    public function pay_articles()
    {
        return $this->hasMany('App\PayArticle','payment_id');
    }

    public function pay_abonnement()
    {
        return $this->hasOne('App\LibraryAbonnement','payment_id');
    }

    /* Scopes */
    public function scopePayRecent($query)
    {
        return $query->where('updated_at', '>=', Carbon::now()->subDays(279));
    }
}
