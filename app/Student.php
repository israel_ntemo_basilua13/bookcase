<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['student_name', 'sex', 'phone', 'domain_exploitation', 'type_student_id', 'user_id'];

    // protected $datas=['birth_at'];

    /* relations */

    public function type_student()
    {
        return $this->belongsTo('App\Type_student');
    }

    public function user()
    {
        return  $this->belongsTo('App\User');
    }

    // public function articles(){

    //     return $this->belongsToMany('App\Article');
    // }

    //getters et setters
    public function setStudentNameAttribute($value)
    {
        $this->attributes['student_name'] = ucfirst($value);
    }


    public function setDomainExploitationAttribute($value)
    {
        $this->attributes['domain_exploitation'] = str_replace(",", "-",  strtoupper($value) . ',' . strtolower($value)). "all-tout-tous";
    }
}
