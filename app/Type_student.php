<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_student extends Model
{
    protected $fillable=['type_student'];


    /* relation */
    public function students()
    {
        return $this->hasMany('App\Student');
    }
}
