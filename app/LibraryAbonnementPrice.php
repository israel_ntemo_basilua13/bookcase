<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryAbonnementPrice extends Model
{
    protected $guarded=[];

    public function type_library_abonnement(){
        return $this->belongsTo('App\TypeLibraryAbonnement');
    }

    public function library(){
        return $this->belongsTo('App\Library');
    }

    public function abonnements(){
        return $this->hasMany('App\LibraryAbonnement');
    }
}
