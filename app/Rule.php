<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{

  use Notifiable;


  protected $fillable = ['rule'];

  public function users()
  {
    return  $this->hasMany('App\User');
  }
}
