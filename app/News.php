<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class News extends Model
{
    protected $fillable = ['date', 'picture', 'title_news', 'describe', 'localization', 'exploitation_domain', 'university_id', 'status_id'];

    protected $dates = ['date'];

    /* Relations */
    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function university()
    {
        return  $this->belongsTo('App\University');
    }

    public function library()
    {
        return  $this->belongsTo('App\Library');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    // Attribiutes
    public function getTitleNewsAttribute($value)
    {
        return ucfirst($value);
    }

    public function setExploitationDomainAttribute($value)
    {
        $this->attributes['exploitation_domain'] = str_replace(",", "-",  strtoupper($value) . ',' . strtolower($value) . ',' . ucfirst($value));
    }
    // Fin attributes
    /* scope*/

    /* recherche des news par rapport a un contains($q) de la part du domaine d'exploitation des students */
    public function scopeSearchByNews($query, $q)
    {
        return $query->where('title_news', 'LIKE', '%' . $q . '%')
            ->orwhere('describe', 'LIKE', '%' . $q . '%')
            ->orwhere('exploitation_domain', 'LIKE', '%' . $q . '%')
            ->orwhere('university_id', 1)
            ->orderBy('date', 'desc');
            // ->orderBy('created_at', 'desc');
    }

    /* recherche des news pa sa validité pour une periode de 60 jours */
    public function scopeValidateByNews($query)
    {
        return $query->where('created_at', '>=', Carbon::now()->subDays(61));
    }

    /* Recherche article public */
    public function scopePublicNews($query)
    {
        return $query->where('status_id', '1');
    }

    /* Recherche article private */
    public function scopePrivateNews($query)
    {
        return $query->where('status_id', '2');
    }
}
