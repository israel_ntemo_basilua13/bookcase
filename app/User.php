<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

// class User extends Authenticatable implements MustVerifyEmail
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'avatar', 'password', 'rule_id'
    ];

    public function rule()
    {
        return $this->belongsTo('\App\Rule');
    }

    public function student()
    {
        return $this->hasOne('App\Student');
    }

    public function teacher()
    {
        return $this->hasOne('App\Teacher', 'user_id');
    }

    public function library()
    {
        return $this->hasOne('App\Library','user_id');
    }

    public function university()
    {
        return $this->hasOne('App\University', 'user_id');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function activations()
    {
        return $this->hasMany('App\Activation');
    }

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function articles()
    {
        return $this->belongsToMany('App\Article', 'student_articles');
    }

    public function studentarticles()
    {
        return $this->hasMany('App\StudentArticle');
    }

  

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucfirst($value);
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    // Fin Attributes
}
