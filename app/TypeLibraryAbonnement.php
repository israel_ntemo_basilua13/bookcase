<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeLibraryAbonnement extends Model
{
    protected $guarded=[];

    public function library_abonnement_prices(){
        return $this->hasMany('App\LibraryAbonnementPrice');
    }
}
