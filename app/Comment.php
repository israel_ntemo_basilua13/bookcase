<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['note', 'news_id', 'user_id'];
    

    /* Relations */
    public function news()
    {
        return $this->belongsTo('App\News');
    }

    public function user()
    {
        return  $this->belongsTo('App\User');
    }
}
