<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['status'];

    public function periods()
    {
        return $this->hasMany('App\Period');
    }

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function agreements()
    {
        return $this->hasMany('App\Agreement');
    }
}
