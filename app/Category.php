<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   protected $fillable = ['category', 'describe'];


   //Attributes
   public function setCategoryAttribute($value)
   {
      return ucfirst($value);
   }


   public function getCategoryAttribute($value)
   {
      return ucfirst($value);
   }

   // Fin Attributes

   // Scopes

   public function articles()
   {
      return $this->belongsToMany('App\Article', 'article_categories');
   }
}
