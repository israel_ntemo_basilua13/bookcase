<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversityReference extends Model
{
    protected $fillable = ['university_id', 'reference_id'];
}
