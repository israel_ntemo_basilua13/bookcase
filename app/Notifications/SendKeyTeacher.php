<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendKeyTeacher extends Notification
{
    use Queueable;

    protected $datas;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
      $this->datas = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->datas['type_request']=="created"){

            return (new MailMessage)
                        ->subject('Clé de création d\'un compte Instructeur Bwanya')
                        ->line('Cher/Chère '.$this->datas['name_attach'].',')
                        ->line('La réception de ce mail sous-entend une préinscription via la référence '.$this->datas['official_number'].  ' au BookCase par '.$this->datas['university_name'].' .')
                        ->line('Voici la clé d’accès vous permettant de créer un compte Instructeur et de rejoindre la famille Bwanya')
                        ->line('*Clé de création :')
                        ->line($this->datas['code_bookcase'])
                        ->line('Créer maintenant votre compte, pour bénéficier des services que BookCase met a disposition, juste pour vous.')
                        ->action('Créer maintenant', url('/inquiry/'.$this->datas['official_number']))
                        ->line('Thank you for using our application!');
        }
        else{
            return (new MailMessage)
                        ->subject('Ajout d\'une institution sur votre compte Bwanya')
                        ->line('Cher/Chère '.$this->datas['name_attach'].',')
                        ->line('Votre compte Bwanya via la référence '.$this->datas['official_number'].  ' ,vient d\'etre attaché a l\'Institution '.$this->datas['university_name'].' .')
                        ->line('Vous disposez a présent, le droit de posté vos articles et autres dans les collections de cette Institution.')
                        ->action('Explorer le', url('/login'))
                        ->line('Thank you for using our application!');

        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
