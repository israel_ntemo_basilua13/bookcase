<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ConfirmNotify extends Notification
{
    use Queueable;

    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->user = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // La réception de ce courriel implique une inscription réussie au Bwanya
        if ($this->user->rule_id == 1) {
            return (new MailMessage)
                ->subject('Création de compte Bwanya,' . $this->user->name . ' !!!')
                ->line('Vous venez de rejoindre la grande famille Bwanya, grâce au compte ' . $this->user->rule->rule . '.')
                ->line('Profitez des fonctionnalités tout en sachant, que vous disposez de trois ans  d\'utilisation gratuite et, payante après cette période de validité.')
                ->line('N\'est ce pas génial !!!')
                ->action('Explorer Bwanya', url('/'))
                ->line('La réception de ce courriel implique une inscription réussie au Bwanya')
                ->line('Merci d\'utiliser notre Plateforme, pour vous rendre service');
        } else {
            return (new MailMessage)
                ->subject('Création de compte Bwanya,' . $this->user->name . ' !!!')
                ->line('Vous venez de rejoindre la grande famille Bwanya, grâce au compte' . $this->user->rule->rule . '.')
                ->line('Profitez des fonctionnalités tout en sachant, que vous disposez de deux semaines  d\'utilisation gratuite et, payante après cette période de validité.')
                ->line('N\'est ce pas génial !!!')
                ->action('Explorer Bwanya', url('/'))
                ->line('La réception de ce courriel implique une inscription réussie au Bwanya')
                ->line('Merci d\'utiliser notre Plateforme, pour vous rendre service');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
