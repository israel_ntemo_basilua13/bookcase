<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = ['like_at', 'like_to', 'join_to', 'user_id', 'news_id'];

    protected $dates = ['like_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function news()
    {
        return $this->belongsTo('App\News');
    }
}
