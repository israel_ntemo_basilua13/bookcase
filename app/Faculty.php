<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $fillable = ['title', 'describe'];

    public function departments()
    {
        return $this->hasMany('App\Department');
    }

    public function universities()
    {
        return $this->belongsToMany('App\University', 'university_faculties');
    }
}
