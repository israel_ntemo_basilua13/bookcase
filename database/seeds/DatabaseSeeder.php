<?php

use App\User;
use App\University;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(RulesTableSeeder::class);
        // $this->call(UsersTableSeeder::class);

        ///factory(Rule::class,3)->create();

        DB::table('rules')->insert(
            [
                ['rule' => "student"],
                ['rule' => "manager"],
                ['rule' => "administrator"],
                ['rule' => "teacher"],
                ['rule' => "Library"]
            ]
        );

        DB::table('type_students')->insert(
            [
                ['type_student' => "Etudiant"],
                ['type_student' => "Non Etudiant"]
            ]
        );

        DB::table('statuses')->insert(
            [
                ['status' => 1],
                ['status' => 0]
            ]
        );

        DB::table('titles')->insert(
            [
                ['lampoon' => "University"],
                ['lampoon' => "Professeur Ordinaire"],
                ['lampoon' => "Professeur Associé"],
                ['lampoon' => "Chef de Travaux"],
                ['lampoon' => "Assistant"]
            ]
        );

        DB::table('categories')->insert(
            [
                ['category' => "Livre"],
                ['category' => "Thèse"],
                ['category' => "Syllabus"],
                ['category' => "Memoire"],
                ['category' => "TFC"],
                ['category' => "TP"]
            ]
        );

        
        factory(User::class, 1)->create();
        factory(University::class, 1)->create();

        DB::table('Faculties')->insert(
            [
                    'title' => "Public",
                    'describe'=> "Faculté du BookCase"
            ]);

        DB::table('university_faculties')->insert(
            [
                    'university_id' => 1,
                    'faculty_id'=> 1
            ]);

        DB::table('departments')->insert(
            [
                    'title_department' => "Public",
                    'faculty_id'=> 1
            ]);

                
        DB::table('documents')->insert(
            [
                // dossier vu par tout le monde
                [
                    'document_name' => "Public",
                    'university_id'=>1,
                    'department_id'=>1
                ],
                // dossier private utilisé par chaque teacher
                [
                    'document_name' => "Draft",
                    'university_id'=>1,
                    'department_id'=>1
                ]
            ]
        );
        DB::table('exchangs')->insert(
            [
                [
                    'exchange' => 2000
                ],
                [
                    'exchange' => 2100
                ],
            ]
        );

    }
}
