<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\University;
use Faker\Generator as Faker;

$factory->define(University::class, function (Faker $faker) {
    return [
        'name_university' => 'BookCase',
        'sigle' => 'BookCase',
        'picture' => 'enfin plan.jpg',
        'longitude' => 4000,
        'latitude' => 300,
        'adress' => 'kinshasa',
        'describe' => 'La Plateforme BookCase',
        'email' => 'strongconceptcpt@gmail.com',
        'phone' => '0816103725',
        'user_id' =>1,
    ];
});
