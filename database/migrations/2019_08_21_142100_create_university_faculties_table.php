<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversityFacultiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_faculties', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedBigInteger('university_id')->index();
            $table->unsignedBigInteger('faculty_id')->index();
            
            $table->timestamps();

            $table->foreign('university_id')
            ->references('id')
            ->on('universities')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('faculty_id')
            ->references('id')
            ->on('faculties')
            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('university_faculties');
    }
}
