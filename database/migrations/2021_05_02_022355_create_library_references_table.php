<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_references', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('library_id')->index();
            $table->unsignedBigInteger('reference_id')->index();
            $table->timestamps();

            $table->foreign('library_id')
                ->references('id')
                ->on('libraries')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('reference_id')
                ->references('id')
                ->on('references')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_references');
    }
}
