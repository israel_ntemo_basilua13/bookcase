<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('article_name');
            $table->text('slug');
            $table->date('publishing_at');
            $table->text('author');
            $table->text('domain_exploitation');
            $table->string('file')->nullable();
            $table->string('video')->nullable();
            $table->string('edition');
            $table->string('price');
            $table->string('devise')->default("CDF");
            // Pour savoir si c'st un professeur (TCH) ou une université(UNIV) ou library (LBRY)
            $table->string('membership')->default("TCH");
            $table->unsignedBigInteger('teacher_id')->nullable()->index();
            $table->unsignedBigInteger('library_id')->nullable()->index();
            $table->unsignedBigInteger('status_id')->index();
            $table->unsignedBigInteger('exchang_id')->nullable()->index();

            $table->timestamps();

            $table->foreign('teacher_id')
                ->references('id')
                ->on('teachers')
                ->onUpdate('cascade');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onUpdate('cascade');

            $table->foreign('library_id')
                ->references('id')
                ->on('libraries')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('exchang_id')
                ->references('id')
                ->on('exchangs')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
