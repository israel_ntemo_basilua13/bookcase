<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('support_code')->nullable();
            // gain établi pour chaque support
            $table->integer('fee');
            $table->string('fee_devise');
            // part teacher pour  chaque achat
            $table->string('price_teacher');
            $table->string('price_devise_teacher');
            $table->unsignedBigInteger('article_id')->index();
            $table->unsignedBigInteger('cart_id')->index();
            $table->timestamps();

            $table->foreign('article_id')
                ->references('id')
                ->on('articles')
                ->onUpdate('cascade');

            $table->foreign('cart_id')
                ->references('id')
                ->on('carts')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_articles');
    }
}
