<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryAbonnementPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_abonnement_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_abonnement');
            $table->integer('price');
            $table->string('devise');
            $table->integer('number_mouth');
            $table->boolean('promotion')->default(0);
            $table->date('timing_promotion')->nullable();
            $table->boolean('type_abonn_validation')->default(1);
            $table->unsignedBigInteger('library_id')->index();
            $table->unsignedBigInteger('type_library_abonnement_id')->index();
            $table->timestamps();

            $table->foreign('library_id')
            ->references('id')
            ->on('libraries')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('type_library_abonnement_id')
            ->references('id')
            ->on('type_library_abonnements')
            ->onUpdate('cascade')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_abonnement_prices');
    }
}
