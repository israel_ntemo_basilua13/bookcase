<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryAbonnementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_abonnements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('timing_abonnement');
            $table->date('start_date');
            $table->date('end_date');
            // gain plateforme pour  chaque reabonnement
            $table->integer('fee');
            $table->string('fee_devise');
            // part library pour  chaque reabonnement
            $table->string('price_lbry');
            $table->string('price_devise_lbry');
            $table->unsignedBigInteger('library_abonnement_price_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('payment_id')->index();
            $table->timestamps();


            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('library_abonnement_price_id')
            ->references('id')
            ->on('library_abonnement_prices')
            ->onUpdate('cascade')
            ->onDelete('restrict');

            $table->foreign('payment_id')
            ->references('id')
            ->on('payments')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_abonnements');
    }
}
