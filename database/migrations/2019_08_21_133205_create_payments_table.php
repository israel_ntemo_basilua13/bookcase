<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number_transaction');
            // Library ou article
            $table->string('to_concern')->default('ARTICLE');;
            $table->integer('price');
            $table->string('exchange');
            $table->string('phone');
            $table->string('provider');
            $table->integer('fee');
            $table->string('fee_devise');
            $table->string('status')->default('EN COURS');
            $table->unsignedBigInteger('user_id')->index();

            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
