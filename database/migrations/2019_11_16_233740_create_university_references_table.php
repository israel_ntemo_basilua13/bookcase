<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversityReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_references', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('university_id')->index();
            $table->unsignedBigInteger('reference_id')->index();

            $table->timestamps();

            $table->foreign('university_id')
                ->references('id')
                ->on('universities')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('reference_id')
                ->references('id')
                ->on('references')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('university_references');
    }
}
