<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('student_name')->unique();
            $table->string('sex');
            $table->string('phone');
            $table->string('domain_exploitation');
            $table->unsignedBigInteger('type_student_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            
            $table->timestamps();

            $table->foreign('type_student_id')
            ->references('id')
            ->on('type_students')
            ->onUpdate('cascade');

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
