<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_university')->unique();
            $table->string('sigle')->unique();
            $table->string('picture')->nullable();
            $table->string('logo_picture')->nullable();
            $table->bigInteger('longitude')->unique();
            $table->bigInteger('latitude')->unique();
            $table->string('adress');
            $table->text('describe')->nullable();
            $table->string('email')->nullable();
            $table->Integer('phone');
            $table->unsignedBigInteger('user_id')->index();

            $table->timestamps();

             $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universities');
    }
}
