<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('start_at');

            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('university_id')->index();
            $table->unsignedBigInteger('document_id')->nullable();

            $table->timestamps();

            $table->foreign('university_id')
            ->references('id')
            ->on('universities')
            ->onUpdate('cascade');


            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activations');
    }
}
