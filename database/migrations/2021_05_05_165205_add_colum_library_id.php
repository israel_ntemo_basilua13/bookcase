<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumLibraryId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agree_libraries', function (Blueprint $table) {
            $table->unsignedBigInteger('library_id')->after('status_id')->nullable()->index();

            $table->foreign('library_id')
                ->references('id')
                ->on('libraries')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agree_libraries', function (Blueprint $table) {
            $table->dropColumn('library_id');
        });
    }
}
