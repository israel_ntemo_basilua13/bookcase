<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('picture')->nullable();
            $table->string('title_news');
            $table->text('describe');
            $table->string('localization')->nullable();
            $table->text('exploitation_domain')->nullable();
            $table->unsignedBigInteger('university_id')->nullable()->index();
            $table->unsignedBigInteger('library_id')->nullable()->index();
            $table->unsignedBigInteger('status_id')->index();

            $table->timestamps();

            $table->foreign('university_id')
                ->references('id')
                ->on('universities')
                ->onUpdate('cascade');

            $table->foreign('library_id')
                ->references('id')
                ->on('libraries')
                ->onUpdate('cascade');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
