<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreeLibrariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agree_libraries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Library_name')->unique();
            $table->string('email_agreement')->unique();
            $table->string('file_request');
            $table->string('code_activation')->nullable();
            $table->unsignedBigInteger('status_id')->index();
            $table->timestamps();

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agree_libraries');
    }
}
