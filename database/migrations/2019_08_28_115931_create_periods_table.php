<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('start_at');
            $table->date('end_at');
            $table->unsignedBigInteger('status_id')->index();
            $table->unsignedBigInteger('activation_id')->index();

            $table->timestamps();

            $table->foreign('status_id')
            ->references('id')
            ->on('statuses')
            ->onUpdate('cascade');

            $table->foreign('activation_id')
            ->references('id')
            ->on('activations')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periods');
    }
}
