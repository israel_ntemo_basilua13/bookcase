<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_teacher');
            $table->string('sex');
            $table->Integer('phone');
            $table->text('domain_exploitation');
            $table->string('visibilities');
            $table->unsignedBigInteger('title_id')->index();
            $table->unsignedBigInteger('user_id')->index();

            $table->timestamps();

            $table->foreign('title_id')
                ->references('id')
                ->on('titles')
                ->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
