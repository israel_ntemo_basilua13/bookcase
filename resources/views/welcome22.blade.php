<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BookCase') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet"> -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/_style.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Styles -->

</head> 

<body>
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="full-menu">
                <div class="content-menu">
                    <h1 class="mt-3">Menu</h1>
                    <ul>
                        <li class="mt-4"><a href="{{url('/')}}">Accueil</a></li>
                        <li><a href="{{url('/')}}">Galerie</a></li>
                        <li><a href="{{url('/')}}">Privilèges</a></li>
                        <li><a href="{{url('/')}}">Fonctionnalités</a></li>
                        <li><a href="{{url('/')}}">Sécurité</a></li>
                        <li><a href="{{url('/')}}">Assistance</a></li>
                    </ul>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
                <div class="container-fluid">
                    <a href="{{url('/')}}" class="logo_bookcase">
                        <img src="{{asset('storage/uploads/images/logo-bwanya.png')}}" alt="">
                    </a>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item mr-4">
                                <a class="nav-link nav-link-menu show-item topbar-link active " href="{{url('/')}}">Accueil</a>
                            </li>

                            <!-- <li class="nav-item mr-4 ">
                                <a class="nav-link nav-link-menu show-item topbar-link" href="#">Galerie</a>
                            </li>

                            <li class="nav-item mr-4">
                                <a class="nav-link nav-link-menu show-item topbar-link" href="#">Privilèges</a>
                            </li>


                            <li class="nav-item mr-4">
                                <a class="nav-link nav-link-menu show-item topbar-link" href="#">Fonctionnalités</a>
                            </li>
                            <li class="nav-item mr-4">
                                <a class="nav-link nav-link-menu show-item topbar-link" href="#">Sécurité</a>
                            </li> -->

                            <li class="nav-item mr-4">
                                <a class="nav-link nav-link-menu show-item topbar-link btn btn-primary" href="{{route('login')}}" style="color:#fff!important">Se connecter</a>
                            </li>

                        </ul>

                    </div>
                    <!-- <div class="menu-toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div> -->
                </div>
            </nav>

            <div class="content_banner">
                <img src="/storage/uploads/images/row.png" alt="" class="img-float">
                <img src="/storage/uploads/images/row.png" alt="" class="img-float1">
                <div class="circle1"></div>
                <div class="circle2"></div>
                <div class="circle3"></div>
                <div class="circle4"></div>
                <div class="banner">
                    <span class="span"></span>
                    <span class="span"></span>
                    <span class="span"></span>
                    <span class="span"></span>
                    <span class="span"></span>
                    <span class="span"></span>
                    <span class="span"></span>
                    <span class="span"></span>
                    <span class="span"></span>
                    <span class="span"></span>

                    <div class="carousel slide carousel-fade " data-ride="carousel" id="carouselExampleFade">
                        <div class="carousel-inner">

                            <div class="carousel-item active">
                                <div class="container banner-content" id="banner-content">
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="text-left">
                                                <h2>Avec Bwanya</h2>
                                                <h5 class="welcome slideLeft"> Vaguer au coeur de l'interaction <span style="color:#D74D52;font-size:70px">Universitaire</span></h5>
                                                <!-- <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore voluptates beatae quae excepturi natus eveniet cumque molestias magni pariatur ex!</p> -->
                                                <br>
                                                @if (Route::has('login'))
                                                @auth
                                                <a href="{{ route('login') }}" class="btn btn-default slideBottom">Se Connecter</a>
                                                @else
                                                <a href="{{ route('choice') }}" class="btn btn-primary slideBottom">Créer un compte</a>
                                                <a href="{{ route('login') }}" class="btn btn-default slideBottom">Se Connecter</a>
                                                @endauth
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="div-img">
                                        <div class="line"></div>
                                        <div class="line"></div>
                                        <div class="over-play"></div>
                                        <img src="/storage/uploads/images/bg1.jpg" alt="byk-rdc-photo-cover">
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container banner-content" id="banner-content">
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="text-left">
                                                <h2>Avec Bwanya</h2>
                                                <h5 class="welcome slideLeft"> Vivez autrement la vie <br> <span style="color:#D74D52;font-size:70px">Universitaire</span></h5>
                                                <!-- <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore voluptates beatae quae excepturi natus eveniet cumque molestias magni pariatur ex!</p> -->
                                                <br>
                                                @if (Route::has('login'))
                                                @auth
                                                <a href="{{ route('login') }}" class="btn btn-default slideBottom">Se Connecter</a>
                                                @else
                                                <a href="{{ route('choice') }}" class="btn btn-primary slideBottom">Créer un compte</a>
                                                <a href="{{ route('login') }}" class="btn btn-default slideBottom">Se Connecter</a>
                                                @endauth
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="div-img">
                                        <div class="line"></div>
                                        <div class="line"></div>
                                        <div class="over-play"></div>
                                        <img src="/storage/uploads/images/bg2.jpg" alt="byk-rdc-photo-cover">
                                    </div>
                                </div>
                            </div>

                            <div class="carousel-item">
                                <div class="container banner-content" id="banner-content">
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="text-left">
                                                <h2>Avec Bwanya</h2>
                                                <h5 class="welcome slideLeft"> Votre Université entre <br> vos mains et plus près <br> de vous</h5>
                                                <!-- <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore voluptates beatae quae excepturi natus eveniet cumque molestias magni pariatur ex!</p> -->
                                                <br>
                                                @if (Route::has('login'))
                                                @auth
                                                <a href="{{ route('login') }}" class="btn btn-default slideBottom">Se Connecter</a>
                                                @else
                                                <a href="{{ route('choice') }}" class="btn btn-primary slideBottom">Créer un compte</a>
                                                <a href="{{ route('login') }}" class="btn btn-default slideBottom">Se Connecter</a>
                                                @endauth
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="div-img">
                                        <div class="line"></div>
                                        <div class="line"></div>
                                        <div class="over-play"></div>
                                        <img src="/storage/uploads/images/bg3.jpg" alt="byk-rdc-photo-cover">
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                        <i class="fa fa-arrow-left"></i>
                        <span class="sr-only"></span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                        <i class="fa fa-arrow-right"></i>
                        <span class="sr-only"></span>
                    </a>
                </div>
            </div>
            <div class="content-card">
                <span class="span"></span>
                <span  class="span"></span>
                <div class="sm-avatar sm-avatar1">
                    <img src="/storage/uploads/images/pic3.jpg" alt="">
                </div>
                <div class="sm-avatar sm-avatar2">
                    <img src="/storage/uploads/images/pic3.jpg" alt="">
                </div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="text-center mb-5">
                                <h1>Pourquoi choisir Bwanya ?</h1>
                                <!-- <p>Par ce qu'un choix judicieux est une clé pour une excellente réussite.</p> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-6 col-md-6">
                            <a href="{{ route('article_show',['type'=>'all']) }}" class="card card1">
                                <div class="card-body">
                                    <div class="text-left">
                                        <span><i class="fa fa-edit"></i></span>
                                        <h5>Cours</h5>
                                        <p>Vos leçons et vos supports d'enseignement en format numérique.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <a href="{{ route('article_show',['type'=>'all']) }}" class="col-lg-4 col-sm-6 col-md-6">
                            <div class="card card2">
                                <div class="card-body">
                                    <div class="text-left">
                                        <span><i class="fa fa-graduation-cap"></i></span>
                                        <h5>Thèse, Mémoire,TFC</h5>
                                        <p>Tous les oeuvres scientifiques trouvent leur vitrîne.</p>
                                    </div>
                                </div>
                            </div>
                        </diav>
                        <a href="{{ route('studentLibrary') }}" class="col-lg-4 col-sm-6 col-md-6">
                            <div class="card card3">
                                <div class="card-body">
                                    <div class="text-left">
                                        <span><i class="fa fa-book"></i></span>
                                        <h5>Biblothèque</h5>
                                        <p>Trouvez un bon coin de lecture et d'apprentissage</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <!-- <div class="col-lg-4">
                            <div class="card card4">
                                <div class="card-body">
                                    <div class="text-left">
                                        <span><i class="fa fa-graduation-cap"></i></span>
                                        <h5>Biblothèque</h5>
                                        <p>ravaux de grande envergure, trouve sa vitrine numérique et complète à travers elle l'oeuvre scientifique.</p>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="about">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="card">
                                <div class="circle1"></div>
                                <div class="circle2"></div>
                                <div class="circle3"></div>
                                <div class="line"></div>
                                <div class="sm-img sm-img1">
                                    <img src="/storage/uploads/images/bg-1.jpg" alt="">
                                </div>
                                <div class="sm-img sm-img2">
                                    <img src="/storage/uploads/images/bg-2.jpg" alt="">
                                </div>
                                <div class="sm-img sm-img3">
                                    <img src="/storage/uploads/images/bg-2.jpg" alt="">
                                </div>
                                <div class="sm-img sm-img4">
                                    <img src="/storage/uploads/images/bg-3.jpg" alt="">
                                </div>
                                <img src="/storage/uploads/images/img-fond.png" alt="" class="img-fluid" style="filter:grayscale(1);">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="text-left mt-4">
                                <h1>Que fait précisement Bwanya ?</h1>
                                <p class="lg-para">Elle vous téléporte grâce à un simple clic dans un univers scientifique de votre choix sans que vous ayez à éffectuer un pas.</p>
                            </div>
                            <div class="text-left text-icon">
                                <span class="over-play"></span>
                                <span class="icon-sm"><i class="fa fa-bank"></i></span>
                                <h5>Actualités universitaires</h5>
                                <p>Publiées et reçues en temps réel de partout où vous êtes.</p>
                            </div>
                            <div class="text-left text-icon">
                                <span class="over-play over-play1"></span>
                                <span class="icon-sm icon-sm1"><i class="fa fa-search"></i></span>
                                <h5>Recherche scientifique</h5>
                                <p>Un champs vaste avec une variété d'ouvrages à votre disposition.</p>
                            </div>
                            <div class="text-left text-icon">
                                <span class="over-play over-play2"></span>
                                <span class="icon-sm icon-sm2"><i class="fa fa-user"></i></span>
                                <h5>Gestion de compte</h5>
                                <p>Vous avez le bâton de commande entre la vente ou l'achat des ouvrages scientifiques, la participation aux activités académiques(Forums, dêbat...).</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="counterUp">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6 col-md-6">
                            <div class="card">
                                <div class="line"></div>
                                <div class="text-center text-white">
                                    <i class="fa fa-graduation-cap"></i>
                                    <br>
                                    <h1>200000</h1>
                                    <h6>Etudiants</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-md-6">
                            <div class="card">
                                <div class="line"></div>
                                <div class="text-center text-white">
                                    <i class="fa fa-file" style="opacity:1; color: #fff;"></i>
                                    <br>
                                    <h1>200000</h1>
                                    <h6>Document</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-md-6">
                            <div class="card">
                                <div class="line"></div>
                                <div class="text-center text-white">
                                    <i class="fa fa-user"></i>
                                    <br>
                                    <h1>200000</h1>
                                    <h6>Enseignants</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-md-6">
                            <div class="card">
                                <div class="line"></div>
                                <div class="text-center text-white">
                                    <i class="fa fa-bank"></i>
                                    <br>
                                    <h1>200000</h1>
                                    <h6>Institutions</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block_">
                <span></span>
                <span></span>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <div class="text-left">
                                <h1>Bwanya, un outil conçu pour les amoureux de la science</h1>
                                <a href="#" class="btn btn-primary">Manual d'utilisation</a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <div class="card">
                                <div class="play-video">
                                    <i class="fa fa-play"></i>
                                </div>
                                <div class="over-play"></div>
                                <img src="/storage/uploads/images/bg4.jpg" alt="byk-rdc-photo-cover">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block_1">
                <img src="/storage/uploads/images/bg-4.jpg" alt="" class="img-cover">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="text-center text-white">
                                <h1>Réjoignez la grande famille Bwanya !</h1>
                                <p>Nous sommes des étudiants et des chercheurs venant de plusieurs cieux, animés du désir d'apprendre et diversifier l'apport scientifique. Réjoignez-nous, échageons et partageons les connaisssances.</p>
                                <a href="{{ route('choice') }}" class="btn btn-primary">Rejoindre maintenant</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="article">
                <div class="temoignage">

                    <div class="text-center">
                        <h1>Témoignages</h1>
                    </div>

                    <div class="carousel slide" data-ride="carousel" id="carouselExampleControls">

                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleControls" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleControls" data-slide-to="1"></li>
                            <li data-target="#carouselExampleControls" data-slide-to="2"></li>
                        </ol>

                        <div class="carousel-inner" style="overflow:visible">
                            <div class="carousel-item active">
                                <div class="container">

                                    <div class="row justify-content-center">
                                        <div class="col-md-6 col-lg-8">
                                            <div class="card wow">
                                                <span class="quote"><i class="fa fa-quote-right"></i></span>
                                                <div class="d-flex">
                                                    <div class="avatar-tem">
                                                        <img src="/storage/uploads/images/bg-2.jpg" alt="" class="img-cover">
                                                    </div>
                                                    <div class="text-left mt-4 center ml-3">
                                                        <h6 class="name_tem mb-0">Herve Geliba</h6>
                                                        <small style="color: #D74D52">Etudiant</small>
                                                        <div class="star">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="container">
                                                        <div class="text-left center ">
                                                            <p class="paragraph-tem "><sup><i class="fa fa-angle-double-left"></i></sup>
                                                             Bwanya vient nous rapprocher de nos universités et nos professeurs. Nous prenons connaissance en temps réel des événements académiques qui jadis nous étaient non connus. C'est vraiment une belle histoire d'interaction entre étudiants, professeurs et universités.<sup><i class="fa fa-angle-double-right "></i> </sup></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container">

                                    <div class="row justify-content-center">
                                        <div class="col-md-6 col-lg-8">
                                            <div class="card wow">
                                                <span class="quote"><i class="fa fa-quote-right"></i></span>
                                                <div class="d-flex">
                                                    <div class="avatar-tem">
                                                        <img src="/storage/uploads/images/bg-2.jpg" alt="" class="img-cover">
                                                    </div>
                                                    <div class="text-left mt-4 center ml-3">
                                                        <h6 class="name_tem mb-0">Herve Geliba</h6>
                                                        <small style="color: #D74D52">Professeur</small>
                                                        <div class="star">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="container">
                                                        <div class="text-left center ">
                                                            <p class="paragraph-tem "><sup><i class="fa fa-angle-double-left"></i></sup>
                                                            bwanya est une plate forme phare dans mon enseignement. Grâce à elle la gestion de mes étudiants devient plus rapide, simple et sûre.<sup><i class="fa fa-angle-double-right "></i> </sup></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container">

                                <div class="row justify-content-center">
                                    <div class="col-md-6 col-lg-8">
                                        <div class="card wow">
                                            <span class="quote"><i class="fa fa-quote-right"></i></span>
                                            <div class="d-flex">
                                                <div class="avatar-tem">
                                                    <img src="/storage/uploads/images/bg-2.jpg" alt="" class="img-cover">
                                                </div>
                                                <div class="text-left mt-4 center ml-3">
                                                    <h6 class="name_tem mb-0">Herve Geliba</h6>
                                                    <small style="color: #D74D52">Université</small>
                                                    <div class="star">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="container">
                                                    <div class="text-left center ">
                                                        <p class="paragraph-tem "><sup><i class="fa fa-angle-double-left"></i></sup>
                                                        Boost l'impact de nos actualités et nous offre une meilleure connaissance de nos personnels; un moyen pour nous de valoriser leurs oeuvres, elle permet aussi d'archiver l'ensemble des travaux (T.F.C, Mémoire et Thèse) effectués dans nos facultés.<sup><i class="fa fa-angle-double-right "></i> </sup></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" id="topBtn"><i class="fa fa-angle-up"></i></button>
            </div>
        </div>
        <footer class="page-footer" style="background:#15222e;">

        <div class="container text-center text-md-left" style="padding:2%;">
            <div class="row mb-md-0 mb-3 justify-content-center">
                <div class="col-md-6">
                    <div class="text-center">
                    <h5 class=" text-upper text-white center" style="font-size:30px">Bwanya</h5>
                    <hr style="background:#fff;opacity:.3">
                    <div class="d-flex justify-content-between">
                        <div class="contact">
                            <h4 class="text-upper text-white center">Téléphone</h4>
                            <h5>
                                <span class="sm-icon">
                                    <i class="fa fa-phone"></i>
                                </span>
                                 <span class="phone">+243 000 000 000</span>
                            </h5>
                        </div>
                        <div class="contact">
                            <h4 class="text-upper text-white center">Email</h4>
                            <h5>
                                <span class="sm-icon">
                                    <i class="fa fa-envelope"></i>
                                </span> 
                               <span class="phone">support@bwanya.com</span>
                            </h5>
                        </div>
                    </div>
                    <div class="social-icons mt-4">
                        <a href="https://www.facebook.com/" class="social"><i id="social-fb" class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/" class="social"><i id="social-tw" class="fa fa-twitter"></i></a>
                        <a href="https://plus.google.com/" class="social"><i id="social-gp" class="fa fa-instagram"></i></a>
                        <a href="mailto:bootsnipp@gmail.com" class="social"><i id="social-em" class="fa fa-envelope"></i></a>
                    </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Copyright -->
        <div class="footer-copyright text-center aligns-items-center py-3 text-white">
            <small>Copyright All Right Reserved © 2021</small>
            <a href="{{ route('storeNews') }}">Bwanya</a>
        </div>
        <!-- Copyright -->

    </footer>
    </div>
    
    <!-- Footer -->

    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/wow.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {


            $(window).scroll(function() {

                if ($(this).scrollTop() > 40) {
                    $("#topBtn").addClass("active");
                } else {
                    $("#topBtn").removeClass("active");
                }
            });

            $("#topBtn").click(function() {
                $('html,body').animate({
                    scrollTop: 0
                }, 400);
            });
            $('.menu-toggle').click(function(){
                $(this).toggleClass('active')
                $('.full-menu').toggleClass('active')
            })
            // $('.carousel-control-prev').click(function(){
            //     $('.welcome').addClass('slidetransition')
            // })
        });




        // var parallax = document.getElementById('parallax');

        // window.addEventListener('scroll', function() {
        //     parallax.style.top = +window.pageYOffset + 'px';
        //     parallax.style.backgroundPositionY = - +window.pageYOffset / 2 + 'px';
        // })

        // var parallax2 = document.getElementById('parallax2');

        // window.addEventListener('scroll', function() {
        //     parallax2.style.top = +window.pageYOffset + 'px';
        //     parallax2.style.backgroundPositionY = - +window.pageYOffset / 2 + 'px';
        // })

        // var parallax3 = document.getElementById('parallax3');

        // window.addEventListener('scroll', function() {
        //     parallax3.style.top = +window.pageYOffset + 'px';
        //     parallax3.style.backgroundPositionY = - +window.pageYOffset / 2 + 'px';
        // })
        $('.carousel-fade').carousel({
            interval: 8000
        })
    </script>


</body>

</html>