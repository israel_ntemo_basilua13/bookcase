<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Virtual BookCase') }} - {{ $title, 'Acceuil'}}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

    <!-- font-awesome -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    @yield('style')
    <link href="{{ asset('css/_style.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('css/_home.css') }}" rel="stylesheet"> --}}

    {{-- <!-- Scripts -->
    <script type="module" src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Pour facebook -->
    <meta property="og:url"           content="{{Request::url()}}" />
    <meta property="og:type"          content="Bookcase-app" />
    <meta property="og:title"         content="Actualité bookcase" />
    <meta property="og:description"   content="L'université entre vos mains et plus près de vous !!!" />
    <meta property="og:image"         content="https://www.your-domain.com/path/image.jpg" />

    <style>
        /* .navbar{
            position: relative;
            padding: 1.5rem 3rem;
            padding-top: 1.5rem;
            padding-right: 3rem;
            padding-bottom: 1.5rem;
            padding-left: 3rem;
            background-color :#008080 !important;
            font-family: 'Century Gothic' !important;
            font-color:white !important;
            /*#bd6f27d6
        } */

        /* .navbar-brand,.nav-link{
            color:white!important;
        } */

        .py-4 {
            background-color: white;
            font-family: 'Century Gothic' !important;
        }



        .link_account_created {
            color: white !important;
            background-color: #e74d4e;
            padding: 10px 15px !important;
            border-radius: 50px;
        }

        a:hover {
            text-decoration: none;
        }
    </style>
</head>

<body>
    <div id="app">

        @if($title=='login')
        @else
        @include('layouts.menu_default')
        @endif


        <main>
            @yield('content')
        </main>

        <button class="btn btn-primary" id="topBtn"><i class="fa fa-angle-up"></i></button>
        @if (Route::has('login'))
            @auth
            @if(Auth::user()->rule_id==1)
                    @if ($dataCore->CartList()["cart_count"] > 0)
                    <a href="{{route('Cart.index')}}" class="panier-lg active" style="z-index: 99999 !important;">
                    @else
                    <a href="{{route('Cart.index')}}" class="panier-lg" style="z-index:999999 !important;">
                    @endif
                        <span class="num">{{$dataCore->CartList()["cart_count"]}}</span>
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
                        </svg>

                        <div class="content-cart">
                            <div class="drop-item-sm">
                                <div class="header">
                                    Panier
                                </div>
                                @foreach ($dataCore->CartList()["panier_content"] as $item)
                                <div class="article-cart d-flex">
                                    <div class="icon">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </div>
                                    <div class="text-left">
                                        <h6>{{$item->name}}</h6>
                                        @if($item->model->devise=="USD")
                                        <span>{{$item->price}} $</span>
                                        @else
                                        <span>{{($item->price) / 2000}} $</span>
                                        @endif
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </a>
                @endif
            @endauth
        @endif
    </div>

    <!-- le flash message ne s'affiche que quand jquery est present et sa presence bloque le dropdown de se deroulé -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    {{-- <script src="{{ asset('js/popper.min.js') }}"></script> --}}
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{asset('js/wow.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.btn-menu').click(function(){
            $('.btn-menu').toggleClass('active');
            $('.block-icon').toggleClass('active');
        })
            $('[data-toggle="tooltip"]').tooltip()


            $('.toggleMenu').click(function(e) {
                e.preventDefault();
                $('.sidebar').toggleClass('sidebarOpen')
                $('.global_link').toggleClass('global_link-toggle')
                $('.toggleMenu').attr('title', 'close menu')
                $('.row-translate').toggleClass('translateX')
            })

            $('.open-input-search').click(function(e) {
                e.preventDefault();
                $('.search-general').toggleClass('show-input-search');

            })
            var lastScrollTop = 0;
            $(window).scroll(function() {
                var scroll = window.pageYOffset  || document.documentElement.scrollTop;
                if(scroll  >  lastScrollTop){
                    $('.btn-menu').removeClass('show-btn')
                }
                else{
                     $('.btn-menu').addClass('show-btn')
                }
                lastScrollTop  =  scroll;

                if ($(this).scrollTop() > 40) {
                    $("#topBtn").addClass('active');
                    $(".navStudent").addClass('active');
                    $(".navApp").addClass('active');
                } else {
                    $("#topBtn").removeClass('active');
                    $(".navStudent").removeClass('active');
                    $(".navApp").removeClass('active');
                }
            });

            $("#topBtn").click(function() {
                $('html,body').animate({
                    scrollTop: 0
                }, 400)
            })

            $('.show-cardfordemande').click(function(){
                $('.cardfordemande').slideDown()
                $('.show-cardfordemande').fadeOut(100)
            })
            

            $('.fa-check-circle-o').hide()

            $('.cardfordemande input').keyup(function(){

                $('.hidden-card').fadeIn('slow')

            })

            $('.btn-login').click(function(){
                $('.spam').html('chargement')
                $('.spinner-border-sm').addClass('spinner-border')
            })
            $('.cardfordemande').submit(function(){

                $('.cardfordemande').slideUp(500)
                $('.change').slideUp(500, function(){

                $('.change').html('Demande éffectuée')
                    $('.change').slideDown('slow')
                    $('.fa-check-circle-o').slideDown('slow')
                })
                $('.hidden').slideUp(500, function(){
                    $('.hidden').html("Merci d'avoir soumis votre demande d'admission, vous recevrez le code de validité dans votre boîte Email après un délai de 3 jours de traitement...")
                    $('.hidden').slideDown('slow')
                })

                return  false;
               
            })

            $('.fa-arrow-down').click(function(){
                $('.comment').slideDown()
            })
            $('.btn-preview').click(function(){
                $('.spinner-border-sm').addClass('spinner-border')
                $('.load-text').html('Chargement')
                $('.preview-card').slideDown()
            })
            $('.hidden-preview-card').click(function(){
                $('.preview-card').slideUp(500)
            })
            $('.btn-files').click(function(){
                $('.spinner-border-sm').addClass('spinner-border')
                $('.load-text').html('Chargement')
            })
            new WOW().init()
        })
    </script>
    @include('flashy::message')
    @yield('scriptis')
</body>

</html>
