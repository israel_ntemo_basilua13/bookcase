@inject('notviewsnews','App\Utilities\NotViewNews')
@inject('dataCore','App\Utilities\CoreData')
<!-- FQN le Full Qualy Name -->

@extends('layouts.app',[ 'title' => 'app'])


@section('content')
<div class="container-fluid pb-1" style="padding-left:90px;background:#f7f7f8;">
    <div class="row justify-content-center row-translate">
        <button class="btn-float1"><i class="fa fa-plus"></i></button>
        <div class="navLeft">
            <div class="navLeft-content">
                @if (Auth::user()->rule_id==1)
                {{-- Home (News)  --}}
                <a href="{{route('home')}}">
                    @if($notviewsnews->notviewsnews()->count()>0)
                    <span class="number badge-danger text-center">
                        {{ $notviewsnews->notviewsnews()->count()}}
                    </span>
                    @endif
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                        <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
                    </svg>
                </a>
                {{-- Article --}}
                <a href="{{route('article.index') }}" class="discution">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmark-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                        <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                    </svg>
                </a>

                {{-- library --}}
                <a href="{{route('studentLibrary') }}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                    </svg>
                </a>

                {{-- <a href="#">
                            <span class="number">2</span>
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-envelope" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
                            </svg>
                            <div class="discution-content">
                                <div class="discution-header">
                                    <h5>Messages</h5>
                                </div>
                                <div class="scroll">
                                    <div class="discution-body d-flex">
                                        <div class="avatar-sm">
                                            <img src="/storage/uploads/images/p.jpg" alt="img" class="img-cover">
                                        </div>
                                        <div class="info-user">
                                            <h6>Pedrien Kinkani</h6>
                                            <span>Lorem ipsum dolor sit amet.</span>
                                            <span style="color:#d74d52">Il y a 3min</span>
                                            <span class="active"></span>
                                        </div>
                                    </div>
                                    <div class="discution-body d-flex">
                                        <div class="avatar-sm">
                                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-cover">
                                        </div>
                                        <div class="info-user">
                                            <h6>Falonne Kintatudi</h6>
                                            <span>Lorem ipsum dolor sit amet.</span>
                                            <span style="color:#d74d52">Il y a 3min</span>
                                            <span class="active"></span>
                                        </div>
                                    </div>
                                    <div class="discution-body d-flex">
                                        <div class="avatar-sm">
                                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-cover">
                                        </div>
                                        <div class="info-user">
                                            <h6>Falonne Kintatudi</h6>
                                            <span>Lorem ipsum dolor sit amet.</span>
                                            <span style="color:#d74d52">Il y a 8min</span>
                                            <span class="unactive"></span>
                                        </div>
                                    </div>
                                    <div class="discution-body d-flex">
                                        <div class="avatar-sm">
                                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-cover">
                                        </div>
                                        <div class="info-user">
                                            <h6>Falonne Kintatudi</h6>
                                            <span>Lorem ipsum dolor sit amet.</span>
                                            <span style="color:#d74d52">Il y a 8min</span>
                                            <span class="unactive"></span>
                                        </div>
                                    </div>
                                    <div class="discution-body d-flex">
                                        <div class="avatar-sm">
                                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-cover">
                                        </div>
                                        <div class="info-user">
                                            <h6>Falonne Kintatudi</h6>
                                            <span>Lorem ipsum dolor sit amet.</span>
                                            <span style="color:#d74d52">Il y a 8min</span>
                                            <span class="unactive"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a> --}}

                {{-- Corps professoral --}}
                <a href="{{ route('univ_teacher')}}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-people" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1h7.956a.274.274 0 0 0 .014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C13.688 10.629 12.718 10 11 10c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 0 0 .022.004zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10c-1.668.02-2.615.64-3.16 1.276C1.163 11.97 1 12.739 1 13h3c0-1.045.323-2.086.92-3zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z" />
                    </svg>
                </a>

                {{-- University --}}
                <a href="{{ route('universities')}}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M7.646 1.146a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5H9.5a.5.5 0 0 1-.5-.5v-4H7v4a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4h3.5V7.707L8 2.207l-5.5 5.5z" />
                        <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                    </svg>
                </a>


                <a href="{{ route('private_news') }}">
                    @if($notviewsnews->newsprivate()->count()>0)
                    <span class="number badge badge-success">
                        {{ $notviewsnews->newsprivate()->count()}}
                    </span>
                    @endif
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-envelope" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z" />
                    </svg>

                </a>

                {{-- <a href="#">
                            <span class="number">7</span>
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bell" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2z"/>
                            <path fill-rule="evenodd" d="M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z"/>
                            </svg>
                            <div class="discution-content">
                                <div class="discution-header">
                                    <h5>Notifications</h5>
                                </div>
                                <div class="scroll">
                                    <div class="discution-body d-flex">
                                        <div class="avatar-sm">
                                        <div class="avatar-sm">
                                            <i class="fa fa-bell-o"></i>
                                        </div>
                                        </div>
                                        <div class="info-user">
                                            <h6>Pedrien Kinkani</h6>
                                            <span>A publié un TP</span>
                                            <span style="color:#d74d52">Il y a 3min</span>
                                            <span class="active"></span>
                                        </div>
                                    </div>
                                    <div class="discution-body d-flex">
                                        <div class="avatar-sm">
                                            <i class="fa fa-bell-o"></i>
                                        </div>
                                        <div class="info-user">
                                            <h6>Falonne Kintatudi</h6>
                                            <span>A publié un nouveau article</span>
                                            <span style="color:#d74d52">Il y a 3min</span>
                                            <span class="active"></span>
                                        </div>
                                    </div>
                                    <div class="discution-body d-flex">
                                        <div class="avatar-sm">
                                        <div class="avatar-sm">
                                            <i class="fa fa-bell-o"></i>
                                        </div>
                                        </div>
                                        <div class="info-user">
                                            <h6>Falonne Kintatudi</h6>
                                            <span>Lorem ipsum dolor sit amet.</span>
                                            <span style="color:#d74d52">Il y a 8min</span>
                                            <span class="unactive"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a> --}}

                <a href="#">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-gear" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 0 1 4.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 0 1-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 0 1 1.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 0 1 2.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 0 1 2.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 0 1 1.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 0 1-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 0 1 8.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 0 0 1.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 0 0 .52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 0 0-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 0 0-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 0 0-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 0 0-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 0 0 .52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 0 0 1.255-.52l.094-.319z" />
                        <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 1 0 0 4.492 2.246 2.246 0 0 0 0-4.492zM4.754 8a3.246 3.246 0 1 1 6.492 0 3.246 3.246 0 0 1-6.492 0z" />
                    </svg>
                    <div class="discution-content">
                        <div class="discution-header">
                            <h5>Paramètres</h5>
                        </div>
                        <div class="scroll">
                            <div class="discution-body d-flex">
                                <div class="avatar-sm">
                                    <img src="/storage/uploads/images/p.jpg" alt="img" class="img-cover">
                                </div>
                                <div class="info-user">
                                    <h6>Pedrien Kinkani</h6>
                                    <span>Lorem ipsum dolor sit amet.</span>
                                    <span style="color:#d74d52">Il y a 3min</span>
                                    <span class="active"></span>
                                </div>
                            </div>
                            <div class="discution-body d-flex">
                                <div class="avatar-sm">
                                    <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-cover">
                                </div>
                                <div class="info-user">
                                    <h6>Falonne Kintatudi</h6>
                                    <span>Lorem ipsum dolor sit amet.</span>
                                    <span style="color:#d74d52">Il y a 3min</span>
                                    <span class="active"></span>
                                </div>
                            </div>
                            <div class="discution-body d-flex">
                                <div class="avatar-sm">
                                    <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-cover">
                                </div>
                                <div class="info-user">
                                    <h6>Falonne Kintatudi</h6>
                                    <span>Lorem ipsum dolor sit amet.</span>
                                    <span style="color:#d74d52">Il y a 8min</span>
                                    <span class="unactive"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

                {{-- <a href="#" class="avatar-user">
                            <img src="/storage/uploads/images/p.jpg" alt="img" class="img-cover">
                        </a> --}}
                @elseif(Auth::user()->rule_id==4)
                {{-- Home (News)  --}}
                <a href="{{route('dashboard')}}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                        <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
                    </svg>
                </a>

                {{-- Article --}}
                <a href="{{route('teacher.index') }}" class="discution">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                    </svg>
                </a>

                {{-- Draft (Brouillon) --}}
                <a href="{{route('draft') }}" class="discution">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmark-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                        <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                    </svg>
                </a>

                {{-- University --}}
                <a href="{{ route('universities')}}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M7.646 1.146a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5H9.5a.5.5 0 0 1-.5-.5v-4H7v4a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4h3.5V7.707L8 2.207l-5.5 5.5z" />
                        <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                    </svg>
                </a>
                @elseif(Auth::user()->rule_id==2)
                {{-- Home (News)  --}}
                <a href="{{route('wall')}}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                        <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
                    </svg>
                </a>

                {{-- New News --}}
                <a href="{{route('storeNews') }}" class="discution">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmark-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                        <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                    </svg>
                </a>

                {{-- Article Ressource --}}
                <a href="{{route('ArticleRessource') }}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                    </svg>
                </a>

                {{-- Teacher Ressource --}}
                <a href="{{ route('TeacherRessource')}}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-people" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1h7.956a.274.274 0 0 0 .014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C13.688 10.629 12.718 10 11 10c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 0 0 .022.004zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10c-1.668.02-2.615.64-3.16 1.276C1.163 11.97 1 12.739 1 13h3c0-1.045.323-2.086.92-3zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z" />
                    </svg>
                </a>

                {{-- University Ressource --}}
                <a href="{{ route('InstitutionRessource')}}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M7.646 1.146a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5H9.5a.5.5 0 0 1-.5-.5v-4H7v4a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4h3.5V7.707L8 2.207l-5.5 5.5z" />
                        <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                    </svg>
                </a>
                @elseif(Auth::user()->rule_id==5)
                {{-- Home  --}}
                <a href="{{route('dashboardLibrary')}}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                        <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
                    </svg>
                </a>
                {{-- Article --}}
                <a href="{{route('library.index') }}" class="discution">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                    </svg>
                </a>
                @else
                @endif
            </div>
        </div>

        <div class="navStudent">
            <div class="text-center text-white">
                <h4 style="margin-top:60px">Salut {{ Auth::user()->name }}
                    !</h4>
                <!-- <p style="color:#fff;opacity:.8">Vous avez 4 messages et 7 notifications</p> -->
            </div>

            <!-- @if(Auth::user()->rule_id==1)
                @if ($dataCore->CartList()["cart_count"] > 0)
                <a href="{{route('Cart.index')}}" class="panier-lg active" style="z-index: 99999 !important;">
                @else
                <a href="{{route('Cart.index')}}" class="panier-lg" style="z-index:999999 !important;">
                @endif
                    <span class="num">{{$dataCore->CartList()["cart_count"]}}</span>
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
                    </svg>

                    <div class="content-cart">
                        <div class="drop-item-sm">
                            <div class="header">
                                Panier
                            </div>
                            @foreach ($dataCore->CartList()["panier_content"] as $item)
                            <div class="article-cart d-flex">
                                <div class="icon">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                                <div class="text-left">
                                    <h6>{{$item->name}}</h6>
                                    @if($item->model->devise=="USD")
                                    <span>{{$item->price}} $</span>
                                    @else
                                    <span>{{($item->price) / 2000}} $</span>
                                    @endif
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </a>
            @endif -->

            
        </div>

        @yield('content_student')
    </div>

    <div class="sidebar" id="sidebar_id">
        <div class="global_link">
            <a href="#" title="Menu" data-toggle="tooltip" data-placement="right" class="toggleMenu">
                <span class="topbar"></span>
                <span class="middlebar"></span>
                <span class="bottombar"></span>
            </a>

            @if(Auth::user()->rule_id==1)

            <a href="{{ route('home') }}">
                <i class="fa fa-forumbee" title="Actualités" data-toggle="tooltip" data-placement="right"></i>
                <span class="title_link">actualités</span>
                <!-- fa fa-calendar -->
                @if($notviewsnews->notviewsnews()->count()>0)
                <span class="badge badge-danger text-center">
                    <small>
                        {{ $notviewsnews->notviewsnews()->count()}}
                    </small>
                </span>
                @endif
            </a>

            <br>
            <a href="{{ route('article.index') }}">
                <i class="fa fa-clone" title="Articles" data-toggle="tooltip" data-placement="right"></i>
                <span class="title_link">Articles</span>
            </a>

            <br>
            <a href="{{ route('article_recent') }}">
                <!-- <i class="fa fa-book-reader"></i> -->
                <i class="fa fa-paste" title="Articles Récents" data-toggle="tooltip" data-placement="right"></i>
                <span class="title_link">Articles recents</span>
            </a>

            <br>
            <a href="{{ route('univ_teacher')}}">
                <i class="fa fa-user" title="Enseignents" data-toggle="tooltip" data-placement="right"></i>
                <span class="title_link">Enseignents</span>
            </a>

            <!-- <a href="" title="Université" data-toggle="tooltip" data-placement="right">
                    <i class="fa fa-bank"></i>
                </a> -->

            <br>
            <a href="{{ route('universities')}}">
                <i class="fa fa-bank" title="Institutions" data-toggle="tooltip" data-placement="right"></i>
                <span class="title_link">Institutions</span>
            </a>

            <br>
            <a href="#">
                <i class="fa fa-envelope-o" title="Messages" data-toggle="tooltip" data-placement="right"></i>
            </a>

            <br>
            <a href="#">
                <i class="fa fa-bell-o" title="Notifications" data-toggle="tooltip" data-placement="right"></i>
            </a>
            <br>

            @elseif(Auth::user()->rule_id==4)

            <a href="{{ route('dashboard') }}" title="Dashboard " data-toggle="tooltip" data-placement="right">
                <i class="fa fa-dashboard"></i>
                <span class="title_link">Dashbord</span>
            </a>

            <br>
            <a href="{{ route('teacher.index') }}" title="Articles en ligne" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-files-o"></i>
                <span class="title_link">Articles</span>

            </a>

            <br>
            <a href="{{ route('draft') }}" title="Brouillons" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-edit"></i>
                <span class="title_link">Brouillons</span>
            </a>

            <br>
            <a href="" title="Revenus" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-dollar"></i>
                <span class="title_link">Revenus</span>
            </a>

            <br>
            <a href="" title="Corps professoraux" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-user"></i>
                <span class="title_link">Corps professoraux</span>
            </a>

            <br>
            <a href="{{ route('universities')}}" title="Institutions" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-bank"></i>
                <span class="title_link">Institutions</span>
            </a>
            @elseif(Auth::user()->rule_id==2)

            <a href="{{route('wall')}}" title="Dashboard" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-dashboard"></i>
                <span class="title_link">Dashbord</span>
            </a>
            <br>

            <a href="{{ route('storeNews')}}" title="Gestion d'actualité" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-users"></i>
                <span class="title_link">Gestion d'actualité</span>
            </a>
            <br>

            <a href="{{route('ArticleRessource')}}" title="Gestion d'article" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-files-o"></i>
                <span class="title_link">Gestion d'article</span>
            </a>
            <br>

            <a href="{{ route('TeacherRessource')}}" title="Gestion d'instructeurs" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-graduation-cap"></i>
                <span class="title_link">Gestion d'instructeur</span>
            </a>
            <br>

            <a href="{{route('InstitutionRessource')}}" title="Ressources Institutionnelles" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-folder-open"></i>
                <span class="title_link">Ressources Institutionnelles</span>
            </a>

            @endif
        </div>
    </div>

    <div class="sidebartop">
        <div class="text-center">

            @if(Auth::user()->rule_id==1)
            <a href="{{ route('home') }}" title="Actualité" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-forumbee"></i>
                <!-- fa fa-calendar -->
                @if($notviewsnews->notviewsnews()->count()>0)
                <span class="badge badge-danger text-center">
                    <small>
                        {{ $notviewsnews->notviewsnews()->count()}}
                    </small>
                </span>
                @endif

            </a>

            <a href="{{ route('article_recent') }}" title="Documents Récents" data-toggle="tooltip" data-placement="right">
                <!-- <i class="fa fa-book-reader"></i> -->
                <i class="fa fa-paste"></i>

            </a>

            <a href="{{ route('article.index') }}" title="Documents" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-clone"></i>
            </a>

            <a href="{{ route('Payment',['type'=>'all']) }}" title="Panier" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-cart-arrow-down"></i>
            </a>
            @elseif(Auth::user()->rule_id==4)
            <a href="" title="Documents" data-toggle="tooltip" data-placement="right">
                <i class="fa fa-clone"></i>
            </a>
            @endif

        </div>
    </div>
    <div class="block-icon">
        @if (Auth::user()->rule_id==1)
        <a href="{{route('home')}}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
            </svg>
        </a>
        <a href="{{route('article.index') }}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmark-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
            </svg>
        </a>
        <a href="{{route('studentLibrary') }}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
            </svg>
        </a>
        <a href="{{ route('univ_teacher')}}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-people" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1h7.956a.274.274 0 0 0 .014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C13.688 10.629 12.718 10 11 10c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 0 0 .022.004zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10c-1.668.02-2.615.64-3.16 1.276C1.163 11.97 1 12.739 1 13h3c0-1.045.323-2.086.92-3zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z" />
            </svg>
        </a>
        <a href="{{ route('universities')}}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M7.646 1.146a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5H9.5a.5.5 0 0 1-.5-.5v-4H7v4a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4h3.5V7.707L8 2.207l-5.5 5.5z" />
                <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
            </svg>
        </a>
        <a href="{{ route('private_news') }}" class="sm-block-icon">
            @if($notviewsnews->newsprivate()->count()>0)
            <span class="number badge badge-success">
                {{ $notviewsnews->newsprivate()->count()}}
            </span>
            @endif
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-envelope" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z" />
            </svg>
        </a>
        @elseif(Auth::user()->rule_id==2)
        {{-- Home (News)  --}}
        <a href="{{route('wall')}}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
            </svg>
        </a>

        {{-- New News --}}
        <a href="{{route('storeNews') }}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmark-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
            </svg>
        </a>

        {{-- Article Ressource --}}
        <a href="{{route('ArticleRessource') }}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
            </svg>
        </a>

        {{-- Teacher Ressource --}}
        <a href="{{ route('TeacherRessource')}}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-people" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1h7.956a.274.274 0 0 0 .014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C13.688 10.629 12.718 10 11 10c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 0 0 .022.004zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10c-1.668.02-2.615.64-3.16 1.276C1.163 11.97 1 12.739 1 13h3c0-1.045.323-2.086.92-3zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z" />
            </svg>
        </a>

        {{-- University Ressource --}}
        <a href="{{ route('InstitutionRessource')}}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M7.646 1.146a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5H9.5a.5.5 0 0 1-.5-.5v-4H7v4a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4h3.5V7.707L8 2.207l-5.5 5.5z" />
                <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
            </svg>
        </a>
        @elseif(Auth::user()->rule_id==4)
        {{-- Home (News)  --}}
        <a href="{{route('dashboard')}}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
            </svg>
        </a>

        {{-- Article --}}
        <a href="{{route('teacher.index') }}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
            </svg>
        </a>

        {{-- Draft (Brouillon) --}}
        <a href="{{route('draft') }}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmark-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
            </svg>
        </a>

        {{-- University --}}
        <a href="{{ route('universities')}}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M7.646 1.146a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5H9.5a.5.5 0 0 1-.5-.5v-4H7v4a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4h3.5V7.707L8 2.207l-5.5 5.5z" />
                <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
            </svg>
        </a>
        @elseif(Auth::user()->rule_id==5)
        {{-- Home  --}}
        <a href="{{route('dashboardLibrary')}}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
            </svg>
        </a>
        {{-- Article --}}
        <a href="{{route('library.index') }}" class="sm-block-icon">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
            </svg>
        </a>
        @else
        @endif
    </div>
    <div class="btn-menu show-btn">
        <span class="top_bar"></span>
        <span class="middle_bar"></span>
        <span class="bottom_bar"></span>
    </div>
</div>
@endsection

@section('scriptis')
<script>
    // $(function() {
    //     $(".menu_toggle").click(function() {
    //         $("#id_menu_s_student").toggleClass("menu_s_student_open");
    //         $(this).toggleClass("open-menu");
    //     });
    // });
</script>
@yield('script_article')
@endsection