@inject('notviewsnews','App\Utilities\NotViewNews')

<nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top navApp" >
    <div class="container-fluid">
       <a href="{{url('/')}}" class="logo_bookcase" >
                        <img src="{{asset('storage/uploads/images/logo-bwanya.png')}}" alt="">
        </a>
        <div class="collapse navbar-collapse nav-user" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto nav-user-content">
                @guest

                @if($title=='Login')

                <li class="nav-item  mr-4">
                    <a href="{{ route('choice') }}" class="show-item  btn btn-outline-primary mr-3">Créer un compte</a>
                </li>

                @elseif($title=='UserCreate' or $title=='Compte' or $title=='Student')

                <li class="nav-item  mr-4">
                    <a class="nav-link nav-link-menu btn btn_seconnecter show-item topbar-link" href="{{ route('login') }}">Se Connecter</a>
                </li>

                @endif

                @else

                <!-- <li class="nav-item">
                    <a href="" class="nav-link fa-icon-menu mr-5 open-input-search"><i class="fa fa-search" style="font-size:20px;"></i></a>
                </li> -->

                @if($notviewsnews->user->rule_id==1)
                <input type="text" class="form-control search-general" placeholder="Recherche dans bookcase...">
                <!-- <li class="nav-item">
                    <a href="" class="nav-link fa-icon-menu mr-5" title="Message de promotion" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-envelope-o " style="transform:rotate(45deg);font-size:20px;"></i>
                        <sup>
                            <span class="badge badge-success">
                                4
                            </span>
                        </sup></a>
                </li> -->

                <li class="nav-item">
                    <a href="{{ route('private_news') }}" class="nav-link fa-icon-menu mr-5" title="Infos privées" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-info " style="transform:rotate(45deg);font-size:20px;"></i>
                        <sup>
                            @if($notviewsnews->newsprivate()->count()>0)
                            <span class="badge badge-success">
                                {{ $notviewsnews->newsprivate()->count()}}
                            </span>
                            @endif
                        </sup>
                    </a>
                </li>
                
                <!-- <li class="nav-item">
                    <a href="{{ route('private_news') }}" title="Notifications" data-toggle="tooltip" data-placement="bottom" class="nav-link fa-icon-menu mr-5"><i class="fa fa-bell-o" style="transform:rotate(45deg);font-size:20px;"></i>
                        <sup>
                            @if($notviewsnews->notviewsnewsprivate()->count()>0)
                            <span class="badge badge-success">
                                {{ $notviewsnews->notviewsnewsprivate()->count()}}
                            </span>
                            @endif
                        </sup>
                    </a>
                </li> -->


                <!-- <input type="text" class="form-control search-general" placeholder="Recherche dans bookcase..."/> -->
                <!-- <li class="nav-item">
                    <a href="" class="nav-link fa-icon-menu mr-5" title="Boite privée" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-envelope-o" style="transform:rotate(45deg);font-size:20px;"></i>
                        <sup>
                            <span class="badge badge-success">
                                4
                            </span>
                        </sup></a>
                </li> -->
                @endif
                <li class="nav-item dropdown ml-5 mr-4">
                    <a id="navbarDropdown" class="nav-link pt-3 dropdown-toggle user_name_link  show-item topbar-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative;" v-pre>
                        <span class="avatar-sm">
                            {{ strtoupper($notviewsnews->user->name[0]) }}
                        </span>
                        @if($notviewsnews->user->rule_id==2)
                        {{ $notviewsnews->user->university->sigle}} <span class="caret"></span>
                        @else
                        {{ Str::limit($notviewsnews->user->name, 5,'.') }}
                        <span class="caret"></span>
                        @endif
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="text-align:center">
                        <a class="dropdown-item" href="{{ route('profile') }}">
                            <!-- onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" -->
                            <i class="fa fa-user"></i>
                            Mon compte
                        </a>
                        @if($notviewsnews->user->rule_id==1)

                        <a class="dropdown-item" href="{{ route('profile') }}">
                            <!-- onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" -->
                            <i class="fa fa-download"></i>
                            Téléchargement
                        </a>

                        <a class="dropdown-item" href="{{ route('profile') }}">
                            <!-- onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" -->
                            <i class="fa fa-heart"></i>
                            Favorites
                        </a>

                        @elseif($notviewsnews->user->rule_id==4)
                        @endif
                        <a class="dropdown-item" href="{{ route('profile') }}">
                            <i class="fa fa-question"></i>
                            Aide
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i>
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
                            @csrf
                        </form>
                    </div>

                </li>

                @endguest
            </ul>
        </div>

    </div>
</nav>