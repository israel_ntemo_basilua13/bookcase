@extends('layouts.student_layout')

@section('content_student')
    <div class="modal fade margin" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-left:50px;">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter des brouillons</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <form method="POST" action="{{ route('article.store')}}" enctype="multipart/form-data" style="padding:20px 30px; position relative;">
                        @csrf
                        <div class="form-group row justify-content-center">
                            <div class="col-md-12">
                                <label for="title">Edition</label>
                                <input type="number" name="edition" class="form-control">
                                <input type="hidden" name="teacher_id" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title">Fichier</label>
                            <div class="input-group mb-3">
                                <!-- <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary btn-sm" type="button" id="inputGroupFileAddon03">Télécharger</button>
                                </div> -->
                                <div class="custom-file">
                                    <input type="file" name="file_article[]" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" accept=".pdf,.txt,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple>
                                    <label class="custom-file-label" for="inputGroupFile03">Choose file</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-12 mb-2">
                                <label for="title">Catégorie</label>
                                <!-- name=category_id[]   multiple si la selection passe a plusieurs-->
                                <select class="form-control" id="category_id" name="category_id">
                                    @foreach($categories as $categor)
                                    <option value="{{$categor->id}}">{{$categor->category}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                        </div>

                        <div class="form-group row justify-content-center" style="display:none;">
                            <div class="col-md-12">
                                <input type="text" name="statut_id" id="statut_id" class="form-control" value="2">
                                <input type="text" name="collect" id="collect" class="form-control" value="{{$document->id}}">
                                <input type="number" name="price" class="form-control" value="0">
                            </div>
                        </div>

                        <!-- <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div> -->
                        <button type="submit" class="btn btn-primary btn-login btn-float">
                            {{ __('Stocké brouillon') }}
                        </button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade margin" id="modalpublishing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-left:50px;">
                    <h5 class="modal-title" id="exampleModalLabel">Publier cet article</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                </div>

                <div class="modal-body">

                    <form method="POST" id="form_publish" enctype="multipart/form-data" style="padding:20px 30px; position relative;">
                        @csrf
                        <div class="form-group row justify-content-center">
                            <div class="col-md-12">
                                <label for="title">Coût unitaire</label>
                                <input type="number_pub" name="price" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row justify-content-center" style="display:none;">
                            <div class="col-md-12">
                                <input type="text" name="collect_pub" id="collect_pub" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-12 col-lg-12">
                                <div class="text-center">
                                    <button class="btn btn-primary btn-sm text-white" type="button" data-toggle="modal" data-target="#ModalFolder">Choisir la collection pour les articles</button>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div> -->
                        <div class="row justify-content-center mt-3 text-center">
                            <small>En publiant ce brouillon, cet article devient un article publié </small>
                        </div>

                        <button type="submit" class="btn btn-primary btn-login btn-float">
                            {{ __('Publier brouillon') }}
                        </button>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade margin" id="ModalFolder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-left:50px;">
                    <h5 class="modal-title" id="exampleModalLabel">Choisir la collection</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <!-- <form method="POST" action="{{ route('article.store')}}" enctype="multipart/form-data" style="padding:20px 30px; position relative;">
                        @csrf -->

                    <div class="form-group row justify-content-center">

                        <div class="col-md-12 col-lg-12">
                            <div class="alert alert-light">
                                <!-- <div class="text-left div-caption">
                                    <h6 class="card-caption"><i class="fa fa-folder"></i> Collections attachés</h6>
                                </div> -->
                                <div class="row justify-content-between">
                                    @foreach($documents as $docum)
                                    <div class="col-6">
                                        <div class="card-header bg-white card-header-avatar text-left">
                                            <input type="checkbox" name="doc{{$docum->id}}" id="doc{{$docum->id}}" class="form-check-input" value="true">
                                            <i class="fa fa-folder" style="color:#f5d471;">
                                                <div class="card-pseudo pseudo" style="font-weight:400;  font-family: 'Century Gothic' !important;">
                                                    <span> {{ $docum->document_name}}</span>
                                                </div>
                                            </i>

                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                                <div class="row justify-content-center mt-3 text-center">
                                    <small>Les collections representent les dossiers dans lequel vont etres déposés les fichiers sélectionnés. </small>
                                    <strong>
                                        N.B : Si aucun fichier n'est selectionné, les fichiers seront placés dans le dossier public accessible a tous
                                    </strong>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                    </div>

                    <button type="button" class="btn btn-primary btn-login btn-float" id="choice_folder">
                        Appliquer
                    </button>
                    <!-- </form> -->
                </div>

            </div>
        </div>
    </div>

    <!-- <div class="text-left">
        <h1 class="title mb-4"><i class="fa fa-files-o"></i> Brouillons</h1>
        <button class="btn-primary btn-sm btn mb-4" type="button" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> Ajouter un nouveau brouillon</button>
    </div> -->

<div class="container-fluid margin">
    
    <div class="row-translate" style="margin-top: 30px;">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                
                <div class="card card-table">

                 <div class="add" data-toggle="modal"      data-target="#exampleModal">
                    </div>
                    <div class="text-left">
                        <h4>Brouillons</h4>
                    </div>

                    <table class="table table-responsive-md table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Titre</th>
                                <th>Catégorie</th>
                                <th>Edition</th>
                                <th>Date de modification</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
        
                            <span style="display:none;">{{$var = 0}}</span>
        
                            @foreach($articles as $article)
                            <tr>
                                <th scope="row">{{$var+1}}</th>
                                <td><a href="{{route('download_file',$article->id)}}" data-toggle="tooltip" title="Cliquer pour télécharger">{{$article->article_name}}</a></td>
                                <td>
                                    <strong>
                                        @if(count($article->categories)!=0)
                                        @foreach($article->categories as $ctg)
                                        @if($loop->first)
                                        <span>{{$ctg->category}}</span>
                                        @else
                                        ,<br><span>{{$ctg->category}}</span>
                                        @endif
                                        @endforeach
                                        @else
                                        <p class="text-center">-</p>
                                        @endif
                                    </strong>
                                </td>
        
                                <td>{{$article->edition}}</td>
                                <td>
                                    {{date("d M. Y",strtotime($article->publishing_at) )}}
                                </td>
        
                                <td>
                                    <div class="action d-flex">
                                        <button class="btn mr-2 btn-edit" id="id_article{{$article->id}}" data-toggle="modal" data-target="#modalpublishing">
                                            <i class="fa fa-forumbee"></i>
                                        </button>
                                        <!-- <button class="btn mr-2 btn-edit">
                                            <i class="fa fa-pencil"></i>
                                        </button> -->
                                        <!-- <a href="{{ route('article_delete',$article->id) }}" class="btn  btn-delete">
                                            <i class="fa fa-trash-o"></i>
                                        </a> -->
                                        <button  data-toggle="modal" data-target="#ModalDeleteFile{{$article->id}}" class="btn  btn-delete">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                        <!-- <span class="action-plus"><i class="fa fa-ellipsis-h"></i></span>
                                        <div class="show-option-plus"> {{ session()->flash('article_id',1534) }}
                                            <p style="cursor:pointer;" name="select_article" id="id_article{{$article->id}}" data-toggle="modal" data-target="#modalpublishing">
                                                <i class="fa fa-forumbee"></i> Publier</p>
                                            <p href=""><i class="fa fa-pencil"></i> Editer</p>
                                            <p id="file{{$article->id}}" style="cursor:pointer;" title="Supprimer" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash-o"></i> Supprimer</p>
                                            <a href="{{ route('article_delete',$article->id) }}"><i class="fa fa-trash-o"></i> Supprimer</a>
                                        </div> -->
                                    </div>
                                </td>
                            </tr>
        
                            <span style="display:none;">{{$var = $var+1}}</span>
                            @endforeach
        
                        </tbody>
                    </table>
                </div>
    
                <div class="row justify-content-center">
                    {{ $articles->links() }}
                </div>
    
            </div>
        </div>

    </div>


    
</div>
    @foreach($articles as $article)
    <div class="modal fade margin" id="ModalDeleteFile{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9999999999999999;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-left:50px;padding-bottom: 0px;">
                    <h6 class=" text-sm" id="exampleModalLabel">Opération de confirmation</h6>
                    <button type="button" class="close text-sm" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                        <div class="row justify-content-center mb-5 mt-2 text-center">
                            <div class="col-md-10">
                                <strong class="h6">
                                    Souhaitez-vous vraiment supprimer   ce fichier :  <br><strong class="text-uppercase text-danger"> {{$article->article_name}}</strong>  ?
                                </strong>
                            </div>
                        </div>
                    <a href="{{ route('article_delete',$article->id) }}" class="btn btn-primary btn-login btn-float text-uppercase" id="choice_folder">
                        Confirmer
                    </a>
                    <!-- </form> -->
                </div>

            </div>
        </div>
    </div>
    @endforeach
@endsection

@section('scriptis')
<script>
    $(document).ready(function() {
        // vers voir le nom au niveau de l'input cachant le input de type file avec bootstrap
        $('.custom-file-input').on('change', function(event) {
            var inputFile = event.currentTarget;
            $(inputFile).parent()
                .find('.custom-file-label')
                .html(inputFile.files[0].name);
        });


        @foreach($articles as $art)
        $('#file{{$art->id}}').click(function() {

            $.ajax({
                type: "GET",
                url: "{{ route('article_delete',$art->id) }}",
            });

            $('#file{{$art->id}}').parent().parent().parent().parent().hide();

        });


        $('#id_article{{$art->id}}').click(function() {
            $('#form_publish').attr("action", "{{ route('draft_publish',$art->id)}}");
        });
        @endforeach


        @foreach($documents as $domt)
        $('#doc{{$domt->id}}').click(function() {
            var ds = $('#collect_pub').val();
            // alert(ds);
            // $(".#docselect:contains('3')").addClass("btn btn - primary ");

            // if ($('#doc{{$domt->id}}').val() == "true") {

            //a verifier
            $('#collect_pub').val(ds +
                "{{$domt->id}}-");

            // } else {

            // }

        });
        @endforeach

        //a verifier
        $('#choice_folder').click(function() {

            $('#ModalFolder').modal('hide');
        });

        // $('#select_article').click(function() {
        //     alert($('#form_publish').text());
        // });


    });
</script>
@endsection