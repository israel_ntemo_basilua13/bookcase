@section('style')
<link href="{{ asset('css/_style.css') }}" rel="stylesheet">
@endsection

@extends('layouts.app',['title' => 'Teacher'])

@section('content')
<div style="background:url('/storage/uploads/images/bgdiv.jpg');
    background-size: content;" id="div_general">

    <div class="container" style="margin-top:100px;" id="container-max">
        <div class="row justify-content-center">
            <div class="col-md-4  text-center">

                <div class="alert alert-info alert-dismissible alert-show fade-right">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <div class="text-center">
                        <p class="h5  lead"> Bonjour <strong class="text_user_name">{{ $user->name }}</strong>, et bienvenue au <strong class="text-acceuil">BookCase !</strong></p>
                        <p class="h6">Aider nous à faire une plus ample connaissance de vous,
                            <br>
                            <span class="h6"> afin d'ameliorer la <span style="color:#3f4161">qualité</span> de <span style="color:#3f4161">nos services</span> et satisfaire au mieux à <span style="color:#3f4161">vos attentes</span>.</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-around">
            <div class="col-md-10 col-lg-6">

                <div class="card" style="border-radius: 10px;padding:20px 30px;margin-top:20px">

                    <!-- {{ __('Register') }} -->
                    <div class="card-body">

                        <form method="POST" action="{{ route('teacher.store')}}">
                            @csrf
                            <div class="form-group row justify-content-center mb-4">

                                <div class="col-md-12">
                                    <input id="name_teacher" type="text" class="form-control" name="name_teacher" required placeholder="Nom Complet" value="{{ old('name_teacher')}}">
                                    <input id="reference" type="hidden" class="form-control" name="reference" required value="{{ $reference->id}}">
                                    {!! $errors->first('name_teacher','<p class="error-msg">:message</p>') !!}
                                    <!-- @error('student_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror -->
                                </div>
                            </div>

                            <div class="form-group  row justify-content-center mb-2">

                                <label for="sex" class="col-md-4 text-left col-6 col-lg-5">
                                    <input type="radio" name="sex" id="sex" class="form-check-input " value="M">Homme <i class="fa fa-male"></i>
                                </label>

                                <label for="sex" class="col-md-4  form-check-label text-left col-6 col-lg-5">
                                    <input type="radio" name="sex" id="sex" class="form-check-input" value="F">Femme <i class="fa fa-female"></i>
                                </label>

                                @error('sex')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group row justify-content-center mb-4">

                                <div class="col-md-12">
                                    <input id="phone" type="tel" class="form-control" name="phone" placeholder="Phone" required autocomplete="phone">
                                </div>

                            </div>

                            <div class="form-group row justify-content-center mb-4">

                                <div class="col-md-12">
                                    <textarea name="domain_exploitation" id="domain_exploitation" class=" @error('domain_exploitation') is-invalid @enderror form-control" required placeholder="Domaine d'exploitation, Ex: art - informatique - science - batiment - sculpture - musique"></textarea>
                                    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                                    @error('domain_exploitation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row justify-content-center">
                                <div class="col-md-12 mb-2">
                                    <select class="form-control" id="title_id" name="title_id">
                                        <!-- <option value="Titre">Titre</option> -->
                                        @foreach($titles as $title)
                                        <option value="{{$title->id }}">{{$title->lampoon }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                            </div>

                            <div class="form-group row justify-content-center">

                                <label for="mail_visibility" class="col-md-4 text-left col-6 col-lg-5" style="font-size:12px;">
                                    <input type="checkbox" name="mail_visibility" id="mail_visibility" class="form-check-input" value="true">Rendre visible votre Email
                                </label>

                                <label for="phone_visibility" class="col-md-4  form-check-label text-left col-6 col-lg-5" style="font-size:12px;">
                                    <input type="checkbox" name="phone_visibility" id="phone_visibility" class="form-check-input" value="true">Rendre visible votre numéro de téléphone
                                </label>
                            </div>
                            <div class="from-group row justify-content-center">
                                <div class="col-md-7">
                                    <button type="submit" class="btn btn-primary btn-block btn-auth btn-login">
                                        Sauvegarder vos infos
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>

@endsection
