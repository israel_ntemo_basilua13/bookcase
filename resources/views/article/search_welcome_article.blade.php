@extends('layouts.app',['title' => 'Article'])

@section('content')

<div class="container" style="margin-top:100px;">
    <div class="row justify-content-around">

        <div class="col-md-9 card_file text-center">
            <form method="POST" action="{{ route('article_home')}}">
                @csrf
                <div class="col-md-12 mb-3 mt-4 col-11 col-sm-11 col-lg-12">
                    <div class="form-group col-md-12 col-lg-12 ">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon1" style="border:none;"><i class="fa fa-search"></i></button>
                            </div>
                            <input type="search" name="search_article" id="search_article" class="form-control form-control-lg" placeholder="Chercher cours, tfc, mémoire, livre etc..." aria-label="Example text with button addon" aria-describedby="button-addon1">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
    <div class="col-md-12 card_file text-center" style="margin-top:60px;">

@if($articles->count() == 0)
<div class="container">
    <div class="py-4 p_color_75">
        <div class="row justify-content-center">

            <h2>Aucun contenu n'est disponible avec votre recherche</h2>

        </div>
    </div>
</div>

@else

<h3 class="h3 text-left p_titile_article">Contenus recherchés</h3>

<div class="row">
    

        @foreach($articles as $article)


        <div class="col-md-6 col_hover col-lg-4">
            <div class="row">
                <div class="col-md-6 col-sm-5 col-5">
                    <div class="card" style="padding:30px 20px;overflow:hidden;">

                        <span class="prix bg-success">Gratuit</span>

                        <div class="text-center">
                            <i class="fa fa-file-pdf-o " style="font-size:60px;"></i>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-5 col-5" style="margin-top:50px;">
                    <div class="text-left">


                        @if (Route::has('login'))
                        @auth
                        <a href="{{route('article.show',$article->id)}}">
                            <h6 class="title_article">
                                {{ Str::limit($article->article_name, 20) }}
                            </h6>
                        </a>
                        @else

                        <a href="{{route('login')}}">
                            <h6 class="title_article">
                                {{ Str::limit($article->article_name, 20) }}
                            </h6>
                        </a>
                        @endauth
                        @endif

                        <p class="paragraph-article">Ed. {{$article->edition}}</p>
                        <p class="paragraph-article">At. {{$article->author}}</p>

                        @if($article->categories()->count() >0)
                        @foreach($article->categories as $categorys)
                        <p class="paragraph-article">{{$categorys->category}}</p>
                        @endforeach
                        @else
                        <!-- <p class="paragraph-article">Cat. Inconnu</p> -->
                        @endif

                        @if($article->documents()->count() >0)
                        @foreach($article->documents as $collects)
                        <p class="paragraph-article">{{$collects->document_name}}</p>
                        @endforeach
                        @else

                        <!-- <p class="paragraph-article">Collection. Inconnu</p> -->
                        @endif


                        @if (Route::has('login'))
                        @auth
                        <button class="btn btn-sm btn-download btn-primary btn-login fadeIn" style="font-size:8px;"><i class="fa fa-download"></i>
                            <a href="{{ route('download_file',$article->id)}}">
                                Télécharger
                            </a>
                        </button>
                        @else

                        <button class="btn btn-sm btn-download btn-primary btn-login fadeIn" style="font-size:8px;"><i class="fa fa-download"></i>
                            <a href="{{ route('login')}}">
                                Télécharger
                            </a>
                        </button>
                        @endauth
                        @endif



                    </div>

                </div>

                <div class="text-right">

                </div>
            </div>
        </div>

        @endforeach
    
</div>

@endif

<div class="row mt-5">
    <div class="col-md-12 mb-4">
        @if (Route::has('login'))
        @auth
        <a href="{{ route('article.index')}}" class="btn btn-info" style=" color:white !important;">Decouvrez notre galerie d'articles </a>
        @else
        <a href="{{ route('login')}}" class="btn btn-info" style="color:white !important;">Decouvrez notre galerie d'articles </a>
        @endauth
        @endif
    </div>
</div>
</div>
    </div>
</div>

@endsection