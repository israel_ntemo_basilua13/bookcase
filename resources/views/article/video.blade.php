@extends('layouts.student_layout')

@section('content_student')
    <div class="col-md-12 card_file margin">

        {{-- <div class="container-fluid"> --}}
        <!-- // Gratuit -->
        <div class="row mt-4">
            <div class="col-lg-3">
                <div class="card card-category">
                    <div class="card-header">
                        <form action="">
                            <div class="col-input">
                                <span class="icon" style="font-size: 15px; top: 7px;left:10px; background:#fff!important;">
                                    <i class="fa fa-search"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Faite la recherche ici...">
                            </div>
                        </form>
                    </div>
                    <div class="text-left">
                        <a href="{{ route('article_show',['type'=>'all']) }}"><i class="fa fa-file-o"></i> Tous</a>
                        <a href="#"><i class="fa fa-user"></i> Pdf</a>
                        <a href="#"><i class="fa fa-play"></i> Vidéos</a>
                        <a href="{{ route('article_show',['type'=>'free']) }}"><i class="fa fa-smile-o"></i> Gratuits</a>
                        <a href="{{ route('article_show',['type'=>'payant']) }}"><i class="fa fa-dollar"></i> Payants</a>
                        <a href="#"><i class="fa fa-user"></i> Auteurs</a>
                        <a href="{{ route('ArticleCollection')}}"><i class="fa fa-folder-o"></i> Collections</a>
                        <a href="{{ route('ArticleCollection')}}"><i class="fa fa-folder-o"></i> Mes favoris</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row">
                        <div class="col-lg-4">
                            <div class="card card-art card-video">
                                <div class="card-body">
                                    <div class="block-img-video">
                                        <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid img-video">
                                        <div class="play-video show-video" data-toggle="modal" data-target="#modal-video">
                                            <i class="fa fa-play"></i>
                                        </div>
                                    </div>
                                    <h5 class="mt-3">
                                        Wordpress for Beginners - Master Wordpress Quickly
                                    </h5>
                                    <div class="timing d-flex align-items-center">
                                        <i class="fa fa-clock-o mr-2"></i>
                                        <span>00:53:01 Heures</span>
                                    </div>
                                </div>
                                <div class="card-header d-flex align-items-center" style="border-radius: 0 0 25px 25px">
                                    <div class="avatar-auteur">
                                        <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                                    </div>
                                    <div class="content-auteur text-white">
                                        <h5>John Doe</h5>
                                        <span>Prof</span>
                                    </div>
                                    <div class="type">
                                        <p>Gratuit</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card card-art card-video">
                                <div class="card-body">
                                    <div class="block-img-video">
                                        <img src="/storage/uploads/images/bg-2.jpg" alt="img" class="img-fluid img-video">
                                        <div class="play-video show-video" data-toggle="modal" data-target="#modal-video">
                                            <i class="fa fa-play"></i>
                                        </div>
                                    </div>
                                    <h5 class="mt-3">
                                        Wordpress for Beginners - Master Wordpress Quickly
                                    </h5>
                                    <div class="timing d-flex align-items-center">
                                        <i class="fa fa-clock-o mr-2"></i>
                                        <span>00:53:01 Heures</span>
                                    </div>
                                </div>
                                <div class="card-header d-flex align-items-center" style="border-radius: 0 0 25px 25px">
                                    <div class="avatar-auteur">
                                        <img src="/storage/uploads/images/bg-2.jpg" alt="img" class="img-fluid">
                                    </div>
                                    <div class="content-auteur text-white">
                                        <h5>John Doe</h5>
                                        <span>Prof</span>
                                    </div>
                                    <div class="type">
                                        <p>Gratuit</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card card-art card-video">
                                <div class="card-body">
                                    <div class="block-img-video">
                                        <img src="/storage/uploads/images/bg-1.jpg" alt="img" class="img-fluid img-video">
                                        <div class="play-video show-video" data-toggle="modal" data-target="#modal-video">
                                            <i class="fa fa-play"></i>
                                        </div>
                                    </div>
                                    <h5 class="mt-3">
                                        Wordpress for Beginners - Master Wordpress Quickly
                                    </h5>
                                    <div class="timing d-flex align-items-center">
                                        <i class="fa fa-clock-o mr-2"></i>
                                        <span>00:53:01 Heures</span>
                                    </div>
                                </div>
                                <div class="card-header d-flex align-items-center" style="border-radius: 0 0 25px 25px">
                                    <div class="avatar-auteur">
                                        <img src="/storage/uploads/images/bg-1.jpg" alt="img" class="img-fluid">
                                    </div>
                                    <div class="content-auteur text-white">
                                        <h5>John Doe</h5>
                                        <span>Prof</span>
                                    </div>
                                    <div class="type">
                                        <p>Gratuit</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card card-art card-video">
                                <div class="card-body">
                                    <div class="block-img-video">
                                        <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid img-video">
                                        <div class="play-video show-video" data-toggle="modal" data-target="#modal-video">
                                            <i class="fa fa-play"></i>
                                        </div>
                                    </div>
                                    <h5 class="mt-3">
                                        Wordpress for Beginners - Master Wordpress Quickly
                                    </h5>
                                    <div class="timing d-flex align-items-center">
                                        <i class="fa fa-clock-o mr-2"></i>
                                        <span>00:53:01 Heures</span>
                                    </div>
                                </div>
                                <div class="card-header d-flex align-items-center" style="border-radius: 0 0 25px 25px">
                                    <div class="avatar-auteur">
                                        <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                                    </div>
                                    <div class="content-auteur text-white">
                                        <h5>John Doe</h5>
                                        <span>Prof</span>
                                    </div>
                                    <div class="type">
                                        <p>Gratuit</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card card-art card-video">
                                <div class="card-body">
                                    <div class="block-img-video">
                                        <img src="/storage/uploads/images/bg-2.jpg" alt="img" class="img-fluid img-video">
                                        <div class="play-video show-video" data-toggle="modal" data-target="#modal-video">
                                            <i class="fa fa-play"></i>
                                        </div>
                                    </div>
                                    <h5 class="mt-3">
                                        Wordpress for Beginners - Master Wordpress Quickly
                                    </h5>
                                    <div class="timing d-flex align-items-center">
                                        <i class="fa fa-clock-o mr-2"></i>
                                        <span>00:53:01 Heures</span>
                                    </div>
                                </div>
                                <div class="card-header d-flex align-items-center" style="border-radius: 0 0 25px 25px">
                                    <div class="avatar-auteur">
                                        <img src="/storage/uploads/images/bg-2.jpg" alt="img" class="img-fluid">
                                    </div>
                                    <div class="content-auteur text-white">
                                        <h5>John Doe</h5>
                                        <span>Prof</span>
                                    </div>
                                    <div class="type">
                                        <p>Gratuit</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card card-art card-video">
                                <div class="card-body">
                                    <div class="block-img-video">
                                        <img src="/storage/uploads/images/bg-1.jpg" alt="img" class="img-fluid img-video">
                                        <div class="play-video show-video" data-toggle="modal" data-target="#modal-video">
                                            <i class="fa fa-play"></i>
                                        </div>
                                    </div>
                                    <h5 class="mt-3">
                                        Wordpress for Beginners - Master Wordpress Quickly
                                    </h5>
                                    <div class="timing d-flex align-items-center">
                                        <i class="fa fa-clock-o mr-2"></i>
                                        <span>00:53:01 Heures</span>
                                    </div>
                                </div>
                                <div class="card-header d-flex align-items-center" style="border-radius: 0 0 25px 25px">
                                    <div class="avatar-auteur">
                                        <img src="/storage/uploads/images/bg-1.jpg" alt="img" class="img-fluid">
                                    </div>
                                    <div class="content-auteur text-white">
                                        <h5>John Doe</h5>
                                        <span>Prof</span>
                                    </div>
                                    <div class="type">
                                        <p>Gratuit</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>

        </div>

        {{-- </div> --}}
    </div>
    <div class="modal fade margin" style="top:-80px;" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header justify-content-end" style="padding-left:50px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="block-video">
                        <div class="block-show-video">
                            <iframe src="/storage/uploads/videos/video.mp4" frameborder="0" id="iframe-video"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script_article')
<script>
    $(function() {

        $("#search_article").keyup(function() {

            var search = $(this).val();

            search = $.trim(search);

            $(".title_article").removeClass("color_v");
            $(".title_article").parent().parent().parent().parent().css("display", "none");


            if (search != "") {

                $(".title_article:contains('" + search + "')").addClass("color_v");
                $(".title_article:contains('" + search + "')").show();
                $(".title_article:contains('" + search + "')").parent().parent().parent().parent().css("display", "flex");

            } else {
                $(".title_article").removeClass("color_v");
                $(".title_article").parent().parent().parent().parent().css("display", "flex");
            }


        });
    });
</script>
@endsection
