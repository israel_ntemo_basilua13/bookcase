@inject('notviewsnews','App\Utilities\NotViewNews')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')

@section('content_student')
<div class="col-md-12 card_file margin mx-auto p-5" style="margin-top: 150px !important">
    <div class="row">
        <div class="col-md-12">
            <div class="text-left">
                <p class="title text-left">Details - {{$article->article_name}}</p>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-3 card text-center card_show_file col-sm-3 col-3 ">
            <!-- <img src="/storage/uploads/images/bgbc3.jpg" alt="" class="img-fluid"> -->
            <p><i class="fa fa-file-pdf-o fa_file pt-4"></i></p>
            <!-- <span class="show_file_span_extension">PDF</span> -->
        </div>


        <div class="col-md-9 card p-4 col-sm-9 col-9" style="overflow:hidden">

            @if($article->price == 0)
            <span class="badge-free">Gratuit</span>
            @else
            <span class="badge-free" style="background:#ffa500!important; box-shadow: #ffa500 0 0 2px 0 !important;">Payant</span>
            @endif

            <div class="row">
                <div class="col-md-7 col_detail-article">
                    <div class="container">
                        <p class="h3">{{$article->article_name}}</p>
                        <p class="paragraph-article">Auteur : {{$article->author}}</p>

                        <p class="h6 p_color_78">
                            Université/Institution : <br><br>
                            @foreach($univ as $univ)
                            * {{$univ->university->sigle}} <br>
                            @endforeach
                        </p>

                        <p class="h6 p_color_78">Ed. {{$article->edition}}</p>

                        <!-- <p class="paragraph-article">Domaine d'exploitation : {{$article->domain_exploitation}}</p> -->


                        @if($article->categories->count() >0)
                        <span class="mt-2">Categorie: </span>
                        @foreach($article->categories as $categorys)
                        <p class="paragraph-article">{{$categorys->category}}</p>
                        @endforeach
                        @else
                        <!-- <p class="paragraph-article">Cat. Inconnu</p> -->
                        @endif

                        @if($article->documents->count() >0)
                        <span class="mt-2">Collections: </span>

                        @foreach($article->documents as $collects)
                        <p class="paragraph-article"> * {{$collects->document_name}}</p>
                        @endforeach
                        @else

                        <!-- <p class="paragraph-article">Collection. Inconnu</p> -->
                        @endif


                        <p class="h6 p_color_78 mt-2"> publié le {{ date("d.M.Y",strtotime($article->publishing_at)) }}</p>
                        <!-- <span class="p_color_78 "><span> 328 Vues</span>, <i class="badge badge-secondary badge-dark-file">150</i> Téléchargements</span> -->

                        <!-- <p class="h6">{{$article->price}}</p> -->

                        <div class="action_article text-center">
                            <i class="fa fa-download"><sup><span class="badge badge-success">{{ $notviewsnews->filedownloadcount($article->id)->count()}}</span></sup></i>
                            <i class="fa fa-heart-o ml-2"><sup><span class="badge badge-success">12</span></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="text-center mt-5">
                        <div class="star mb-4">
                            <i class="fa fa-star" style="color:gold;"></i>
                            <i class="fa fa-star" style="color:gold;"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <p class="pb-4"><small>Dernier mis à jour: Le {{ date("d.M.Y",strtotime($article->updated_at)) }}</small></p>
                        @if($article->price == 0)
                        <a href="{{ route('download_file',$article->id)}}" class="btn btn-info  btn_download "><i class="fa fa-download"></i> Télecharger</a>
                        @else
                        <p>
                            <span class="">Prix : {{$article->price}}$</span>
                        </p>
                        <a href="{{route('add_panier',$article->id)}}" class="btn btn_download" style="background:#ffa500!important;"> <i class="fa fa-cart-arrow-down"></i> Ajouter au panier </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection