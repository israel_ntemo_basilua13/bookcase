@extends('layouts.student_layout')

@section('content_student')

@if($recent_articles->count() == 0)

<div class="container-fluid p-4 ml-4 recent_phone" style="margin-top:5rem;">
    <div class="text-left">
        <h1 class="title mb-4 ml-md-5">Articles Récents</h1>
    </div>
    <div class="text-center">
        <i class="fa fa-files-o" style="font-size:200px;"></i>
        <P>Vous disposez d'aucun article pour le moment, Veuillez découvrir notre galerie d'articles </p>
        <a href="{{ route('article.index')}}" class="btn-primary btn-sm btn">Explorer la galerie Bookcase</a>
    </div>
</div>



@else
<div class="col-md-9 card_file margin">

    <div class="row justify-content-center">
        <div class="col-10 col-md-11 col-sm-10 col-lg-12">
            <p class="text-justify h2 p_titile_article title">Articles Récents</p>
        </div>


        <div class="container-fluid">

            <!-- // Gratuit -->
            <div class="row">

                @foreach($recent_articles as $art)


                <div class="col-md-6 col_hover col-lg-4">
                    <div class="row">
                        <div class="col-md-6 col-sm-5 col-5">
                            <div class="card" style="padding:30px 20px;overflow:hidden;">
                                @if($art->article->price == 0)
                                <span class="prix bg-success">Gratuit</span>
                                @else
                                <span class="num">{{$art->article->price}}</span>
                                <span class="prix bg-warning" style="background:#ffa500!important;">Payant</span>
                                @endif
                                <div class="text-center">
                                    <i class="fa fa-file-pdf-o " style="font-size:60px;"></i>
                                </div>
                            </div>
                        </div>



                        <div class="col-md-6 col-sm-5 col-5" style="margin-top:50px;">
                            <div class="text-left">
                                <!-- <a href="{{route('article.show',$art->article->id)}}"> -->
                                <h6 class="title_article">
                                    {{ Str::limit($art->article->article_name, 20) }}

                                </h6>
                                <!-- </a> -->
                                <p class="paragraph-article">Ed. {{$art->article->edition}}</p>
                                <p class="paragraph-article">At. {{$art->article->author}}</p>

                                @if($art->article->categories->count() >0)
                                @foreach($art->article->categories as $categorys)
                                <p class="paragraph-article">{{$categorys->category}}</p>
                                @endforeach
                                @else
                                <!-- <p class="paragraph-article">Cat. Inconnu</p> -->
                                @endif

                                @if($art->article->documents->count() >0)
                                @foreach($art->article->documents as $collect)
                                <p class="paragraph-article">{{$collect->document_name}}</p>
                                @endforeach
                                @else
                                <!-- <p class="paragraph-article">Collection. Inconnu</p> -->
                                @endif

                                <div class="action" style="background:#32e4cd;">
                                    <span class="action-plus"><i class="fa fa-trash-o"></i></span>
                                    <div class="show-option-plus">
                                        <a href=""><i class="fa fa-eye"></i>Voir le detail</a>
                                        <a href=""><i class="fa fa-download"></i> Télélchager</a>
                                    </div>
                                </div> 

                            </div>

                        </div>

                        <div class="text-right">

                        </div>
                    </div>
                </div>

                @endforeach
            </div>

        </div>

    </div>
</div>

@endif

@endsection


@section('script_article')
<script>
    $(function() {

        $("#search_article").keyup(function() {
            var search = $(this).val();

            search = $.trim(search);

            $(".title_article").removeClass("color_v");
            $(".title_article").parent().parent().parent().css("display", "none");

            if (search != "") {
                $(".title_article:contains('" + search + "')").addClass("color_v");
                $(".title_article:contains('" + search + "')").show();

                $(".title_article:contains('" + search + "')").parent().parent().parent().css("display", "flex");

            } else {
                $(".title_article").removeClass("color_v");
                $(".title_article").parent().parent().parent().css("display", "flex");
            }


        });
    });
</script>
@endsection