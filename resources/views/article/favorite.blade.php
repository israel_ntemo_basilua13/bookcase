@extends('layouts.student_layout')

@section('content_student')
    <div class="col-md-12 card_file margin" style="margin-top: 150px !important;">
        <!-- // Gratuit -->
        <div class="row mt-4">
            <div class="col-lg-3">
                <div class="card card-category">
                    <div class="card-header">
                        <form action="">
                            <input type="text" class="form-control" placeholder="Faite la recherche ici...">
                        </form>
                    </div>
                    <div class="text-left">
                        <a href="{{ route('article_show',['type'=>'all']) }}"><i class="fa fa-file-o"></i> Tous</a>
                        <a href="{{ route('article_show',['type'=>'free']) }}"><i class="fa fa-smile-o"></i> Gratuits</a>
                        <a href="{{ route('article_show',['type'=>'payant']) }}"><i class="fa fa-dollar"></i> Payants</a>
                        <a href="#"><i class="fa fa-user"></i> Auteurs</a>
                        <a href="{{ route('ArticleCollection')}}"><i class="fa fa-folder-o"></i> Collections</a>
                        <a href="{{ route('ArticleFavorites')}}"><i class="fa fa-folder-o"></i> Mes favoris</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row mt-5">
                    @foreach($articles_favorites as $article)
                        <div class="col-lg-4">
                            <div class="card card-art">
                                <div class="card-header d-flex">
                                    <div class="avatar-auteur">
                                        <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                                    </div>
                                    <div class="content-auteur text-white">
                                        <h5>{{$article->author}}</h5>
                                        @if($article->membership =="LBRY")
                                        <span>{{$article->library->name_library }}</span>
                                        @else
                                        <span>{{$article->teacher->title->lampoon}}</span>
                                        @endif
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="text-left">
                                        <h4><a class="text-secondary" href="{{route('article.show',$article->id)}}">{{ Str::limit($article->article_name, 20) }}</a></h4>
                                        @if($article->categories()->count() >0)
                                        @foreach($article->categories as $categorys)
                                        <span class="category">{{$categorys->category}}</span>
                                        @endforeach
                                        @else
                                        @endif
    
                                        <br>
                                        @if($article->documents()->count() >0)
                                        @foreach($article->documents as $collects)
                                        <span class="type">{{$collects->document_name}}</span>
                                        @endforeach
                                        @else
                                        @endif
    
                                        <br>
                                        <span class="badge badge-primary">Edition {{$article->edition}}</span>
                                        <br>
                                        @if($article->price == 0)
                                        <p class="mb-0 text-right payant" style="font-size:22px;color:#151370;">Gratuit</p>
                                        @else
                                        <p class="mb-0 text-right payant" style="font-size:22px;color:#151370;">{{$article->price}} {{$article->devise}}</p>
                                        @endif
                                        <span style="display:block;margin-top:5px"><i class="fa fa-download"></i> 10000 Téléchargements</span>
                                    </div>
                                </div>
                                <!-- <div class="card-footer">
                                    @if($article->price == 0)
                                    <a href="{{route('download_file',$article->id)}}" class="btn btn-primary btn-sm btn-download-sm"><i class="fa fa-download"></i> Télécahger</a>
                                    {{-- <a href=""><i class="fa fa-download"></i> Télélchager</a> --}}
                                    @else
                                    {{-- <a href="{{route('add_panier',$article->id)}}"><i class="fa fa-cart-arrow-down"></i>Mis au panier</a>
                                    <a href="#" class="btn btn-primary btn-sm btn-achat"><i class="fa fa-dollar"></i> Acheter</a> --}}
                                    @if($article->video !=null)
                                    <span class="btn-video">
                                        <i class="fa fa-play"></i>
                                        <span class="Tooltip-sm">Voir la video</span>
                                    </span>
                                    @endif
    
                                    <a href="{{route('add_panier',$article->id)}}" class="btn btn-cart ml-2 btn btn-primary btn-sm btn-achat">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cart text-white" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                                        </svg>
                                    </a>
                                    @endif
                                </div> -->
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script_article')
<script>
    $(function() {

        $("#search_article").keyup(function() {

            var search = $(this).val();

            search = $.trim(search);

            $(".title_article").removeClass("color_v");
            $(".title_article").parent().parent().parent().parent().css("display", "none");


            if (search != "") {

                $(".title_article:contains('" + search + "')").addClass("color_v");
                $(".title_article:contains('" + search + "')").show();
                $(".title_article:contains('" + search + "')").parent().parent().parent().parent().css("display", "flex");

            } else {
                $(".title_article").removeClass("color_v");
                $(".title_article").parent().parent().parent().parent().css("display", "flex");
            }


        });
    });
</script>
@endsection