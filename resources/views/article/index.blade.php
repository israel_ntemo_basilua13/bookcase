@extends('layouts.student_layout')

@section('content_student')
    <div class="col-md-12 card_file margin" style="margin-top: 150px !important;">
        <!-- // Gratuit -->
        <div class="row mt-4">
            <div class="col-lg-3">
                <div class="card card-category">
                    <div class="card-header">
                        <form action="">
                            <input type="text" class="form-control" placeholder="Faite la recherche ici...">
                        </form>
                    </div>
                    <div class="text-left">
                        <a href="{{ route('article_show',['type'=>'all']) }}"><i class="fa fa-file-o"></i> Tous</a>
                        <a href="{{ route('article_show',['type'=>'free']) }}"><i class="fa fa-smile-o"></i> Gratuits</a>
                        <a href="{{ route('article_show',['type'=>'payant']) }}"><i class="fa fa-dollar"></i> Payants</a>
                        <!-- <a href="#"><i class="fa fa-user"></i> Auteurs</a> -->
                        <a href="{{ route('ArticleCollection')}}"><i class="fa fa-folder-o"></i> Collections</a>
                        <a href="{{ route('ArticleFavorites')}}"><i class="fa fa-folder-o"></i> Mes favoris</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row mt-5">
                    @foreach($articles->filter(function($value){
                        return $value->library_id == null;
                    }) as $article)

                        @if($article->video !=null && $article->video !="")
                        <div class="col-lg-4">
                            <div class="card card-art card-video">
                                <div class="card-body">
                                    <div class="block-img-video">
                                        <img src="/storage/uploads/images/bg-1.jpg" alt="img" class="img-fluid img-video">
                                        <div class="play-video show-video" data-toggle="modal" data-target="#modal-video">
                                            <i  class="fa fa-play play_article"></i>
                                        </div>
                                    </div>
                                    <h5 class="mt-3">{{$article->article_name}}</h5>
                                    <!-- <div class="timing d-flex align-items-center">
                                        <i class="fa fa-clock-o mr-2"></i>
                                        <span>00:53:01 Heures</span>
                                    </div> -->
                                </div>
                                <div class="card-header d-flex align-items-center" style="border-radius: 0 0 25px 25px">
                                    <div class="avatar-auteur">
                                        <img src="/storage/uploads/images/bg-1.jpg" alt="img" class="img-fluid">
                                    </div>
                                    <div class="content-auteur text-white">
                                        <h5>{{$article->author}}</h5>
                                        <span>{{$article->teacher->title->lampoon}}</span>
                                    </div>
                                    <div class="type">
                                        <!-- <p>Gratuit</p> -->
                                          @if($article->price == 0)
                                        <p class="mb-0 text-right payant" style="font-size:22px;color:#151370;">Gratuit</p>
                                        @else
                                        <p class="mb-0 text-right payant" style="font-size:22px;color:#151370;">{{$article->price}} {{$article->devise}}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    @if($article->price == 0)
                                    <a href="{{route('download_file',$article->id)}}" class="btn btn-primary btn-sm btn-download-sm">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book text-white" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                                        </svg>
                                        
                                        Lire</a>
                                    @else
                                    <a href="{{route('add_panier',$article->id)}}" class="btn btn-cart ml-2 btn btn-primary btn-sm btn-achat">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cart text-white" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                                        </svg>
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="col-lg-4">
                            <div class="card card-art">
                                <div class="card-header d-flex">
                                    <div class="avatar-auteur">
                                        <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                                    </div>
                                    <div class="content-auteur text-white">
                                        <h5>{{$article->author}}</h5>
                                        <span>{{$article->teacher->title->lampoon}}</span>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="text-left">
                                        <h4><a class="text-secondary" href="{{route('article.show',$article->id)}}">{{ Str::limit($article->article_name, 20) }}</a></h4>
                                        @if($article->categories()->count() >0)
                                        @foreach($article->categories as $categorys)
                                        <span class="category">{{$categorys->category}}</span>
                                        @endforeach
                                        @else
                                        @endif
    
                                        <br>
                                        @if($article->documents()->count() >0)
                                        @foreach($article->documents as $collects)
                                        <span class="type text-uppercase">{{$collects->document_name}}</span>
                                        @endforeach
                                        @else
                                        @endif
                                        <br>
                                        <span class="badge badge-primary">Edition {{$article->edition}}</span>
                                        <br>
                                        @if($article->price == 0)
                                        <p class="mb-0 text-right payant" style="font-size:22px;color:#151370;">Gratuit</p>
                                        @else
                                        <p class="mb-0 text-right payant" style="font-size:22px;color:#151370;">{{$article->price}} {{$article->devise}}</p>
                                        @endif
                                        <span style="display:block;margin-top:5px"><i class="fa fa-download"></i> 10000 Téléchargements</span>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    @if($article->price == 0)
                                    <a href="{{route('download_file',$article->id)}}" class="btn btn-primary btn-sm btn-download-sm">
                                        <!-- <i class="fa fa-download  mx-1"></i>Lire</a> -->
                                        <!-- <i class="fa fa-book  mx-1"></i> -->
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book text-white" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                                        </svg>
                                        
                                        
                                        Lire</a>
                                    {{-- <a href=""><i class="fa fa-download"></i> Télélchager</a> --}}
                                    @else
                                    {{-- <a href="{{route('add_panier',$article->id)}}"><i class="fa fa-cart-arrow-down"></i>Mis au panier</a>
                                    <a href="#" class="btn btn-primary btn-sm btn-achat"><i class="fa fa-dollar"></i> Acheter</a> --}}
                                    @if($article->video !=null)
                                    <span class="btn-video">
                                        <i class="fa fa-play"></i>
                                        <span class="Tooltip-sm">Voir la video</span>
                                    </span>
                                    @endif
    
                                    <a href="{{route('add_panier',$article->id)}}" class="btn btn-cart ml-2 btn btn-primary btn-sm btn-achat">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cart text-white" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                                        </svg>
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif

                    @endforeach
                    
                    
                </div>
            </div>
        </div>
        
    </div>

    <div class="modal fade margin" style="top:-80px;" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header justify-content-end" style="padding-left:50px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="block-video">
                        <div class="block-show-video">
                            <!-- <iframe src="/storage/uploads/files/videox7.mp4" frameborder="0" id="iframe-video"></iframe> -->
                            <video src="/storage/uploads/videos/video.mp4" controls="true" class="video-article"></video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script_article')
<script>
    $(function() {

        $("#search_article").keyup(function() {

            var search = $(this).val();

            search = $.trim(search);

            $(".title_article").removeClass("color_v");
            $(".title_article").parent().parent().parent().parent().css("display", "none");


            if (search != "") {

                $(".title_article:contains('" + search + "')").addClass("color_v");
                $(".title_article:contains('" + search + "')").show();
                $(".title_article:contains('" + search + "')").parent().parent().parent().parent().css("display", "flex");

            } else {
                $(".title_article").removeClass("color_v");
                $(".title_article").parent().parent().parent().parent().css("display", "flex");
            }


        });

        var video= document.querySelector(".video-article")

        $(".play_article").click(function() {
            // $('.block-show-video iframe').attr('src','/storage/uploads/files/'+$(this).parent().parent().parent().children('h5').eq(0).text());
            $('.block-show-video video').attr('src','/storage/uploads/files/'+$(this).parent().parent().parent().children('h5').eq(0).text());
              video.play();
        });


        $("#modal-video .close").click(function(){
            video.pause();
            video.currentTime = 0;
        })

        $("#modal-video").mouseover(function(){
        })
        // var lirevideo  = setInterval(function(){
        //      if($('#modal-video').hasClass('in')){
        //     }else{
        //         video.pause();
        //         video.currentTime = 0;
        //     }
        //   },1000);

    });
</script>
@endsection