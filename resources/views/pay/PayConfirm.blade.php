@inject('notviewsnews','App\Utilities\NotViewNews')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')

@section('content_student')
<div class="container-fluid margin px-3 my-5">
    <div class="row justify-content-center align-items-center py-5" style="height: 80vh; margin: auto;">
        <div class="col-md-4 py-5">
            <div class="card card-check">
                <div class="card-header bg-white" style="position: relative;">
                    <h5 class="mb-0">Confirmer le paiement</h5>
                    <div class="avatar-user">
                        <img src="{{asset('storage/uploads/images/banner-bg2.jpg')}}" alt="">
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <form action="{{route('PayConfirm')}}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-lg-12 mt-3">
                                        <input type="hidden" value="{{ $reference }}" name="reference">
                                        <button class="btn btn-primary" name="btn-valide">Confirmer le paiement</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scriptis')
<script>
    $('#valid-num1').click(function () {
        $('#col-1').toggleClass('hidden')
    })
    $('#valid-num2').click(function () {
        $('#col-2').toggleClass('hidden')
    })
    $('#valid-num3').click(function () {
        $('#col-3').toggleClass('hidden')
    })

</script>
@endsection
