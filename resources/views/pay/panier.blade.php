@inject('notviewsnews','App\Utilities\NotViewNews')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')

@section('content_student')


<div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card card-check">
                <div class="card-header bg-white" style="position: relative;">
                    <h5 class="mb-0">Details achats</h5>
                    <div class="avatar-user">
                        <img src="{{asset('storage/uploads/images/banner-bg2.jpg')}}" alt="">
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-left">
                        <span class="nombre-art">
                            Nombre d'articles
                            <span>{{$panier_count}}</span>
                        </span>
                        <br>
                        <h6 class="total-sm mt-2 mb-4" style="font-weight: 600;">
                            Total
                            <span>{{$panier_subtotal}}Fc</span>
                        </h6>
                        <h5 class="total-lg mt-2" style="font-weight: 600;">
                            Total net
                            <span
                                class="franc">{{str_replace(' ','',$panier_subtotal) + ((str_replace(' ','',$panier_subtotal) * 20)/100) }}
                                Fc</span>
                            <span
                                class="dollar">{{(str_replace(' ','',$panier_subtotal) + ((str_replace(' ','',$panier_subtotal) * 20)/100)) / 2000 }}$</span>
                        </h5>
                    </div>
                    <hr>
                    <span class="nombre-art">
                        Choix de compte
                    </span>
                    <ul class="nav nav-tabs mb-5" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active nav-money" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                aria-controls="home" aria-selected="true">
                                <img src="{{asset('storage/uploads/images/mpesa.jpg')}}" alt="">
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link nav-money" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                aria-controls="profile" aria-selected="false">
                                <img src="{{asset('storage/uploads/images/AIRTELLOGO.png')}}" alt="">
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link nav-money" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                aria-controls="contact" aria-selected="false">
                                <img src="{{asset('storage/uploads/images/47.png')}}" alt="">
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <form action="{{route('CartPay')}}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    @if ($user->student->phone !=null)
                                    <div class="col-lg-12 d-flex">
                                        <input type="checkbox" name="num-book" id="valid-num1" class="mr-1">
                                        <label for="num-book">Utiliser votre numéro de compte
                                            Bwanya</label>
                                    </div>
                                    <div class="col-lg-12 col-input" id="col-1">
                                        <span class="icon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input type="text" class="form-control form-control-panier"
                                            placeholder="Inserer votre numéro de téléphone" name="num">
                                    </div>
                                    @endif

                                    <div class="col-lg-12 mt-3 col-input">
                                        <span class="icon">
                                            <i class="fa fa-money"></i>
                                        </span>
                                        <select type="text" class="form-control form-control-panier" name="devise">
                                            <option value="CDF">
                                                CDF
                                            </option>
                                            <option value="USD">
                                                USD
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <input type="hidden" value="mpesa" name="provider">
                                        <button class="btn btn-primary" name="btn-valide">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <form action="{{route('CartPay')}}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    @if ($user->student->phone !=null)
                                    <div class="col-lg-12  d-flex">
                                        <input type="checkbox" name="num-book" id="valid-num1" class="mr-2">
                                        <label for="num-book">Utiliser votre numéro de compte
                                            Bwanya</label>
                                    </div>
                                    <div class="col-lg-12 col-input" id="col-1">
                                        <span class="icon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input type="text" class="form-control form-control-panier"
                                            placeholder="Inserer votre numéro de téléphone" name="num">
                                    </div>
                                    @endif

                                    <div class="col-lg-12 col-input mt-3">
                                        <span class="icon">
                                            <i class="fa fa-money"></i>
                                        </span>
                                        <select type="text" class="form-control form-control-panier" name="devise">
                                            <option value="CDF">
                                                CDF
                                            </option>
                                            <option value="USD">
                                                USD
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <input type="hidden" value="airtel" name="provider">
                                        <button class="btn btn-primary" name="btn-valide">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <form action="{{route('CartPay')}}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    @if ($user->student->phone !=null)
                                    <div class="col-lg-12 d-flex">
                                        <input type="checkbox" name="num-book" id="valid-num1" class="mr-2">
                                        <label for="num-book">Utiliser votre numéro de compte
                                            Bwanya</label>
                                    </div>
                                    <div class="col-lg-12 col-input" id="col-1">
                                        <span class="icon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input type="text" class="form-control form-control-panier"
                                            placeholder="Inserer votre numéro de téléphone" name="num">
                                    </div>
                                    @endif

                                    <div class="col-lg-12 mt-3 col-input">
                                        <span class="icon">
                                            <i class="fa fa-money"></i>
                                        </span>
                                        <select type="text" class="form-control form-control-panier" name="devise">
                                            <option value="CDF">
                                                CDF
                                            </option>
                                            <option value="USD">
                                                USD
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <input type="hidden" value="orange" name="provider">
                                        <button class="btn btn-primary" name="btn-valide">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid margin px-3">
    <div class="card card-table" style="margin-top: 30px">
        <div class="text-left">
            <h1 class="title">Panier</h1>
            @if ($panier_count > 0)
            <p>Vous avez {{$panier_count}} {{$panier_count == 1 ? 'article' : 'articles'}} dans le panier</p>
            @else
            <p>Votre panier est vide</p>
            @endif
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card card-panier">
                    <div class="row">
                        @foreach ($panier_content as $item)
                        <div class="col-lg-12">
                            <div class="card card-content">
                                <div class="row">
                                    <div class="col-lg-2 col-4">
                                        <div class="logo-article">
                                            <i class="fa fa-file-pdf-o"></i>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-8">
                                        <div class="text-left">
                                            <h5 class="title-article">{{$item->name}}</h5>
                                            <span
                                                class="category-article">{{$item->model->categories[0]->category}}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-12">
                                        <div class="text-left content-text-art">
                                            <h4 class="price-article-fc">{{$item->price}} Fc</h4>
                                            <br>
                                            <span class="price-article-dollar">{{($item->price) / 2000}} $</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="text-right">
                                            <a href="{{route('CartDestroy',$item->rowId)}}" class="btn btn-delete"><i
                                                    class="fa fa-trash"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <a href="{{route('CartEmpty')}}" class="price-article-fc text-white bg-danger p-2 btn">Vider le
                        panier</a>
                    <button class="btn btn-check" data-toggle="modal" data-target="#exampleModal5">Check-out</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scriptis')
<script>
    $('#valid-num1').click(function () {
        $('#col-1').toggleClass('hidden')
    })
    $('#valid-num2').click(function () {
        $('#col-2').toggleClass('hidden')
    })
    $('#valid-num3').click(function () {
        $('#col-3').toggleClass('hidden')
    })

</script>
@endsection
