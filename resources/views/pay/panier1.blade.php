@inject('notviewsnews','App\Utilities\NotViewNews')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')

@section('content_student')

<div class="container mt-5 py-5" style="margin-top: 200px !important;">
    <div class="text-left">
        <h1 class="title">Panier</h1>
        @if ($panier_count > 0)
        <p>Vous avez {{$panier_count}} {{$panier_count == 1 ? 'article' : 'articles'}} dans le panier  <a href="{{route('CartEmpty')}}" class="ml-5 price-article-fc text-white bg-danger p-2" style="font-size: 12px !important; border-radius:50px !important; font-family='Century Gothic !important;'">Vider le panier</a></p>
        @else
            <p>Votre panier est vide</p>
        @endif
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card card-panier">
                <div class="row">
                    @foreach ($panier_content as $item)
                    <div class="col-lg-12">
                        <div class="card card-content">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="logo-article">
                                        <i class="fa fa-file"></i>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="text-left">
                                        <h5 class="title-article">{{$item->name}}</h5>
                                        <span class="category-article">{{$item->model->categories[0]->category}}</span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="text-left">
                                        <h4 class="price-article-fc">{{$item->price}} Fc</h4>
                                        <br>
                                        <span class="price-article-dollar">{{($item->price) / 2000}} $</span>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="text-right">
                                    <a href="{{route('CartDestroy',$item->rowId)}}" class="btn btn-delete"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-check">
                <div class="card-header bg-white" style="position: relative;">
                    <h5 class="mb-0">Details achats</h5>
                    <div class="avatar-user">
                    <img src="{{asset('storage/uploads/images/banner-bg2.jpg')}}" alt="">
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-left text-white">
                        <span class="nombre-art">
                            Nombre d'articles
                        <span>{{$panier_count}}</span>
                        </span>
                        <br>
                        <h6 class="total-sm mt-2" style="font-weight: 600;">
                            Total
                        <span>{{$panier_subtotal}}Fc</span>
                        </h6>
                        <hr>
                        <h5 class="total-lg mt-2" style="font-weight: 600;">
                            Total net
                            <span class="franc">{{str_replace(' ','',$panier_subtotal) + ((str_replace(' ','',$panier_subtotal) * 20)/100) }} Fc</span>
                            <span class="dollar">{{(str_replace(' ','',$panier_subtotal) + ((str_replace(' ','',$panier_subtotal) * 20)/100)) / 2000 }}$</span>
                        </h5>
                    </div>
                    <hr>
                    <span class="nombre-art text-white">
                        Choix de compte
                    </span>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active nav-money" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                aria-controls="home" aria-selected="true">
                                <img src="{{asset('storage/uploads/images/mpesa.jpg')}}" alt="">
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link nav-money" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                aria-controls="profile" aria-selected="false">
                                <img src="{{asset('storage/uploads/images/AIRTELLOGO.png')}}" alt="">
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link nav-money" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                aria-controls="contact" aria-selected="false">
                                <img src="{{asset('storage/uploads/images/47.png')}}" alt="">
                            </a>
                        </li>
                    </ul>
                    <hr>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <form action="{{route('CartPay')}}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    @if ($user->student->phone !=null)
                                    <div class="col-lg-12">
                                        <input type="checkbox" name="num-book" id="valid-num1">
                                        <label for="num-book" style="color: #fff;">Utiliser votre numéro de compte
                                            Bwanya</label>
                                    </div>
                                    <div class="col-lg-12" id="col-1">
                                        <input type="text" class="form-control form-control-panier"
                                            placeholder="Inserer votre numéro de téléphone" name="num">
                                    </div>
                                    @endif

                                    <div class="col-lg-12 mt-3">
                                        <select type="text" class="form-control form-control-panier" name="devise">
                                            <option value="CDF">
                                                CDF
                                            </option>
                                            <option value="USD">
                                                USD
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <input type="hidden" value="mpesa" name="provider">
                                        <button class="btn btn-primary" name="btn-valide">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <form action="{{route('CartPay')}}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    @if ($user->student->phone !=null)
                                    <div class="col-lg-12">
                                        <input type="checkbox" name="num-book" id="valid-num1">
                                        <label for="num-book" style="color: #fff;">Utiliser votre numéro de compte
                                            Bwanya</label>
                                    </div>
                                    <div class="col-lg-12" id="col-1">
                                        <input type="text" class="form-control form-control-panier"
                                            placeholder="Inserer votre numéro de téléphone" name="num">
                                    </div>
                                    @endif

                                    <div class="col-lg-12 mt-3">
                                        <select type="text" class="form-control form-control-panier" name="devise">
                                            <option value="CDF">
                                                CDF
                                            </option>
                                            <option value="USD">
                                                USD
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <input type="hidden" value="airtelmoney" name="provider">
                                        <button class="btn btn-primary" name="btn-valide">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <form action="{{route('CartPay')}}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    @if ($user->student->phone !=null)
                                    <div class="col-lg-12">
                                        <input type="checkbox" name="num-book" id="valid-num1">
                                        <label for="num-book" style="color: #fff;">Utiliser votre numéro de compte
                                            Bwanya</label>
                                    </div>
                                    <div class="col-lg-12" id="col-1">
                                        <input type="text" class="form-control form-control-panier"
                                            placeholder="Inserer votre numéro de téléphone" name="num">
                                    </div>
                                    @endif

                                    <div class="col-lg-12 mt-3">
                                        <select type="text" class="form-control form-control-panier" name="devise">
                                            <option value="CDF">
                                                CDF
                                            </option>
                                            <option value="USD">
                                                USD
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <input type="hidden" value="orangemoney" name="provider">
                                        <button class="btn btn-primary" name="btn-valide">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scriptis')
<script>
$('#valid-num1').click(function(){
    $('#col-1').toggleClass('hidden'
    )
    })
    $('#valid-num2').click(function(){
    $('#col-2').toggleClass('hidden'
    )
    })
    $('#valid-num3').click(function(){
    $('#col-3').toggleClass('hidden'
    )
})
</script>
@endsection