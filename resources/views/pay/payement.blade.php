@inject('notviewsnews','App\Utilities\NotViewNews')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')

@section('content_student')

<div class="container mt-5"
>
    <div class="text-left">
        <h1 class="title">Panier</h1>
        <p>Vous avez 3 articles dans le panier</p>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card card-panier">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-content">
                            <div class="row">
                                <cdiv class="col-lg-2">
                                    <div class="logo-article">
                                        <i class="fa fa-file"></i>
                                    </div>
                                </cdiv>
                                <cdiv class="col-lg-5">
                                    <div class="text-left">
                                        <h5 class="title-article">Livre d'informatique</h5>
                                        <span class="category-article">Livre</span>
                                    </div>
                                </cdiv>
                                <div class="col-lg-3">
                                    <div class="text-left">
                                        <h4 class="price-article-fc">20.000 Fc</h4>
                                        <br>
                                        <span class="price-article-dollar">10$</span>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="text-right">
                                        <button class="btn btn-delete"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card card-content">
                            <div class="row">
                                <cdiv class="col-lg-2">
                                    <div class="logo-article">
                                        <i class="fa fa-file"></i>
                                    </div>
                                </cdiv>
                                <cdiv class="col-lg-5">
                                    <div class="text-left">
                                        <h5 class="title-article">Livre d'informatique</h5>
                                        <span class="category-article">Livre</span>
                                    </div>
                                </cdiv>
                                <div class="col-lg-3">
                                    <div class="text-left">
                                        <h4 class="price-article-fc">20.000 Fc</h4>
                                        <br>
                                        <span class="price-article-dollar">10$</span>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="text-right">
                                        <button class="btn btn-delete"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card card-content">
                            <div class="row">
                                <cdiv class="col-lg-2">
                                    <div class="logo-article">
                                        <i class="fa fa-file"></i>
                                    </div>
                                </cdiv>
                                <cdiv class="col-lg-5">
                                    <div class="text-left">
                                        <h5 class="title-article">Livre d'informatique</h5>
                                        <span class="category-article">Livre</span>
                                    </div>
                                </cdiv>
                                <div class="col-lg-3">
                                    <div class="text-left">
                                        <h4 class="price-article-fc">20.000 Fc</h4>
                                        <br>
                                        <span class="price-article-dollar">10$</span>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="text-right">
                                        <button class="btn btn-delete"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card card-content">
                            <div class="row">
                                <cdiv class="col-lg-2">
                                    <div class="logo-article">
                                        <i class="fa fa-file"></i>
                                    </div>
                                </cdiv>
                                <cdiv class="col-lg-5">
                                    <div class="text-left">
                                        <h5 class="title-article">Livre d'informatique</h5>
                                        <span class="category-article">Livre</span>
                                    </div>
                                </cdiv>
                                <div class="col-lg-3">
                                    <div class="text-left">
                                        <h4 class="price-article-fc">20.000 Fc</h4>
                                        <br>
                                        <span class="price-article-dollar">10$</span>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="text-right">
                                        <button class="btn btn-delete"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card card-content">
                            <div class="row">
                                <cdiv class="col-lg-2">
                                    <div class="logo-article">
                                        <i class="fa fa-file"></i>
                                    </div>
                                </cdiv>
                                <cdiv class="col-lg-5">
                                    <div class="text-left">
                                        <h5 class="title-article">Livre d'informatique</h5>
                                        <span class="category-article">Livre</span>
                                    </div>
                                </cdiv>
                                <div class="col-lg-3">
                                    <div class="text-left">
                                        <h4 class="price-article-fc">20.000 Fc</h4>
                                        <br>
                                        <span class="price-article-dollar">10$</span>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="text-right">
                                        <button class="btn btn-delete"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-check">
                <div class="card-header bg-white" style="position: relative;">
                    <h5 class="mb-0">Details achats</h5>
                    <div class="avatar-user">
                        <img src="public/storage/uploads/images/banner-bg2.jpg" alt="">
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-left text-white">
                        <span class="nombre-art">
                            Nombre d'articles
                            <span>5</span>
                        </span>
                        <br>
                        <h6 class="total-sm mt-2" style="font-weight: 600;">
                            Total
                            <span>20.000Fc</span>
                        </h6>
                        <hr>
                        <h5 class="total-lg mt-2" style="font-weight: 600;">
                            Total net
                            <span class="franc">20.000Fc</span>
                            <span class="dollar">10$</span>
                        </h5>
                    </div>
                    <hr>
                    <span class="nombre-art text-white">
                        Choix de compte
                    </span>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active nav-money" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                aria-controls="home" aria-selected="true">
                                <img src="public/storage/uploads/images/mpesa.jpg" alt="">
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link nav-money" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                aria-controls="profile" aria-selected="false">
                                <img src="public/storage/uploads/images/AIRTELLOGO.png" alt="">
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link nav-money" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                aria-controls="contact" aria-selected="false">
                                <img src="public/storage/uploads/images/47.png" alt="">
                            </a>
                        </li>
                    </ul>
                    <hr>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <form action="">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <input type="checkbox" name="num-book" id="valid-num1">
                                        <label for="num-book" style="color: #fff;">Utiliser votre numéro de compte
                                            Bwanya </label>
                                    </div>
                                    <div class="col-lg-12" id="col-1">
                                        <input type="text" class="form-control form-control-panier"
                                            placeholder="Inserrez votre numéro de téléphone" name="num-tel">
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <select type="text" class="form-control form-control-panier" name="devise">
                                            <option value="">
                                                Devise
                                            </option>
                                            <option value="">
                                                CDF
                                            </option>
                                            <option value="">
                                                USD
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <button class="btn btn-primary" name="btn-valide">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <form action="">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <input type="checkbox" name="num-book" id="valid-num2">
                                        <label for="num-book" style="color: #fff;">Utiliser votre numéro de compte
                                            Bwanya </label>
                                    </div>
                                    <div class="col-lg-12" id="col-2">
                                        <input type="text" class="form-control form-control-panier"
                                            placeholder="Inserrez votre numéro de téléphone" name="num-tel">
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <select type="text" class="form-control form-control-panier" name="devise">
                                            <option value="">
                                                Devise
                                            </option>
                                            <option value="">
                                                CDF
                                            </option>
                                            <option value="">
                                                USD
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <button class="btn btn-primary" name="btn-valide">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <form action="">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <input type="checkbox" name="num-book" id="valid-num3">
                                        <label for="num-book" style="color: #fff;">Utiliser votre numéro de compte
                                            Bwanya </label>
                                    </div>
                                    <div class="col-lg-12" id="col-3">
                                        <input type="text" class="form-control form-control-panier"
                                            placeholder="Inserrez votre numéro de téléphone" name="num-tel">
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <select type="text" class="form-control form-control-panier" name="devise">
                                            <option value="">
                                                Devise
                                            </option>
                                            <option value="">
                                                CDF
                                            </option>
                                            <option value="">
                                                USD
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <button class="btn btn-primary" name="btn-valide">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scriptis')
$('#valid-num1').click(function(){
    $('#col-1').toggleClass('hidden'
    )
    })
    $('#valid-num2').click(function(){
    $('#col-2').toggleClass('hidden'
    )
    })
    $('#valid-num3').click(function(){
    $('#col-3').toggleClass('hidden'
    )
})
@endsection