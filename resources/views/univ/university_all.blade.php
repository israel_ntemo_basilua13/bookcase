@extends('layouts.student_layout')

@section('content_student')
<div class="col-md-12  col-sm-12 col-lg-12 margin no-padding">
    <div class="content">

        <div class="container-fluid">
            <div class="row justify-content-center">
                @foreach($universities as $university)
                <div class="col-lg-3">
                    <div class="card card-show-teacher">
                        <div class="plus">
                            <i class="fa fa-angle-down"></i>
                            <div class="sub_menu">
                                <ul>
                                    <li>
                                        <i class="fa fa-map-marker"></i> {{$university->describe}}
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"></i>{{$university->email}}
                                    </li>
                                    <li>
                                        <i class="fa fa-phone"></i>{{$university->phone}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-header">
                            <div class="text-center">
                                <h4>{{$university->name_university}}</h4>
                            </div>
                            <div class="avatar-teacher">
                                <img src="/storage/uploads/avatars/{{$university->logo_picture}}" alt="img" class="img-cover">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                                <span>Université</span>
                                <br>
                                <a href="{{ route('activation_store',$university->id) }}" class="btn">S'attacher <i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        <!-- <div class="card-footer d-flex justify-content-between">
                            <div class="ouvrages text-center">
                                <span class="number">120</span>
                                <br>
                                <span>Articles</span>
                            </div>
                            <div class="ouvrages text-center">
                                <span class="number">120</span>
                                <br>
                                <span>Articles</span>
                            </div>
                            <div class="ouvrages text-center">
                                <span class="number">120</span>
                                <br>
                                <span>Articles</span>
                            </div>
                        </div> -->
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row justify-content-center my-md-4">
                {{ $universities->links() }}
            </div>
        </div>


    </div>
</div>
@endsection

@section('scriptis')
<script>
    $(document).ready(function() {
        $('.cr').click(function() {
            $(this).toggleClass("slide-in");
        });
    });
</script>
@endsection