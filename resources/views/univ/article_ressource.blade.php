@extends('layouts.student_layout')

@section('content_student')
<div class="container margin">
    <div class="row justify-content-center">
            <div class="col-md-11 col-11 col-lg-12">
                <div class="text-left">
                    <h1 class="title">Gestion d'article en ligne</h1>
                </div>
            </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-10" style="margin-top:-20px">
            <div class="card card_create_news">
                <img src="/storage/uploads/avatars/{{$university->logo_picture}}" alt="" class="avatar avatarforcard_create_new rounded-circle avatar_student_teach_univ">
                <div class="card-header" style="box-shadow:0px 1px 0px 0 rgba(0,0,0,.2)">
                   <div class="container">
                        <ul class="nav nav-tabs ml-auto" id="myTab" role="tablist" style="border-bottom:none!important;">
                            <li class="nav-item">
                                <a class="nav-link active nav_link" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true"><i class="fa fa-users"></i>Articles publiés</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav_link" id="file-tab" data-toggle="tab" href="#file" role="tab" aria-controls="file" aria-selected="false"><i class="fa fa-file"></i>Créer Article</a>
                            </li>
                            
                        </ul>
                   </div>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="myTabContent">
                        <!-- All Articles -->
                        <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                            <div class="container">

                               <div class="card-body">
                                    <div class="text-center">
                                        <i class="fa fa-warning text-warning fa-2x"></i>
                                        <small>
                                            <p>
                                                All Articles
                                            </p>
                                        </small>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <small class="load-text"></small>
                                    <span class="spinner-border-sm load" role="status" aria-hidden="true" ></span>
                                </div>
                                
                            </div>
                        </div>
                        <!-- Fin All Articles -->

                        <!-- Créer file -->
                        <!-- method="POST" action="{{route('article.store')}}"  -->
                        <div class="tab-pane fade" id="file" role="tabpanel" aria-labelledby="file-tab">
                            <div class="container pt-md-4">
                                <form method="POST" action="{{ route('article.store')}}" enctype="multipart/form-data" style="padding:20px 30px; position relative;">
                                    @csrf
                                    <div class="form-group row justify-content-center">
                                        <div class="col-md-6">
                                            <label for="title">Edition</label>
                                            <input type="number" name="edition" class="form-control">
                                            <input type="hidden" name="price" class="form-control" value="0">
                                            <input type="hidden" name="statut_id" id="statut_id" class="form-control" value="1">
                                            <input type="hidden" name="collect" id="collect" class="form-control" value="1">
                                        </div>
                                    </div>

                                    <div class="form-group row justify-content-center">
                                        <div class="col-md-6">
                                            <label for="title">Fichier / Fichiers</label>
                                            <div class="input-group mb-3">
                                                <!-- <div class="input-group-prepend">
                                                        <button class="btn btn-outline-secondary btn-sm" type="button" id="inputGroupFileAddon03">Télécharger</button>
                                                    </div> -->
                                                <div class="custom-file">
                                                    <input type="file" name="file_article[]" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" accept=".pdf,.txt,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple>
                                                    <label class="custom-file-label" for="inputGroupFile03">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row justify-content-center">
                                        <div class="col-md-6">
                                            <label for="title">Catégorie</label>
                                            <!-- name=category_id[]   multiple si la selection passe a plusieurs-->
                                            <select class="form-control" id="category_id" name="category_id">
                                                @foreach($categories as $categor)
                                                <option value="{{$categor->id}}">{{$categor->category}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <br>
                                    </div>

                                    <div class="card-footer bg-white">
                                        <div class="container content-button pb-4 pt-4">
                                            <button type="submit" class="btn btn-primary btn-sm badge btn-save" style="background-color:#6f42c1 !important; border:none !important;"><i class="fa fa-plus"></i> Ajouter fichier(s)</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- Fin Créer file -->


                    </div>
                </div>                
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
</div>
@endsection

@section('scriptis')
<script>
    $(document).ready(function() {

        $('.custom-file-input').on('change', function(event) {
            var inputFile = event.currentTarget;
            $(inputFile).parent()
                .find('.custom-file-label')
                .html(inputFile.files[0].name);
                 });

        $('#title').change(function(){
            var title = $(this).val();
            $('#title_ap').text("Sujet : " + title.toString());
        });



        // Sa marche, reste la reinitialisation
        // var form_news_public  = document.getElementById('news_public');
        // var request_form = new XMLHttpRequest();

        // form_news_public.addEventListener('submit',function(e){
        //     e.preventDefault();
        //     var formdata  = new FormData(form_news_public);

        //     request_form.open('post',"{{route('newstore')}}");
        //     request_form.send(formdata);
        //     return false;
        // });
    });
</script>
@endsection