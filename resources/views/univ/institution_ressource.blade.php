@extends('layouts.student_layout')

@section('content_student')
<div class="container margin">
    <div class="row justify-content-center">
            <div class="col-md-11 col-11 col-lg-12">
                <div class="text-left">
                    <h1 class="title">Gestion des ressources</h1>
                </div>
            </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-11" style="margin-top:-20px">
            <div class="card card_create_news">
                <img src="/storage/uploads/avatars/{{$university->logo_picture}}" alt="" class="avatar avatarforcard_create_new rounded-circle avatar_student_teach_univ">
                <div class="card-header" style="box-shadow:0px 1px 0px 0 rgba(0,0,0,.2)">
                   <div class="container">
                        <ul class="nav nav-tabs ml-auto" id="myTab" role="tablist" style="border-bottom:none!important;">
                            <li class="nav-item">
                                <a class="nav-link active nav_link" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true"><i class="fa fa-users"></i>Ressources {{$university->name_university}}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link nav_link" id="faculty-tab" data-toggle="tab" href="#faculty" role="tab" aria-controls="faculty" aria-selected="false"><i class="fa fa-file"></i>Créer Faculté </a>
                            </li>

                            @if($facultys->count()>0)
                            <li class="nav-item">
                                <a class="nav-link nav_link" id="department-tab" data-toggle="tab" href="#department" role="tab" aria-controls="department" aria-selected="false"><i class="fa fa-file"></i>Créer Département /Section </a>
                            </li>
                            
                            @if($departmenties->count()>0)
                            <li class="nav-item">
                                <a class="nav-link nav_link" id="collection-tab" data-toggle="tab" href="#collection" role="tab" aria-controls="collection" aria-selected="false"><i class="fa fa-folder"></i>Créer Collection / Promotion</a>
                            </li>
                            @endif

                            @endif

                        </ul>
                   </div>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="myTabContent">

                        <!-- All Ressources -->
                        <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                            <div class="container">

                               <div class="card-body">
                                    <div class="text-center">
                                        <i class="fa fa-warning text-warning fa-2x"></i>
                                        <small>
                                            <p>
                                                All Ressources
                                            </p>
                                        </small>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <small class="load-text"></small>
                                    <span class="spinner-border-sm load" role="status" aria-hidden="true" ></span>
                                </div>
                                
                            </div>
                        </div>
                        <!-- Fin All Ressources -->

                        <!-- Créer Promotion -->
                        <div class="tab-pane fade" id="collection" role="tabpanel" aria-labelledby="collection-tab">
                            <div class="container pt-md-4">
                                <form method="POST" action="{{ route('InstitutionStore')}}" enctype="multipart/form-data" style="padding:20px 30px; position relative;">
                                    @csrf
                                    <div class="form-group row justify-content-center">
                                        <div class="col-md-6">
                                            <label for="document_name">Nom Promotion / Collection</label>
                                            <input type="text" name="document_name" class="form-control">
                                            
                                            <input type="hidden" name="indice" class="form-control" value="collection">
                                        </div>
                                    </div>

                                    <div class="form-group row justify-content-center">
                                        <div class="col-md-6">
                                            <label for="department_id">Département / Section</label>
                                            <select class="form-control" id="department_id" name="department_id">
                                                @foreach($departmenties->sortBy('title_department') as $department)
                                                <option value="{{$department->id }}">{{$department->title_department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <br>
                                    </div>

                                    <div class="card-footer bg-white">
                                        <div class="container content-button pb-4 pt-4">
                                            <button type="submit" class="btn btn-primary btn-sm badge btn-save" style="background-color:#6f42c1 !important; border:none !important;"><i class="fa fa-plus"></i> Sauvegarder  </button>
                                        </div>
                                    </div>

                                    <div class="form-group row justify-content-center hidden-sm" style="background: url('/storage/uploads/images/enfin plan2.jpg');">
                                        <div class="col-md-6">
                                            <small>
                                                <i class="fa fa-info" style="color:blue; font-size:18px; padding-right:5px !important;"></i>
                                                    Chèr/Chère Manager, <br>
                                                    Une promotion ou collection, répresente un dossier dans lequel,
                                                    les instructeurs (Professeurs et autres),déposeront leurs articles pour une reference a votre institution.<br>
                                                    <strong>En créant une collection, elle sera suffixé : /{{$university->user->name}} </strong><br> 
                                                    Veuillez vous reféré a vos promotions pour le nom de celui-ci.
                                            </small>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                        <!-- Fin Créer Promotion -->

                         <!-- Créer Département -->
                        <div class="tab-pane fade" id="department" role="tabpanel" aria-labelledby="department-tab">
                            <div class="container pt-md-4">
                                <form method="POST" action="{{ route('InstitutionStore')}}" enctype="multipart/form-data" style="padding:20px 30px; position relative;">
                                    @csrf
                                    <div class="form-group row justify-content-center">
                                        <div class="col-md-6">
                                            <label for="title_department">Nom Departement / Section</label>
                                            <input type="text" name="title_department" class="form-control">
                                            <input type="hidden" name="indice" class="form-control" value="department">
                                        </div>
                                    </div>

                                    <div class="form-group row justify-content-center">
                                        <div class="col-md-6">
                                            <label for="faculty_id">Faculté</label>
                                        
                                            <select class="form-control" id="faculty_id" name="faculty_id">
                                                @foreach($facultys->sortBy('title') as $faculty)
                                                <option value="{{$faculty->id }}">{{$faculty->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <br>
                                    </div>

                                    <div class="card-footer bg-white">
                                        <div class="container content-button pb-4 pt-4">
                                            <button type="submit" class="btn btn-primary btn-sm badge btn-save" style="background-color:#6f42c1 !important; border:none !important;"><i class="fa fa-plus"></i> Sauvegarder  </button>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                        <!-- Fin Création Département -->

                         <!-- Créer Faculté -->
                        <div class="tab-pane fade" id="faculty" role="tabpanel" aria-labelledby="faculty-tab">
                            <div class="container pt-md-4">
                                <form method="POST" action="{{ route('InstitutionStore')}}" enctype="multipart/form-data" style="padding:20px 30px; position relative;">
                                    @csrf
                                    <div class="form-group row justify-content-center">
                                        <div class="col-md-6">
                                            <label for="title">Nom Faculté</label>
                                            <input type="text" name="title" class="form-control">
                                            
                                            <input type="hidden" name="indice" class="form-control" value="faculty">
                                        </div>
                                    </div>


                                    <div class="form-group row justify-content-center">
                                        <div class="col-md-6">
                                            <label for="describe">Description<span class="info" title="pedrieieieieiieeieieieieieieieieiieeiei" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span></label>
                                            <textarea name="describe" cols="30" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>

                                    <div class="card-footer bg-white">
                                        <div class="container content-button pb-4 pt-4">
                                            <button type="submit" class="btn btn-primary btn-sm badge btn-save" style="background-color:#6f42c1 !important; border:none !important;"><i class="fa fa-plus"></i> Enregistrer </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <!-- Fin Créer Faculté -->

                    </div>
                </div>                
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
</div>
@endsection

@section('scriptis')
<script>
    $(document).ready(function() {

        $('.custom-file-input').on('change', function(event) {
            var inputFile = event.currentTarget;
            $(inputFile).parent()
                .find('.custom-file-label')
                .html(inputFile.files[0].name);
                 });

        $('#title').change(function(){
            var title = $(this).val();
            $('#title_ap').text("Sujet : " + title.toString());
        });



        // Sa marche, reste la reinitialisation
        // var form_news_public  = document.getElementById('news_public');
        // var request_form = new XMLHttpRequest();

        // form_news_public.addEventListener('submit',function(e){
        //     e.preventDefault();
        //     var formdata  = new FormData(form_news_public);

        //     request_form.open('post',"{{route('newstore')}}");
        //     request_form.send(formdata);
        //     return false;
        // });
    });
</script>
@endsection