@extends('layouts.student_layout')

@section('content_student')
<div class="container-fluid margin">
    <div class="row" style="margin-top:30px;">
        <div class="col-lg-3">
        <div class="card card-profil-sm"><div class="card-header"><div class="text-center"><div class="avatar-lg"><img src="/storage/uploads/avatars/{{$university->logo_picture}}" alt="img" class="img-cover"> <span data-toggle="modal" data-target="#exampleModal1" class="change-profil"><i class="fa fa-plus"></i> <div class="sm-tooltip">
            
                                            Modifier mon avatar
                                        </div></span></div> <h5 class="name-user-lg">{{$university->user->name }}</h5> <p class="mb-0">{{$university->user->rule->rule}}</p></div></div> <hr> <div class="container"><div class="row"><div class="col-lg-6"><div class="text-left"><h6>Institution</h6> <p><i class="fa fa-bank"></i> {{$university->name_university}}</p></div></div> <div class="col-lg-6"><div class="text-left"><h6>Téléphone</h6> <p><i class="fa fa-phone"></i> {{ $university->phone }}</p></div></div></div></div></div>
        </div>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-3">
                    <a href="{{route('storeNews') }}">
                        <div class="card card-compte">
                            <div class="icon">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"  class="bi bi-book">
                                    <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"></path>
                                </svg>
                            </div>
                            <div class="text-left">
                                <h1>{{$news_all->count()}}</h1>
                                <h6>News</h6>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="{{route('ArticleRessource') }}">
                        <div class="card card-compte">
                            <div class="icon">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"  class="bi bi-book">
                                    <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"></path>
                                </svg>
                            </div>
                            <div class="text-left">
                                @if($teacher_reference->count()>0)
                                    <h1>{{$teacher_reference->count()}}</h1>
                                @else
                                    <h5>Vous ne disposez d'aucun Instructeur</h5>
                                @endif
                                <h6>Articles</h6>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="{{ route('TeacherRessource')}}">
                        <div class="card card-compte">
                            <div class="icon">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"  class="bi bi-book">
                                    <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"></path>
                                </svg>
                            </div>
                            @if($teacher_reference->count()>0)
                                <div class="text-left d-flex justify-content-between">
                                    <h1>{{$teacher_reference->count()}} <span style="color: #32E4CD;">Attachés</span></h1>
                                    <h1>{{$teacher_univ->count()}} <span>créés</span></h1>
                                </div>
                            @else
                                <div class="text-left d-flex">
                                    <small><p>Vous ne disposez d'aucun Instructeur</p></small>
                                </div>
                            @endif
                            <h6>Instructeurs</h6>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="{{ route('StudentRessource')}}">
                        <div class="card card-compte">
                            <div class="icon">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"  class="bi bi-user">
                                    <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"></path>
                                </svg>
                            </div>
                            <div class="text-left">
                                @if($student_univ->count()>0)
                                    <h1>{{$student_belongsTo->count()}}</h1>
                                @else
                                    <strong>
                                        <p>Vous ne disposez d'aucun étudiant.<br>Veuillez incité vos étudiants a utilisé notre plateforme
                                        </p>
                                    </strong>
                                @endif
                                <h6>Etudiants</h6>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="{{ route('InstitutionRessource')}}#faculty">
                        <div class="card card-compte">
                            <div class="icon">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"  class="bi bi-book">
                                    <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"></path>
                                </svg>
                            </div>
                            <div class="text-left">
                                @if($faculties->count()>0)
                                    <h1>{{$facultys->count()}}</h1>
                                @else
                                    <small>
                                        <p class="mt-3">Vous ne disposez d'aucun faculté<br>Veuillez en crée 
                                        </p>
                                    </small>
                                @endif
                                <h6>Facultés /Sections</h6>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="{{ route('DepartmentRessource')}}">
                        <div class="card card-compte">
                            <div class="icon">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"  class="bi bi-book">
                                    <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"></path>
                                </svg>
                            </div>
                            <div class="text-left">
                                @if($departments->count()>0)
                                    <h1>{{$departmenties->count()}}</h1>
                                @else
                                    <small>
                                        <p class="mt-3">Vous ne disposez d'aucun département<br>Veuillez en crée 
                                        </p>
                                    </small>
                                @endif
                                <h6>Département</h6>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="{{ route('CollectionRessource')}}">
                        <div class="card card-compte">
                            <div class="icon">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"  class="bi bi-book">
                                    <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"></path>
                                </svg>
                            </div>
                            <div class="text-left">
                                @if($collections_all->count()>0)
                                    <h1>{{$collections->count()}}</h1>
                                @else
                                    <small>
                                        <p class="mt-3">Vous ne disposez d'aucun promotion<br>Veuillez en crée 
                                        </p>
                                    </small>
                                @endif
                                <h6>Promotion</h6>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- News -->
                <!-- <a href="{{ route('storeNews')}}" class="col-md-6">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">News</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    @if($all_news->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success">{{$news_all->count()}}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h6>
                                            statistique
                                            </h6>
                                            <div class="circle-stat">
                                                <span class="progress-content">
                                                    <span class="bg-progress" style="height:{{ ceil(abs( ($news_all->count()*100)/$all_news->count()) )}}%"></span>
                                                </span>
                                            <small class="percentage text-warning">
                                                {{ ceil(abs( ($news_all->count()*100)/$all_news->count()) )}}%
                                            </small>
                                            </div>
                                        </div>  
                                    </div>
                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucune actualité</p></small>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </a> -->

                <!-- Articles -->
                <!-- <a href="{{ route('TeacherRessource')}}" class="col-md-6">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Articles</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">  
                                <div class="row">
                                    @if($teacher_reference->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success mb-3">
                                               <span>{{$teacher_reference->count()}}</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="text-center">
                                        <h6>
                                           statistique
                                        </h6>
                                        <div class="circle-stat">
                                            <span class="progress-content">
                                                <span class="bg-progress" style="height:{{ ceil(abs( ($teacher_univ->count()*100)/$teacher_reference->count()) )}}%"></span>
                                            </span>
                                           <small class="percentage text-warning">
                                            {{ ceil(abs( ($teacher_univ->count()*100)/$teacher_reference->count()) )}}%
                                           </small>
                                        </div>
                                      </div> 
                                    </div>
                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucun Instructeur</p></small>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </a> -->

                <!-- Instructeurs -->
                <!-- <a href="{{ route('TeacherRessource')}}" class="col-md-5">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Instructeurs</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">  
                                <div class="row">
                                    @if($teacher_reference->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success mb-3">
                                               <span class="type">attachés</span>
                                               <span>{{$teacher_reference->count()}}</span>
                                            </span>
                                            <span class="cumpter badge badge-success">
                                               <span class="type">Créés</span>
                                               <span>{{$teacher_univ->count()}}</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="text-center">
                                        <h6>
                                           statistique
                                        </h6>
                                        <div class="circle-stat">
                                            <span class="progress-content">
                                                <span class="bg-progress" style="height:{{ ceil(abs( ($teacher_univ->count()*100)/$teacher_reference->count()) )}}%"></span>
                                            </span>
                                           <small class="percentage text-warning">
                                            {{ ceil(abs( ($teacher_univ->count()*100)/$teacher_reference->count()) )}}%
                                           </small>
                                        </div>
                                      </div> 
                                    </div>
                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucun Instructeur</p></small>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </a> -->

                <!-- Etudiant -->

                <!-- <a href="{{ route('StudentRessource')}}" class="col-md-5">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Etudiants</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    @if($student_univ->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success">{{$student_belongsTo->count()}}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h6>
                                            statistique
                                            </h6>
                                            <div class="circle-stat">
                                                <span class="progress-content">
                                                    <span class="bg-progress" style="height:{{ceil(abs( ($student_belongsTo->count()*100)/$student_univ->count()) )}}%"></span>
                                                </span>
                                                <small class="percentage text-warning">
                                                    {{ceil(abs( ($student_belongsTo->count()*100)/$student_univ->count()) )}}%
                                                </small>
                                            </div>
                                        </div> 
                                    </div>
                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <p>Vous ne disposez d'aucun étudiant.
                                                <br>
                                                Veuillez incité vos étudiants a utilisé notre plateforme
                                            </p>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </a> -->

                <!-- Faculté -->
                <!-- <a href="{{ route('FacultyRessource')}}" class="col-md-4">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Facultés /Sections</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    @if($faculties->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success">{{$facultys->count()}}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h6>
                                            statistique
                                            </h6>
                                            <div class="circle-stat">
                                                <span class="progress-content">
                                                    <span class="bg-progress" style="height:{{ceil(abs( ($facultys->count()*100)/$faculties->count()) )}}%"></span>
                                                </span>
                                                <small class="percentage  text-warning">
                                                    {{ceil(abs( ($facultys->count()*100)/$faculties->count()) )}}%
                                                </small>
                                            </div>
                                        </div>  
                                    </div>
                                    @else
                                     <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucune faculté
                                                <br>
                                                    Veuillez en crée
                                            </p></small>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </a> -->

                <!-- Departements -->
                <!-- <a href="{{ route('DepartmentRessource')}}" class="col-md-4">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Départements</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    @if($departments->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success">{{$departmenties->count()}}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h6>
                                            statistique
                                            </h6>
                                            <div class="circle-stat">
                                                <span class="progress-content">
                                                    <span class="bg-progress" style="height:{{ceil(abs( ($departmenties->count()*100)/$departments->count()) )}}%"></span>
                                                </span>
                                                <small class="percentage text-warning">
                                                {{ceil(abs( ($departmenties->count()*100)/$departments->count()) )}}%
                                                </small>
                                            </div>
                                        </div>  
                                    </div>
                                     @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucun Département
                                                <br>
                                                Veuillez en crée 
                                            </p></small>
                                        </div>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>

                    </div>
                </a> -->

                <!-- Promotions -->
                <!-- <a href="{{ route('CollectionRessource')}}" class="col-md-4">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Promotions</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">

                                    @if($collections_all->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success">{{$collections->count()}}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h6>
                                            statistique
                                            </h6>
                                            <div class="circle-stat">
                                                <span class="progress-content">
                                                    <span class="bg-progress" style="height:{{ceil(abs( ($collections->count()*100)/$collections_all->count()) )}}%"></span>
                                                </span>
                                                <small class="percentage text-warning">
                                                    {{ceil(abs( ($collections->count()*100)/$collections_all->count()) )}}%
                                                </small>
                                            </div>
                                        </div>  
                                    </div>

                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucune Promotion.
                                                <br>
                                                Veuillez en crée 
                                            </p></small>
                                        </div>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </a> -->

                <!-- Top news -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Nos Recentes Nouvelles</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                    @if($News->count()>0)

                                    @foreach($News as $news)
                                      <div class="text-left content-news">
                                        <h6>{{$news->title_news}}</h6>
                                        <small>
                                            {{$news->created_at->diffForHumans()}}
                                            <span class="pl-2">
                                            (
                                            @if ( ceil(abs( strtotime(date("Y-m-d",strtotime($news->date) )) - strtotime(date("Y-m-d")) ) / 86400) > 0)
                                            Il y a

                                            {{ ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400) }}
                                            {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400)) }}

                                            @else
                                            Aujourd'hui
                                            @endif
                                            )
                                            </span>
                                        </small><br>
                                        <small><i class="fa fa-map-marker"></i> {{$news->localization}}</small>
                                        <div class="progress mb-2 progressbar progressForNews">
                                           
                                        @if($news->status_id==1)
                                            <div class="progress-bar bg-purpe" role="progressbar" style="width:25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                                        @else
                                            <div class="progress-bar bg-danger" role="progressbar" style="width:25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                                        @endif   
                                        </div>
                                      </div>
                                    @endforeach

                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-info text-primary fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucune actualité publié récemment</p></small>
                                        </div>
                                    </div>
                                    @endif

                                     {{ $News->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection
