@extends('layouts.student_layout')

@section('content_student')
<div class="container margin">
    <div class="row justify-content-center">
            <div class="col-md-11 col-11 col-lg-12">
                <div class="text-left">
                    <h1 class="title">Gestion d'actualités</h1>
                </div>
            </div>
    </div>
    <div class="row justify-content-center" style="margin-top:-20px">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header" class="mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                    <div class="text-left" style="position:relative;">
                    <h5 style="color:#48465b; font-weight:500;">All News</h5>
                    <span class="text-public type-news">Publique</span>
                    <span class="text-private type-news">Privées</span>
                    </div>
                </div>
                <div class="card-body">
                    
                @foreach($News as $news)
                    @if($news->status_id==1)
                    <div class="text-left content-news public">
                    @else
                    <div class="text-left content-news private">
                    @endif

                       <h6>{{$news->title_news}}</h6>
                       <small>

                       {{$news->created_at->diffForHumans()}}
                        <span class="pl-2">
                           (
                           @if ( ceil(abs( strtotime(date("Y-m-d",strtotime($news->date) )) - strtotime(date("Y-m-d")) ) / 86400) > 0)
                           Il y a

                           {{ ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400) }}
                           {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400)) }}

                           @else
                           Aujourd'hui
                           @endif
                           )
                        </span>

                       </small>
                       <div class="more">
                                <span class="option"><i class="fa fa-ellipsis-h"></i></span>
                                <div class="show-option">
                                    <i class="fa fa-graduation-cap"></i>
                                    <i class="fa fa-comment-o"></i>
                                    <i class="fa fa-eye"></i>
                                </div>
                            </div>
                    </div>
                @endforeach  
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card card_create_news">
                <img src="/storage/uploads/avatars/{{$university->logo_picture}}" alt="" class="avatar avatarforcard_create_new rounded-circle avatar_student_teach_univ">
                <div class="card-header" style="box-shadow:0px 1px 0px 0 rgba(0,0,0,.2)">
                   <div class="container">
                        <ul class="nav nav-tabs ml-auto" id="myTab" role="tablist" style="border-bottom:none!important;">
                            <li class="nav-item">
                                <a class="nav-link active nav_link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-users"></i> Actualité publique</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav_link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-lock"></i>Actualité privée</a>
                            </li>
                        </ul>
                   </div>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="myTabContent">
                        
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="container">

                                <!-- News Public -->
                                <!-- method="POST" action="{{route('newstore')}}"  -->
                                <form id="news_public" method="POST" action="{{route('newstore')}}"  enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row justify-content-center">
                                            <div class="col-md-6">
                                                <label for="title">Titre</label>
                                                <input type="text" name="title" id="title" class="form-control">
                                                <input type="radio" name="news_public" style="display:none;" checked="true" class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="localization">Localisation</label>
                                                <input type="text" name="localization" id="localization" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group row justify-content-center">
                                            <div class="col-md-6">
                                                <label for="describe">Description <span class="info" title="pedrieieieieiieeieieieieieieieieiieeiei" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span></label>
                                                <textarea name="describe" id="describe" cols="30" rows="2" class="form-control"></textarea>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="domains">Domaine d'exploitation <span class="info" title="pedrieieieieiieeieieieieieieieieiieeiei" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span></label>
                                                <textarea name="domains" cols="30" rows="2" class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row justify-content-center">
                                            <div class="col-md-10">
                                                <label for="picture">Picture</label>
                                                <div class="input-group mb-3">
                                                    <div class="custom-file">
                                                        <input type="file" name="picture_news" id="picture_news" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" accept=".jpg,.jpeg,.png">
                                                        <label class="custom-file-label" for="inputGroupFile03">Importer un image</label>
                                                        <span class="btn-file"><i class="fa fa-camera"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card-footer bg-white">
                                            <div class="container content-button pb-4 pt-4">
                                                <button type="submit" class="btn btn-primary btn-sm badge btn-save" style="background-color:#6f42c1 !important; border:none !important;"><i class="fa fa-plus"></i> Publier l'actualité public</button>
                                                </div>
                                        </div>
                                </form>
                                <!-- Fin News Public -->

                                <div class="text-center">
                                    <small class="load-text"></small>
                                    <span class="spinner-border-sm load" role="status" aria-hidden="true" ></span>
                                </div>

                                <div class="row justify-content-center">
                                    <div class="col-md-10">
                                        <div class="card preview-card" style="margin-bottom: 50px;">
                                           
                                                <span class="hidden-preview-card"><i class="fa fa-close"></i></span>
                                            

                                            <div class="card-header bg-white card-header-avatar text-left">
                                                
                                                @if(substr(ucfirst($university->name_university),0,1)=='U')
                                                <span class="card_university_inst_u avatar">U</span>
                                                @elseif(substr(ucfirst($university->name_university),0,1)=='A')
                                                <span class="card_university_inst_a avatar">A</span>
                                                @elseif(substr(ucfirst($university->name_university),0,1)=='I')
                                                <span class="card_university_inst_i avatar">Is</span>
                                                @else
                                                <span class="card_university_inst_o avatar">{{substr($university->name_university,0,1)}}</span>
                                                @endif
                                                
                                                <div class="card-pseudo">
                                                   {{$university->name_university}}
                                                </div>
                                                <div class="card-date">
                                                    publié,
                                                    aujourd'hui
                                                </div>
                                            </div>
                                            <a href="#">
                                                <div class="card-img">
                                                    <img class="card-img-top" id="picture_news_ap" src="/storage/uploads/images/picture.jpg" alt="">
                                                </div>
                                            </a>
                                            <div class="card-body">

                                                <div class="container">
                                                    <div class="text-left" style="margin-bottom: 20px">

                                                        <p class="h5 mt-3" id="title_ap" style="color:#d74d52;font-weight:600">Sujet : Title</p>
                                                        <p class="card-text" id="describe_ap">
                                                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore ut perspiciatis suscipit alias! Ratione, nihil esse libero voluptatibus voluptate suscipit molestias beatae nesciunt recusandae molestiae iure perspiciatis aperiam, consequatur eaque.
                                                        </p>
                                                        <span class="locatisation" id="localization_ap"><i class="fa fa-map-marker" style="color:#d74d52"></i>Bandale, Kinshasa hotel pullman</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer bg-white card-stat">
                                                <div class="container">
                                                    <div class="row text-center">
                                                        <p class="h4 col-4 tchek like"><i class="fa fa-thumbs-up" title="J'aime" data-toggle="tooltip" data-placement="top"></i><span class="badge badge-success">5</span></p>

                                                        <p class="h4 col-4 tchek join"><i class="fa fa-anchor" title="J'y participe" data-toggle="tooltip" data-placement="top">
                                                            </i>
                                                            <span class="badge badge-success">5</span>
                                                        </p>

                                                        <p class="h4 col-4"><i class="fa fa-share" title="Je partage" data-toggle="tooltip" data-placement="top">
                                                            </i>
                                                        </p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="container">
                               <!-- News Private -->
                                <!-- method="POST" action="{{route('newstore')}}"  -->
                                <form id="news_private"  method="POST" action="{{route('newstore')}}"  enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row justify-content-center">
                                            <div class="col-md-6">
                                                <label for="title">Titre</label>
                                                <input type="text" name="title" id="title" class="form-control">
                                                <input type="radio" name="news_private" style="display:none;" checked class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="localization">Localisation</label>
                                                <input type="text" name="localization" id="localization" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group row justify-content-center">
                                            <div class="col-md-12">
                                                <label for="describe">Description <span class="info" title="pedrieieieieiieeieieieieieieieieiieeiei" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span></label>
                                                <textarea name="describe" id="describe" cols="30" rows="2" class="form-control"></textarea>
                                                <input type="hidden" name="domains"  value="toutes" class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row justify-content-center">
                                            <div class="col-md-10">
                                                <label for="picture">Picture</label>
                                                <div class="input-group mb-3">
                                                    <div class="custom-file">
                                                        <input type="file" name="picture_news" id="picture_news" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" accept=".jpg,.jpeg,.png">
                                                        <label class="custom-file-label" for="inputGroupFile03">Importer un image</label>
                                                        <span class="btn-file"><i class="fa fa-camera"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card-footer bg-white">
                                            <div class="container content-button pb-4 pt-4">
                                                <button type="submit" class="btn btn-primary btn-sm badge btn-save" style="background-color:#6f42c1 !important; border:none !important;"><i class="fa fa-plus"></i> Publier l'actualité privée</button>
                                                </div>
                                        </div>
                                </form>
                                <!-- Fin News Private -->
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <div class="card-footer bg-white" style="border-top:1px solid rgba(0,0,0,.2)!important">
                    <div class="container content-button pb-4 pt-4">
                        <button class="btn btn-primary btn-sm badge btn-preview"><i class="fa fa-search-plus"></i> Aperçu</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection

@section('scriptis')
<script>
    $(document).ready(function() {

        $('.custom-file-input').on('change', function(event) {
            var inputFile = event.currentTarget;
            $(inputFile).parent()
                .find('.custom-file-label')
                .html(inputFile.files[0].name);
                 });

        $('#title').change(function(){
            var title = $(this).val();
            $('#title_ap').text("Sujet : " + title.toString());
        });

        $('#localization').change(function(){
            var localization = $(this).val();
            $('#localization_ap').html("<i class=\"fa fa-map-marker\" style=\"color:#d74d52\"></i>" +localization.toString());
        });

        $('#describe').change(function(){
            var describe = $(this).val();
            $('#describe_ap').html(describe.toString());
        });

        $('#picture_news').change(function(){
            var describe = $(this).val();
            // .removeAttr("title")
            $('#picture_news_ap').attr("src", "/storage/uploads/images/black_camera.jpg");

        });

        $('.tchek').click(function() {
            $(this).toggleClass("stat-color");

            if ($(this).hasClass('stat-color')) {
                $(this).children('.badge').text((parseInt($(this).children().text(), 10) + 1).toString());
            } else {
                 $(this).children('.badge').text((parseInt($(this).children().text(), 10) - 1).toString());
            }
        });

        $('.tchek').mouseover(function() {
            if ($(this).hasClass('stat-color')) {
                if($(this).hasClass('like')){
                    $(this).children().attr("data-original-title", "Aimé").removeAttr("title");
                }
                else{
                    $(this).children().attr("data-original-title", "j'y serais").removeAttr("title");
                }
            } else {
                 if($(this).hasClass('like')){
                    $(this).children().attr("data-original-title", "J'aime").removeAttr("title");
                }
                else{
                    $(this).children().attr("data-original-title", "j'y participe").removeAttr("title");
                }
            }
        });


        // Sa marche, reste la reinitialisation
        // var form_news_public  = document.getElementById('news_public');
        // var request_form = new XMLHttpRequest();

        // form_news_public.addEventListener('submit',function(e){
        //     e.preventDefault();
        //     var formdata  = new FormData(form_news_public);

        //     request_form.open('post',"{{route('newstore')}}");
        //     request_form.send(formdata);
           
        //     form_news_public.reset(); 
        //     return false;

        // });
    });
</script>
@endsection