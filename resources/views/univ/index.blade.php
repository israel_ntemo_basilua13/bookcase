@extends('layouts.app',['title' => 'app'])

@section('content')
<div  class="bg-choice" style="height: 100vh;">
    <div class="container" style="z-index:1; position:relative;">

        <div class="row justify-content-center margin ">
            <div class="col-md-5 slideBox1">
                <div class="card px-4 py-5 Up" style="margin-top:-40px;background: #f7f7f7;">
                    <div class="text-center">
                        <h4 class="change">1. Demande d'admission</h4>
                        <i class="fa fa-check-circle-o fa-4x text-success"></i>
                        <p class="hidden">Faites votre demande d'admission et obtenez le code de validité via votre email après une durée d'analyse maximum de deux semaines.</p>
                        <button class="btn btn-primary btn-sm show-cardfordemande badge">cliquez içi</button>
                    </div>
                    <div class="container">
                        @if (session()->has('type_agreem'))
                        <form class="cardfordemande" id="admission" enctype="multipart/form-data" style="">
                            @csrf
                            <div class="form-group row justify-content-center mb-4">
                                <div class="col-md-12">
                                    <label for="name_inst">Nom Library</label>
                                    <input id="name_inst" type="text" class="form-control  @error('name_inst') is-invalid @enderror" name="name_inst" value="{{ old('name_inst') }}" required autocomplete="name_inst" autofocus>

                                    @error('name_inst')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row justify-content-center mb-4">
                                <div class="col-md-12">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title">Lettre de demande</label>
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" name="file_request" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" accept=".pdf,.txt,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                        <label class="custom-file-label" for="inputGroupFile03">Importer un fichier</label>
                                        <span class="btn-file"><i class="fa fa-paperclip"></i></span>
                                    </div>
                                </ol>
                            </div>

                            <!-- <button type="submit" class="btn btn-primary btn-login btn-float hidden-card"> -->
                            <button type="submit" class="btn btn-primary btn-login btn-float">
                                <span class="spinner-border-sm  rotate_spinner" role="status" aria-hidden="true"></span>
                                Envoyer
                            </button>

                            <!-- <button type="submit" class="btn btn-primary btn-block btn-auth btn-login btn-float">
                                <span class="spinner-border-sm rotate_spinner" role="status" aria-hidden="true" ></span>
                                <span class="spam">Sauvegarder les infos</span>
                            </button> -->
                        </form>
                        @else
                        <form class="cardfordemande" id="admission" enctype="multipart/form-data" style="">
                            @csrf
                            <div class="form-group row justify-content-center mb-4">
                                <div class="col-md-12 col-input">
                                    <label for="name_inst">Nom Université/Institution</label>
                                    <span class="icon"><i class="fa fa-bank"></i></span>
                                    <input id="name_inst" type="text" class="form-control  @error('name_inst') is-invalid @enderror" name="name_inst" value="{{ old('name_inst') }}" required autocomplete="name_inst" autofocus placeholder="Veuillez inserez le nom université">

                                    @error('name_inst')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row justify-content-center mb-4">
                                <div class="col-md-12 col-input">
                                    <label for="email">Email</label>
                                    <span class="icon"><i class="fa fa-envelope"></i></span>
                                    <input id="email" type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Veuillez inserez  Email université">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title">Lettre de demande</label>
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" name="file_request" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" accept=".pdf,.txt,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                        <label class="custom-file-label" for="inputGroupFile03">Importer un fichier</label>
                                        <span class="btn-file"><i class="fa fa-paperclip"></i></span>
                                    </div>
                                </ol>
                            </div>

                            <!-- <button type="submit" class="btn btn-primary btn-login btn-float hidden-card"> -->
                            <button type="submit" class="btn btn-primary btn-login btn-float">
                                <span class="spinner-border-sm  rotate_spinner" role="status" aria-hidden="true"></span>
                                Envoyer
                            </button>

                            <!-- <button type="submit" class="btn btn-primary btn-block btn-auth btn-login btn-float">
                                <span class="spinner-border-sm rotate_spinner" role="status" aria-hidden="true" ></span>
                                <span class="spam">Sauvegarder les infos</span>
                            </button> -->
                        </form>
                        @endif

                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row justify-content-center">
            <div class="col-md-6">
                <button class="btn btn-primary">Envoyer une demande</button>
            </div>
            <div class="col-md-6">
                <button class="btn btn-success">Valider une demande</button>
            </div>
        </div> -->

    </div>

</div>
@endsection

@section('scriptis')
<script>
    $(document).ready(function() {
        $('.custom-file-input').on('change', function(event) {
            var inputFile = event.currentTarget;
            $(inputFile).parent()
                .find('.custom-file-label')
                .html(inputFile.files[0].name);

            // debut 
         var form  = document.getElementById('admission');

         var request = new XMLHttpRequest();

         form.addEventListener('submit',function(e){
             e.preventDefault();
             var formdata  = new FormData(form);

             request.open('post',"{{route('agreement.store')}}");
             request.send(formdata);

            // request.onreadystatechange = function() {
            // if (request.readyState == 4 && request.status == 200) {
            //     alert('bien');
            // }
            // };
         });

            // Fin script
    
        });

    });

</script>
@endsection
