@extends('layouts.student_layout')

@section('content_student')
<div class="container margin">
    <div class="row justify-content-center">
            <div class="col-md-9 col-9 offset-md-3 col-lg-12">
                <div class="text-left">
                    <h1 class="title">Promotions</h1>
                </div>
            </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10" style="margin-top:-20px">
            <div class="card card_create_news">
                
                <div class="card-body mt-5">
                    <div class="tab-content" id="myTabContent">
                        
                        <!-- All Promotion -->
                        <div class="tab-pane fade show active" id="teacher" role="tabpanel" aria-labelledby="teacher-tab">
                           
                            @if($collections->count()>0)
                            @foreach($collections as $collection)
                            <div class="card-header bg-white card-header-avatar text-left" style="height: 100% !important;">
                                <div class="card-pseudo pseudo" style="font-weight:400;margin-left:30px!important">
                                    <i class="fa fa-folder fa-3x" style="color:#f5d471;">
                                    </i>
                                   <span class="pl-md-2" style="font-size:1.4em;">{{$collection->document_name}}</span>
                                </div>

                                <div class="card-date function" style="margin-left:30px!important">
                                        <br>
                                        <span class="pl-5">* Faculté : {{$collection->department->faculty->title}} </span>
                                        <span class="pl-5">* Département/Section :  {{$collection->department->title_department}}</span>
                                </div>

                                <div class="more">
                                    <span class="option"><i class="fa fa-ellipsis-h"></i></span>
                                    <div class="show-option">
                                        <i class="fa fa-comment-o"></i>
                                        <i class="fa fa-eye"></i>
                                        <i class="fa fa-trash text-danger" title="Supprimer" data-toggle="tooltip" data-placement="top"></i>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="card-body">
                                <div class="text-center">
                                    <i class="fa fa-info text-info fa-5x pt-5 mt-md-5"></i>
                                    <small>
                                        <p>Vous ne disposez d'aucune promotion rattaché a votre université </p>
                                        <p>Veuillez en crée, a de fin d'utilisation</p>
                                    </small>
                                </div>
                            </div>
                            @endif
                        </div>
                        <!-- Close All Promotion -->

                    </div>
                </div>                
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
</div>
@endsection
