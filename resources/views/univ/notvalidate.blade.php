@extends('layouts.app',['title' => 'app'])

@section('content')
    <div class="container">
        <div class="row justify-content-center margin">
            <div class="col-md-6">
                <div class="card px-4 py-5 Up">
                    <div class="text-center" style="background: url('/storage/uploads/images/enfin plan2.jpg');">
                        <i class="fa fa-info" style="font-size:200px;"></i>
                        <p>
                            Votre droit de creation de compte n'est pas valide.<br>

                        @if(session()->has('notreference'))
                        <small>Veuillez consulter votre boite email, pour récuperer le droit d'accès. </small>
                        </p>
                        @else
                        <small>Veuillez consulter votre boite email, au cas ou vous aviez soumis une demande. </small>
                        </p>
                        <small> Pour soumettre une demande, veuillez suivre cliquez sur ce lien ci-dessous. </small>
                        <p>
                            <br>
                            <a href="{{ route('agreement.index')}}" class="px-4 py-2 btn-primary btn-sm">Soumettre une demande</a>
                        </p>
                        @endif
                    </div>

                    <!-- <h4>2. Validation d'admission</h4>
                    <p>Inserez le code de validité après récupération de celui-ci sur votre e-mail afin de créer votre compte Université/ Institut</p>
                    <button class="btn btn-primary btn-sm show-cardfordemande badge">cliquez içi</button> -->
                            <!-- <div class="container">
                                <form class="cardfordemande" method="POST" action="{{route('agreement.store')}}" enctype="multipart/form-data" style="">
                                    @csrf
                                    <div class="form-group row justify-content-center mb-4">
                                        <div class="col-md-12">
                                            

                                            @error('name_inst')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-login btn-float hidden-card">
                                        Valider
                                    </button>
                                </form>
                            </div> -->
                </div> 
            </div>
                    

    </div>
@endsection
