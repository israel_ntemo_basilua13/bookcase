@extends('layouts.student_layout')

@section('content_student')
<div class="container margin">
    <div class="row">
        <div class="col-md-12">
            <div class="text-left">
                <h1 class="title mb-4">Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <div class="text-center">
                <h6>
                    Année Académique
                </h6>
                <div class="circle-year">
                    <small>100%</small>
                </div>
            </div> 
        </div>
        <div class="col-md-10">
            <div class="row justify-content-center">

                <!-- News -->
                <a href="{{ route('storeNews')}}" class="col-md-6">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">News</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    @if($all_news->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success">{{$news_all->count()}}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h6>
                                            statistique
                                            </h6>
                                            <div class="circle-stat">
                                                <span class="progress-content">
                                                    <span class="bg-progress" style="height:{{ ceil(abs( ($news_all->count()*100)/$all_news->count()) )}}%"></span>
                                                </span>
                                            <small class="percentage text-warning">
                                                {{ ceil(abs( ($news_all->count()*100)/$all_news->count()) )}}%
                                            </small>
                                            </div>
                                        </div>  
                                    </div>
                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucune actualité</p></small>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

                <!-- Articles -->
                <a href="{{ route('TeacherRessource')}}" class="col-md-6">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Articles</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">  
                                <div class="row">
                                    @if($teacher_reference->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success mb-3">
                                               <span>{{$teacher_reference->count()}}</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="text-center">
                                        <h6>
                                           statistique
                                        </h6>
                                        <div class="circle-stat">
                                            <span class="progress-content">
                                                <span class="bg-progress" style="height:{{ ceil(abs( ($teacher_univ->count()*100)/$teacher_reference->count()) )}}%"></span>
                                            </span>
                                           <small class="percentage text-warning">
                                            {{ ceil(abs( ($teacher_univ->count()*100)/$teacher_reference->count()) )}}%
                                           </small>
                                        </div>
                                      </div> 
                                    </div>
                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucun Instructeur</p></small>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

                <!-- Instructeurs -->
                <a href="{{ route('TeacherRessource')}}" class="col-md-5">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Instructeurs</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">  
                                <div class="row">
                                    @if($teacher_reference->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success mb-3">
                                               <span class="type">attachés</span>
                                               <span>{{$teacher_reference->count()}}</span>
                                            </span>
                                            <span class="cumpter badge badge-success">
                                               <span class="type">Créés</span>
                                               <span>{{$teacher_univ->count()}}</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="text-center">
                                        <h6>
                                           statistique
                                        </h6>
                                        <div class="circle-stat">
                                            <span class="progress-content">
                                                <span class="bg-progress" style="height:{{ ceil(abs( ($teacher_univ->count()*100)/$teacher_reference->count()) )}}%"></span>
                                            </span>
                                           <small class="percentage text-warning">
                                            {{ ceil(abs( ($teacher_univ->count()*100)/$teacher_reference->count()) )}}%
                                           </small>
                                        </div>
                                      </div> 
                                    </div>
                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucun Instructeur</p></small>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

                <!-- Etudiant -->
                <a href="{{ route('StudentRessource')}}" class="col-md-5">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Etudiants</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    @if($student_univ->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success">{{$student_belongsTo->count()}}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h6>
                                            statistique
                                            </h6>
                                            <div class="circle-stat">
                                                <span class="progress-content">
                                                    <span class="bg-progress" style="height:{{ceil(abs( ($student_belongsTo->count()*100)/$student_univ->count()) )}}%"></span>
                                                </span>
                                                <small class="percentage text-warning">
                                                    {{ceil(abs( ($student_belongsTo->count()*100)/$student_univ->count()) )}}%
                                                </small>
                                            </div>
                                        </div> 
                                    </div>
                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucun étudiant.
                                                <br>
                                                Veuillez incité vos étudiants a utilisé notre plateforme
                                            </p></small>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

                <!-- Faculté -->
                <a href="{{ route('FacultyRessource')}}" class="col-md-4">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Facultés /Sections</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    @if($faculties->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success">{{$facultys->count()}}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h6>
                                            statistique
                                            </h6>
                                            <div class="circle-stat">
                                                <span class="progress-content">
                                                    <span class="bg-progress" style="height:{{ceil(abs( ($facultys->count()*100)/$faculties->count()) )}}%"></span>
                                                </span>
                                                <small class="percentage  text-warning">
                                                    {{ceil(abs( ($facultys->count()*100)/$faculties->count()) )}}%
                                                </small>
                                            </div>
                                        </div>  
                                    </div>
                                    @else
                                     <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucune faculté
                                                <br>
                                                    Veuillez en crée
                                            </p></small>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

                <!-- Departements -->
                <a href="{{ route('DepartmentRessource')}}" class="col-md-4">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Départements</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    @if($departments->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success">{{$departmenties->count()}}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h6>
                                            statistique
                                            </h6>
                                            <div class="circle-stat">
                                                <span class="progress-content">
                                                    <span class="bg-progress" style="height:{{ceil(abs( ($departmenties->count()*100)/$departments->count()) )}}%"></span>
                                                </span>
                                                <small class="percentage text-warning">
                                                {{ceil(abs( ($departmenties->count()*100)/$departments->count()) )}}%
                                                </small>
                                            </div>
                                        </div>  
                                    </div>
                                     @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucun Département
                                                <br>
                                                Veuillez en crée 
                                            </p></small>
                                        </div>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>

                    </div>
                </a>

                <!-- Promotions -->
                <a href="{{ route('CollectionRessource')}}" class="col-md-4">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Promotions</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">

                                    @if($collections_all->count()>0)
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="cumpter badge badge-success">{{$collections->count()}}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h6>
                                            statistique
                                            </h6>
                                            <div class="circle-stat">
                                                <span class="progress-content">
                                                    <span class="bg-progress" style="height:{{ceil(abs( ($collections->count()*100)/$collections_all->count()) )}}%"></span>
                                                </span>
                                                <small class="percentage text-warning">
                                                    {{ceil(abs( ($collections->count()*100)/$collections_all->count()) )}}%
                                                </small>
                                            </div>
                                        </div>  
                                    </div>

                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-warning text-warning fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucune Promotion.
                                                <br>
                                                Veuillez en crée 
                                            </p></small>
                                        </div>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </a>

                <!-- Top news -->
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header mt-3" style="border-bottom: 1px solid rgba(0,0,0,.125)!important;">
                            <div class="text-left">
                                <h5 style="color:#48465b; font-weight:500;" class="ml-4">Top news</h5 style="color:#48465b; font-weight:500;">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                    @if($News->count()>0)

                                    @foreach($News as $news)
                                      <div class="text-left content-news">
                                        <h6>{{$news->title_news}}</h6>
                                        <small>
                                            {{$news->created_at->diffForHumans()}}
                                            <span class="pl-2">
                                            (
                                            @if ( ceil(abs( strtotime(date("Y-m-d",strtotime($news->date) )) - strtotime(date("Y-m-d")) ) / 86400) > 0)
                                            Il y a

                                            {{ ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400) }}
                                            {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400)) }}

                                            @else
                                            Aujourd'hui
                                            @endif
                                            )
                                            </span>
                                        </small><br>
                                        <small><i class="fa fa-map-marker"></i> {{$news->localization}}</small>
                                        <div class="progress mb-2 progressbar progressForNews">
                                           
                                        @if($news->status_id==1)
                                            <div class="progress-bar bg-purpe" role="progressbar" style="width:25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                                        @else
                                            <div class="progress-bar bg-danger" role="progressbar" style="width:25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                                        @endif   
                                        </div>
                                      </div>
                                    @endforeach

                                    @else
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="fa fa-info text-primary fa-2x"></i>
                                            <small><p>Vous ne disposez d'aucune actualité publié récemment</p></small>
                                        </div>
                                    </div>
                                    @endif

                                     {{ $News->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection
