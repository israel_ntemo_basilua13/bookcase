@section('style')
<link href="{{ asset('css/_style.css') }}" rel="stylesheet">
@endsection

@extends('layouts.app',['title' => 'app'])

@section('content')
<div class="bg-choice">
    <div class="container">
        
        <div class="row justify-content-around">
            <div class="col-md-10 col-lg-5 bg-white">
                <ul id="progress">
                    <li>Account Setup</li>
                    <li class="active">Personal Details</li>

                </ul>

                <div class="card slideBox1" style="border-radius: 10px;padding:20px 30px;background:white;z-index: 1;">
                    <div class="text-left mb-4">

                        <h2 class="heading title-form page2" style="font-size: 28px;">University Account</h2>

                    </div>

                    <div class="card-body">
                        
                        <form method="POST" action="{{route('university.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row mb-4">
                                
                                <div class="col-md-12">
                                <label for="file_picture">Sigle</label>
                                <div class="input-group mb-3">
                                    <input type="text" name="sigle" class="form-control" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03">
                                </div>
                               
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                
                                <div class="col-md-6">
                                <label for="file_picture">Picture</label>
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" name="file_picture" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" accept=".jpg,.jpeg,.png">
                                        <label class="custom-file-label" for="inputGroupFile03">Importer un fichier</label>
                                        <span class="btn-file"><i class="fa fa-image"></i></span>
                                    </div>
                                </div>
                                </div>
                                <div class="col-md-6">
                                   <label for="file_logo">logo</label>
                                   <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" name="file_logo" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" accept=".jpg,.jpeg,.png">
                                        <label class="custom-file-label" for="inputGroupFile03">Importer un fichier</label>
                                        <span class="btn-file"><i class="fa fa-image"></i></span>
                                    </div>
                                    </div>
                                </div> 
                            </div>
                            
                            <div class="form-group row  mb-4">
                                <div class="col-md-12">
                                    <label for="describe">Description</label>
                                    <textarea name="describe" id="describe" class="form-control" required></textarea>
                                </div>
                            </div>
                            <div class="form-group row  mb-4">
                                
                                <div class="col-md-12">
                                    <label for="adress">Adresse</label>
                                    <textarea name="adress" id="adress" class="form-control" required></textarea>
                                    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <div class="col-md-12">
                                    <label for="phone">Téléphone</label>
                                    <input id="phone" type="tel" class="form-control" name="phone"  required autocomplete="phone">
                                    
                                    <input type="hidden" id="agreem_id"  name="agreem_id" value="{{$agreement->id}}">
                                </div>
                            </div>
                            <div class="form-group row  mb-4">
                                
                                <div class="col-md-6">
                                    <label for="longitude">Longitude</label>
                                    <input id="longitude" type="number" class="form-control" name="longitude"  required autocomplete="phone">
                                </div>
                                <div class="col-md-6">
                                    <label for="latitude">Latitude</label>
                                    <input id="latitude" type="number" class="form-control" name="latitude"  required autocomplete="phone">
                                </div>

                            </div>
                            
                            <button type="submit" class="btn btn-primary btn-block btn-auth btn-login btn-float">
                                <span class="spinner-border-sm rotate_spinner" role="status" aria-hidden="true" ></span>
                                <span class="spam">Sauvegarder les infos</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center mt-4">
            <small class="text-secondary">BookCase <?php echo strftime('%Y') ?> &copy;</small>

        </div>
    </div>
</div>
@endsection

@section('scriptis')
<script>
    $(document).ready(function() {
        $('.custom-file-input').on('change', function(event) {
            var inputFile = event.currentTarget;
            $(inputFile).parent()
                .find('.custom-file-label')
                .html(inputFile.files[0].name);
        });
    });

</script>
@endsection