@extends('layouts.student_layout')

@section('content_student')
<div class="container margin">
    <div class="row justify-content-center">
            <div class="col-md-9 col-9 offset-md-3 col-lg-12">
                <div class="text-left">
                    <h1 class="title">Etudiants</h1>
                </div>
            </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10" style="margin-top:-20px">
            <div class="card card_create_news">
                <!-- <img src="/storage/uploads/avatars/{{$university->logo_picture}}" alt="" class="avatar avatarforcard_create_new rounded-circle avatar_student_teach_univ"> -->
                
                <div class="card-body mt-5">
                    <div class="tab-content" id="myTabContent">
                        
                        <!-- All Teacher -->
                        <div class="tab-pane fade show active" id="teacher" role="tabpanel" aria-labelledby="teacher-tab">
                           
                            @if($student_belongsTo->count()>0)
                            @foreach($student_belongsTo as $user)
                            <div class="card-header bg-white card-header-avatar text-left" >
                                <img src="/storage/uploads/avatars/{{$user->avatar}}" alt="" class="avatar rounded-circle avatar_student_teach_univ" style="width:50px!important;height:50px!important">
                                <div class="card-pseudo pseudo text-uppercase" style="font-weight:400;margin-left:30px!important ">
                                   {{$user->student->student_name}}
                                </div>
                                <div class="card-date function" style="margin-left:30px!important;font-size:14px important;">
                                        <br>
                                        <span>* Sexe : {{$user->student->sex}}</span>
                                        <span class="pl-5">* {{$user->email}}</span>
                                        <span class="pl-5">* {{$user->student->phone}}</span>
                                </div>
                                <div class="more">
                                    <span class="option"><i class="fa fa-ellipsis-h"></i></span>
                                    <div class="show-option">
                                        <i class="fa fa-comment-o"></i>
                                        <i class="fa fa-eye"></i>
                                        <!-- <i class="fa fa-graduation-cap"></i> -->
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="card-body">
                                <div class="text-center">
                                    <i class="fa fa-info text-info fa-5x pt-5 mt-md-5"></i>
                                    <small>
                                        <p>Vous ne disposez d'aucun étudiant rattaché a votre université </p>
                                        <p>Veuillez incité vos étudiant a utilisé la plateforme et a se rattaché a votre université</p>
                                    </small>
                                </div>
                            </div>
                            @endif
                        </div>
                        <!-- Close All Teacher -->

                    </div>
                </div>                
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
</div>
@endsection
