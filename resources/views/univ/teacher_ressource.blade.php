@extends('layouts.student_layout')

@section('content_student')
<div class="container margin">
    <div class="row justify-content-center">
            <div class="col-md-11 col-11 col-lg-12">
                <div class="text-left">
                    <h1 class="title">Gestion d'instructeurs</h1>
                </div>
            </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10" style="margin-top:-20px">
            <div class="card card_create_news">
                <img src="/storage/uploads/avatars/{{$university->logo_picture}}" alt="" class="avatar avatarforcard_create_new rounded-circle avatar_student_teach_univ">
                
                <div class="card-header" style="box-shadow:0px 1px 0px 0 rgba(0,0,0,.2)">
                   <div class="container">
                        <ul class="nav nav-tabs ml-auto" id="myTab" role="tablist" style="border-bottom:none!important;">
                            <li class="nav-item">
                                <a class="nav-link active nav_link" id="teacher-tab" data-toggle="tab" href="#teacher" role="tab" aria-controls="teacher" aria-selected="true"><i class="fa fa-users"></i>Corps Professoral</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link nav_link" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create" aria-selected="false"><i class="fa fa-plus"></i>Créer Instructeur</a>
                            </li>

                        </ul>
                   </div>
                </div>

                <div class="card-body">
                    <div class="tab-content" id="myTabContent">
                        
                        <!-- All Teacher -->
                        <div class="tab-pane fade show active" id="teacher" role="tabpanel" aria-labelledby="teacher-tab">
                           
                            @if($teacher_univ->count()>0)
                            @foreach($teacher_univ as $user)
                            <div class="card-header bg-white card-header-avatar text-left" >
                                <img src="/storage/uploads/avatars/{{$user->avatar}}" alt="" class="avatar rounded-circle avatar_student_teach_univ" style="width:50px!important;height:50px!important">
                                <div class="card-pseudo pseudo" style="font-weight:400;margin-left:30px!important">
                                   {{$user->teacher->name_teacher}}
                                </div>
                                <div class="card-date function" style="margin-left:30px!important">
                                    {{$user->teacher->title->lampoon}} <br>
                                        <span>* Sexe : {{$user->teacher->sex}}</span>
                                        <span class="pl-5">* {{$user->email}}</span>
                                        <span class="pl-5">* {{$user->teacher->phone}}</span>
                                </div>
                                <div class="more">
                                    <span class="option"><i class="fa fa-ellipsis-h"></i></span>
                                    <div class="show-option">
                                        <i class="fa fa-graduation-cap"></i>
                                        <i class="fa fa-comment-o"></i>
                                        <a href="{{route('show_teacher',$user->teacher->id)}}"><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="card-body">
                                <div class="text-center">
                                    <i class="fa fa-warning text-warning fa-2x"></i>
                                    <small><p>Vous ne disposez d'aucun Instructeur pour le moment.
                                        <br> Veuillez en créer
                                    </p></small>
                                </div>
                            </div>
                            @endif
                        </div>
                        <!-- Close All Teacher -->

                        <!-- Créer Teacher -->
                        <!-- method="POST" action="{{route('teacherstore')}}"  -->
                        <div class="tab-pane fade" id="create" role="tabpanel" aria-labelledby="create-tab">
                            <div class="container pt-md-4">
                              
                                <form id="news_private"  method="POST" action="{{route('teacherstore')}}"  enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row justify-content-center">
                                            <div class="col-md-6">
                                                <label for="name_attach">Nom Instructeur</label>
                                                <input type="text" name="name_attach" id="name_attach" class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="form-group row justify-content-center">
                                            <div class="col-md-6">
                                                <label for="email_attach">Email Instructeur</label>
                                                <input type="email" name="email_attach" id="email_attach" class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="form-group row justify-content-center">
                                            <div class="col-md-6">
                                                <label for="official_number">Matricule / Réference d'identification  <span class="info" title="pedrieieieieiieeieieieieieieieieiieeiei" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span></label>
                                                <input type="text" name="official_number" id="official_number" class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="card-footer bg-white">
                                            <div class="container content-button pb-4 pt-4">
                                                <button type="submit" class="btn btn-primary btn-sm badge btn-save" style="background-color:#6f42c1 !important; border:none !important;"><i class="fa fa-plus"></i> Créer Instructeur</button>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                        <!-- Fin Créer Teacher -->

                    </div>
                </div>                
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
</div>
@endsection

@section('scriptis')
<script>
    $(document).ready(function() {

        $('.custom-file-input').on('change', function(event) {
            var inputFile = event.currentTarget;
            $(inputFile).parent()
                .find('.custom-file-label')
                .html(inputFile.files[0].name);
                 });

        $('#title').change(function(){
            var title = $(this).val();
            $('#title_ap').text("Sujet : " + title.toString());
        });



        // Sa marche, reste la reinitialisation
        // var form_news_public  = document.getElementById('news_public');
        // var request_form = new XMLHttpRequest();

        // form_news_public.addEventListener('submit',function(e){
        //     e.preventDefault();
        //     var formdata  = new FormData(form_news_public);

        //     request_form.open('post',"{{route('newstore')}}");
        //     request_form.send(formdata);
        //     return false;
        // });
    });
</script>
@endsection