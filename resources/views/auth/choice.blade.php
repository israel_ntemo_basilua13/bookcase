@extends('layouts.app',['title'=>'Compte'])

@section('content')

<div class="bg-choice">
    <div class="container" style="position:relative;z-index:6">
        <div class="row justify-content-center">
            <div class="col-10  row_student_teacher">
                <div class="row">
                    <div class="col-12 text-left mb-4">
                        <h1 style="color:#ffff">Choix de Compte</h1>
                        {{-- <h1 style="color:#151370">Choix de Comptes</h1> --}}
                        {{-- <p style="width:450px;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam expedita quod, sint rem, commodi nobis facilis sequi quae, doloremque id quos a excepturi saepe minus!</p> --}}
                    </div>
                </div>
                <div class="card_student_teacher" style="margin-top:-1px;">
                    <div class="row justify-content-center">
                        <div class="col-md-6 col-lg-4">
                            <a href="{{ route('register',['rule_id'=>1]) }}">
                                <div class="card compte_student_teacher compte_etudiant student_account choice_box1">
                                    <div class="card-header">
                                        <span class="icon-sm">
                                            <i class="fa fa-graduation-cap"></i>
                                        </span>
                                        <p class="text-center p_compte_type mb-0" style="font-size: 15px;">Compte étudiant</p>
                                    </div>
                                    <div class="card-body">
                                        <div class="text-left">
                                            <p class="p_compte"><i class="fa fa-check-circle " style="color: #cd5c5c;"></i>Fluidifiez votre documentation</p>
                                            <p class="p_compte"><i class="fa fa-check-circle " style="color: #cd5c5c;"></i>Acquérez différemment vos supports de cours. </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <a href="{{ route('agreement.index') }}">
                                <div class="card compte_student_teacher compte_etudiant student_account choice_box3">
                                    <div class="card-header">
                                        <span class="icon-sm">
                                            <i class="fa fa-bank"></i>
                                        </span>
                                        <p class="text-center p_compte_type mb-0" style="font-size: 15px;">Compte Université/Institut</p>
                                    </div>
                                    <div class="card-body">
                                        <div class="text-left">
                                            <p class="p_compte"><i class="fa fa-check-circle " style="color: #cd5c5c;"></i>Garantissez vos activités universitaires</p>
                                            <p class="p_compte"><i class="fa fa-check-circle " style="color: #cd5c5c;"></i>Postez des actualités </p>
                                            <p class="p_compte"><i class="fa fa-check-circle " style="color: #cd5c5c;"></i>Immortalisez les articles (Mémoire, T.f.c, Thèse, etc...) </p>
                                        </div>
                                    </div>
                                </div>

                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <a href="{{ route('agreement_library') }}">
                                <div class="card compte_student_teacher compte_etudiant student_account choice_box3">
                                    <div class="card-header">
                                        <span class="icon-sm">
                                            <i class="fa fa-book"></i>
                                        </span>
                                        <p class="text-center p_compte_type mb-0" style="font-size: 15px;">Compte Bibliothèque</p>
                                    </div>
                                    <div class="card-body">
                                        <div class="text-left">
                                            <p class="p_compte"><i class="fa fa-check-circle " style="color: #cd5c5c;"></i>Retrouvez un livre parmi tant d'autres</p>
                                            <p class="p_compte"><i class="fa fa-check-circle " style="color: #cd5c5c;"></i>Créer votre coin de lecture des livres favoris</p>
                                        </div>
                                    </div>
                                </div>

                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center mt-1">
            <small class="text-secondary">Bwanya <?php echo strftime('%Y') ?> &copy;</small>
        </div>
    </div>
</div>

@endsection