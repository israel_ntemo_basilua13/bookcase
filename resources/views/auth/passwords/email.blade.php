@extends('layouts.app',['title' => 'Email'])

@section('content')
<div class="bg-choice">
<div class="container">
    <div class="row justify-content-center ">
        <div class="col-md-4 margin">
            <div class="card slideBox1" style="border-radius: 10px;padding:20px 30px;">
                

                <div class="card-body">
                    <div class="text-left mb-4 mt-4">

                        <h2 class="heading title-form" style="font-size: 28px;">Récupération du compte</h2>
                        <small>
                        <p>Vous allez recevoir un lien sur votre email pour réinitialiser votre mot de passe</p>

                        </small>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            

                            <div class="col-md-12">
                                <label for="email" class=" col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                    
                        <button type="submit" class="btn btn-primary btn-block btn-login btn-float">
                                    {{ __('Envoyer') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
