<div style="margin:0px;font-family:'Century Gothic';">
    <div width="100%" style="padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
        <div style="max-width: 500px; padding:50px 0;  margin: 0px auto; font-size: 14px;">

            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px; border : 1px solid ">
                <tbody>
                    <tr>
                        <img src="{{ $message->embed(public_path().'/storage/uploads/logo_icon/logo_acceuil.PNG') }}" class="img-fluid" style="padding-left:12px;">
                        <td style="vertical-align: top; padding-top:10px; padding-left:35%; font-size:20px; font-weight:1px;color: #b2b2b5;">
                            Nouveau utilisateur
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- <p style="position:absolute; top:65px;left:711px; font-size:large;"> BookCase</p> -->

            <div style="padding: 30px; background: #fff; font-weight:100px;">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tbody>
                        <tr>
                            <td><b>Chère/Cher {{ $data['name'] }},</b>
                                <p style="font-weight:100;">
                                    Vous venez de rejoindre la grande famille BookCase, grâce au compte {{ $data['role'] }} .
                                </p>
                                <p style="font-weight:100; margin-top:20px">
                                    Profitez des fonctionnalités tout en sachant, que vous disposez de {{ $data['time'] }} d'utilisation gratuite et, payante après cette période de validité.
                                </p>

                                <div style="text-align: center; margin-top:30px">
                                    <a href="https://virtualcase.000webhostapp.com" style="display: inline-block; padding: 11px 30px; font-size:12px; color: #fff; background:#e74d4e; text-decoration:none;"> Explorer BookCase </a><br>
                                </div>

                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin-top:30px; text-align:center;">
                    <small> La réception de ce courriel implique une inscription réussie au BookCase</small>
                    <p>
                        <b>- L'équipe BookCase</b>
                    </p>
                </div>
            </div>

            <div style="background: #f8f8f8; text-align: center; font-size:12px; color: #b2b2b5; padding: 20px;">
                <p> Virtual BookCase &copy;<?php echo strftime('%Y') ?> </p>
            </div>
        </div>
    </div>
</div>