@extends('layouts.app',['title'=> 'login'])

@section('content')
<div class="bg-choice">
    <div class="container" style="position:relative;z-index:6">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="card login_form" style="border-radius: 25px;">
                    <div class="card-header">
                        <h4>Connexion</h4>
                    </div>
                    <div class="card-body">
                        <div class="icon-login">
                            <i class="fa fa-user"></i>
                        </div>
                        <!-- <div class="text-left mb-4 mt-3">
                            <small>
                                <p>Pas de compte ? <a href="{{ route('choice') }}" style="color:#6f42c1" > Créer maintenant !</a></p>
                            </small>

                        </div> -->
                        <form method="POST" action="{{ route('login') }}" class="">
                            @csrf

                            <div class="form-group row justify-content-center mb-4 mt-4">


                                <div class="col-md-12 col-input">
                                    <label for="email">
                                        Email
                                    </label>
                                    <span class="icon"><i class="fa fa-envelope"></i></span>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Inserez votre email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row justify-content-center">

                                <div class="col-md-12 col-input">
                                    <label for="password">
                                        Mot de passe
                                    </label>
                                    <span class="icon"><i class="fa fa-lock"></i></span>
                                    <div class="input-group">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Inserez votre mot de passe">
                                    </div>

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row justify-content-center ">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <div class="text-left d-flex">
                                            <input class="form-check-input text-secondary" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label " for="remember" style="margin-top:7px;margin-left:7px">
                                                {{ __('Se souvenir de moi') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-2">
                                <div class="col-md-12">
                                    <div class="text-left">
                                        @if (Route::has('password.request'))
                                        <small><a class="" href="{{ route('password.request') }}" style="color:#d74d52;font-size:14px">
                                                {{ __('Mot de passe oublié ?') }}
                                            </a></small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row mb-4">
                                <div class="col-md-12">
                                    <div class="text-left">
                                        <small><a class="" href="{{ route('choice') }}" style="color:#151370;font-size:14px">
                                                {{ __('Créer un compte') }}
                                            </a></small>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-login btn-float">
                                <span class="spinner-border-sm rotate_spinner" role="status" aria-hidden="true" ></span>
                                <span class="spam">Me connecter</span>
                            </button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="text-center mt-5">
            <small class="text-secondary bg-white">Bwanya <?php echo strftime('%Y') ?> &copy;</small>

        </div>
    </div>
</div>
</div>

@endsection