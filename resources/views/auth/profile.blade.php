@inject('notviewsnews','App\Utilities\NotViewNews')
@inject('dataCore','App\Utilities\CoreData')

@section('style')
<link href="{{ asset('css/_style.css') }}" rel="stylesheet">

@endsection

@extends('layouts.student_layout',['title' => 'Profile'])

@section('content_student')
   

    <div class="modal fade" id="exampleModalScrollable" style="margin-top:100px;" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">

        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Liste des institutitons</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    @foreach($universities as $university)
                    <div class="card-header bg-white card-header-avatar text-left">
                        <img src="/storage/uploads/avatars/{{$university->logo_picture}}" alt=""
                            class="avatar rounded-circle avatar_student_teach_univ">
                        <div class="card-pseudo pseudo" style="font-weight:400">
                            {{$university->name_university }}
                        </div>
                        <div class="card-date function">
                            <i class="fa fa-map-marker"></i> {{ Str::limit($university->adress, 15) }}
                        </div>
                        <div class="more">
                            <span class="option"><i class="fa fa-ellipsis-h"></i></span>
                            <div class="show-option">
                                <a href="{{ route('activation_store',$university->id) }}" class="a_non_actif"><i
                                        class="fa fa-graduation-cap" title="S'attacher" data-toggle="tooltip"
                                        data-placement="top"></i></a>
                                <i class="fa fa-heart-o" title="J'aime" data-toggle="tooltip" data-placement="top"></i>
                                <i class="fa fa-eye" title="Voir en detail" data-toggle="tooltip"
                                    data-placement="top"></i>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade margin" id="exampleModal1" tabindex="-1" role="dialog"                aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Importer la photo profil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('profile')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="input-group mt-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary btn-sm btn-files" type="submit"
                                        id="inputGroupFileAddon03">Changer</button>
                                </div>
                                <div class="custom-file">
                                    <input type="file" name="avatar" class="custom-file-input" id="inputGroupFile03"
                                        aria-describedby="inputGroupFileAddon03" accept=".jpg,.jpeg,.png">
                                    <label class="custom-file-label" for="inputGroupFile03" id="strong">Choose
                                        file</label>
                                </div>
                            </div>
                        </div>

                    </form>
                    <div class="text-center">
                        <small class="load-text"></small>
                        <span class="spinner-border-sm load" role="status" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-12 col-lg-12" style="margin-top:120px;z-index:1000;">
       

    <div class="row">
                <div class="col-lg-3">
                    <div class="card card-profil-sm">
                        <div class="card-header">
                            <div class="text-center">
                                <div class="avatar-lg">
                                    <img src="/storage/uploads/avatars/{{ $student->user->avatar }}" alt="img" class="img-cover">
                                    <span class="change-profil">
                                        <i class="fa fa-plus" data-toggle="modal" data-target="#exampleModal1"
                                data-whatever="@mdo"></i>
                                        <!-- <input type="file"> -->
                                        <div class="sm-tooltip">
                                            Modifier mon avatar
                                        </div>
                                    </span>
                                </div>
                                <h5 class="name-user-lg">{{ $student->user->name }}</h5>
                                <p class="mb-0">{{ ucfirst($student->user->rule->rule) }}</p>
                            </div>
                        </div>
                        <hr>
                        <div class="container">
                                <div class="row">
                                    @if($student->user->rule_id !=5)
                                    <div class="col-lg-6">
                                        <div class="text-left">
                                            <h6>Institution</h6>
                                            <p><i class="fa fa-bank"></i> 
                                                @if($activation[0]->university->name_university=='BookCase')
                                                Aucune institution attachée
                                                @else
                                                {{ $activation[0]->university->sigle }}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-lg-6">
                                            <div class="text-left">
                                                <h6>Téléphone</h6>
                                                <p><i class="fa fa-phone"></i> {{$student->phone}}</p>
                                            </div>
                                        </div>
                                </div>
                            </div>
                    </div>
                    <div class="card card-calendar text-center"><div id="monthName"><i class="fa fa-calendar"></i>  {{ $mois_day }}</div> <div id="dayName"> {{ $jour_day }}</div> <div id="dayNumber">  {{ getdate()['mday'] }}</div> <div id="year"> {{ getdate()['year'] }}</div></div>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        @if($student->user->rule_id==4 || $student->user->rule_id==5)
                        <div class="col-lg-3">
                            <div class="card card-compte">
                                <div class="icon">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
                                    </svg>
                                </div>
                                <a href="{{ route('ArticleCollection')}}" class="text-left">
                                    <h1>
                                        {{ $cours->count() }}
                                    </h1>
                                    <h6>Mes cours</h6>
                                </a>
                            </div>
                        </div>
                        @else
                        @endif

                        @if($student->user->rule_id !=5)
                        <div class="col-lg-3">
                                <div class="card card-compte">
                                    <div class="icon" style="background: #d74d522c; color:#d74d52;">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-people" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1h7.956a.274.274 0 0 0 .014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C13.688 10.629 12.718 10 11 10c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 0 0 .022.004zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10c-1.668.02-2.615.64-3.16 1.276C1.163 11.97 1 12.739 1 13h3c0-1.045.323-2.086.92-3zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"/>
                                        </svg>
                                    </div>
                                    <a href="{{ route('univ_teacher')}}">
                                        <h1>
                                         @if(Auth::user()->rule_id==1)
                                        {{ $data_teachers->count() }}
                                        @else
                                        30
                                        @endif
                                        </h1>
                                        <h6>Mes professeurs</h6>
                                    </a>
                                </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="card card-compte">
                                <div class="icon" style="background: #fcc4712c; color:#fce71;">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cart-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                                        <path fill-rule="evenodd" d="M11.354 5.646a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L8 8.293l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                                    </svg>
                                </div>
                                <a href="{{ route('ArticleFavorites')}}"  class="text-left">
                                    <h1>
                                        @if(Auth::user()->rule_id==1)
                                        {{$pays->count()}}
                                        @else
                                        30
                                        @endif
                                    </h1>
                                    <h6>Mes Achats</h6>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="card card-compte">
                                <div class="icon" style="background: #32e4cc1c; color:#32e4cd;">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-download" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                        <path fill-rule="evenodd" d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                                    </svg>
                                </div>
                                <a href="{{ route('ArticleFavorites')}}" class="text-left">
                                    <h1>
                                        @if(Auth::user()->rule_id==1)
                                        {{$articles_downs->count()}}
                                        @else
                                        30
                                        @endif
                                    </h1>
                                    <h6>Mes téléchargements</h6>
                                </a>
                            </div>
                        </div>
                        @endif
                        
                        @if($student->user->rule_id==5)
                        <div class="col-lg-3">
                            <div class="card card-compte">
                                <div class="icon" style="background: #32e4cc1c; color:#32e4cd;">
                                   <svg width="1em" height="1em" viewBox="0 0  16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
                                    </svg>
                                </div>
                                <a href="{{ route('ArticleFavorites')}}" class="text-left">
                                    <h1>
                                        {{$consultations->count()}}
                                    </h1>
                                    <h6>Consultations</h6>
                                </a>
                            </div>
                        </div>
                        @endif

                        @if($student->user->rule_id==1 || $student->user->rule_id==5)
                        <div class="col-lg-3">
                                <div class="card card-compte">
                                    <div class="icon" style="background: #0852942c; color:#085294;">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-rss" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                            <path d="M5.5 12a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
                                            <path fill-rule="evenodd" d="M2.5 3.5a1 1 0 0 1 1-1c5.523 0 10 4.477 10 10a1 1 0 1 1-2 0 8 8 0 0 0-8-8 1 1 0 0 1-1-1zm0 4a1 1 0 0 1 1-1 6 6 0 0 1 6 6 1 1 0 1 1-2 0 4 4 0 0 0-4-4 1 1 0 0 1-1-1z"/>
                                        </svg>
                                    </div>
                                    <a  href="{{route('studentLibrary') }}#librarys" class="text-left">
                                        <h1>
                                        @if($student->user->rule_id==1)
                                        {{ $abonnements->count() }}
                                        </h1>
                                        <h6>Mes abonnements</h6>
                                        @else
                                        {{$dataCore->CountAbon($student)}}
                                        </h1>
                                        <h6>Abonnements</h6>
                                        @endif
                                    </a>
                                </div>
                        </div>
                        @endif

                        @if($student->user->rule_id !=5)
                        <div class="col-lg-9">
                                <div class="card card-calendar">
                                    <div id="monthName" class="pl-4 mt-2" style="text-transform: uppercase;">Statistiques</div>
                                    <div class="block-progress">
                                        <div class="progess-content">
                                            <h6>Téléchagerment éffectué</h6>
                                            @if(Auth::user()->rule_id==1)
                                             <div class="progress">
                                                <div class="bar" style="background: #32E4CD;width: {{round($pourc_down,2)}}%"></div>
                                                <div class="pourcent">
                                                    {{round($pourc_down,2)}}%
                                                </div>
                                            </div>
                                            @else
                                                <div class="progress">
                                                    <div class="bar" style="background: #32E4CD;width: 45%"></div>
                                                    <div class="pourcent">
                                                        45%
                                                    </div>
                                                </div>
                                            @endif

                                        </div>
                                        <div class="progess-content">
                                            <h6>Actualité prise part</h6>
                                            @if(Auth::user()->rule_id==1)
                                             <div class="progress">
                                                <div class="bar" style="background: rgb(252, 196, 113);width:    {{round($pourc_join,2)}}%"></div>
                                                <div class="pourcent">
                                                   {{round($pourc_join,2)}}%
                                                </div>
                                            </div>
                                            @else
                                             <div class="progress">
                                                <div class="bar" style="background: rgb(252, 196, 113);width: 55%"></div>
                                                <div class="pourcent">
                                                    55%
                                                </div>
                                            </div>
                                            @endif
                                        </div>

                                        <div class="progess-content">
                                            <h6>Actualités aimée</h6>
                                            @if(auth::user()->rule_id==1)
                                                <div class="progress">
                                                    <div class="bar" style="background: #d74d52;width: {{round($pourc_like,2)}}%"></div>
                                                    <div class="pourcent">
                                                        {{round($pourc_like,2)}}%
                                                    </div>
                                                </div>
                                                @else
                                                <div class="progress">
                                                    <div class="bar" style="background: #d74d52;width: 35%"></div>
                                                    <div class="pourcent">
                                                        35%
                                                    </div>
                                                </div>
                                            @endif
                                        </div>


                                        <div class="progess-content">
                                            <h6>Achat effectué</h6>
                                            @if(Auth::user()->rule_id==1)
                                              <div class="progress">
                                                <div class="bar" style="background: #6f42c1;width: {{round($pourc_pay,2)}}%"></div>
                                                <div class="pourcent">
                                                    {{round($pourc_pay,2)}}%
                                                </div>
                                            </div>
                                            @else
                                            <div class="progress">
                                                <div class="bar" style="background: #6f42c1;width: 75%"></div>
                                                <div class="pourcent">
                                                    75%
                                                </div>
                                            </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="card card-calendar">
                        <div id="monthName" class="pl-4"><i class="fa fa-edit"></i> Mes cours</div>
                        <div class="content-teacher d-flex">
                            <div class="avatar-teacher">
                                <div class="icon">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                            </div>
                            <div class="text-left">
                                <h6 class="name">Système d'exploitation</h6>
                                <span class="function">Cours</span>
                            </div>
                        </div>
                        <div class="content-teacher d-flex">
                            <div class="avatar-teacher">
                                <div class="icon">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                            </div>
                            <div class="text-left">
                                <h6 class="name">Algorithme</h6>
                                <span class="function">Cours</span>
                            </div>
                        </div>
                        <div class="content-teacher d-flex">
                            <div class="avatar-teacher">
                                <div class="icon">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                            </div>
                            <div class="text-left">
                                <h6 class="name">Base de Données</h6>
                                <span class="function">Cours</span>
                            </div>
                        </div>
                        <div class="content-teacher d-flex">
                            <div class="avatar-teacher">
                                <div class="icon">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                            </div>
                            <div class="text-left">
                                <h6 class="name">Web</h6>
                                <span class="function">Cours</span>
                            </div>
                        </div>
                    </div>
                </div>
                @if($student->user->rule_id==1)
                <div class="col-lg-6">
                    <div class="card card-calendar">
                        <div id="monthName" class="pl-4"><i class="fa fa-user"></i> Mes professeurs</div>
                        
                        @foreach($data_teachers as $teacher)
                        <div class="content-teacher d-flex">
                            <div class="avatar-teacher">
                                <img src="/storage/uploads/avatars/{{$teacher->user->avatar}}" alt="img" class="img-fluid">
                            </div>
                            <div class="text-left">
                                <h6 class="name"> <a href="{{route('show_teacher',$teacher->user->teacher->id)}}">
                                    {{$teacher->user->teacher->name_teacher}}
                                </a></h6>
                                <span class="function">{{$teacher->user->teacher->title->lampoon}}</span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    
                </div>
                @else
                @endif

                </div>
    </div>

@endsection

@section('scriptis')
<script>
    $(document).ready(function() {
        $('.custom-file-input').on('change', function(event) {
            var inputFile = event.currentTarget;
            $(inputFile).parent()
                .find('.custom-file-label')
                .html(inputFile.files[0].name);
        });
    });
</script>

@endsection