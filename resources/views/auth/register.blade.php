@section('style')
<link href="{{ asset('css/_direction.css') }}" rel="stylesheet">
@endsection

@extends('layouts.app',['title' => 'UserCreate'])

@section('content')

    
@if($rule->id==4)
    <div class="modal fade margin" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"  style="z-index: 9999999999999;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Vérification de droit de création</h5>
                    <button type="button" class="close" id="close_modal" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <form method="POST" action="{{route('verif_teacher')}}" enctype="multipart/form-data" id="inquiry">
                        @csrf

                        <div class="form-group row justify-content-center mb-4">
                            <div class="col-md-10">
                                <input id="official_number" type="hidden" class="form-control" name="official_number" required placeholder="Number Identification" value="{{session()->get('reference')->official_number}}">
                                
                                            <div class="card-body">
                                                <div class="text-center">
                                                    <i class="fa fa-warning text-warning fa-2x"></i>
                                                    <small>
                                                        <p >
                                                        @if(is_null(session()->get('reference')->name_attach))
                                                        Salut, <br>
                                                        vous ne possedez aucun référencement <br>
                                                        Veuillez l'avoir avant de poursuivre cette étape.
                                                        @else
                                                        Cher/Chère {{session()->get('reference')->name_attach}}, <br>
                                                        vous avez été réferencé par la réference {{session()->get('reference')->official_number}}.<br>
                                                        Veuillez entrez la clé de création récu pour poursuivre
                                                        @endif

                                                        </p>
                                                    </small>
                                                </div>
                                            </div>

                                <label for="code_bookcase">Code Bookcase</label>
                                <input id="code_bookcase" type="text" class="form-control" name="code_bookcase" required>
                            </div>
                        </div>

                        <div class="card-footer bg-white justify-content-center">
                            <div class="container content-button pb-4 pt-4">
                                <button type="submit" class="col-md-7 btn btn-primary btn-sm badge btn-save" style="background-color:#6f42c1 !important; border:none !important; font-size:.9rem !important;">Vérifier droit </button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <a href="" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" id="a_verif_droit" class="form-group row justify-content-center mb-5 pt-5">
        @if(session()->has('msg'))
        <p class="text-danger" id="verif_too">{{session()->get('msg')}}</p>
        @endif
        <p class="justify-content-center text-center form-group m-5 p-5" id="a_droit_text">
            <div class="card-body">
                <div class="text-center pt-5">
                    <i class="fa fa-warning text-warning fa-4x"></i>
                    <!-- <small> -->
                        <p class="text-white"  style="font-size:2rem:important;">
                            Les champs concernant la page de vérification du droit de création, n'ont pas été bien remplis. <br> Veuillez conformement les remplir, afin de continuer votre inscription.
                        </p>
                    <!-- </small> -->
                </div>
            </div>
        </p>

        
        <div class="col-md-12 justify-content-center text-center pt-5">
            <p class="btn btn-warning text-white">
                Verification de droit de création
            </p>
        </div>
    </a>
@endif
<div class="bg-choice" >
        
        <div class="container" style="margin-top:50px;" >

            <div class="row justify-content-center" id="max-container">

                <div class="col-md-6 col-lg-4">

                    <!-- {{ __('Register') }} -->
                    <ul id="progress">
                        <li class="active">Account Setup</li>
                        <li>Personal Details</li>

                    </ul>
                    <div class="card slideBox1" style="border-radius: 10px;padding:20px 20px; z-index:1;background:white;">
                        <div class="card-body">
                            <div class="text-left mb-4">

                                <h2 class="heading title-form page1" style="font-size: 28px;">{{ucfirst($rule->rule)}} Account</h2>

                            </div>
                        @if($rule->id ==4)

                            <form method="POST" action="{{ route('register',['rule_id' =>$rule->id])}}">

                                @csrf

                                <div class="text-center">
                                    <small>
                                        <i class="fa fa-info" style="color:blue; font-size:18px;"></i>
                                                Cher/Chère <strong>Instructeur</strong>, <br>
                                                veuillez créer votre compte, en attaché a cet email : <strong>{{ session()->get('reference')->email}}.</strong>
                                    </small>
                                    <input id="email" type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{session()->get('reference')->email}}" required autocomplete="email" style="display:none;">
                                </div>

                                <div class="form-group rµow justify-content-center mb-4">
                                    <div class="col-md-12 col-input">
                                        <label for="name">Username</label>
                                        <span class="icon"><i class="fa fa-envelope"></i></span>
                                        <input id="name" type="text" class="form-control  @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row justify-content-center mb-4">
                                    <div class="col-md-12">
                                        <label for="password">Password</label>
                                        
                                        <input id="password" type="password" class="form-control  @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row justify-content-center mb-4">

                                    <div class="col-md-12">
                                        <label for="password-confirm">Confirm Password</label>
                                        <input id="password-confirm" type="password" class="form-control " name="password_confirmation" required autocomplete="new-password">
                                    </div>

                                </div>

                                <button type="submit" class="btn btn-primary btn-login btn-float">
                                        Etape Suivant
                                            <!-- {{ __('Register') }} -->
                                </button>
                            </form>

                        @elseif($rule->id ==2 || $rule->id ==5)
                            @if(session()->has('agreement'))
                            <form method="POST" action="{{ route('register',['rule_id' =>$rule->id])}}" style="">
                                    @csrf
                                    <div class="text-center" style="background: url('/storage/uploads/images/enfin plan2.jpg');">
                                       @if($rule->id ==5)
                                       <small>
                                           <i class="fa fa-info" style="color:blue; font-size:18px;"></i>
                                               Chère Bibliothèque <strong>{{ session()->get('agreement')->Library_name }}</strong>, <br>
                                               veuillez créer un Manager de votre compte, avec comme accès,<br>
                                               l'email: <strong>{{ session()->get('agreement')->email_agreement }}</strong> et le password que vous allez entré
                                       </small>
                                         <input id="email" type="hidden" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ session()->get('agreement')->email_agreement }}" required autocomplete="email">
                                       <input id="name" type="hidden" class="form-control  @error('name') is-invalid @enderror" name="name" value="{{ session()->get('agreement')->Library_name }}" required autocomplete="name" autofocus>
                                       @else
                                       <small>
                                           <i class="fa fa-info" style="color:blue; font-size:18px;"></i>
                                               Chère Institution/Université <strong>{{ session()->get('agreement')->name_institution }}</strong>, <br>
                                               veuillez créer un Manager de votre compte, avec comme accès,<br>
                                               l'email: <strong>{{ session()->get('agreement')->email_agreement }}</strong> et le password que vous allez entré
                                       </small>
                                       <input id="email" type="hidden" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ session()->get('agreement')->email_agreement }}" required autocomplete="email">
                                       <input id="name" type="hidden" class="form-control  @error('name') is-invalid @enderror" name="name" value="{{ session()->get('agreement')->name_institution }}" required autocomplete="name" autofocus>
                                       @endif
                                    </div>

                                    <!-- <div class="form-group row justify-content-center mb-4">

                                        <div class="col-md-12">
                                            <label for="name">Username</label>
                                            <input id="name" type="text" class="form-control  @error('name') is-invalid @enderror" name="name" value="{{ session()->get('agreement')->name_institution }}" required autocomplete="name" autofocus>
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row justify-content-center mb-4">
                                        <div class="col-md-12">
                                            <label for="email">Email</label>
                                            <input id="email" type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="" required autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div> -->


                                    <div class="form-group row justify-content-center mb-4">
                                        <div class="col-md-12">
                                            <label for="password">Password</label>
                                            <input id="password" type="password" class="form-control  @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                            <input id="agreem_id" type="text" style="display:none;"  name="agreem_id" value="{{ session()->get('agreement')->id }}">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row justify-content-center mb-4">

                                        <div class="col-md-12">
                                            <label for="password-confirm">Confirm Password</label>
                                            <input id="password-confirm" type="password" class="form-control " name="password_confirmation" required autocomplete="new-password">
                                        </div>

                                    </div>

                                    <button type="submit" class="btn btn-primary btn-login btn-float">
                                            Etape Suivant
                                                <!-- {{ __('Register') }} -->
                                    </button>

                            </form>
                            @else
                                <div class="text-center" style="background: url('/storage/uploads/images/enfin plan2.jpg');">
                                    <i class="fa fa-info" style="font-size:200px;"></i>
                                    <p>
                                        Vous ne posez pas de droit de creation de ce type de compte. <br> <br>
                                    <small>Veuillez soumettre une demande, pour créer un compte de type {{ ucfirst($rule->rule)}} </small>
                                    </p>
                                </div>
                            @endif
                        @else
                        <form method="POST" action="{{ route('register',['rule_id' =>$rule->id])}}" style="">
                                @csrf
                                <div class="form-group row justify-content-center mb-4">


                                    <div class="col-md-12 col-input">
                                        <label for="name">Username</label>
                                        <span class="icon"><i class="fa fa-user"></i></span>
                                        <input id="name" type="text" class="form-control  @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Inserez votre Username">

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row justify-content-center mb-4">

                                    <div class="col-md-12 col-input">
                                        <label for="email">Email</label>
                                        <span class="icon"><i class="fa fa-envelope"></i></span>
                                        <input id="email" type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Inserez votre email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row justify-content-center mb-4">
                                    <div class="col-md-12 col-input">
                                        <label for="password">Password</label>
                                        <span class="icon"><i class="fa fa-lock"></i></span>
                                        <input id="password" type="password" class="form-control  @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Inserez votre mot de passe">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row justify-content-center mb-4">

                                    <div class="col-md-12 col-input">
                                        <label for="password-confirm">Confirm Password</label>
                                        <span class="icon"><i class="fa fa-lock"></i></span>
                                        <input id="password-confirm" type="password" class="form-control " name="password_confirmation" required autocomplete="new-password" placeholder="Inserez votre mot de passe">
                                    </div>

                                </div>

                                <button type="submit" class="btn btn-primary btn-login btn-float">
                                        Etape Suivant
                                            <!-- {{ __('Register') }} -->
                                </button>
                        </form>
                        @endif
                        </div>
                    </div>

                </div>
           
            </div>

        </div>

        <div class="text-center pt-5">
            <!-- @if(session()->has('agreement'))
                    <p class="text-danger" id="verif_too" style="display:none;" >{{session()->get('agreement')->id}}</p>
                @endif -->
                <small class="text-secondary bg-white">BookCase <?php echo strftime('%Y') ?> &copy;</small>
        </div>
</div>
@endsection

@section('scriptis')
<script>
    $(document).ready(function() {

        @if($rule->id ==4)
                if ($('#verif_too').text() == "created") {

                    $('#max-container').show();
                    $('#a_verif_droit').hide();

                } else {

                    $('#a_verif_droit').trigger('click');
                    $('#max-container').hide();
                    $('#a_verif_droit').show();
                }
        @endif

        // $('#exampleModal').mouseleave(function() {

        //     if ($('#verif_too').text() == "created") {

        //         $('#container-max').show();
        //         $('#a_verif_droit').hide();

        //     } else {

        //         // $('#a_verif_droit').trigger('click');
        //         $('#container-max').hide();
        //         $('#a_verif_droit').show();
        //     }
        // });




        // alert($session()->get('msg')}})

        // $('#container-max').hide();
        // if (Session::has('success') != "created") {

        //     $('#exampleModal').show();

        // }
        // $('#btn_verif_droit').click(function() {

        //     $data = new Array();

        //     $data["number"] = $('#official_number').val();
        //     $data["code"] = $('#code_bookcase').val();


        //     // alert($data["code"]);

        //    


        // });


        // var form_news_public  = document.getElementById('inquiry');
        // var request_form = new XMLHttpRequest();

        // form_news_public.addEventListener('submit',function(e){
        //     e.preventDefault();
        //     var formdata  = new FormData(form_news_public);

        //     request_form.open('post',"{{route('verif_teacher',1)}}");
        //     request_form.send(formdata);
        //     // return false;
        // });

    });
</script>
@endsection