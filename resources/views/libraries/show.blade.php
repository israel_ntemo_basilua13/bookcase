@inject('dataCore','App\Utilities\CoreData')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')

@section('content_student')
<div class="col-md-12 card_file margin" style="margin-top:100px">
    <div class="card card-biblo card-biblo-lg">
        <div class="card-img card-img-lg">
            <img src="/storage/uploads/images/{{$lib->picture}}" alt="img" class="img-cover">
            <div class="avatar-biblo avatar-biblo-lg">
                <img src="/storage/uploads/avatars/{{$lib->logo}}" alt="img">
            </div>
        </div>
        <div class="card-body">
            <div class="text-center">
                <h6><i class="fa fa-check"></i>{{$lib->name_library}}</h6>
                <br>
                <small class="mb-0 ml-1"><i class="fa fa-globe mr-1"></i><a href="" class="text-small">{{$lib->website}}</a></small>
                <br>
                <span class="phone ml-1"><i class="fa fa-phone mr-1"></i>{{$lib->phone}}</span>
            </div>
            <div class="text-center">
                @if($dataCore->AbonUser($lib) != 0)
                <small class="float-left my-2 mx-2 p-1 bg-danger text-white" style="border-radius:10px;">{{$dataCore->AbonUser($lib)}} abonn.</small>
                @endif
                <div class="network-biblo">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <hr>
            <div class="d-flex">
                <input type="text" class="form-control" placeholder="Recherche...">
                <div class="abonne text-center">
                    <span class="number">{{$dataCore->CountAbon($lib)}}</span>
                    <br>
                    <span>{{$dataCore->CountAbon($lib) <= 1 ? 'Abonné' : 'Abonnés'}}</span>
                </div>
                <div class="article text-center">
                    <span class="number">{{ $lib->articles->count() }}</span>
                    <br>
                    <span>Ouvrages</span>
                </div>
                <a href="{{route('abonnements',$lib->id)}}" class="btn-primary btn-sm" style="height: 30px;margin-top: 7px;">S'abonner</a>
            </div>

        </div>
    </div>
    {{-- // Library --}}
    <div class="row">
        @if($lib->articles->count()>0)
        @foreach($lib->articles as $artic)
        <div class="col-lg-3 col-sm-6 col-md-6">
            <div class="card card-book">
                <div class="edition">
                    Edition {{ $artic->edition}}
                </div>
                <div class="text-left">
                    <div class="icon-book">
                        <i class="fa fa-book"></i>
                    </div>
                    <h6>{{ $artic->article_name}}</h6>
                    <p> Emis,
                        @if ( ceil(abs( strtotime(date("Y-m-d",strtotime( $artic->created_at) )) -
                        strtotime(date("Y-m-d")) ) / 86400) > 0)
                        il y a

                        {{ ceil(abs(strtotime(date("Y-m-d",strtotime($artic->created_at))) - strtotime(date("Y-m-d"))) / 86400) }}
                        {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($artic->created_at))) - strtotime(date("Y-m-d"))) / 86400)) }}

                        @else
                        aujourd'hui
                        @endif
                    </p>
                    <hr>
                    <p>
                        <span class="avatar">
                            <img src="/storage/uploads/images/bg-1.jpg" alt="img">
                        </span>
                        <span>{{ $artic->author}}</span>
                    </p>
                    @if($dataCore->ValidateViewArticle($lib) == true)
                    <button class="btn btn-sm">Lire le contenu</button>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
        @endif
    </div>

    <div class="opacity-fond">
        <span>
            <i class="fa fa-times"></i>
        </span>
    </div>


    @endsection


    @endsection


    @section('script_article')
    <script>
        $(function() {

            $("#search_article").keyup(function() {

                var search = $(this).val();

                search = $.trim(search);

                $(".title_article").removeClass("color_v");
                $(".title_article").parent().parent().parent().parent().css("display", "none");


                if (search != "") {

                    $(".title_article:contains('" + search + "')").addClass("color_v");
                    $(".title_article:contains('" + search + "')").show();
                    $(".title_article:contains('" + search + "')").parent().parent().parent().parent().css("display", "flex");

                } else {
                    $(".title_article").removeClass("color_v");
                    $(".title_article").parent().parent().parent().parent().css("display", "flex");
                }


            });
        });
    </script>
    @endsection