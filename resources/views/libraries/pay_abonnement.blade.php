@inject('dataCore','App\Utilities\CoreData')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')

@section('content_student')
<div class="col-md-12 card_file margin">

    <div class="row justify-content-center">
        <div class="col-10 col-md-11 col-sm-10 col-lg-12 ml-md-5 pl-4 mt-md-2 mb-5">
            <p class="text-justify h2 p_titile_abonn title">Choisissez le bundle de votre choix</p>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row justify-content-center">
            @foreach($abonnements as $abonn)
                <div class="col-lg-3">
                    <div class="card card-art" style="border:1px solid #0E0C57 !important;">
                        <div class="card-header d-flex"  style="background: #0E0C57 !important;">
                            <div class="avatar-auteur">
                                <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                            </div>
                            <div class="content-auteur text-white">
                                <h5 class="text-white">{{$abonn->name_abonnement}}</h5>
                            </div>
                            <div class="icon">
                                <i class="fa fa-file-pdf-o"></i>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="text-left">
                                <span class="category">{{$abonn->library->name_library}}</span>
                                <br>
                                <span class="type">{{$abonn->type_library_abonnement->type_name}}</span>
                                <br>
                                @if($abonn->promotion==1)
                                <span class="badge badge-primary">Promotion</span>
                                @endif
                                <br>
                                <p class="mb-0 text-right payant bg-danger" style="font-size:22px;color:#ffff !important;">
                                {{$abonn->price}}@if($abonn->devise =="USD")$
                                @else
                                Fc
                                @endif
                                   / {{$abonn->number_mouth}} Mois
                                </p>
                                <span style="display:block;margin-top:5px"><i class="fa fa-users mr-2"></i>{{$abonn->abonnements->count()}}
                                    {{$abonn->abonnements->count() > 1 ? 'abonnés' : 'abonné'}}</span>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#Modalabonnement"> S'abonner</button>
                        </div>
                    </div>
                </div>

                {{-- // modal --}}
                <div class="modal fade" id="Modalabonnement" style="margin-top:25px; z-index:1 !important;" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document" style="width:200% !important;">
                        <div class="modal-content" style="width:200% !important; max-width:200%;">
                            <div class="modal-header">
                                <h6 class="modal-title ml-3" id="exampleModalLabel">Paiement Abonnement Library</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:-15px !important;">
                                    <small><i class="fa fa-close"></i></small>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row justidy-content-center">
                                        <div class="col-lg-12">
                                            <div class="card card-check">
                                                <div class="card-body">
                                                    <ul class="nav nav-tabs text-center" id="myTab" role="tablist">
                                                        <li class="nav-item" role="presentation">
                                                            <a class="nav-link active nav-money" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                                                aria-controls="home" aria-selected="true">
                                                                <img src="{{asset('storage/uploads/images/mpesa.jpg')}}" alt="">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" role="presentation">
                                                            <a class="nav-link nav-money" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                                                aria-controls="profile" aria-selected="false">
                                                                <img src="{{asset('storage/uploads/images/AIRTELLOGO.png')}}" alt="">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" role="presentation">
                                                            <a class="nav-link nav-money" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                                                aria-controls="contact" aria-selected="false">
                                                                <img src="{{asset('storage/uploads/images/47.png')}}" alt="">
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <hr>
                                                    <div class="tab-content" id="myTabContent">
                                                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                            <form action="{{route('pay_abonnement')}}" method="POST">
                                                                @csrf
                                                                <div class="form-group row">
                                                                    <div class="col-lg-12 mt-3">
                                                                        <select type="text" class="form-control form-control-panier" name="timing_abonnement">
                                                                            <option value="1">
                                                                                1 mois
                                                                            </option>
                                                                            <option value="2">
                                                                                2 mois
                                                                            </option>
                                                                            <option value="4">
                                                                                4 mois
                                                                            </option>
                                                                            <option value="12">
                                                                                1 année
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" id="col-1">
                                                                        
                                                                        <input type="text" class="form-control form-control-panier"
                                                                            placeholder="Inserer le numéro de téléphone" name="num">
            
                                                                        {{-- // les données en arrière plan --}}
                                                                        <input type="hidden" name="price" value="{{$abonn->price}}">
                                                                        <input type="hidden" name="abonnement_price_id" value="{{$abonn->id}}">
                                                                        {{-- // End les données en arrière plan --}}
                                                                    </div>
            
                                                                    <div class="col-lg-12 mt-3">
                                                                        <select type="text" class="form-control form-control-panier" name="devise">
                                                                            <option value="CDF">
                                                                                CDF
                                                                            </option>
                                                                            <option value="USD">
                                                                                USD
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12 mt-3">
                                                                        <input type="hidden" value="mpesa" name="provider">
                                                                        <button class="btn-primary py-2 px-3 float-right" name="btn-valide">Valider</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                            <form action="{{route('pay_abonnement')}}" method="POST">
                                                                @csrf
                                                                <div class="form-group row">
                                                                    <div class="col-lg-12 mt-3">
                                                                        <select type="text" class="form-control form-control-panier" name="timing_abonnement">
                                                                            <option value="1">
                                                                                1 mois
                                                                            </option>
                                                                            <option value="2">
                                                                                2 mois
                                                                            </option>
                                                                            <option value="4">
                                                                                4 mois
                                                                            </option>
                                                                            <option value="12">
                                                                                1 année
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" id="col-1">
                                                                        <input type="text" class="form-control form-control-panier"
                                                                            placeholder="Inserer le numéro de téléphone" name="num">
            
                                                                        {{-- // les données en arrière plan --}}
                                                                        <input type="hidden" name="price" value="{{$abonn->price}}">
                                                                        <input type="hidden" name="devise" value="{{$abonn->devise}}">
                                                                        {{-- // End les données en arrière plan --}}
                                                                    </div>
            
                                                                    <div class="col-lg-12 mt-3">
                                                                        <select type="text" class="form-control form-control-panier" name="devise">
                                                                            <option value="CDF">
                                                                                CDF
                                                                            </option>
                                                                            <option value="USD">
                                                                                USD
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12 mt-3">
                                                                        <input type="hidden" value="airtelmoney" name="provider">
                                                                        <button class="btn-primary py-2 px-3 float-right" name="btn-valide">Valider</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                                            <form action="{{route('pay_abonnement')}}" method="POST">
                                                                @csrf
                                                                <div class="form-group row">
                                                                    <div class="col-lg-12 mt-3">
                                                                        <select type="text" class="form-control form-control-panier" name="timing_abonnement">
                                                                            <option value="1">
                                                                                1 mois
                                                                            </option>
                                                                            <option value="2">
                                                                                2 mois
                                                                            </option>
                                                                            <option value="4">
                                                                                4 mois
                                                                            </option>
                                                                            <option value="12">
                                                                                1 année
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" id="col-1">
                                                                        <input type="text" class="form-control form-control-panier"
                                                                            placeholder="Inserer le numéro de téléphone" name="num">
            
                                                                        {{-- // les données en arrière plan --}}
                                                                        <input type="hidden" name="price" value="{{$abonn->price}}">
                                                                        <input type="hidden" name="devise" value="{{$abonn->devise}}">
                                                                        {{-- // End les données en arrière plan --}}
                                                                    </div>
            
                                                                    <div class="col-lg-12 mt-3">
                                                                        <select type="text" class="form-control form-control-panier" name="devise">
                                                                            <option value="CDF">
                                                                                CDF
                                                                            </option>
                                                                            <option value="USD">
                                                                                USD
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12 mt-3">
                                                                        <input type="hidden" value="orangemoney" name="provider">
                                                                        <button class="btn-primary py-2 px-3 float-right" name="btn-valide">Valider</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- // End modal --}}
            @endforeach

        </div>
    </div>

</div>

@endsection
