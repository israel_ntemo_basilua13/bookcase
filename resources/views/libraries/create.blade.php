@section('style')
<link href="{{ asset('css/_style.css') }}" rel="stylesheet">
@endsection

@extends('layouts.app',['title' => 'app'])

@section('content')
<div class="bg-choice mb-4" style="margin-top: 100 !important;">
    <div class="container">
        
        <div class="row justify-content-around">
            <div class="col-md-10 col-lg-5">
                <ul id="progress">
                    <li>Account Setup</li>
                    <li class="active">Personal Details</li>

                </ul>

                <div class="card slideBox1" style="border-radius: 10px;padding:20px 30px;background:white;z-index: 1;">
                    <div class="text-left mb-4">

                        <h2 class="heading title-form page2" style="font-size: 28px;">Library Account</h2>

                    </div>

                    <div class="card-body">
                        
                        <form method="POST" action="{{route('library.store')}}" enctype="multipart/form-data">
                            @csrf
                            
                            <div class="form-group row mb-4">
                                <div class="col-md-6">
                                    <label for="file_picture">Picture</label>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="file_picture" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" accept=".jpg,.jpeg,.png">
                                            <label class="custom-file-label" for="inputGroupFile03">Importer un fichier</label>
                                            <span class="btn-file"><i class="fa fa-image"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                   <label for="file_logo">logo</label>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="file_logo" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" accept=".jpg,.jpeg,.png">
                                            <label class="custom-file-label" for="inputGroupFile03">Importer un fichier</label>
                                            <span class="btn-file"><i class="fa fa-image"></i></span>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                           
                            <div class="form-group row mb-4">
                                <div class="col-md-12">
                                    <label for="file_picture">Phone</label>
                                    <div class="input-group mb-3">
                                        <input type="text" name="phone" class="form-control"  required autocomplete="phone">
                                    </div>
                                </div>
                                <input type="hidden" id="agreem_id"  name="agreem_id" value="{{$agreement->id}}">
                                <input type="hidden" class="form-control" name="name_library" value="{{$user->name}}" required>
                                <input type="hidden" class="form-control" name="email" value="{{$user->email}}" required>
                            </div>

                            <div class="form-group row  mb-4">
                                <div class="col-md-12">
                                    <label for="describe">adresse</label>
                                    <textarea name="adress" id="describe" class="form-control" required></textarea>
                                    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                                </div>
                            </div>
                            <div class="form-group row  mb-4">
                                <div class="col-md-12">
                                    <label for="describe">Description</label>
                                    <textarea name="describe" id="describe" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group row  mb-4">
                                <div class="col-md-12">
                                    <label for="file_picture">Website</label>
                                    <div class="input-group mb-3">
                                        <input type="text" name="website" class="form-control" >
                                    </div>
                                </div>
                            </div>
                         
                            <button type="submit" class="btn btn-primary btn-block btn-auth btn-login btn-float">
                                <span class="spinner-border-sm rotate_spinner" role="status" aria-hidden="true" ></span>
                                <span class="spam">Sauvegarder les infos</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center mt-4">
            <small class="text-secondary">BookCase <?php echo strftime('%Y') ?> &copy;</small>

        </div>
    </div>
</div>
@endsection

@section('scriptis')
<script>
    $(document).ready(function() {
        $('.custom-file-input').on('change', function(event) {
            var inputFile = event.currentTarget;
            $(inputFile).parent()
                .find('.custom-file-label')
                .html(inputFile.files[0].name);
        });
    });

</script>
@endsection