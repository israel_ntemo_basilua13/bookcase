@extends('layouts.student_layout')

@section('content_student')
<div class="container-fluid margin">
    <div class="row-translate" style="margin-top: 30px">
        <div class="row">
            <!-- <div class="col-md-2">
                <div class="text-left">
                    <h1 class="title mb-4">Dashboard</h1>
                </div>
                <div class="card">
                    <div class="card-header text-left bg-blue text-white bg-blue">2021 <i class="fa fa-calendar"></i></div>
                    <div class="list-group" style="border:none;">
                        <div class="text-left">

                            <a href="#" class="list-group-item list-group-item-action">Janvier</a>
                            <a href="#" class="list-group-item list-group-item-action">Février</a>
                            <a href="#" class="list-group-item list-group-item-action">Mars</a>
                            <a href="#" class="list-group-item list-group-item-action">Avril</a>
                            <a href="#" class="list-group-item list-group-item-action">Mai</a>
                            <a href="#" class="list-group-item list-group-item-action">Juin</a>
                            <a href="#" class="list-group-item list-group-item-action">Juillet</a>
                            <a href="#" class="list-group-item list-group-item-action">Août</a>
                            <a href="#" class="list-group-item list-group-item-action">Septembre</a>
                            <a href="#" class="list-group-item list-group-item-action">Octobre</a>
                            <a href="#" class="list-group-item list-group-item-action">Novembre</a>
                            <a href="#" class="list-group-item list-group-item-action active">Decembre</a>
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="col-lg-3">
                <div class="card card-profil-sm">
                    <div class="card-header">
                        <div class="text-center">
                            <div class="avatar-lg">
                                <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-cover">
                                <span class="change-profil">
                                    <i class="fa fa-plus"></i>
                                    <input type="file">
                                    <div class="sm-tooltip">
                                        Modifier mon avatar
                                    </div>
                                </span>
                            </div>
                            <h5 class="name-user-lg">{{$library->name_library}}</h5>
                            <p class="mb-0">{{ $library->website }}</p>
                        </div>
                    </div> 
                    <hr> 
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="text-center">
                                    <h6>Téléphone</h6>
                                    <p><i class="fa fa-phone mr-2"></i>{{ $library->phone }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-calendar text-center">
                    <div id="monthName">
                        <i class="fa fa-calendar"></i>
                        {{ $mois_day }}
                    </div>
                    <div id="dayName">
                        {{ $jour_day }}
                    </div>
                    <div id="dayNumber">
                        {{ getdate()['mday'] }}
                    </div>
                    <div id="year">
                        {{ getdate()['year'] }}
                    </div>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row justify-content-center">
                    <div class="col-md-3">
                        <div class="card card-compte">
                            <div class="icon">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-book">
                                    <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"></path>
                                </svg>
                            </div>
                            <div class="text-left">
                                <h1>{{$db_articles->count()}}</h1>
                                <h6>Mes articles</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card card-compte">
                            <div class="icon" style="background: rgba(215, 77, 82, 0.173) none repeat scroll 0% 0%; color: rgb(215, 77, 82);">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-people">
                                    <path fill-rule="evenodd" d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1h7.956a.274.274 0 0 0 .014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C13.688 10.629 12.718 10 11 10c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 0 0 .022.004zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10c-1.668.02-2.615.64-3.16 1.276C1.163 11.97 1 12.739 1 13h3c0-1.045.323-2.086.92-3zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"></path>
                                </svg>
                            </div>
                            <div class="text-left">
                                <h1>{{$db_collections->count()}}</h1>
                                <h6>Mes dossiers</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card card-compte">
                            <div class="icon" style="background: rgba(252, 196, 113, 0.173) none repeat scroll 0% 0%; color: rgb(252, 196, 113);">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-cart-check">
                                    <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"></path>
                                    <path fill-rule="evenodd" d="M11.354 5.646a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L8 8.293l2.646-2.647a.5.5 0 0 1 .708 0z"></path>
                                </svg>
                            </div>
                            <div class="text-left">
                                <h1>{{$db_revenus}} $</h1>
                                <h6>Mes revenus</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card card-compte">
                            <div class="icon" style="background: rgba(50, 228, 204, 0.11) none repeat scroll 0% 0%; color: rgb(50, 228, 205);">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"  class="bi bi-download">
                                    <path fill-rule="evenodd" d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"></path>
                                    <path fill-rule="evenodd" d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"></path>
                                </svg>
                            </div>
                            <div class="text-left">
                                <h1>{{$art_students->count()}}</h1>
                                <h6>Mes étudiants</h6>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-calendar">
                            <div id="monthName" class="pl-4">
                                <i class="fa fa-book"></i>
                                Mes nouveaux articles
                            </div>

                            @foreach($db_new_articles as $new_article)
                            <div class="content-teacher d-flex">

                                @if($new_article->status_id==1)
                                    <div class="type line">
                                        En ligne
                                @else
                                        <div class="type offline">
                                        Brouillon
                                @endif
                                <!-- <div class="type line"> -->
                                </div>
                                <div class="avatar-teacher">
                                    <div class="icon">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </div>
                                </div>
                                <div class="text-left">
                                    <h6 class="name">{{$new_article->article_name}}</h6>
                                    <span class="function">Edition {{$new_article->edition}}</span>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card card-calendar">
                            <div id="monthName" class="pl-4">
                                <i class="fa fa-list"></i>
                                Recentes consultations
                            </div>
                            @foreach($students_views as $stud)
                            <div class="content-teacher d-flex">
                                <div class="type">
                                    <span>Dernier : </span>
                                    @if ( ceil(abs( strtotime(date("Y-m-d",strtotime($stud->articles->last()->created_at) )) -
                                    strtotime(date("Y-m-d")) ) / 86400) > 0)
                                    il y a

                                    {{ ceil(abs(strtotime(date("Y-m-d",strtotime($stud->articles->last()->created_at))) - strtotime(date("Y-m-d"))) / 86400) }}
                                    {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($stud->articles->last()->created_at))) - strtotime(date("Y-m-d"))) / 86400)) }}

                                    @else
                                      aujourd'hui
                                    @endif

                                </div>
                                <div class="avatar-teacher">
                                    <div class="icon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                <div class="text-left">
                                    <h6 class="name">{{ $stud->student->student_name }}</h6>
                                    @if($stud->activations->sortByDesc('created_at')->first()->university->name_university!='BookCase')
                                    <span class="function pl-2">
                                            <i class="fa fa-bank"></i>
                                         {{$stud->activations->sortByDesc('created_at')->first()->university->name_university}}
                                    </span>
                                    @else
                                    <span class="function pl-2 text-secondary">
                                        Attaché a aucune université
                                    </span>
                                    @endif
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>


@endsection