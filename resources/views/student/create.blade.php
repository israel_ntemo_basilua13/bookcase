@section('style')
<link href="{{ asset('css/_style.css') }}" rel="stylesheet">
@endsection

@extends('layouts.app',['title' => 'Student'])

@section('content')
<div class="bg-choice">
    <div class="container">
        
        <div class="row justify-content-around">
            <div class="col-md-10 col-lg-4 margin" style="margin-top: 40px !important;">
            <ul id="progress">
                    <li>Account Setup</li>
                    <li class="active">Personal Details</li>

                </ul>

                <div class="card slideBox1" style="border-radius: 10px;padding:20px 30px;z-index:1;background:white;">
                    <div class="text-left mb-4">

                        <h2 class="heading title-form page2" style="font-size: 28px;">{{$user->rule->rule}} Account</h2>

                    </div>

                    <div class="card-body">
                        
                        <form method="POST" action="{{ route('student.store')}}">
                            @csrf
                            <div class="form-group row mb-4">
                                
                                <div class="col-md-12">
                                    <label for="student_name">Nom Complet</label>
                                    <input id="student_name" type="text" class="form-control" name="student_name" required  value="{{ old('student_name')}}">
                                    {!! $errors->first('student_name','<p class="error-msg">:message</p>') !!}
                                    <!-- @error('student_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror -->
                                </div>
                            </div>

                            <div class="form-group row  mb-4 ml-2">


                                <label for="sex" class="col-md-6 text-left col-6 col-lg-6">
                                    <input type="radio" name="sex" id="sex" class="form-check-input " checked="true" value="Homme">Homme <i class="fa fa-male"></i>
                                </label>

                                <label for="sex" class="col-md-6  form-check-label text-left col-6 col-lg-6">
                                    <input type="radio" name="sex" id="sex" class="form-check-input" value="Femme">Femme <i class="fa fa-female"></i>
                                </label>

                                @error('sex')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <!-- <div class="form-group form-check-inline">
                           
                        </div> -->


                            <div class="form-group row  mb-4">
                                
                                <div class="col-md-12">
                                    <label for="phone">Téléphone</label>
                                    <input id="phone" type="tel" class="form-control" name="phone"  required autocomplete="phone">
                                </div>

                            </div>

                            <div class="form-group row  mb-4">
                                
                                <div class="col-md-12">
                                    <label for="domain_exploitation">domain d'étude <span class="info" title="pedrieieieieiieeieieieieieieieieiieeiei" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span></label>
                                    <textarea name="domain_exploitation" id="domain_exploitation" class=" @error('domain_exploitation') is-invalid @enderror form-control" required></textarea>
                                    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                                    @error('domain_exploitation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="text-left">
                                <small>En cliquant sur Sauvegarder les infos, I agree that I have read and accepted the <span style="color:#6f42c1">Terms of Use</span> and <span style="color:#6f42c1">Privacy Policy</span>.</small>
                            </div>
                            <!-- <div class="form-group row ">
                                
                                <div class="col-md-12 mb-4">
                                    <label for="type_student">Type student</label>
                                    <select class="form-control" id="type_student_id" name="type_student_id">
                                        @foreach($type_etudiant as $etudiant)
                                        <option value="{{$etudiant->id }}">{{$etudiant->type_student }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                            </div> -->
                            <button type="submit" class="btn btn-primary btn-block btn-auth btn-login btn-float">
                                <span class="spinner-border-sm rotate_spinner" role="status" aria-hidden="true" ></span>
                                <span class="spam">Sauvegarder les infos</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center mt-4">
            <small class="text-secondary">BookCase <?php echo strftime('%Y') ?> &copy;</small>

        </div>
    </div>
</div>
@endsection