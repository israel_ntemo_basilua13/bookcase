@inject('notviewsnews','App\Utilities\NotViewNews')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')

@section('content_student')

<div class="col-md-12  col-sm-12 col-lg-12 col-xl-12 margin no-padding">
    <!-- fisrt enfant a completer le margin top -->
    <div class="row justify-content-center mt-4">
        {{-- // block détails concernant le compte étudiant --}}
        <div class="col-xl-3">
            <div class="card card-profil-sm">
                <div class="card-header text-center">
                    <div class="avatar-lg">
                        <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-cover">
                    </div>
                    <h5 class="name-user-lg">{{$user_student->user->name}}</h5>
                    <p class="mb-0">{{ucfirst($user_student->user->rule->rule)}}</p>
                    {{-- <span style="font-size:14px"><i class="fa fa-bank"></i>{{ucfirst($user_student->user->activations[0]->university->name_university)}}</span>
                    --}}
                    <span style="font-size:14px"><i class="fa fa-bank mr-1"></i>{{ucfirst($user_student->user->activations->last()->university->sigle)}}</span>
                    <a href="{{ route('profile') }}" class=" btn-secondary btn-sm mt-2">Voir mon compte</a>
                </div>
                <div class="card-body text-center text-white">
                    <div class="d-flex justify-content-around">
                        <a href="{{ route('article_show',['type'=>'all']) }} " class="dowload">
                            <span class="number">20</span>
                            <br>
                            <span><i class="fa fa-download"></i></span>
                        </a>
                        <a href="{{ route('ArticleFavorites')}}" class="dowload">
                            <span class="number">{{$support_pays->count()}}</span>
                            <br>
                            <span>
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
                                </svg>
                            </span>
                        </a>
                        <!-- <div class="dowload">
                            <span class="number">15</span>
                            <br>
                            <span><i class="fa fa-file-pdf-o"></i></span>
                        </div> -->
                        <a href="{{ route('ArticleFavorites')}}" class="dowload">
                            <span class="number">{{$favourites->count()}}</span>
                            <br>
                            <span><i class="fa fa-bookmark"></i></span>
                        </a>
                    </div>
                </div>
            </div>

            {{-- <div class="row">
                <div class="col-lg-6">
                    <div class="card card-calendar text-center">
                        <div id="monthName"><i class="fa fa-calendar"></i> Mars</div>
                        <div id="dayName">Lundi</div>
                        <div id="dayNumber">08</div>
                        <div id="year">2021</div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-calendar text-center">
                        <div id="monthName"><i class="fa fa-edit"></i> Mes cours</div>
                        <div id="dayNumber" style="font-size:25px">00</div>
                    </div>
                </div>
            </div> --}}

            {{-- Année Academique --}}
            <!-- <div class="card card-calendar">
                <div id="monthName" class="pl-4"><i class="fa fa-graduation-cap"></i> Année académique</div>
                <div class="content-teacher" style="padding-top:20px;padding-bottom:20px">
                    <div class="circle-chart">
                        <span class="pourcent-lg">35<small>%</small></span>
                        <svg viewBox="0 0 100 100" style="display: block;">
                            <path d="M 50,50 m 0,-46.5 a 46.5,46.5 0 1 1 0,93 a 46.5,46.5 0 1 1 0,-93" stroke="#eee" stroke-width="2" fill-opacity="0"></path>
                            <path d="M 50,50 m 0,-46.5 a 46.5,46.5 0 1 1 0,93 a 46.5,46.5 0 1 1 0,-93" stroke="#ffab00" stroke-width="7" fill-opacity="0" style="stroke-dasharray: 292.209px, 292.209px; stroke-dashoffset: 189.936px;"></path>
                        </svg>
                    </div>
                </div>
            </div> -->
            {{-- Année academique --}}

            <!-- <div class="card card-calendar">
                <div id="monthName" class="pl-4"><i class="fa fa-graduation-cap"></i> Parcours académique</div>
                <div class="content-teacher" style="padding-top:20px;padding-bottom:20px">
                    <div class="progressbar d-flex" style="height:10px">
                        <div class="progressbar-G"></div>
                    </div>
                    <div class="d-flex w-100 justify-content-between mt-1">
                        <span class="level active">G1</span>
                        <span class="level active">
                            G2
                            <div class="tooltip-sm">
                                Vous êtes en G2
                            </div>
                        </span>
                        <span class="level">G3</span>
                    </div>
                </div>
            </div> -->

            {{-- Mes cours --}}
            {{-- <div class="card card-calendar">
                <div id="monthName" class="pl-4"><i class="fa fa-edit"></i> Mes cours</div>
                <div class="content-teacher d-flex">
                    <div class="avatar-teacher">
                        <div class="icon">
                            <i class="fa fa-file-pdf-o"></i>
                        </div>
                    </div>
                    <div class="text-left">
                        <h6 class="name">Système d'exploitation</h6>
                        <span class="function">Cours</span>
                    </div>
                </div>
                <div class="content-teacher d-flex">
                    <div class="avatar-teacher">
                        <div class="icon">
                            <i class="fa fa-file-pdf-o"></i>
                        </div>
                    </div>
                    <div class="text-left">
                        <h6 class="name">Algorithme</h6>
                        <span class="function">Cours</span>
                    </div>
                </div>
                <div class="content-teacher d-flex">
                    <div class="avatar-teacher">
                        <div class="icon">
                            <i class="fa fa-file-pdf-o"></i>
                        </div>
                    </div>
                    <div class="text-left">
                        <h6 class="name">Base de Données</h6>
                        <span class="function">Cours</span>
                    </div>
                </div>
                <div class="content-teacher d-flex">
                    <div class="avatar-teacher">
                        <div class="icon">
                            <i class="fa fa-file-pdf-o"></i>
                        </div>
                    </div>
                    <div class="text-left">
                        <h6 class="name">Web</h6>
                        <span class="function">Cours</span>
                    </div>
                </div>
            </div> --}}
            {{-- End mes cours --}}

            <div class="card card-calendar">
                @if($data_teachers->count()>0)
                    <div id="monthName" class="pl-4"><i class="fa fa-user"></i> Mes enseignants</div>
                    @foreach($data_teachers as $teacher)
                    <div class="content-teacher d-flex">
                        <div class="avatar-teacher">
                            <img src="/storage/uploads/avatars/{{$teacher->user->avatar}}" alt="img" class="img-fluid">
                        </div>
                        <div class="text-left">
                            @if($teacher->user->teacher !=null)
                            <h6 class="name">{{$teacher->user->teacher->name_teacher}}</h6>
                            <span class="function">{{$teacher->user->teacher->title->lampoon}}</span>
                            @endif
                        </div>
                    </div>
                    @endforeach
                @else
                <div class="card-body">
                    <div class="text-center">
                        <i class="fa fa-warning text-warning fa-2x"></i>
                        <small>
                            <p>Vous ne disposez d'aucun enseignants !!!</p>
                        </small>
                    </div>
                </div>
                @endif
            </div>

            {{-- Livrary auquel ont est abonnées : a faire --}}
            <div class="card card-calendar">
                <div id="monthName" class="pl-4"><i class="fa fa-user"></i> Mes abonnements Librarys</div>
                @foreach($library_abonnements as $abonn)
                <div class="content-teacher d-flex">
                    <div class="avatar-teacher">
                        <img src="/storage/uploads/avatars/{{$abonn->library_abonnement_price->library->user->avatar}}" alt="img" class="img-fluid">
                    </div>
                    <div class="text-left">
                        <h6 class="name">{{$abonn->library_abonnement_price->library->name_library}}</h6>
                        <span class="function">
                            <a href="{{$abonn->library_abonnement_price->library->website}}" target="_blank">{{$abonn->library_abonnement_price->library->website}}</a>
                        </span>
                    </div>
                </div>
                @endforeach
            </div>


            <!-- <div class="card card-student alert alert-primary" style="margin-top:-50px;">

                <div class="card-header bg-white card-header-avatar text-left">
                    <img src="/storage/uploads/avatars/{{$user_student->user->avatar}}" alt="img" class="avatar rounded-circle avatar_student_teach_univ">
                    <div class="card-pseudo pseudo" style="font-size:12px;">
                        Salut {{$user_student->user->name}} !
                    </div>
                    <div class="card-date function">
                        Vous vous êtes inscrit, <br> en date du {{date("d-m-Y",strtotime($user_student->user->created_at))}}
                        <br>à {{date("H:i:s",strtotime($user_student->user->created_at))}}
                    </div>
                </div>
            </div> -->
        </div>
        {{-- // End block détails concernant le compte étudiant --}}

            {{-- Block News --}}
            <div class="col-md-12 col-lg-12 col-xl-6 col-sm-12 no-padding">
                @foreach($universities_news as $news)
                @if($news->university_id !=null)
                <div class="card_news">
                @else
                <div class="card_news border border-4 border-info">
                @endif
                        <!-- @if ($loop->last) -->
                    <!-- @else
                    <div class="card_news">
                    @endif -->
                        
                    <div class="card-header text-left d-flex">
                        <div class="avatar-univ">
                            @if($news->university_id !=null)
                            <img src="/storage/uploads/avatars/{{$news->university->logo_picture}}" alt="img" class="img-fluid">
                            @else
                            <img src="/storage/uploads/avatars/{{$news->library->logo}}" alt="img" class="img-fluid">
                            @endif

                        </div>
                        <div class="content-univ">
                            <a href="#">
                                @if($news->university_id !=null)
                                <h4>{{$news->university->name_university}}</h4>
                                @else
                                <h4>{{$news->library->name_library}}</h4>
                                @endif

                            </a>
                            <p><i class="fa fa-users"></i> Public</p>
                            <small><i class="fa fa-clock-o"></i>
                                @if ( ceil(abs( strtotime(date("Y-m-d",strtotime($news->date) )) -
                                strtotime(date("Y-m-d")) ) / 86400) > 0)
                                il y a

                                {{ ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400) }}
                                {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400)) }}

                                @else
                                aujourd'hui
                                @endif
                            </small>
                        </div>

                        @if($news->university_id !=null)
                            <!-- <span class="sm-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span> -->
                        @else
                        <span class="sm-menu" style="font-size: 16px !important;width:150px !important;">
                           <small class="text-uppercase text-danger font-weight-bold"> Actu Bibliothèque {{$news->library->name_library}}</small>
                        </span>
                        @endif

                    </div>
                    
                    {{-- Données du news :menu droite --}}
                    <div class="card-body">
                        @if($news->university_id !=null)
                        <div class="net">
                            <a href=""><i class="fa fa-heart  mr-1"></i><small>{{ $notviewsnews->likecount($news->id)->count()}}</small></a>

                            <a href=""><i class="fa fa-comment-o  mr-1"></i><small>{{$news->comments->count()}}</small></a>
                            <a href=""><i class="fa fa-anchor mr-1"></i><small>{{ $notviewsnews->joincount($news->id)->count()}}</small></a>
                        </div>
                        @endif

                        <a href="{{route('news.show',$news)}}">
                            <img src="/storage/uploads/images/{{$news->picture}}" alt="img" class="img-fluid">
                            <h5 class="mt-3">{{$news->title_news}}</h5>
                        </a>
                        <p class="mt-2">{{ Str::limit($news->describe, 200) }}</p>
                    </div>
                    {{-- Fin Données du news :menu droite --}}

                    <div class="card-footer">
                        <div class="d-flex justify-content-between text-center">

                            {{-- J'aime --}}
                            @if($likes->count() == 0)
                            <div class="likeUp">
                                <a href="#" disabled="disabled" class="likeHeart" id="like_ajax{{$news->id}}">
                                    <i class="fa fa-heart-o"></i>
                                    <span class="number">{{ $notviewsnews->likecount($news->id)->count()}}</span>
                                </a>
                            </div>
                            @else
                            @foreach($likes as $lik)
                            @if($lik->news->id == $news->id)
                            @if($lik->like_to == 1)
                            <div class="likeUp">
                                <a href="#" disabled="disabled" class="likeHeart" id="like_ajax{{$news->id}}">
                                    <i class="fa fa-heart stat-color"></i>
                                    <span class="number">{{ $notviewsnews->likecount($news->id)->count()}}</span>
                                </a>
                            </div>
                            @else
                            <div class="likeUp">
                                <a href="#" disabled="disabled" class="likeHeart" id="like_ajax{{$news->id}}">
                                    <i class="fa fa-heart-o"></i>
                                    <span class="number">{{ $notviewsnews->likecount($news->id)->count()}}</span>
                                </a>
                            </div>
                            @endif
                            @break
                            @else
                            @if($loop->last)
                            <div class="likeUp">
                                <a href="#" disabled="disabled" class="likeHeart" id="like_ajax{{$news->id}}">
                                    <i class="fa fa-heart-o"></i>
                                    <span class="number">{{ $notviewsnews->likecount($news->id)->count()}}</span>
                                </a>
                            </div>
                            @endif
                            @endif
                            @endforeach
                            @endif
                            {{-- End J'aime --}}

                            {{-- Comment --}}
                            <div class="likeUp">
                                <a href="{{route('news.show',$news)}}" class="comment">
                                    <i class="fa fa-comment-o"></i>
                                    <span class="number">{{$news->comments->count()}}
                                        com.
                                </a>
                            </div>
                            {{-- End Comment --}}

                            {{-- reservation --}}
                            @if($likes->count() == 0)
                            <div class="likeUp">
                                <a href="" disabled="disabled" id="join_ajax{{$news->id}}">
                                    <i class="fa fa-anchor"></i>
                                    <span class="number">{{ $notviewsnews->joincount($news->id)->count()}}</span>
                                </a>
                            </div>
                            @else
                            @foreach($likes as $lik)
                            @if($lik->news->id == $news->id)
                            @if($lik->join_to == 1)
                            <div class="likeUp">
                                <a href="#" disabled="disabled" class="likeHeart" id="join_ajax{{$news->id}}">
                                    <i class="fa fa-anchor stat-color"></i>
                                    <span class="number">{{ $notviewsnews->joincount($news->id)->count()}}</span>
                                </a>
                            </div>
                            @else
                            <div class="likeUp">
                                <a href="#" disabled="disabled" class="likeHeart" id="join_ajax{{$news->id}}">
                                    <i class="fa fa-anchor"></i>
                                    <span class="number">{{ $notviewsnews->joincount($news->id)->count()}}</span>
                                </a>
                            </div>
                            @endif
                            @break
                            @else
                            @if($loop->last)
                            <div class="likeUp">
                                <a href="" disabled="disabled" id="join_ajax{{$news->id}}">
                                    <i class="fa fa-anchor"></i>
                                    <span class="number">{{ $notviewsnews->joincount($news->id)->count()}}</span>
                                </a>
                            </div>
                            @endif
                            @endif
                            @endforeach
                            @endif
                            {{-- End reservation --}}

                        </div>
                        <hr>
                        <div class="commentary">
                            <h4 style="font-size: 15px;font-weight:700;margin-bottom:10px">Commentaires
                                @if($news->comments->count()>1)
                                <a href="{{route('news.show',$news)}}" class="float-right text-primary" style="font-size: 12px; top:5px; "> Voir plus de commentaires</a>
                                @endif
                            </h4>
                            @if($news->comments->count()>0)
                            <div class="card-comment mb-3">
                                <div class="block-com d-flex">
                                    <div class="avatar-com  font-weight-bold">
                                        {{ strtoupper($news->comments->last()->user->name[0]) }}
                                    </div>
                                    <div class="block-com-content">
                                        <h5>{{$news->comments->last()->user->name}}</h5>
                                        <p>{{$news->comments->last()->note}}</p>
                                        <div class="text-left mt-2">
                                            <span><i class="fa fa-clock-o"></i>
                                                @if ( ceil(abs( strtotime(date("Y-m-d",strtotime($news->comments->last()->created_at) )) -
                                                strtotime(date("Y-m-d")) ) / 86400) > 0)
                                                il y a

                                                {{ ceil(abs(strtotime(date("Y-m-d",strtotime($news->comments->last()->created_at))) - strtotime(date("Y-m-d"))) / 86400) }}
                                                {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($news->comments->last()->created_at))) - strtotime(date("Y-m-d"))) / 86400)) }}

                                                @else
                                                aujourd'hui
                                                @endif</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="comment-box">
                            <form action="{{route('commentNews')}}" method="POST">
                                @csrf
                                <div class="d-flex">
                                    <div class="avatar-sm">
                                        <div class="icon">
                                            {{ strtoupper($user_student->user->name[0]) }}
                                        </div>
                                        <img src="/storage/uploads/images/bg-1.jpg" alt="img" class="img-fluid">
                                    </div>
                                    <div class="input-form">
                                        <input type="hidden" class="form-control @error('news_id') is-invalid @enderror" name="news_id" value="{{$news->id}}">
                                        <textarea name="note" id="comment" cols="40" class="form-control" placeholder="Tapez votre commentaire..."></textarea>
                                    </div>
                                    <button class="btn btn-primary"><i class="fa fa-comment-o"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                @endforeach

            </div>
            {{-- End Block News --}}




               








            {{-- Block suggestions --}}
            <div class="col-xl-3">
                <div class="card card-calendar" style="margin-top:50px !important;">
                    @if($universities->count()>0)
                    <div id="monthName" class="pl-4" style="font-size:16px">Institutions suggerées</div>
                    @foreach($universities->chunk(4) as $universitie)
                    @foreach($universitie as $university)
                    @if ($user_student->user->activations->last()->university->id != $university->id)
                    <div class="content-teacher d-flex">
                        <div class="avatar-teacher">
                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                        </div>
                        <div class="text-left">
                            <h6 class="name">{{$university->sigle}}</h6>
                            <span class="function">
                                <i class="fa fa-map-marker text-danger small"></i> {{$university->adress}}
                            </span>
                            <button  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalAttacherInstitut{{$university->sigle}}">
                                S'attacher
                                <i class="fa fa-plus"></i>
                            </button>
                            <!-- <a href="{{ route('activation_store',$university->id) }}" class="btn btn-primary btn-sm">
                                S'attacher
                                <i class="fa fa-plus"></i>
                            </a> -->
                        </div>
                    </div>
                    @endif
                    @endforeach
                    @endforeach
                    @else
                    <div class="card-body">
                        <div class="text-center">
                            <i class="fa fa-info text-warning fa-2x"></i>
                            <small>
                                <p>BookCase vous souhaite la bienvenue parmi nous. <br>
                                    Bientot vous aurez accès a votre université.
                                </p>
                            </small>
                        </div>
                    </div>
                    @endif

                    {{-- <div class="content-teacher d-flex">
                    <div class="avatar-teacher">
                        <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                    </div>
                    <div class="text-left">
                        <h6 class="name">Pedrien Kinkani</h6>
                        <span class="function">Professeur</span>
                        <button class="btn btn-primary btn-sm">S'attacher <i class="fa fa-plus"></i></button>
                    </div>
                </div>*--}}
                </div>
                    

                <div class="card card-calendar">
                    @if($data_teachers->count()>0)
                    <div id="monthName" class="pl-4" style="font-size:16px">Top des enseignants suggerés</div>
                    @foreach($data_teachers->chunk(2) as $teachers)
                    @foreach($teachers as $teacher)
                    <div class="content-teacher d-flex">
                        <div class="avatar-teacher">
                            <img src="/storage/uploads/avatars/{{$teacher->user->avatar}}" alt="img" class="img-fluid">
                        </div>
                        <div class="text-left">
                            @if($teacher->user->teacher !=null)
                            <h6 class="name">{{$teacher->user->teacher->name_teacher}}</h6>
                            <span class="function">{{$teacher->user->teacher->title->lampoon}}</span>
                            @endif
                            <div class="star">
                                <i class="fa fa-star-o" style="color:#ffa500;"></i>
                                <i class="fa fa-star-o" style="color:#ffa500;"></i>
                                <i class="fa fa-star-o" style="color:#ffa500;"></i>
                                <i class="fa fa-star-o" style="color:#ffa500;"></i>
                                <i class="fa fa-star-o" style="color:#ffa500;"></i>
                            </div>
                            <button class="btn btn-primary btn-sm">Suivre <i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    @endforeach
                    @endforeach
                    @else
                    <div class="card-body">
                        <div class="text-center">
                            <i class="fa fa-warning text-warning fa-2x"></i>
                            <small>
                                <p>Vous devez vous attacher à une institution pour voir les enseignents attachés à
                                    celle-ci.</p>
                            </small>
                        </div>
                    </div>
                    @endif

                    {{-- <div class="content-teacher d-flex">
                        <div class="avatar-teacher">
                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                        </div>
                        <div class="text-left">
                            <h6 class="name">Pedrien Kinkani</h6>
                            <span class="function">Professeur</span>
                            <div class="star">
                                <i class="fa fa-star-o" style="color:#ffa500;"></i>
                                <i class="fa fa-star-o" style="color:#ffa500;"></i>
                                <i class="fa fa-star-o" style="color:#ffa500;"></i>
                                <i class="fa fa-star-o" style="color:#ffa500;"></i>
                            </div>
                            <button class="btn btn-primary btn-sm">Suivre <i class="fa fa-plus"></i></button>
                        </div>
                    </div> --}}
                </div>

                @if($article_suggestions->count()>0)
                <div class="card card-calendar">
                    <div id="monthName" class="pl-4" style="font-size:16px">Cours à téléchager</div>
                    @foreach($article_suggestions->take(4) as $artic)
                    <div class="content-teacher d-flex">
                        <div class="avatar-teacher">
                            <div class="icon">
                                <i class="fa fa-file-pdf-o"></i>
                            </div>
                        </div>
                        <div class="text-left">
                            <h6 class="name">{{$artic->article_name}}</h6>
                           @if($artic->membership !="LBRY")
                           <span class="function">{{$artic->teacher->name_teacher}}</span>
                           @endif
                            <!-- <div class="progressbar">
                                <div class="progressbar-chart"></div>
                            </div> -->
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif

                {{-- <div class="card card-teacher" style="background:#6f42c1!important;">
                <div class="text-left div-caption">
                    <h6 class="card-caption mt-2">Institution suggerée</h6>
                </div>
                <div class="avatar-teacher">
                        <div class="avatar-sm-teacher">
                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                        </div>
                        <span class="name-teacher">P. Kinkani</span>
                        <span class="function">Prof</span>
                    </div>
                    <div class="avatar-teacher">
                        <div class="avatar-sm-teacher">
                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                        </div>
                        <span class="name-teacher">P. Kinkani</span>
                        <span class="function">Prof</span>
                    </div>
                    <div class="avatar-teacher">
                        <div class="avatar-sm-teacher">
                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                        </div>
                        <span class="name-teacher">P. Kinkani</span>
                        <span class="function">Prof</span>
                    </div>
                    <div class="avatar-teacher">
                        <div class="avatar-sm-teacher">
                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                        </div>
                        <span class="name-teacher">P. Kinkani</span>
                        <span class="function">Prof</span>
                    </div>
                    <div class="avatar-teacher">
                        <div class="avatar-sm-teacher">
                            <img src="/storage/uploads/images/bg-3.jpg" alt="img" class="img-fluid">
                        </div>
                        <span class="name-teacher">P. Kinkani</span>
                        <span class="function">Prof</span>
                    </div>
                @if($universities->count()>0)

                <div class="carousel slide carousel-for-university" data-ride="carousel" id="carouselExampleControls">
                    <div class="carousel-inner" style="overflow:hidden">
                        @foreach($universities->chunk(4) as $universitie)

                        @if($loop->first)
                        <div class="carousel-item active">
                        @else
                        <div class="carousel-item">
                        @endif

                            @foreach($universitie as $university)
                                <div class="card-header bg-white card-header-avatar text-left">
                                    @if(is_null($university->logo_picture))
                                    @else
                                    <img src="/storage/uploads/avatars/{{$university->logo_picture}}" alt="img"
                class="avatar rounded-circle avatar_student_teach_univ">
                @endif

                <div class="card-pseudo pseudo" style="font-weight:400">
                    {{$university->name_university}}
                </div>
                <div class="card-date function" style="color:#6f42c1;">
                    <i class="fa fa-map-marker"></i> {{$university->adress}}
                </div>
                <div class="more">
                    <span class="option"><i class="fa fa-ellipsis-h"></i></span>
                    <div class="show-option">
                        <a href="{{ route('activation_store',$university->id) }}" class="a_non_actif" title="S'attacher" data-toggle="tooltip" data-placement="top"><i class="fa fa-graduation-cap"></i></a>
                        <a href="" title="J'aime" data-toggle="tooltip" data-placement="top"><i class="fa fa-heart-o"></i></a>
                        <a href="" title="Voir" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i></a>
                    </div>
                </div>
            </div>
            @endforeach

        </div>

        @endforeach
    </div>
</div>

<div class="text-center" style="margin-top:-2px;">
    <a href="{{ route('universities')}}" class="btn btn-primary btn-sm fadeIn">Voir plus</a>
</div>
@else
<div class="card-body">
    <div class="text-center">
        <i class="fa fa-info text-warning fa-2x"></i>
        <small>
            <p>BookCase vous souhaite la bienvenue parmi nous. <br>
                Bientot vous aurez accès a votre université.
            </p>
        </small>
    </div>
</div>
@endif
</div> --}}

{{-- <div class="card card_comment">
                    <div class="card-body">
                        <div class="text-center text-white mt-3">
                            <h6>Nous vous remerçions d'avoir réjoint BookCase, grâce à vous, BookCase devient de plus en plus une grande famille.</h6>
                            <i class="fa fa-star text-warning"></i>
                            <i class="fa fa-star text-warning"></i>
                            <i class="fa fa-star-half-o text-warning"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <h6 class="text-white"><i class="fa fa-plus"></i> 1200 Utilisateurs</h6>
                            <h6 class="text-white">Laissez - nous une note</h6>
                            <i class="fa fa-arrow-down"></i>
                        </div>
                    </div>
                    <div class="card-footer bg-white comment">
                        <div class="container">
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="Message">Message</label>
                                    <div class="input-group">
                                        <textarea name="Message" id=""  class="form-control" style="font-size:10px!important"></textarea>
                                        <div class="append">
                                            <button class="btn btn-primary btn-sm" style="border-radius:0px!important"><i class="fa fa-send"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}

</div>
{{-- End  Block suggestions --}}

</div>
<!-- Fin de toutes les actualites -->
</div>



          @if($universities->count()>0)
                    @foreach($universities->chunk(4) as $universitie)
                    @foreach($universitie as $university)
                    @if ($user_student->user->activations->last()->university->id != $university->id)

                    <div class="modal fade margin" id="ModalAttacherInstitut{{$university->sigle}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9999999999999999;">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="padding-left:50px;padding-bottom: 0px;">
                                    <h6 class=" text-sm" id="exampleModalLabel">Opération de confirmation</h6>
                                    <button type="button" class="close text-sm" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        <div class="row justify-content-center mb-5 mt-2 text-center">
                                            <div class="col-md-10">
                                                <strong class="h5">
                                                    Souhaitez-vous vraiment vous attachée  à <strong class="text-uppercase"> {{$university->sigle}}</strong>  ?
                                                </strong>
                                            </div>
                                        </div>
                                    <a href="{{ route('activation_store',$university->id) }}" class="btn btn-primary btn-login btn-float text-uppercase" id="choice_folder">
                                        Confirmer
                                    </a>
                                    <!-- </form> -->
                                </div>
                
                            </div>
                        </div>
                    </div>

                    @endif
                    @endforeach
                    @endforeach
                    @else
                    @endif



@endsection


@section('scriptis')
<script>
    $(document).ready(function() {

        @foreach($universities_news as $news)
        // Click : Au click
        $('#like_ajax{{$news->id}}').click(function() {

            if ($('#like_ajax{{$news->id}}').children("i").hasClass('fa-heart-o')) {
                $('#like_ajax{{$news->id}}').children("i").removeClass(
                    'fa-heart-o text-primary active');
                $('#like_ajax{{$news->id}}').children("i").addClass('fa-heart text-primary');
            } else {
                $('#like_ajax{{$news->id}}').children("i").addClass('fa-heart-o text-primary');
                $('#like_ajax{{$news->id}}').children("i").removeClass('fa-heart text-primary active');
            }

            $.ajax({
                type: "GET",
                url: "{{ route('news_like',$news->id) }}",
            });

            if ($('#like_ajax{{$news->id}}').children("i").hasClass('text-primary')) {
                $('#like_ajax{{$news->id}} .number').text((parseInt($(this).children().text(), 10) + 1)
                    .toString());
            } else {
                $('#like_ajax{{$news->id}} .number').text((parseInt($(this).children().text(), 10) - 1)
                    .toString());
            }
        });

        $('#join_ajax{{$news->id}}').click(function() {

            $(this).children("i").toggleClass("stat-color");
            // if ($('#join_ajax{{$news->id}}').children("i").hasClass('fa-anchor') ) {
            //    $('#join_ajax{{$news->id}}').children("i").removeClass('fa-heart-o text-primary active');
            //    $('#join_ajax{{$news->id}}').children("i").addClass('fa-heart text-primary');
            // } else {
            //    $('#join_ajax{{$news->id}}').children("i").addClass('fa-heart-o text-primary');
            //    $('#join_ajax{{$news->id}}').children("i").removeClass('fa-heart text-primary active');
            // }

            $.ajax({

                type: "GET",
                url: "{{ route('news_join',$news->id) }}",
            });

            if ($('#join_ajax{{$news->id}}').children("i").hasClass('stat-color')) {
                $('#join_ajax{{$news->id}} .number').text((parseInt($(this).children().text(), 10) + 1)
                    .toString());
            } else {
                $('#join_ajax{{$news->id}} .number').text((parseInt($(this).children().text(), 10) - 1)
                    .toString());
            }
        });

        // Mouse Hover : Au survol

        $('#like_ajax{{$news->id}}').mouseover(function() {
            if ($('#like_ajax{{$news->id}}').hasClass('stat-color')) {
                $('#like_ajax{{$news->id}}').children().attr("data-original-title", "Aimé").removeAttr(
                    "title");
            } else {
                $('#like_ajax{{$news->id}}').children().attr("data-original-title", "J'aime")
                    .removeAttr("title");
            }
        });

        $('#join_ajax{{$news->id}}').mouseover(function() {
            if ($('#join_ajax{{$news->id}}').hasClass('stat-color')) {
                $('#join_ajax{{$news->id}}').children().attr("data-original-title", "J'y serais")
                    .removeAttr("title");
            } else {
                $('#join_ajax{{$news->id}}').children().attr("data-original-title", "J'y participe")
                    .removeAttr("title");
            }
        });

        $('.likeHeart').click(function(e) {
            e.preventDefault()
            $('.fa', this).addClass('active')
        })

        @endforeach


        // function refresh() {
        //     $.ajax({
        //         url: "{{route('home')}}", // Ton fichier ou se trouve ton chat
        //         success: function(retour) {
        //             $('#app').html(retour); // rafraichi toute ta DIV " bien sur il lui faut un id
        //         }
        //     });
        // }

        // setInterval(refresh(), 1000)

        // setInterval(
        //     function() {
        //         $('#app').load("{{route('home')}}").fadeIn("slow");
        //     }, 10000);

    });
</script>
@endsection