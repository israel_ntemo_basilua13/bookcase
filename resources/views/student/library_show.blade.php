@inject('dataCore','App\Utilities\CoreData')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')

@section('content_student')
<div class="col-md-12 card_file margin" style="margin-top: 150px !important;">

    {{-- <div class="container-fluid"> --}}
    <!-- // Gratuit -->
    <div class="row mt-4">
        <div class="col-lg-3">
            <div class="card card-category">
                <div class="card-header">
                    <form action="">
                        <input type="text" class="form-control" placeholder="Faite la recherche ici...">
                    </form>
                </div>
                <div class="text-left">
                    {{-- <a href="{{ route('article_show',['type'=>'free']) }}"><i class="fa fa-smile-o"></i> Gratuits</a>
                    <a href="{{ route('article_show',['type'=>'payant']) }}"><i class="fa fa-dollar"></i> Payants</a> --}}
                    <a href="{{ route('studentLibrary',"MFB")}}"><i class="fa fa-smile-o"></i> Mes Favoris</a>
                    <a href="{{ route('ArticleCollection')}}"><i class="fa fa-folder-o"></i> Mes abonnements</a>
                </div>
            </div>
        </div>
        {{-- // Datas articles bibliothèque --}}
        <div class="col-lg-9">
            @if($titles=="ACB")
            {{-- // Pour le visuel des datas non abonnements ACB: Au coeur de biblio --}}
            <h2 class="mx-md-3 p_titile_article">Au coeur des bibliothèques</h2>
            @else
            {{-- Pour des datas favorites des bibliothèques --}}
            <h2 class="mx-md-3 p_titile_article">Mes favoris des bibliothèques</h2>
            @endif

            <div class="row mt-5">
                @foreach($articles as $article)
                <div class="col-lg-4">
                    <div class="card card-book" id="card-book{{$article->id}}">
                        <div class="edition">
                            Edition {{ $article->edition}}
                        </div>
                        <div class="text-left">
                            <div class="icon-book">
                                <i class="fa fa-book"></i>
                            </div>
                            <h6>{{ Str::limit($article->article_name, 20) }}</h6>
                            <p class="text-muted small">{{$article->library->name_library}}</p>
                            <hr>
                            <h6>Auteur</h6>
                            <p>
                                <span class="avatar"><img src="/storage/uploads/avatars/{{$article->library->logo}}" alt="img"></span>
                                <span>{{$article->author}}</span>
                            </p>
                            @if($dataCore->ValidateViewArticle($article->library) == true)
                            <button class="btn btn-sm text-muted">Lire le contenu</button>
                            <span class="float-right h3" id="btn_favoriteN{{$article->id}}" style="cursor: pointer;"><i class="fa fa-heart-o fa-1x"></i></span>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        {{-- // End Data articles bibliothèque --}}

    </div>

    {{-- </div> --}}
    {{-- // Library --}}
    <div class="container-fluid" id="librarys">
        <div class="col-10 col-md-11 col-sm-10 col-lg-12 mb-5 my-md-5">
            <p class="text-justify h2 p_titile_article">Accroît ton savoir</p>
            <span class="text-muted">Accroit ton savoir en t'abonnant a une bibliothèque et choississez votre bundle de tarification</span>
        </div>
        <div class="row">
            @foreach ($dataCore->LibraryList() as $lib)
            <div class="col-lg-3">
                <div class="card card-biblo">
                    <div class="card-img">
                        <img src="/storage/uploads/images/{{$lib->picture}}" alt="img" class="img-cover">
                        <div class="avatar-biblo">
                            <img src="/storage/uploads/avatars/{{$lib->logo}}" alt="img">
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="text-left">
                            <a href="{{route('library.show',$lib->id)}}">
                                <h6><i class="fa fa-check"></i>{{$lib->name_library}}</h6>
                            </a>
                            <small class="mb-0 ml-1"><i class="fa fa-globe mr-1"></i><a href="" class="text-small">{{$lib->website}}</a></small> <br>
                            <span class="phone ml-1"><i class="fa fa-phone mr-1"></i>{{$lib->phone}}</span>
                        </div>
                        <div class="text-right">
                            @if($dataCore->AbonUser($lib) != 0)
                            <small class="float-left my-2 mx-2 p-1 bg-danger text-white" style="border-radius:10px;">{{$dataCore->AbonUser($lib)}} abonn.</small>
                            @endif
                            <div class="network-biblo">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                        <hr>
                        <div class="d-flex">
                            <div class="abonne text-center">
                                <span class="number">{{$dataCore->CountAbon($lib)}}</span>
                                <br>
                                <span>{{$dataCore->CountAbon($lib) <= 1 ? 'Abonné' : 'Abonnés'}}</span>
                            </div>
                            <div class="article text-center">
                                <span class="number">{{$lib->articles->count()}}</span>
                                <br>
                                <span>Ouvrages</span>
                            </div>
                            <a href="{{route('abonnements',$lib->id)}}" class="btn-primary btn-sm" style="height: 30px;margin-top: 7px;">S'abonner</a>
                        </div>

                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@foreach ($articles as $art_lib)
<div class="show-contenu" id="show-contenu{{$art_lib->id}}">
    <div class="contenu">
        <p class="float-right text-muted small mb-3">{{$art_lib->article_name}}</p>
        <iframe id="_frame{{$art_lib->id}}" frameborder="0" style="width:100%; height:100vh;"></iframe>
    </div>
</div>
@endforeach
<div class="opacity-fond">
    <span>
        <i class="fa fa-times"></i>
    </span>
</div>


@endsection


@section('script_article')
<script>
    $(function() {

        $("#search_article").keyup(function() {

            var search = $(this).val();

            search = $.trim(search);

            $(".title_article").removeClass("color_v");
            $(".title_article").parent().parent().parent().parent().css("display", "none");


            if (search != "") {

                $(".title_article:contains('" + search + "')").addClass("color_v");
                $(".title_article:contains('" + search + "')").show();
                $(".title_article:contains('" + search + "')").parent().parent().parent().parent().css("display", "flex");

            } else {
                $(".title_article").removeClass("color_v");
                $(".title_article").parent().parent().parent().parent().css("display", "flex");
            }


        });

        @foreach($articles as $art_lib)
        $('#card-book{{$art_lib->id}} .btn').click(function() {
            $('body').toggleClass('overflow')
            $('#show-contenu{{$art_lib->id}}').toggleClass('active')
            $('.opacity-fond').toggleClass('active')
            $('#_frame{{$art_lib->id}}').prop('src', '/storage/uploads/files/{{$art_lib->article_name}}');
        })

        $('.opacity-fond span').click(function() {
            $('body').removeClass('overflow')
            $('#show-contenu{{$art_lib->id}}').removeClass('active')
            $('.opacity-fond').removeClass('active')
        })

        $('#btn_favoriteN{{$art_lib->id}}').click(function() {

            $.ajax({
                type: "GET",
                url: "{{ route('articleFavoriteStoreToogle',$art_lib->id) }}",
            });

            if ($('#btn_favoriteN{{$art_lib->id}}').children("i").hasClass('fa-heart-o')) {
                $('#btn_favoriteN{{$art_lib->id}}').children("i").removeClass('fa-heart-o text-primary');
                $('#btn_favoriteN{{$art_lib->id}}').children("i").addClass('fa-heart text-primary');
            } else {
                $('#btn_favoriteN{{$art_lib->id}}').children("i").addClass('fa-heart-o text-primary');
                $('#btn_favoriteN{{$art_lib->id}}').children("i").removeClass('fa-heart text-primary');
            }
        })
        @endforeach
    });
</script>
@endsection