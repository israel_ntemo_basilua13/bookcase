@extends('layouts.student_layout')

@section('content_student')


<div class="col-md-12  col-sm-12 col-lg-12 margin no-padding">
    <div class="content">

        <div class="row justify-content-between mt-md-5 pt-md-5">
            <div class="col-md-11 col-12 col-lg-12">
                <div class="text-left">
                    <h5 class=" text-uppercase">Le corps Professoral de l'{{ $user_university->name_university}}</h5>
                </div>
            </div>
        </div>
        @foreach($data_teachers as $teacher)
        <div class="row">
            <div class="col-lg-3">
                @if($teacher->user->teacher !=null)
                <div class="card card-show-teacher">
                    <div class="card-header">
                        <div class="text-center">
                            <h4>{{$teacher->user->teacher->name_teacher}}</h4>
                        </div>
                        <div class="avatar-teacher">
                            <img src="/storage/uploads/avatars/{{$teacher->user->avatar}}" alt="img" class="img-cover">
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="text-center">
                            <span>{{$teacher->user->teacher->title->lampoon}}</span>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-center">
                        <a href="{{route('show_teacher',$teacher->user->teacher->id)}}" class="ouvrages text-center text-uppercase">
                            <!-- <span class="number">120</span>
                            <br> -->
                            <span>EN SAVOIR PLUS</span>
                        </a>
                        <!-- <div class="ouvrages text-center">
                            <span class="number">120</span>
                            <br>
                            <span>Articles</span>
                        </div>
                        <div class="ouvrages text-center">
                            <span class="number">120</span>
                            <br>
                            <span>Articles</span>
                        </div> -->
                    </div>
                </div>
                @else
                <div class="row  justify-content-center align-items-center  text-center">
                    <div class="col-md-12 mt-md-5 pt-md-5">
                        <div class="text-center">
                            <p class="text-center">Le professeur  n'existe pas !!! </p>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        @endforeach

        <div class="row justify-content-center">
            <div class="col-md-9 col-sm-6  p-5">
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>


</div>

@endsection

@section('scriptis')
<script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>
<script>
    $(document).ready(function() {

        var swiper = new Swiper('.swiper-container', {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            coverflowEffect: {
                rotate: 60,
                stretch: 0,
                depth: 400,
                modifier: 1,
                slideShadows: true,
            },
            pagination: {
                el: '.swiper-pagination',
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });
    });
</script>
@endsection