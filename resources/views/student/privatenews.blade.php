@inject('notviewsnews','App\Utilities\NotViewNews')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')

@section('content_student')

<div class="col-md-12  col-sm-12 col-lg-10 margin no-padding"  style="margin-top: 200px !important;">
    <!-- <div class="container-fluid no-padding"> -->


        <!-- toutes les actualites -->
        @if($news_private->count() >0)
            @foreach($news_private as $news)
            <!-- News lock -->
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-12 col-xl-7 col-sm-12 no-padding">
                    <div class="card_news">
                        <div class="card-header text-left d-flex">
                            <div class="avatar-univ">
                                <img src="/storage/uploads/avatars/{{$news->university->logo_picture}}" alt="img" class="img-fluid">
                            </div>
                            <div class="content-univ">
                                <a href="#">
                                    <h4>{{$news->university->name_university}}</h4>
                                </a>
                                <p><i class="fa fa-users"></i> Public</p>
                                <small><i class="fa fa-clock-o"></i>
                                    @if ( ceil(abs( strtotime(date("Y-m-d",strtotime($news->date) )) -
                                    strtotime(date("Y-m-d")) ) / 86400) > 0)
                                    il y a

                                    {{ ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400) }}
                                    {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400)) }}

                                    @else
                                    aujourd'hui
                                    @endif
                                </small>
                            </div>
                            <span class="sm-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </div>
                        {{-- Données du news :menu droite --}}
                        <div class="card-body">
                            <div class="net">
                                <a href=""><i class="fa fa-heart  mr-1"></i><small>{{ $notviewsnews->likecount($news->id)->count()}}</small></a>

                                <a href=""><i class="fa fa-comment-o  mr-1"></i><small>{{$news->comments->count()}}</small></a>
                                <a href=""><i class="fa fa-anchor mr-1"></i><small>{{ $notviewsnews->joincount($news->id)->count()}}</small></a>
                            </div>
                            <a href="{{route('news.show',$news)}}">
                                <img src="/storage/uploads/images/{{$news->picture}}" alt="img" class="img-fluid">
                                <h5 class="mt-3">{{$news->title_news}}</h5>
                            </a>
                            <p class="mt-2">{{ Str::limit($news->describe, 200) }}</p>
                        </div>
                        {{-- Fin Données du news :menu droite --}}

                        <div class="card-footer">
                            <div class="d-flex justify-content-between text-center">

                                {{-- J'aime --}}
                                @if($likes->count() == 0)
                                <div class="likeUp">
                                    <a href="#" disabled="disabled" class="likeHeart" id="like_ajax{{$news->id}}">
                                        <i class="fa fa-heart-o"></i>
                                        <span class="number">{{ $notviewsnews->likecount($news->id)->count()}}</span>
                                    </a>
                                </div>
                                @else
                                @foreach($likes as $lik)
                                @if($lik->news->id == $news->id)
                                @if($lik->like_to == 1)
                                <div class="likeUp">
                                    <a href="#" disabled="disabled" class="likeHeart" id="like_ajax{{$news->id}}">
                                        <i class="fa fa-heart stat-color"></i>
                                        <span class="number">{{ $notviewsnews->likecount($news->id)->count()}}</span>
                                    </a>
                                </div>
                                @else
                                <div class="likeUp">
                                    <a href="#" disabled="disabled" class="likeHeart" id="like_ajax{{$news->id}}">
                                        <i class="fa fa-heart-o"></i>
                                        <span class="number">{{ $notviewsnews->likecount($news->id)->count()}}</span>
                                    </a>
                                </div>
                                @endif
                                @break
                                @else
                                @if($loop->last)
                                <div class="likeUp">
                                    <a href="#" disabled="disabled" class="likeHeart" id="like_ajax{{$news->id}}">
                                        <i class="fa fa-heart-o"></i>
                                        <span class="number">{{ $notviewsnews->likecount($news->id)->count()}}</span>
                                    </a>
                                </div>
                                @endif
                                @endif
                                @endforeach
                                @endif
                                {{-- End J'aime --}}

                                {{-- Comment --}}
                                <div class="likeUp">
                                    <a href="{{route('news.show',$news)}}" class="comment">
                                        <i class="fa fa-comment-o"></i>
                                        <span class="number">{{$news->comments->count()}}
                                            com.
                                    </a>
                                </div>
                                {{-- End Comment --}}

                                {{-- reservation --}}
                                @if($likes->count() == 0)
                                <div class="likeUp">
                                    <a href="" disabled="disabled" id="join_ajax{{$news->id}}">
                                        <i class="fa fa-anchor"></i>
                                        <span class="number">{{ $notviewsnews->joincount($news->id)->count()}}</span>
                                    </a>
                                </div>
                                @else
                                @foreach($likes as $lik)
                                @if($lik->news->id == $news->id)
                                @if($lik->join_to == 1)
                                <div class="likeUp">
                                    <a href="#" disabled="disabled" class="likeHeart" id="join_ajax{{$news->id}}">
                                        <i class="fa fa-anchor stat-color"></i>
                                        <span class="number">{{ $notviewsnews->joincount($news->id)->count()}}</span>
                                    </a>
                                </div>
                                @else
                                <div class="likeUp">
                                    <a href="#" disabled="disabled" class="likeHeart" id="join_ajax{{$news->id}}">
                                        <i class="fa fa-anchor"></i>
                                        <span class="number">{{ $notviewsnews->joincount($news->id)->count()}}</span>
                                    </a>
                                </div>
                                @endif
                                @break
                                @else
                                @if($loop->last)
                                <div class="likeUp">
                                    <a href="" disabled="disabled" id="join_ajax{{$news->id}}">
                                        <i class="fa fa-anchor"></i>
                                        <span class="number">{{ $notviewsnews->joincount($news->id)->count()}}</span>
                                    </a>
                                </div>
                                @endif
                                @endif
                                @endforeach
                                @endif
                                {{-- End reservation --}}

                            </div>
                            <hr>
                            <div class="commentary">
                                <h4 style="font-size: 15px;font-weight:700;margin-bottom:10px">Commentaires
                                    @if($news->comments->count()>1)
                                    <a href="{{route('news.show',$news)}}" class="float-right text-primary" style="font-size: 12px; top:5px; "> Voir plus de commentaires</a>
                                    @endif
                                </h4>
                                @if($news->comments->count()>0)
                                <div class="card-comment mb-3">
                                    <div class="block-com d-flex">
                                        <div class="avatar-com font-weight-bold">
                                            {{ strtoupper($news->comments->last()->user->name[0]) }}
                                            <!-- <img src="/storage/uploads/avatars/{{$news->comments->last()->user->avatar}}" alt="img" class="img-fluid"> -->
                                        </div>
                                        <div class="block-com-content">
                                            <h5>{{$news->comments->last()->user->name}}</h5>
                                            <p>{{$news->comments->last()->note}}</p>
                                            <div class="text-left mt-2">
                                                <span><i class="fa fa-clock-o"></i>
                                                    @if ( ceil(abs( strtotime(date("Y-m-d",strtotime($news->comments->last()->created_at) )) -
                                                    strtotime(date("Y-m-d")) ) / 86400) > 0)
                                                    il y a

                                                    {{ ceil(abs(strtotime(date("Y-m-d",strtotime($news->comments->last()->created_at))) - strtotime(date("Y-m-d"))) / 86400) }}
                                                    {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($news->comments->last()->created_at))) - strtotime(date("Y-m-d"))) / 86400)) }}

                                                    @else
                                                    aujourd'hui
                                                    @endif</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="comment-box">
                                <form action="{{route('commentNews')}}" method="POST">
                                    @csrf
                                    <div class="d-flex">
                                        <div class="avatar-sm">
                                            <div class="icon">
                                                {{ strtoupper($user_student->user->name[0]) }}
                                            </div>
                                            <img src="/storage/uploads/images/bg-1.jpg" alt="img" class="img-fluid">
                                        </div>
                                        <div class="input-form">
                                            <input type="hidden" class="form-control @error('news_id') is-invalid @enderror" name="news_id" value="{{$news->id}}">
                                            <textarea name="note" id="comment" cols="40" class="form-control" placeholder="Tapez votre commentaire..."></textarea>
                                        </div>
                                        <button class="btn btn-primary"><i class="fa fa-comment-o"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End News lock -->
            @endforeach
        @else
        <div class="row justify-content-center align-items-center text-center">
            <div class="col-md-12">
                <p>Aucune actualité dans le cadre privée n'est disponible  !!!</p>
            </div>
        </div>
        @endif
        
        <!-- Fin de toutes les actualites -->

    <!-- </div> -->
</div>

@endsection


@section('scriptis')
<script>
    $(document).ready(function() {
        @foreach($news_private as $news)
        // Click : Au click
        $('#like_ajax{{$news->id}}').click(function() {

            if ($('#like_ajax{{$news->id}}').children("i").hasClass('fa-heart-o')) {
                $('#like_ajax{{$news->id}}').children("i").removeClass(
                    'fa-heart-o text-primary active');
                $('#like_ajax{{$news->id}}').children("i").addClass('fa-heart text-primary');
            } else {
                $('#like_ajax{{$news->id}}').children("i").addClass('fa-heart-o text-primary');
                $('#like_ajax{{$news->id}}').children("i").removeClass('fa-heart text-primary active');
            }

            $.ajax({
                type: "GET",
                url: "{{ route('news_like',$news->id) }}",
            });

            if ($('#like_ajax{{$news->id}}').children("i").hasClass('text-primary')) {
                $('#like_ajax{{$news->id}} .number').text((parseInt($(this).children().text(), 10) + 1)
                    .toString());
            } else {
                $('#like_ajax{{$news->id}} .number').text((parseInt($(this).children().text(), 10) - 1)
                    .toString());
            }
        });

        $('#join_ajax{{$news->id}}').click(function() {

            $(this).children("i").toggleClass("stat-color");
            $.ajax({

                type: "GET",
                url: "{{ route('news_join',$news->id) }}",
            });

            if ($('#join_ajax{{$news->id}}').children("i").hasClass('stat-color')) {
                $('#join_ajax{{$news->id}} .number').text((parseInt($(this).children().text(), 10) + 1)
                    .toString());
            } else {
                $('#join_ajax{{$news->id}} .number').text((parseInt($(this).children().text(), 10) - 1)
                    .toString());
            }
        });

        // Mouse Hover : Au survol

        $('#like_ajax{{$news->id}}').mouseover(function() {
            if ($('#like_ajax{{$news->id}}').hasClass('stat-color')) {
                $('#like_ajax{{$news->id}}').children().attr("data-original-title", "Aimé").removeAttr(
                    "title");
            } else {
                $('#like_ajax{{$news->id}}').children().attr("data-original-title", "J'aime")
                    .removeAttr("title");
            }
        });

        $('#join_ajax{{$news->id}}').mouseover(function() {
            if ($('#join_ajax{{$news->id}}').hasClass('stat-color')) {
                $('#join_ajax{{$news->id}}').children().attr("data-original-title", "J'y serais")
                    .removeAttr("title");
            } else {
                $('#join_ajax{{$news->id}}').children().attr("data-original-title", "J'y participe")
                    .removeAttr("title");
            }
        });

        $('.likeHeart').click(function(e) {
            e.preventDefault()
            $('.fa', this).addClass('active')
        })
        @endforeach

    });
</script>
@endsection