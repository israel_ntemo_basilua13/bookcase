@extends('layouts.student_layout')

@section('content_student')
<div class="my-5 col-md-9 justify-content-center text-center align-items-center">
    <!-- <p class="h4 text-left p_color_78 text-uppercase mb-5" style="font-size: 1rem !important;">Details - Enseignant</p> -->

    <div class="row my-5">
        <div class="col-md-12 my-5">
            <div class="row my-5">
                <div class="col-4 col-md-3">
                    @if($teacher_article->count() == 0)
                    <img src="/storage/uploads/avatars/{{$teacher->user->avatar}}" class="avatar-teacher img-fluid" />

                    @else
                    <img src="/storage/uploads/avatars/{{$teacher_article[0]->teacher->user->avatar}}" class="avatar-teacher img-fluid" />

                    @endif
                </div>
                <div class="col-8 mt-2  text-left">
                    <h5>
                        <strong>
                            @if($teacher_article->count() == 0)
                            {{$teacher->name_teacher}}
                            @else
                            {{$teacher_article[0]->teacher->name_teacher}}
                            @endif
                        </strong>
                    </h5>
                    <div class="mt-4" style="">
                        @if($teacher_article->count() == 0)
                        <p class="text-left">Pseudo : {{$teacher->user->name}}</p>
                        <p class="text-left ">{{$teacher->title->lampoon}}</p>
                        @else
                        <p class="text-left">
                            Pseudo : {{$teacher_article[0]->teacher->user->name}}
                        </p>
                        <p class="text-left">{{$teacher_article[0]->teacher->title->lampoon}}</p>
                        @endif


                        @if($teacher_article->count() == 0)

                        @if($teacher->sex =='M')
                        <p class="text-left p_color_78 teacher_user_name">Sexe : Masculin </p>
                        @else
                        <p class="text-left p_color_78 teacher_user_name">Sexe : Féminin </p>
                        @endif

                        @else

                        @if($teacher_article[0]->teacher->sex =='M')
                        <p class="text-left">Sexe : Masculin </p>
                        @else
                        <p class="text-left p_color_78 teacher_user_name">Sexe : Féminin </p>
                        @endif

                        @endif

                    </div>
                </div>
            </div>
            <div class="row well">
                <div class="col-md-4">
                    <p>
                        <strong class="text-uppercase"> Champ de compétence </strong>
                    </p>
                </div>
                <div class="col-md-8 text-md-left">
                    <p>

                        @if($teacher_article->count() == 0)
                        {{$teacher->domain_exploitation}}

                        @else
                        {{$teacher_article[0]->teacher->domain_exploitation}}

                        @endif

                    </p>
                </div>
            </div>
            <div class="row my-4">
                <div class="col-md-4">
                    <p>
                        <strong class="text-uppercase"> <i class="fa fa-globe"></i> Stat Publication </strong>
                    </p>
                </div>

                <div class="col md-8 text-center ">
                    <div class="row justify-content-between">

                        @if($teacher_article->count() > 0 )
                        <div class="col-md-3 text-center">
                            <p>
                                <strong>{{$teacher_article->count()}} {{str_plural('Article',$teacher_article->count())}} {{str_plural('stocké',$teacher_article->count())}}</strong>
                            </p>
                        </div>
                        @else
                        <div class="col-md-12">
                            <p> Aucun article publié jusqu'a présent.</p>
                        </div>
                        @endif


                        @if($dataThèse->count() > 0 )
                        <div class="col-md-3">
                            <p>
                                <span class="fa_stat_article fa_color_t"> <i class="fa fa-square"></i></span>
                                <strong class="number_stat_article">{{$dataThèse->count()}}</strong> <small>{{str_plural('Thèse',$dataThèse->count())}}</small>
                            </p>
                        </div>
                        @endif


                        @if($dataSyllabus->count() > 0 )
                        <div class="col-md-3">
                            <p>
                                <span class="fa_stat_article fa_color_s"> <i class="fa fa-square"></i></span>
                                <strong class="number_stat_article"> {{$dataSyllabus->count()}} </strong> <small> Syllabus ( Support Académique )</small>
                            </p>
                        </div>
                        @endif

                        @if($dataMemoire->count() > 0 )
                        <div class="col-md-3">
                            <p>
                                <span class="fa_stat_article fa_color_m"> <i class="fa fa-square"></i></span>
                                <strong class="number_stat_article">{{$dataMemoire->count()}}</strong> <small>{{str_plural('Memoire',$dataMemoire->count())}}( T.F.E )</small>
                            </p>
                        </div>
                        @endif


                        @if($dataTFC->count() > 0 )
                        <div class="col-md-2">
                            <p>
                                <span class="fa_stat_article fa_color_tf"> <i class="fa fa-square"></i></span>
                                <strong class="number_stat_article">{{$dataTFC->count()}} </strong> <small>{{str_plural('Travaux de fin de cycle',$dataTFC->count())}}( T.F.C )</small>
                            </p>
                        </div>
                        @endif

                        @if($dataTP->count() > 0 )
                        <div class="col-md-2">
                            <p>
                                <span class="fa_stat_article fa_color_tp"> <i class="fa fa-square"></i></span>
                                <strong class="number_stat_article">{{$dataTP->count()}} </strong> <small>{{str_plural('Travaux Pratique',$dataTP->count())}}( TP )</small>
                            </p>
                        </div>
                        @endif

                        @if($dataLivre->count() > 0 )
                        <div class="col-md-2">
                            <p>
                                <span class="fa_stat_article fa_color_l"> <i class="fa fa-square"></i></span>
                                <strong class="number_stat_article">{{$dataLivre->count()}}</strong> <small> {{str_plural('Livre',$dataLivre->count())}}</small>
                            </p>
                        </div>
                        @endif

                    </div>
                </div>

            </div>
        </div>
    </div>


</div>
@endsection