@section('style')
<link href="{{asset('css/app.css')}}" rel="stylesheet">
@endsection

@extends('layouts.app',['title' => 'NotFound'])

@section('content')


<div class="container">
    <div class="row card">

        <!-- <div class="col-md-12 justify-content-around text-center">
            <p class="display-4">
                <i class="fa fa-support"></i>
            </p>
        </div> -->

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-5 mt-md-4 text-md-right text-center align-items-right">
                    <p class="display-1">
                        <i class="fa fa-info-circle" style="color:teal;"></i>
                    </p>
                    <!-- <img src="/storage/uploads/images/logo_publish.PNG" class="img-fluid notfound_img" /> -->
                </div>

                <div class="col-md-6 mt-md-5 align-items-center text-md-left text-center">
                    <p class="h6">
                        <span class="h1 mb-5">Page non disponible !!!</span>
                        <br>
                        <span class="">Les informations demandées ne sont pas disponibles.
                            <br>Veuillez en essayer une autre.</span>
                    </p>
                </div>

                <div class="col-md-6 mt-4 offset-md-3 text-center">
                    <!-- <img src="/storage/uploads/logo_icon/logo_acceuil.PNG" class="img-fluid" /> -->

                    <span class="small p_color_78">Virtual <span style="color:#cd5c5c">Book</span>Case <span class="ml-2">&copy;</span> <?php echo strftime('%Y') ?> </span>
                </div>
            </div>
        </div>

    </div>
</div>


@endsection