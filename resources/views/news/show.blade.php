@inject('notviewsnews','App\Utilities\NotViewNews')
<!-- FQN le Full Qualy Name -->

@extends('layouts.student_layout')
@section('content_student')
<div class="col-md-12 mt-5 margin no-padding">
    <div class="row mt-5">
        <div class="col-md-9 col-lg-6">
            <div class="card card_news">
                <div class="card-header text-left d-flex">
                    <div class="avatar-univ">
                        @if ($news->university !=null)
                        <img src="/storage/uploads/avatars/{{$news->university->logo_picture}}" alt="img" class="img-fluid">
                        @else
                        <img src="/storage/uploads/avatars/{{$news->library->logo}}" alt="img" class="img-fluid">
                        @endif
                    </div>
                    <div class="content-univ">
                        <a href="#">
                        @if ($news->university !=null)
                            <h4>{{$news->university->name_university}}</h4>
                        @else
                            <h4>{{$news->library->name_library}}</h4>
                        @endif
                        </a>
                        <p><i class="fa fa-users"></i> Public</p>
                        <small><i class="fa fa-clock-o"></i>
                            @if ( ceil(abs( strtotime(date("Y-m-d",strtotime($news->date) )) - strtotime(date("Y-m-d")) ) / 86400) > 0)
                            il y a

                            {{ ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400) }}
                            {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($news->date))) - strtotime(date("Y-m-d"))) / 86400)) }}

                            @else
                            aujourd'hui
                            @endif
                        </small>
                    </div>
                    <span class="sm-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </div>
                <div class="card-body">
                    <img class="card-img-top" src="/storage/uploads/images/{{$news->picture}}" alt="Card image cap">
                    <h5 class="h5 mt-4">{{$news->title_news}}</h5>
                    <p class="card-text">
                        {{ $news->describe }}
                    </p>
                </div>
                <!-- </a> -->
                <!-- <div class="card-footer">
                    <div class="container">
                        <div class="row text-center">

                            @if($like->count()>0)

                            @if($like->like_to == 1)
                            <p class="h4 col-4 stat-color" id="like_ajax"><i class="fa fa-thumbs-up" title=" J'aime" data-toggle="tooltip" data-placement="top"></i><span class="badge badge-success">{{ $notviewsnews->likecount($news->id)->count()}}</span></p>
                            @else
                            <p class=" h4 col-4" id="like_ajax"><i class="fa fa-thumbs-up" title=" J'aime" data-toggle="tooltip" data-placement="top"></i><span class="badge badge-success">{{ $notviewsnews->likecount($news->id)->count()}}</span></p>
                            @endif

                            @if($like->join_to == 1)
                            <p class="h4 col-4  stat-color" id="join_ajax"><i class="fa fa-anchor" title="J'y participe" data-toggle="tooltip" data-placement="top">
                                </i>
                                <small class="badge badge-success">{{$notviewsnews->joincount($news->id)->count()}}</small>
                            </p>
                            @else
                            <p class="h4 col-4 join_ajax" id="join_ajax">
                                <i class="fa fa-anchor" title="J'y participe" data-toggle="tooltip" data-placement="top">
                                </i>
                                <small class="badge badge-success">{{$notviewsnews->joincount($news->id)->count()}}</small>
                            </p>
                            @endif

                            @else
                            <p class="h4 col-4" id="like_ajax" title=" J'aime" data-toggle="tooltip" data-placement="top">
                                <i class="fa fa-thumbs-up"></i><span class="badge badge-success">{{ $notviewsnews->likecount($news->id)->count()}}</span>
                            </p>

                            <p class="h4 col-4" id="join_ajax">
                                <i class="fa fa-anchor" title="J'y participe" data-toggle="tooltip" data-placement="top">
                                </i>
                                <small class="badge badge-success">{{$notviewsnews->joincount($news->id)->count()}}</small>
                            </p>

                            @endif
                            <p class="h4 col-4 fb-share-button" data-layout="button_count" data-href="{{Request::url()}}"><i class="fa fa-share"></i></p>
                        </div>
                    </div>
                </div> -->

                <div class="card-footer">
                    <div class="d-flex justify-content-between text-center">
                        @if($like->count()>0)


                        @if($like->like_to == 1)
                        <div class="likeUp">
                            <a href="#" id="like_ajax" disabled="disabled" class="likeHeart">
                                <i class="fa fa-heart   stat-color" title=" J'aime" data-toggle="tooltip" data-placement="top"></i>
                                <span class="number">
                                    {{ $notviewsnews->likecount($news->id)->count()}}
                                </span>
                            </a>
                        </div>
                        @else
                        <div class="likeUp">
                            <a href="" id="like_ajax" disabled="disabled" class="likeHeart">
                                <i class="fa fa-heart-o" title=" J'aime" data-toggle="tooltip" data-placement="top"></i>
                                <span class="number">
                                    {{ $notviewsnews->likecount($news->id)->count()}}
                                </span>
                            </a>
                        </div>
                        @endif


                        @if($like->join_to == 1)
                        <div class="likeUp">
                            <a href="" id="join_ajax" disabled="disabled" class="likeHeart">
                                <i class="fa fa-anchor   stat-color" title=" J'y participe" data-toggle="tooltip" data-placement="top"></i>
                                <span class="number">
                                    {{$notviewsnews->joincount($news->id)->count()}}
                                </span>
                            </a>
                        </div>
                        @else
                        <div class="likeUp">
                            <a href="" disabled="disabled" id="join_ajax" class="likeHeart">
                                <i class="fa fa-anchor" title="J'y participe" data-toggle="tooltip" data-placement="top"></i>
                                <span class="number">{{$notviewsnews->joincount($news->id)->count()}}</span>
                            </a>
                        </div>
                        @endif


                        @else
                        <div class="likeUp">
                            <a href="#" disabled="disabled" class="likeHeart">
                                <i class="fa fa-heart-o"></i>
                                <span class="number">
                                    {{ $notviewsnews->likecount($news->id)->count()}}
                                </span>
                            </a>
                        </div>
                        <div class="likeUp">
                            <a href="" disabled="disabled">
                                <i class="fa fa-anchor"></i>
                                <span class="number">{{$notviewsnews->joincount($news->id)->count()}}</span>
                            </a>
                        </div>
                        @endif


                        <div class="likeUp">
                            <a href="" class="comment">
                                <i class="fa fa-comment-o"></i>
                                <span class="number">
                                    {{$news->comments->count()}} comm.
                                </span>
                            </a>
                        </div>
                    </div>
                    <hr>
                    <div class="comment-box">
                        <form action="{{route('commentNews')}}" method="POST">
                            @csrf
                            <div class="d-flex">
                                <div class="avatar-sm">
                                    <div class="icon">
                                        P
                                    </div>
                                    <img src="/storage/uploads/images/bg-1.jpg" alt="img" class="img-fluid">
                                </div>
                                <div class="input-form">
                                    <input type="hidden" class="form-control @error('news_id') is-invalid @enderror" name="news_id" value="{{$news->id}}">
                                    <textarea name="note" id="comment" cols="40" class="form-control" placeholder="Tapez votre commentaire..."></textarea>
                                </div>
                                <button class="btn btn-primary"><i class="fa fa-comment-o"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div id="disqus_thread">
            </div>
        </div>

        {{-- Block suggestions --}}
        <div class="col-md-3 col-lg-6">

            <div class="card card_news">
                <div class="card-body">
                    <h4 style="font-size: 15px;font-weight:700;margin-bottom:10px">Commentaires </h4>
                    @if($news->comments->count()>0)
                    <div class="commentary comment_scroll">
                        @foreach($news->comments as $comm)
                        <div class="card-comment mb-3">
                            <div class="block-com d-flex">
                                <div class="avatar-com">
                                    <img src="/storage/uploads/avatars/{{$comm->user->avatar}}" alt="img" class="img-fluid">
                                </div>
                                <div class="block-com-content">
                                    <h5>{{$comm->user->name}}</h5>
                                    <p>{{$comm->note}}</p>
                                    <div class="text-left mt-2">
                                        <span><i class="fa fa-clock-o"></i>
                                            @if ( ceil(abs( strtotime(date("Y-m-d",strtotime($comm->created_at) )) -
                                            strtotime(date("Y-m-d")) ) / 86400) > 0)
                                            il y a

                                            {{ ceil(abs(strtotime(date("Y-m-d",strtotime($comm->created_at))) - strtotime(date("Y-m-d"))) / 86400) }}
                                            {{str_plural('jour',ceil(abs(strtotime(date("Y-m-d",strtotime($comm->created_at))) - strtotime(date("Y-m-d"))) / 86400)) }}

                                            @else
                                            aujourd'hui
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @else
                    <div class="card-comment mb-3">
                        <div class="block-com d-flex">
                            <div class="block-com-content">
                                <p>Aucun commentaire n'est disponible</p>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        {{-- End  Block suggestions --}}
    </div>
</div>
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

    var disqus_config = function() {
        this.page.url = '{{Request::url()}}'; // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = {
            {
                $news-> id
            }
        }; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };

    (function() { // DON'T EDIT BELOW THIS LINE
        var d = document,
            s = d.createElement('script');
        s.src = 'https://bookcase-1.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
</div>


</div>

@endsection


@section('scriptis')
<script>
    $(document).ready(function() {

        // Click : Au click
        $('#like_ajax').click(function(e) {
            e.preventDefault();

            var ds = "{{$news->id}}";

            $(this).children("i").toggleClass("stat-color");

            if ($('#like_ajax').children("i").hasClass('fa-heart-o')) {
                $('#like_ajax').children("i").removeClass(
                    'fa-heart-o text-primary active');
                $('#like_ajax').children("i").addClass('fa-heart text-primary');
            } else {
                $('#like_ajax').children("i").addClass('fa-heart-o text-primary');
                $('#like_ajax').children("i").removeClass('fa-heart text-primary active');
            }

            $.ajax({

                type: "GET",
                url: "{{ route('news_like_show',$news->id) }}",

                // success: function() {
                //     // alert("esimi koo");
                // },
                // error: function(error) {
                //     // alert("ezui te");
                // }

            });

            if ($('#like_ajax').children("i").hasClass('stat-color')) {
                $('#like_ajax .number').text((parseInt($(this).children().text(), 10) + 1).toString());

            } else {
                $('#like_ajax .number').text((parseInt($(this).children().text(), 10) - 1).toString());
            }

        });


        $('#join_ajax').click(function(e) {

            e.preventDefault();
            var ds = "{{$news->id}}";

            $(this).children("i").toggleClass("stat-color");

            $.ajax({

                type: "GET",
                url: "{{ route('news_join_show',$news->id) }}",

                success: function() {
                    // alert("esimi koo");
                },
                error: function(error) {
                    // alert("ezui te");
                }
            });

            if ($('#join_ajax').children("i").hasClass('stat-color')) {
                $('#join_ajax .number').text((parseInt($(this).children().text(), 10) + 1).toString());
            } else {
                $('#join_ajax .number').text((parseInt($(this).children().text(), 10) - 1).toString());
            }

        });

        // Mouse Hover : Au survol

        $('#like_ajax').mouseover(function() {
            if ($('#like_ajax').hasClass('stat-color')) {
                $('#like_ajax').children().attr("data-original-title", "Aimé").removeAttr("title");
            } else {
                $('#like_ajax').children().attr("data-original-title", "J'aime").removeAttr("title");
            }
        });

        $('#join_ajax').mouseover(function() {
            if ($('#join_ajax').hasClass('stat-color')) {
                $('#join_ajax').children().attr("data-original-title", "J'y serais").removeAttr("title");
            } else {
                $('#join_ajax').children().attr("data-original-title", "J'y participe").removeAttr("title");
            }
        });





        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    });
</script>
@endsection