<?php

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $n_users = User::all();
    return view('welcome', compact('n_users'));
});

Route::get('/video', function () {
    return view('article.video');
});

// Route::get('welcome', 'HomeController@welcome')->name('welcome');

Route::get('mail', function () {
    Mail::send('message', array('name' => 'Arjun'), function ($messages) {
        $messages->to('webkoundo@gmail.com', 'Moses Bula')->subject('BookCase en oeuvre');
    });
});

//  Route auth avec verification de mail a utilisé en dev
// Auth::routes();

//  Route auth avec verification de mail a utilisé en prod
Auth::routes(['verify' => true]);

//  Route auth avec verification de mail a utilisé en prod
// Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('/choice', 'UserController@choice_compte')->name('choice');

//Après access choisir quel chemin prendre
Route::get('/path_user{user}', 'UserController@direction_user')->name('path_user');

// Pour le teacher
Route::resource('teacher', 'TeacherController');
Route::post('verif_teacher', 'TeacherController@verification')->name('verif_teacher');

Route::get('inquiry/{code}', 'TeacherController@inquiry')->name('inquiry');

Route::get('/teacher_create/{user}', 'TeacherController@student_create')->name('teacher');


// zone UNIVERSITY
Route::resource('university', 'UniversityController');
Route::resource('agreement', 'AgreementController');
Route::get('agreement_library', 'AgreementController@library')->name('agreement_library');

// library
Route::resource('library', 'LibraryController');

// End library

// Valider la demande de creation ave transmission de cote d'access
    
Route::get('validation/{code}', 'AgreementController@agreement')->name('validation');
Route::get('library_validate/{code}', 'AgreementController@validate_library')->name('library_validate');

// Route::post('v_agreement', 'AgreementController@validateagreement')->name('v_agreement');

// Route::get('dashboard', 'UniversityController@dashboard')->name('dashboard');

// Gestion d'artile d'une université (TFC,TFE, Memoire)
// Creation en function du dernier user inserer
Route::get('/student_create/{user}', 'StudentController@student_create')->name('student_create');
//REST student
Route::resource('student', 'StudentController');


Route::middleware('auth')->group(function(){
//  Route auth avec verification de mail a utilisé en dev
Route::get('/home', 'HomeController@index')->name('home');

Route::get('profile', 'UserController@profile')->name('profile');
Route::post('profile', 'UserController@update_profile')->name('profile');

// ZONE ETUDIANT

// connaitre la liste du corps professoral de l'university d'etude de l'etudiant
Route::get('univ_teacher', 'StudentController@univ_teacher')->name('univ_teacher');

// connaitre les details pertinents d'un corps professoral
Route::get('show_teacher/{id}', 'StudentController@show_teacher')->name('show_teacher');

// liste des news private
Route::get('private_news', 'StudentController@privatenews')->name('private_news');

// pour placer un article dans ses favoris
Route::get('articleFavoriteStoreToogle/{id}', 'ArticleController@article_favorite_store_toggle')->name('articleFavoriteStoreToogle');

//  End ZONE ETUDIANT

// news
Route::resource('news', 'NewsController');

Route::get('news_like/{id}', 'NewsController@like_to')->name('news_like');
Route::get('news_like_show/{id}', 'NewsController@like_to_show')->name('news_like_show');
Route::get('news_join_show/{id}', 'NewsController@join_to_show')->name('news_join_show');
Route::get('news_join/{id}', 'NewsController@join_to')->name('news_join');
Route::post('commentNews', 'NewsController@commetNews')->name('commentNews');

// Route::get('news_join/{id}','NewsController@join_to');

Route::resource('activation', 'ActivationController');
Route::get('activation_store/{id}', 'ActivationController@stor')->name('activation_store');
Route::get('act_destroy/{id}', 'ActivationController@act_destroy')->name('act_destroy');

//pedrien
Route::get('student_artcles', 'TeacherController@student_articles')->name('student_articles');

Route::get('dashboard', 'TeacherController@dashboard')->name('dashboard');

    // Zone article
    Route::resource('article', 'ArticleController');
    // delete article
    Route::get('article_delete/{id}', 'ArticleController@destroy')->name('article_delete');

    // Pour brouillon
    Route::get('draft', 'ArticleController@draft')->name('draft');

    // publication brouillon
    Route::post('draft_publish/{id}', 'ArticleController@draft_publish')->name('draft_publish');

    // Pour le welcome_article
    Route::post('article_home', 'ArticleController@article_welcome')->name('article_home');

    // Pour article : all, free et payant
    Route::get('article_show/{type}', 'ArticleController@article_show')->name('article_show');

  

    // Pour article :: all (par collections)
    Route::get('ArticleCollection', 'ArticleController@article_collections')->name('ArticleCollection'); //ici

    // Pour article : article_recent
    Route::get('article_recent', 'ArticleController@article_recent')->name('article_recent');

    // Pour article : article favorites
    Route::get('ArticleFavorites', 'ArticleController@article_favorites')->name('ArticleFavorites');

    // Pour article : download
    Route::get('download_file/{id}', 'ArticleController@download_file')->name('download_file');

    // Pour article payer
    Route::get('article_payer/{type}', 'ArticleController@paiement_recent')->name('Payment');

    // Article a payer
    Route::get('pay/{id}', 'ArticleController@payments')->name('pay');

    // Fin de zone ARTICLE

    // Library

    Route::get('abonnements/{id}', 'LibraryController@abonnements')->name('abonnements');

    Route::get('abonnements/{id}', 'LibraryController@abonnements')->name('abonnements');
    // payer l'abonnement d'un bundle d'une library
    Route::post('pay_abonnement', 'LibraryController@pay_abonnement')->name('pay_abonnement');
    // pour student et autres utilisateurs en dehors de l'administration de la library
    Route::get('s_libraries/{datav?}', 'StudentController@s_libraries')->name('studentLibrary');

    // dashboard library
    Route::get('dashboardLibrary', 'LibraryController@dashboard')->name('dashboardLibrary');

    // End library


    // Teacher
    // get
    Route::get('TeacherRessource', 'UniversityController@teacher_ressource')->name('TeacherRessource');
    //enregistrer
    Route::post('teacherstore', 'UniversityController@teacherstore_reference')->name('teacherstore');
    // End Teacher


    // Dashbord de l'university
    Route::get('wall', 'UniversityController@dashboard')->name('wall');

    //get
    Route::get('ArticleRessource', 'UniversityController@article_ressource')->name('ArticleRessource');

    // Visualisation de toutes les univcersités par la communauté bookcase
    Route::get('universities', 'UniversityController@university_all')->name('universities');

    // Gestion student
    Route::get('StudentRessource', 'UniversityController@student_ressource')->name('StudentRessource');

    // get:  document
    Route::get('CollectionRessource','UniversityController@promotion_ressource')->name('CollectionRessource');

    // get:  department
    Route::get('DepartmentRessource','UniversityController@department_ressource')->name('DepartmentRessource');

    // get: faculty
    Route::get('FacultyRessource','UniversityController@faculty_ressource')->name('FacultyRessource');


    // Get: Facultés/Departements/Promotion d'une université
    Route::get('InstitutionRessource', 'UniversityController@instutition_ressource')->name('InstitutionRessource');

    // store Instutition : Faculté,Department et collection ou promotion
    Route::post('InstitutionStore','UniversityController@storeinstitution')->name('InstitutionStore');

    //Enregistrer une news
    // get
    Route::get('storeNews', 'UniversityController@storeNews')->name('storeNews');

    // enregistrer
    Route::post('newstore', 'UniversityController@store_news')->name('newstore');


    Route::resource('Cart', 'CartController');
    Route::get('add_panier/{id}', 'CartController@storeart')->name('add_panier');
    Route::get('CartEmpty', 'CartController@CartEmpty')->name('CartEmpty');
    Route::get('CartDestroy/{id}', 'CartController@destroy')->name('CartDestroy');
    Route::post('CartPay', 'CartController@CartPay')->name('CartPay');
    Route::post('PayConfirm', 'CartController@PayConfirm')->name('PayConfirm');
});

